package com.opentext.adaptivemedia.services.impl;
import com.artesia.common.SystemProperty;
import com.opentext.adaptivemedia.common.DeliveryServiceSettings;
import com.opentext.adaptivemedia.common.Setting;
import com.opentext.adaptivemedia.common.Tenant;
import com.opentext.adaptivemedia.common.exception.DeliveryServiceException;
import com.opentext.adaptivemedia.interfaces.AssetDetails;
import com.opentext.adaptivemedia.interfaces.BaseTenantService;
import com.opentext.adaptivemedia.interfaces.TransformerService;
import com.opentext.adaptivemedia.tranformation.ImageProfile;
import com.opentext.adaptivemedia.tranformation.ProfileCollection;
import com.opentext.adaptivemedia.tranformation.TransformationRequest;
import com.opentext.adaptivemedia.utils.DeliveryServiceUtils;
import com.opentext.adaptivemedia.utils.ProcessThreadOuput;
import com.opentext.adaptivemedia.utils.ProcessUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DBSImageMagickTransformerService extends BaseTenantService implements TransformerService {
  public DBSImageMagickTransformerService(Tenant tenant) {
    super(tenant);
  }
  
  private static Log logger = LogFactory.getLog(DBSImageMagickTransformerService.class);
  
  private static Pattern pattern = Pattern.compile("-format\\s+(\\w+)");
  
  public void generateRenditionFile(TransformationRequest request, AssetDetails assetDetails) throws DeliveryServiceException {
    logger.debug(">>generateRenditionFile");
    logger.info("##########Generate Renditon File: " + request.getTransformationAttributes().get("cropprofile"));
    logger.info("##########Transformation Attributes : " + request.getTransformationAttributes());
    String commandToRun = constructCommand(request, assetDetails);
    logger.info("DBSImageMagickTransformerService:" + commandToRun);
    if (commandToRun == null)
      return; 
    try {
      logger.debug(commandToRun);
      if (logger.isDebugEnabled())
        logger.debug("Executing image magick with command : " + commandToRun); 
      long startTime = System.currentTimeMillis();
      Future<ProcessThreadOuput> future = ProcessUtils.execute(this.tenant.getId(), commandToRun, DeliveryServiceSettings.DELIVERY_SERVICE_HOME + "/data/tmp", 120000L);
      String sleepTimeStr = DeliveryServiceSettings.getPropertyValue("amd", "thread.polling.interval", "500");
      int sleepTime = 500;
      try {
        sleepTime = Integer.parseInt(sleepTimeStr);
      } catch (NumberFormatException e) {
        sleepTime = 500;
      } 
      while (!future.isDone()) {
        Thread.sleep(sleepTime);
        logger.debug("waiting for imagemagick to complete");
      } 
      if (logger.isDebugEnabled())
        logger.debug("Execution of imagemagick took : " + (System.currentTimeMillis() - startTime)); 
    } catch (IOException|InterruptedException e) {
      logger.error("Error transforming asset: " + e.getMessage());
      throw new DeliveryServiceException("Error transforming asset ", e);
    } 
    logger.debug("<<generateRenditionFile");
  }
  
  private String constructCommand(TransformationRequest request, AssetDetails assetDetails) throws DeliveryServiceException {
    logger.debug(">>constructCommand");
    Map<String, String> attributes = new HashMap<>(request.getTransformationAttributes());
    String profileName = "DEFAULT";
    String previewKey = this.tenant.getSetting("STAGE_PREVIEW_" + assetDetails
        .getContentType());
    if (previewKey == null)
      previewKey = this.tenant.getSetting("STAGE_PREVIEW"); 
    if ("N".equalsIgnoreCase(previewKey))
      profileName = retrieveProfileNameByFileType(request); 
    String commandToRun = null;
    if (attributes.containsKey("prclid")) {
      String prclIds = attributes.get("prclid");
      attributes.remove("prclid");
      if (prclIds != null) {
        commandToRun = getCommandforProfileCollection(prclIds.split(","), request, assetDetails);
        if (commandToRun == null)
          return null; 
      } 
    } else {
      if (attributes.containsKey("prid"))
        profileName = attributes.get("prid"); 
      logger.info("ProfileName: " + profileName);
      commandToRun = getCommandforProfile(profileName, request, assetDetails, null);
      logger.info("CommandToRun: " + commandToRun);
      if (commandToRun.contains("-format")) {
        Matcher matcher = pattern.matcher(commandToRun);
        if (matcher.find())
          commandToRun = matcher.replaceFirst("\"" + Matcher.quoteReplacement(request.getOutputFile() + File.separator + 
                DeliveryServiceUtils.getOutputFileName(attributes, this.tenant) + "." + matcher.group(1)) + "\" "); 
      } else {
        commandToRun = commandToRun + " \"" + request.getOutputFile() + File.separator + DeliveryServiceUtils.getOutputFileName(attributes, this.tenant) + ".jpg\" ";
      } 
    } 
    String command = null;
    if (SystemProperty.isWindowsEnv()) {
      command = "magick";
    } else {
      command = "convert";
    } 
    commandToRun = command + " \"" + request.getInputFile() + "\" " + commandToRun;
    logger.debug("<<constructCommand");
    return commandToRun;
  }
  
  private String getCommandforProfileCollection(String[] profileCollectionIds, TransformationRequest request, AssetDetails assetDetails) throws DeliveryServiceException {
    StringBuilder commandToRun = new StringBuilder();
    commandToRun.append(" -write mpr:original +delete ");
    boolean filesToTransform = false;
    Map<String, String> attributes = new HashMap<>(request.getTransformationAttributes());
    Map<String, ProfileCollection> profileCollections = this.tenant.getProfileCollections();
    for (String profileCollectionId : profileCollectionIds) {
      List<ImageProfile> profiles = (profileCollections.get(profileCollectionId.trim()) != null) ? ((ProfileCollection)profileCollections.get(profileCollectionId.trim())).getProfilesList() : null;
      if (profiles != null && profiles.size() > 0)
        for (int profileInd = 0; profileInd < profiles.size(); profileInd++) {
          String profile = ((ImageProfile)profiles.get(profileInd)).getProfileName();
          attributes.put("prid", profile);
          String commandForProfile = getCommandforProfile(profile, request, assetDetails, profileCollectionId);
          String writeStr = (profileInd == profiles.size() - 1) ? " " : " -write ";
          String deleteStr = (profileInd == profiles.size() - 1) ? " " : " +delete ";
          String outputFile = null;
          if (commandForProfile.contains("-format")) {
            Matcher matcher = pattern.matcher(commandForProfile);
            if (matcher.find())
              commandForProfile = matcher.replaceFirst(Matcher.quoteReplacement(writeStr + " \"" + request.getOutputFile() + File.separator + 
                    DeliveryServiceUtils.getOutputFileName(attributes, this.tenant) + "." + matcher.group(1)) + "\""); 
            commandForProfile = commandForProfile + deleteStr;
          } else {
            outputFile = "\"" + request.getOutputFile() + File.separator + DeliveryServiceUtils.getOutputFileName(attributes, this.tenant) + ".jpg\" ";
            commandForProfile = commandForProfile + writeStr + outputFile + deleteStr;
          } 
          if (!(new File(outputFile)).exists()) {
            commandToRun.append(" mpr:original " + commandForProfile);
            filesToTransform = true;
          } 
        }  
    } 
    return filesToTransform ? commandToRun.toString() : null;
  }
  
  private String getCommandforProfile(String profileName, TransformationRequest request, AssetDetails assetDetails, String profileCollectionId) throws DeliveryServiceException {
    Map<String, String> attributes = new HashMap<>(request.getTransformationAttributes());
    ImageProfile profileToUse = null;
    StringBuffer sb = new StringBuffer();
    ImageProfile profile = getImageProfileByName(profileName);
    if (profile != null)
      profileToUse = profile; 
    if (profileToUse == null)
      throw new DeliveryServiceException("The image profile " + profileName + " does not exist"); 
    if (request.getTransformationAttributes().get("smartcrop") != null && ((String)request.getTransformationAttributes().get("smartcrop")).equalsIgnoreCase("Y")) {
      String cropCommand = getCommandforCrop(request, assetDetails, profileName, profileCollectionId);
      logger.info("CropCommand:" + cropCommand);
      if (StringUtils.isNotBlank(cropCommand))
        sb.append(cropCommand + " "); 
    } 
    String outputType = attributes.get("format");
    outputType = (outputType == null) ? "jpg" : outputType;
    attributes.remove("format");
    Set<String> inputAttributeNames = attributes.keySet();
    String imageOptions = profileToUse.getImageOptions();
    String[] commands = imageOptions.split(" -");
    List<String> commandsList = new ArrayList<>();
    for (String command : commands) {
      if (command != null && !command.startsWith("-")) {
        commandsList.add("-" + command);
      } else {
        commandsList.add(command);
      } 
    } 
    for (int i = 0; i < commandsList.size(); i++) {
      String pattern = "\\$\\{.*?\\}";
      Matcher matcher = Pattern.compile(pattern).matcher(commandsList.get(i));
      boolean matchFound = false;
      while (matcher.find()) {
        matchFound = true;
        String token = matcher.group();
        if (inputAttributeNames.contains(token.substring(2, token.length() - 1))) {
          sb.append((String)commandsList.get(i) + " ");
          break;
        } 
      } 
      if (!matchFound)
        sb.append((String)commandsList.get(i) + " "); 
    } 
    String commandToRun = sb.toString();
    for (Map.Entry<String, String> entry : attributes.entrySet())
      commandToRun = commandToRun.replace("${" + (String)entry.getKey() + "}", Matcher.quoteReplacement(entry.getValue())); 
    commandToRun = commandToRun.replaceAll("\\$\\{.*?\\}", "");
    logger.info("Returning CommandToRun: " + commandToRun);
    return commandToRun;
  }
  
  private String retrieveProfileNameByFileType(TransformationRequest request) {
    logger.debug(">>retrieveProfileNameByFileType");
    String command = null;
    String profileName = "DEFAULT";
    boolean flag = false;
    if (this.tenant != null) {
      Map<String, Setting> tenantSettingMap = this.tenant.getSettings();
      if (tenantSettingMap != null && !tenantSettingMap.isEmpty())
        for (String setting : tenantSettingMap.keySet()) {
          if (StringUtils.isNotBlank(setting) && setting.endsWith("_PROFILE_SETTINGS")) {
            flag = true;
            break;
          } 
        }  
    } 
    if (flag) {
      if (SystemProperty.isWindowsEnv()) {
        command = "magick identify -format %[colorspace]";
      } else {
        command = "identify -format %[colorspace]";
      } 
      String commandToRun = command + " \"" + request.getInputFile() + "\"";
      try {
        Future<ProcessThreadOuput> future = ProcessUtils.execute(this.tenant.getId(), commandToRun, DeliveryServiceSettings.DELIVERY_SERVICE_HOME + "/data/tmp", 120000L);
        String colorSpace = ((ProcessThreadOuput)future.get()).getStandardOutContents();
        if (colorSpace == null || colorSpace.length() == 0)
          logger.debug("No color space info is returned"); 
        String extension = FilenameUtils.getExtension(request.getInputFile());
        if (StringUtils.isNotBlank(colorSpace) && StringUtils.isNotBlank(extension))
          profileName = this.tenant.getSetting(colorSpace.toUpperCase() + "_" + extension.toUpperCase() + "_PROFILE_SETTINGS"); 
        if (StringUtils.isBlank(profileName))
          profileName = "DEFAULT"; 
      } catch (IOException|InterruptedException|java.util.concurrent.ExecutionException e1) {
        logger.error("Error in retrieving the file information from ImageMagick " + e1.getMessage());
        logger.error(e1.getStackTrace());
        profileName = "DEFAULT";
      } 
    } 
    logger.debug("<<retrieveProfileNameByFileType");
    return profileName;
  }
  
  private String getCommandforCrop(TransformationRequest request, AssetDetails assetDetails, String profile, String collectionId) {
    logger.debug(">>getCommandforCrop");
    String cropInfo = assetDetails.getCropInfo();
    String finalCropData = null;
    String cropCoordinates = null;
    logger.info("******** getCommandforCrop: profile:" + profile);
    logger.info("******** getCommandforCrop: collectionId:" + collectionId);
    if (StringUtils.isBlank(cropInfo))
      return null; 
    if (StringUtils.isNotBlank(collectionId)) {
      Matcher match = Pattern.compile(collectionId + "/" + profile + "([^,]*)").matcher(cropInfo);
      if (match.find())
        cropCoordinates = match.group(0).split("\\^\\^")[1]; 
    } 
    if (StringUtils.isBlank(cropCoordinates) || StringUtils.isBlank(collectionId)) {
      collectionId = "[A-Z^a-z^._-]*";
      profile = "[A-Z^a-z^._-]*";
      Matcher match = Pattern.compile("[A-Z^a-z^._-]*" + "/" + "[A-Z^a-z^._-]*" + "([^,]*)").matcher(cropInfo);
      if (match.find()) {
    	  logger.info("Cropcoordinates are : " + cropCoordinates);
    	  cropCoordinates = match.group(0).split("\\^\\^")[1];  
      }
    } 
    if (StringUtils.isBlank(cropCoordinates))
      return null; 
    String[] dimensions = cropCoordinates.split("@");
    String fileDimensions = retriveFileDimensions(request);
    if (StringUtils.isBlank(fileDimensions))
      return null; 
    String[] fileCoordinates = fileDimensions.split("x");
    int width = Math.round(Float.valueOf(dimensions[2]).floatValue() * Float.valueOf(fileCoordinates[0]).floatValue());
    int height = Math.round(Float.valueOf(dimensions[3]).floatValue() * Integer.valueOf(fileCoordinates[1]).intValue());
    int x = Math.round(Float.valueOf(dimensions[1]).floatValue() * Float.valueOf(fileCoordinates[0]).floatValue());
    int y = Math.round(Float.valueOf(dimensions[0]).floatValue() * Integer.valueOf(fileCoordinates[1]).intValue());
    finalCropData = String.valueOf(width) + "x" + String.valueOf(height) + "+" + String.valueOf(x) + "+" + String.valueOf(y);
    logger.debug("<<getCommandforCrop");
    return "-crop " + finalCropData;
  }
  
  private String retriveFileDimensions(TransformationRequest request) {
    logger.debug(">>retriveFileDimensions");
    String command = null;
    String dimension = null;
    if (SystemProperty.isWindowsEnv()) {
      command = "magick identify -ping -format \"%[fx:w]x%[fx:h]\"";
    } else {
      command = "identify -ping -format \"%[fx:w]x%[fx:h]\"";
    } 
    String commandToRun = command + " \"" + request.getInputFile() + "\"";
    try {
      Future<ProcessThreadOuput> future = ProcessUtils.execute(this.tenant.getId(), commandToRun, DeliveryServiceSettings.DELIVERY_SERVICE_HOME + "/data/tmp", 120000L);
      dimension = ((ProcessThreadOuput)future.get()).getStandardOutContents();
      if (dimension == null || dimension.length() == 0)
        logger.debug("No dimensions returned"); 
    } catch (IOException|InterruptedException|java.util.concurrent.ExecutionException e1) {
      logger.error("Error in retrieving the file dimensions from ImageMagick " + e1.getMessage());
      logger.error(e1.getStackTrace());
    } 
    logger.debug("<<retriveFileDimensions");
    return dimension;
  }
  
  private ImageProfile getImageProfileByName(String profileName) {
    ImageProfile profile = null;
    if (this.tenant.getImageProfiles() != null)
      profile = (ImageProfile)this.tenant.getImageProfiles().get(profileName); 
    return profile;
  }
}