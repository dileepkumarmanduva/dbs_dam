package com.opentext.adaptivemedia.services.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URLDecoder;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import com.artesia.asset.Asset;
import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.selection.AssetIdsSelectionContext;
import com.artesia.asset.selection.SelectionContext;
import com.artesia.common.MetadataFieldConstants;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.export.ExportContentRequest;
import com.artesia.export.ExportRequest;
import com.artesia.export.templates.DeliveryTemplate;
import com.artesia.metadata.DomainValue;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataElement;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataTableField;
import com.artesia.metadata.MetadataValue;
import com.artesia.security.SecuritySession;
import com.opentext.adaptivemedia.common.DeliveryServiceSettings;
import com.opentext.adaptivemedia.common.Tenant;
import com.opentext.adaptivemedia.common.exception.DeliveryServiceException;
import com.opentext.adaptivemedia.interfaces.AssetDetails;
import com.opentext.adaptivemedia.renditions.RenditionRequest;
import com.opentext.adaptivemedia.utils.ProcessThreadOuput;
import com.opentext.adaptivemedia.utils.ProcessUtils;
import com.opentext.job.Status;
import com.opentext.mediamanager.restapi.assets.representations.AssetCollectionRepresentation;
import com.opentext.mediamanager.restapi.assets.representations.AssetRepresentation;
import com.opentext.mediamanager.restapi.jobs.representations.ExportJobHandle;
import com.opentext.mediamanager.restapi.jobs.representations.JobRepresentation;
import com.opentext.mediamanager.restapi.params.DataLoadRequestParam;
import com.opentext.mediamanager.restapi.params.ExportRequestParam;
import com.opentext.mediamanager.restapi.params.SelectionContextParam;
import com.opentext.mediamanager.restapi.session.representations.SessionRepresentation;
import com.opentext.mediamanager.restapi.util.JsonMoxyConfigurationContextResolver;
import com.opentext.mediamanager.restapi.util.MOXyContextResolver;

public class DBSRESTContentServiceImpl extends DefaultRESTContentServiceImpl {

	private static final Log log = LogFactory.getLog(DBSRESTContentServiceImpl.class);

	private WebTarget rootTarget;

	private volatile SecuritySession session;

	private String otmmRestURL;

	private String teamsUserName = "tsuper";

	private String teamsPassword = "tsuper";

	private List<String> excludeMimeTypesList = null;

	private static HashMap<Class, JAXBContext> contextHashMap = new HashMap<>();

	private static Set<String> assetIdSet = Collections.synchronizedSet(new HashSet<>());
	public DBSRESTContentServiceImpl(Tenant tenant) {
		super(tenant);
		if (this.tenant != null) {
			this.otmmRestURL = this.tenant.getSetting("OTMM_URL");
			this.teamsUserName = this.tenant.getSetting("OTMM_USER_NAME");
			this.teamsPassword = this.tenant.getSetting("OTMM_USER_PASSWORD");
		} 

		Client client = null;
		if (this.otmmRestURL != null && this.otmmRestURL.toLowerCase().startsWith("https")) {
			String trustStoreFile = System.getProperty("ssl.truststore.file");
			String keyStoreFile = System.getProperty("ssl.keystore.file");
			String keyStorePwd = System.getProperty("ssl.keystore.password");
			String trustStorePwd = System.getProperty("ssl.truststore.password");
			SslConfigurator sslConfig = SslConfigurator.newInstance().trustStoreFile(trustStoreFile).trustStorePassword(trustStorePwd).keyStoreFile(keyStoreFile).keyPassword(keyStorePwd);
			SSLContext sslContext = sslConfig.createSSLContext();
			client = ClientBuilder.newBuilder().sslContext(sslContext).build();
		} else {
			client = ClientBuilder.newClient();
		} 
		client.register(MOXyContextResolver.class);
		client.register(JsonMoxyConfigurationContextResolver.class);
		client.register(MultiPartFeature.class);
		this.rootTarget = client.target(this.otmmRestURL);
		Runtime.getRuntime().addShutdownHook(new RestSessionLogoutHook());
	}

	//	  public DefaultRESTContentServiceImpl(Tenant t) {
	//	    super(t);
	//	    if (this.tenant != null) {
	//	      this.otmmRestURL = this.tenant.getSetting("OTMM_URL");
	//	      this.teamsUserName = this.tenant.getSetting("OTMM_USER_NAME");
	//	      this.teamsPassword = this.tenant.getSetting("OTMM_USER_PASSWORD");
	//	    } 
	//	    Client client = null;
	//	    if (this.otmmRestURL != null && this.otmmRestURL.toLowerCase().startsWith("https")) {
	//	      String trustStoreFile = System.getProperty("ssl.truststore.file");
	//	      String keyStoreFile = System.getProperty("ssl.keystore.file");
	//	      String keyStorePwd = System.getProperty("ssl.keystore.password");
	//	      String trustStorePwd = System.getProperty("ssl.truststore.password");
	//	      SslConfigurator sslConfig = SslConfigurator.newInstance().trustStoreFile(trustStoreFile).trustStorePassword(trustStorePwd).keyStoreFile(keyStoreFile).keyPassword(keyStorePwd);
	//	      SSLContext sslContext = sslConfig.createSSLContext();
	//	      client = ClientBuilder.newBuilder().sslContext(sslContext).build();
	//	    } else {
	//	      client = ClientBuilder.newClient();
	//	    } 
	//	    client.register(MOXyContextResolver.class);
	//	    client.register(JsonMoxyConfigurationContextResolver.class);
	//	    client.register(MultiPartFeature.class);
	//	    this.rootTarget = client.target(this.otmmRestURL);
	//	    Runtime.getRuntime().addShutdownHook(new RestSessionLogoutHook());
	//	  }

	public AssetDetails stageContentForID(RenditionRequest requestDetails, String stagingDirectory) throws DeliveryServiceException {
		log.debug(">>stageContentForID");
		validateAndLogin();
		AssetDetails assetDetails = new AssetDetails();
		try {
			log.info("===================REQUEST DETAILS START===================");
			String smartCropProfile= requestDetails.getRenditionAttributes().get("cropprofile");
			log.info("Value for smartCropProfile is :" + smartCropProfile);
			log.info("===================REQUEST DETAILS END===================");
			assetDetails = getLatestAssetId(requestDetails);
			String mimetypes = this.tenant.getSetting("STAGE_PREVIEW_EXCLUDE_MIME_TYPES");
			if (mimetypes != null && this.excludeMimeTypesList == null)
				this.excludeMimeTypesList = (List<String>)Stream.<String>of(mimetypes.split(",")).map(String::trim).collect(Collectors.toList()); 
			String previewKey = "N";
			if (this.excludeMimeTypesList != null && this.excludeMimeTypesList.contains(assetDetails.getMimeType())) {
				log.info("Requested Asset mime type is in exclude list, use original content always: " + assetDetails
						.getMimeType());
			} else {
				previewKey = this.tenant.getSetting("STAGE_PREVIEW_" + assetDetails.getContentType());
				if (previewKey == null)
					previewKey = this.tenant.getSetting("STAGE_PREVIEW"); 
			} 
			if ("VIDEO".equalsIgnoreCase(assetDetails.getContentType()))
				previewKey = "Y"; 
			long startTime = System.currentTimeMillis();
			while (assetIdSet.contains(assetDetails.getAssetId())) {
				long currentTime = System.currentTimeMillis();
				if (currentTime - startTime > 300000L) {
					log.error("The other threads is staging the asset.");
					throw new DeliveryServiceException("Time out waiting for the other threads to complete staging the asset.", 404);
				} 
				Thread.sleep(15000L);
			} 
			assetIdSet.add(assetDetails.getAssetId());
			DataLoadRequestParam dataLoadRequestParam = new DataLoadRequestParam();
			if ("Y".equalsIgnoreCase(previewKey))
				dataLoadRequestParam.setLoadPreviewInfo(true); 
			List<String> metadataFieldList = new ArrayList<>();
			metadataFieldList.add("DBS.FIELD.INTELLIGENT CROP.RECOMMENDED DATA");
			metadataFieldList.add("DBS.FIELD.INTELLIGENT CROP.COLLECTION NAME");
			metadataFieldList.add("DBS.FIELD.INTELLIGENT CROP.PROFILE NAME");
			metadataFieldList.add("DBS.FIELD.INTELLIGENT CROP.SELECTED CROP");
			dataLoadRequestParam.setMetadataFieldsToReturn(metadataFieldList);
			if ("MSOFFICE".equalsIgnoreCase(assetDetails.getContentType()))
				dataLoadRequestParam.setPdfLoadPreviewInfo(true); 
			Map<String, String> queryParams = new HashMap<>();
			queryParams.put("load_type", "custom");
			queryParams.put("data_load_request", getJSONRepresentation(dataLoadRequestParam));
			validateAndLogin();
			String apiEndpoint = "/assets/" + assetDetails.getAssetId();
			Response response = getRequestBuilder(apiEndpoint, (Map)queryParams).get();
			if (response.getStatus() != 200) {
				log.error("API call fails for " + assetDetails.getAssetId() + " at " + apiEndpoint);
				throw new DeliveryServiceException("API call fails with http status : " + response.getStatus());
			} 
			log.debug("API call successful for endpoint : " + apiEndpoint);
			AssetRepresentation assetRep = (AssetRepresentation)response.readEntity(AssetRepresentation.class);
			Asset asset = assetRep.getAsset();
			log.info("Asset is : " + asset);
			MetadataCollection mc = asset.getMetadata();
			log.info("Metadata Collection is : " + mc);
			if (mc != null && !mc.isEmpty()) {
				List<MetadataElement> elementList = mc.getMetadataElementList();
				StringBuilder sb = new StringBuilder();
				if (elementList != null && !elementList.isEmpty()) {
					String prclid = null;
					String pid = null;
					String cropcordinates = null;
					for (MetadataElement metadataElement : elementList) {
						if (metadataElement.isField() && !metadataElement.isTabular())
							continue; 
						MetadataTableField tableField = (MetadataTableField)metadataElement;
						log.info("An iteration: tableField.getId" + tableField.getId());
						log.info("tableField.getValue().getStringValue(): " + tableField.getValue().getStringValue());
						
						if (tableField.getId().equalsId(MetadataFieldConstants.METADATA_FIELD_ID__COLLECTION_NAME) && tableField
								.getValue() != null && 
								StringUtils.isNotBlank(tableField.getValue().getStringValue())) {
							
							log.info("Flag fro PROFILE -1: " + StringUtils.equals(tableField.getValue().getStringValue(), smartCropProfile));
							log.info("SmartCrop Profile is - 1: " + smartCropProfile);
							log.info(tableField.getValue().getStringValue());
							prclid = tableField.getValue().getStringValue();
//							continue;
						} 
						if (tableField.getId().equalsId(MetadataFieldConstants.METADATA_FIELD_ID__PROFILE_NAME) && tableField
								.getValue() != null && 
								StringUtils.isNotBlank(tableField.getValue().getStringValue())) {
							pid = tableField.getValue().getStringValue();
							log.info("Flag fro PROFILE -2: " + StringUtils.equals(tableField.getValue().getStringValue(), smartCropProfile));
							log.info(tableField.getValue().getStringValue());
							log.info("SmartCrop Profile is - 2: " + smartCropProfile);
//							continue;
						} 
						if (tableField.getId().equalsId(MetadataFieldConstants.METADATA_FIELD_ID__USER_SELECTED_CROP) && tableField
								.getValue() != null && 
								StringUtils.isNotBlank(tableField.getValue().getStringValue())) {
							cropcordinates = tableField.getValue().getStringValue();
							log.info("User Selected Crop : " + cropcordinates);
//							continue;
						} 
						if (tableField.getId().equalsId(MetadataFieldConstants.METADATA_FIELD_ID__RECOMMENDED_CROP) && tableField
								.getValue() != null && 
								StringUtils.isNotBlank(tableField.getValue().getStringValue()) && 
								StringUtils.isBlank(cropcordinates))
							cropcordinates = tableField.getValue().getStringValue(); 
							log.info("Recommended Crop : " + cropcordinates);
					} 
					if (StringUtils.isNotBlank(prclid) && StringUtils.isNotBlank(pid) && 
							StringUtils.isNotBlank(cropcordinates))
						sb.append(prclid).append("/").append(pid).append("^^").append(cropcordinates).append(","); 
				} 
				if (StringUtils.isNotBlank(sb.toString())) {
					String cropInfo = sb.deleteCharAt(sb.length() - 1).toString();
					log.info("Setting Crop Info : " + cropInfo);
					assetDetails.setCropInfo(cropInfo);
				} 
			} 
			if ("Y".equalsIgnoreCase(previewKey)) {
				stagePreviewForAsset(assetDetails.getAssetId(), stagingDirectory, assetDetails, asset, response);
			} else {
				stageOriginalForAsset(assetDetails.getAssetId(), stagingDirectory, assetDetails);
			} 
			writeAssetDetailsFile(stagingDirectory, assetDetails);
			registerAssetWithOTMM(requestDetails.getId());
			registerAssetWithOTMM(assetDetails.getAssetId());
			assetIdSet.remove(assetDetails.getAssetId());
		} catch (URIException e) {
			log.error("URI Exception when invoking OTMM Funciton  " + e.getMessage());
			DeliveryServiceException.logStackTrace(log, (Exception)e);
			throw new DeliveryServiceException("Exception while staging asset ", e);
		} catch (JAXBException e) {
			log.error("JAXBException when invoking OTMM Funciton  " + e.getMessage());
			DeliveryServiceException.logStackTrace(log, (Exception)e);
			throw new DeliveryServiceException("Exception while staging asset ", e);
		} catch (BaseTeamsException e) {
			log.error("BaseTeams when invoking OTMM Funciton  " + e.getMessage());
			DeliveryServiceException.logStackTrace(log, (Exception)e);
			throw new DeliveryServiceException("Exception while staging asset ", e);
		} catch (InterruptedException e) {
			log.error("Interrupted while waiting for export to complete" + e.getMessage());
			DeliveryServiceException.logStackTrace(log, e);
		} catch (IOException e) {
			log.error("IO Exception" + e.getMessage());
			DeliveryServiceException.logStackTrace(log, e);
			throw new DeliveryServiceException("Exception while staging asset ", e);
		} finally {
			if (assetDetails.getAssetId() != null && !assetDetails.getAssetId().isEmpty() && assetIdSet.contains(assetDetails.getAssetId()))
				assetIdSet.remove(assetDetails.getAssetId()); 
		} 
		log.debug("<<stageContentForID");
		return assetDetails;
	}

	private void registerAssetWithOTMM(String assetId) throws DeliveryServiceException {
		log.debug(">>registerAssetWithOTMM");
		validateAndLogin();
		String adaptiveMediaServerId = this.tenant.getSetting("DELIVERY_SERVER_ID");
		if (StringUtils.isBlank(adaptiveMediaServerId))
			adaptiveMediaServerId = DeliveryServiceSettings.getPropertyValue("amd", "delivery.server.id", "OTMM.NOTIFIER.ADAPTIVE.MEDIA.DELIVERY"); 
		Form form = new Form();
		form.param("notifier_id", adaptiveMediaServerId);
		String apiEndpoint = "/assets/" + assetId + "/notifier";
		Response response = (Response)this.rootTarget.path(apiEndpoint).request().header("X-Requested-By", Integer.valueOf(this.session.getId())).post(Entity.entity(form, "application/x-www-form-urlencoded"), Response.class);
		if (response.getStatus() != 200) {
			log.error("API call fails for " + assetId + " at " + apiEndpoint);
			throw new DeliveryServiceException("API call fails with http status : " + response.getStatus());
		} 
		log.debug("API call successful for endpoint : " + apiEndpoint);
		log.info("OTMM Notifier Response Code for assetId: " + assetId + " : " + response.getStatusInfo().getStatusCode() + " : " + response
				.getStatusInfo().getReasonPhrase());
		log.debug("<<registerAssetWithOTMM");
	}

	private void stageOriginalForAsset(String assetId, String stagingDirectory, AssetDetails assetDetails) throws DeliveryServiceException, JAXBException, BaseTeamsException, InterruptedException, IOException {
		log.debug(">>stageOriginalForAsset");
		Long jobId = exportAsset(new AssetIdentifier(assetId));
		long startTime = System.currentTimeMillis();
		while (isJobActive(jobId)) {
			long currentTime = System.currentTimeMillis();
			if (currentTime - startTime > 300000L) {
				log.error("The export process is not complete in the stipulated time.");
				throw new DeliveryServiceException("Time out waiting for the export to complete.", 404);
			} 
			Thread.sleep(10000L);
		} 
		writeFiletoStagingArea(jobId, stagingDirectory, assetDetails);
		log.debug("<<stageOriginalForAsset");
	}

	private void stagePreviewForAsset(String assetId, String stagingDirectory, AssetDetails assetDetails, Asset asset, Response response) throws JAXBException, DeliveryServiceException, BaseTeamsException, InterruptedException, IOException {
		log.debug(">>stagePreviewForAsset");
		if (asset.getRenditionContent() != null && asset.getRenditionContent().getPreviewContent() != null) {
			String renditionId = asset.getRenditionContent().getPreviewContent().getId().asString();
			String renditionName = asset.getRenditionContent().getPreviewContent().getName();
			String apiEndpoint = "/renditions/" + renditionId;
			response = getRequestBuilder(apiEndpoint).get();
			if (response.getStatus() != 200) {
				log.error("API call fails for " + assetId + " at " + apiEndpoint);
				throw new DeliveryServiceException("API call fails with http status : " + response.getStatus());
			} 
			log.debug("API call successful for endpoint : " + apiEndpoint);
			InputStream previewStream = (InputStream)response.readEntity(InputStream.class);
			File outputFile = new File(stagingDirectory, renditionName);
			Files.copy(previewStream, outputFile.toPath(), new CopyOption[] { StandardCopyOption.REPLACE_EXISTING });
			if ("BITMAP".equalsIgnoreCase(assetDetails.getContentType()))
				applyWaterMark(outputFile.getAbsolutePath(), outputFile.getAbsolutePath()); 
		} else if (asset.getRenditionContent() != null && asset.getRenditionContent().getPdfPreviewContent() != null) {
			String renditionId = asset.getRenditionContent().getPdfPreviewContent().getId().asString();
			String renditionName = asset.getRenditionContent().getPdfPreviewContent().getName();
			String apiEndpoint = "/renditions/" + renditionId;
			response = getRequestBuilder(apiEndpoint).get();
			if (response.getStatus() != 200) {
				log.error("API call fails for " + assetId + " at " + apiEndpoint);
				throw new DeliveryServiceException("API call fails with http status : " + response.getStatus());
			} 
			log.debug("API call successful for endpoint : " + apiEndpoint);
			InputStream previewStream = (InputStream)response.readEntity(InputStream.class);
			File outputFile = new File(stagingDirectory, renditionName);
			Files.copy(previewStream, outputFile.toPath(), new CopyOption[] { StandardCopyOption.REPLACE_EXISTING });
		} else {
			stageOriginalForAsset(assetId, stagingDirectory, assetDetails);
		} 
		log.debug("<<stagePreviewForAsset");
	}

	private void writeFiletoStagingArea(Long jobId, String stagingDirectory, AssetDetails assetDetails) throws IOException, DeliveryServiceException {
		log.debug(">>writeFiletoStagingArea");
		log.info("Job Id - " + jobId);
		if (jobId == null)
			return; 
		String apiEndpoint = "jobs/" + jobId + "/exports/contents";
		Response response = getRequestBuilder(apiEndpoint).get();
		if (response.getStatus() != 200) {
			log.error("API call fails for endpoint : " + apiEndpoint);
			throw new DeliveryServiceException("API call fails with http status : " + response.getStatus());
		} 
		log.debug("API call successful for endpoint : " + apiEndpoint);
		InputStream inputStream = (InputStream)response.readEntity(InputStream.class);
		if (inputStream == null)
			return; 
		String assetName = "default_assetname.jpg";
		String nameHeader = response.getHeaderString("Content-Disposition");
		if (nameHeader != null) {
			assetName = nameHeader.replaceFirst("(?i)^.*filename\\*=utf-8''\"?([^\"]+)\"?.*$", "$1");
			assetName = URLDecoder.decode(assetName, "UTF8");
		} 
		File opFile = new File(stagingDirectory, assetName);
		if (opFile.exists()) {
			log.info("File already exists: " + opFile.getPath());
			return;
		} 
		Files.copy(inputStream, opFile.toPath(), new CopyOption[] { StandardCopyOption.REPLACE_EXISTING });
		if ("BITMAP".equalsIgnoreCase(assetDetails.getContentType()))
			applyWaterMark(opFile.getAbsolutePath(), opFile.getAbsolutePath()); 
		log.debug("<<writeFiletoStagingArea");
	}

	private Long exportAsset(AssetIdentifier assetIdentifier) throws JAXBException, URIException, BaseTeamsException, DeliveryServiceException {
		ExportRequestParam exportRequestParam = new ExportRequestParam();
		ExportRequest exportRequest = new ExportRequest();
		exportRequestParam.setExportRequest(exportRequest);
		exportRequest.setWriteXml(false);
		ExportContentRequest exportContentRequest = new ExportContentRequest(true, false);
		exportRequest.setContentRequest(exportContentRequest);
		DeliveryTemplate template = new DeliveryTemplate();
		template.setTransformerId(new TeamsIdentifier("ARTESIA.TRANSFORMER.PROFILE.SINGLEASSET.DOWNLOAD"));
		template.setId(new TeamsIdentifier("ARTESIA.TRANSFORMER.PROFILE.SINGLEASSET.DOWNLOAD.DEFAULT"));
		exportRequest.setDeliveryTemplate(template);
		SelectionContextParam selectionContextParam = new SelectionContextParam();
		AssetIdsSelectionContext selectionContext = new AssetIdsSelectionContext();
		List<TeamsIdentifier> assetIds = new ArrayList<>();
		assetIds.add(assetIdentifier);
		selectionContext.setAssetIds(assetIds);
		selectionContextParam.setSelectionContext((SelectionContext)selectionContext);
		Form form = new Form();
		form.param("selection_context", getJSONRepresentation(selectionContextParam));
		form.param("export_request", getJSONRepresentation(exportRequestParam));
		form.param("export_contents", "assets_and_assets_list");
		validateAndLogin();
		String apiEndpoint = "/jobs/exports";
		Response response = (Response)this.rootTarget.path(apiEndpoint).request().header("X-Requested-By", Integer.valueOf(this.session.getId())).post(Entity.entity(form, "application/x-www-form-urlencoded"), Response.class);
		if (response.getStatus() != 200) {
			log.error("API call fails for endpoint : " + apiEndpoint);
			throw new DeliveryServiceException("API call fails with http status : " + response.getStatus());
		} 
		log.debug("API call successful for endpoint : " + apiEndpoint);
		ExportJobHandle exportJobHandle = (ExportJobHandle)response.readEntity(ExportJobHandle.class);
		return exportJobHandle.getExportResponse().getJobId();
	}

	private boolean isJobActive(Long jobId) throws DeliveryServiceException {
		validateAndLogin();
		String apiEndpoint = "/jobs/" + jobId;
		Response response = getRequestBuilder(apiEndpoint).get();
		if (response.getStatus() != 200) {
			log.error("API call fails for endpoint : " + apiEndpoint);
			throw new DeliveryServiceException("API call fails with http status : " + response.getStatus());
		} 
		log.debug("API call successful for endpoint : " + apiEndpoint);
		JobRepresentation jobRep = (JobRepresentation)response.readEntity(JobRepresentation.class);
		if (jobRep != null && jobRep.getJob() != null) {
			log.info("Job Status - " + jobRep.getJob().getStatus());
			return Status.INPROGRESS.equals(jobRep.getJob().getStatus());
		} 
		return false;
	}

	private AssetDetails getLatestAssetId(RenditionRequest renditionRequest) throws BaseTeamsException, DeliveryServiceException, JAXBException, URIException {
		log.debug(">>getLatestAssetId");
		Map<String, Set<String>> allowedMetadataValues = this.tenant.getMetadatafilters();
		AssetDetails assetDetails = new AssetDetails();
		String assetId = renditionRequest.getId();
		String assetIdParam = (String)renditionRequest.getRenditionAttributes().get("vid");
		DataLoadRequestParam dataLoadRequestParam = new DataLoadRequestParam();
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("load_type", "custom");
		Set<String> metadataFieldsSet = allowedMetadataValues.keySet();
		List<String> metadataFieldsToReturn = new ArrayList<>(metadataFieldsSet);
		dataLoadRequestParam.setMetadataFieldsToReturn(metadataFieldsToReturn);
		queryParams.put("data_load_request", getJSONRepresentation(dataLoadRequestParam));
		validateAndLogin();
		String apiEndpoint = "/assets/" + assetId + "/versions";
		Response response = getRequestBuilder(apiEndpoint, (Map)queryParams).get();
		if (response.getStatus() != 200) {
			log.error("API call fails for " + assetId + " at " + apiEndpoint);
			throw new DeliveryServiceException("API call fails with http status : " + response.getStatus());
		} 
		log.debug("API call successful for endpoint : " + apiEndpoint);
		AssetCollectionRepresentation assetCollection = (AssetCollectionRepresentation)response.readEntity(AssetCollectionRepresentation.class);
		List<Asset> assetVersions = assetCollection.getAssetList();
		if (assetVersions == null || assetVersions.size() == 0) {
			log.debug(">>getLatestAssetId :: asset is not available for given assetId " + assetId + " in " + this.rootTarget.getUri().toString());
			throw new DeliveryServiceException("The asset is not found!", 404);
		} 
		String version = (String)renditionRequest.getRenditionAttributes().get("version");
		Integer intVersion = null;
		if (version != null && !"".equals(version.trim()))
			try {
				intVersion = Integer.valueOf(Integer.parseInt(version));
			} catch (NumberFormatException e) {
				throw new DeliveryServiceException("The requested version of the asset does not exist", 404);
			}  
		Asset latestAsset = null;
		if (assetIdParam != null && assetIdParam.length() > 0) {
			for (Asset asset : assetVersions) {
				if (asset.getAssetId().equals(new AssetIdentifier(assetIdParam))) {
					latestAsset = asset;
					break;
				} 
			} 
		} else {
			if (intVersion != null && intVersion.intValue() <= 0)
				throw new DeliveryServiceException("The requested version of the asset does not exist", 404); 
			if (intVersion != null && intVersion.intValue() > 0) {
				boolean assetVersionFound = false;
				for (int i = 1; i <= assetVersions.size(); i++) {
					if (((Asset)assetVersions.get(i - 1)).getVersion() == intVersion.intValue()) {
						latestAsset = assetVersions.get(i - 1);
						assetVersionFound = true;
						break;
					} 
				} 
				if (!assetVersionFound)
					throw new DeliveryServiceException("The requested version of the asset does not exist", 404); 
			} else {
				latestAsset = assetVersions.get(assetVersions.size() - 1);
			} 
		} 
		if (latestAsset == null)
			throw new DeliveryServiceException("The requested version of the asset does not exist", 404); 
		boolean isValidMetadataState = validateAssetForMetadata(latestAsset);
		if (!isValidMetadataState)
			throw new DeliveryServiceException("The requested version of the asset is not available", 404); 
		String contentTypes = DeliveryServiceSettings.getPropertyValue("asset", "contenttypes", null);
		if (contentTypes != null)
			if (!contentTypes.contains(latestAsset.getContentType()))
				throw new DeliveryServiceException("The asset type is not supported", 501);  
		assetDetails.setAssetId(latestAsset.getAssetId().asString());
		assetDetails.setOriginalAssetID(latestAsset.getOriginalAssetId().asString());
		assetDetails.setVersion(Integer.valueOf(latestAsset.getVersion()));
		assetDetails.setContentType(latestAsset.getContentType());
		assetDetails.setMimeType(latestAsset.getMimeType());
		assetDetails.setAssetName(latestAsset.getName());
		log.debug("<<getLatestAssetId");
		return assetDetails;
	}

	private void validateAndLogin() throws DeliveryServiceException {
		log.debug(">>validateAndLogin");
		if (this.session == null) {
			log.debug("Establishing session with Media Management server...");
			login();
		} else {
			Response response = this.rootTarget.path("sessions").request().header("otmmauthtoken", this.session.getMessageDigest()).get();
			if (response.getStatus() != 200) {
				log.warn("Existing session with Media Management server has invalidated, establishing session again. Status : " + response

						.getStatus());
				this.session = null;
				login();
			} else {
				log.debug("Existing session with Media Management server is valid, continuing using the same.");
			} 
		} 
		log.debug("<<validateAndLogin");
	}

	private synchronized void login() throws DeliveryServiceException {
		log.debug(">>login");
		if (this.session != null) {
			log.debug("Another thread created OTMM session, reusing the same");
			return;
		} 
		CookieManager manager = new CookieManager();
		manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		CookieHandler.setDefault(manager);
		Form form = new Form();
		form.param("username", this.teamsUserName);
		form.param("password", this.teamsPassword);
		Response response = (Response)this.rootTarget.path("/sessions").request().post(Entity.entity(form, "application/x-www-form-urlencoded"), Response.class);
		if (response.getStatus() == 200) {
			SessionRepresentation sessionRepresentation = (SessionRepresentation)response.readEntity(SessionRepresentation.class);
			if (sessionRepresentation != null) {
				log.debug("Established session with Media Management server");
				this.session = sessionRepresentation.getSession();
			} 
			if (sessionRepresentation == null)
				throw new DeliveryServiceException("Error creating security session", 500); 
		} else {
			log.error("Failed to establish session with Media Management server. Status : " + response.getStatus());
			throw new DeliveryServiceException("Error creating security session", 500);
		} 
		log.debug("<<login");
	}

	private Invocation.Builder getRequestBuilder(String path, Map<String, ? extends Object> parameterMap) throws URIException {
		WebTarget parameterizedWebTarget = this.rootTarget.path(path);
		if (parameterMap != null)
			for (Map.Entry<String, ? extends Object> entry : parameterMap.entrySet()) {
				if (entry.getValue() instanceof String)
					parameterizedWebTarget = parameterizedWebTarget.queryParam(entry.getKey(), new Object[] { URIUtil.encodeQuery((String)entry.getValue(), "UTF-8") }); 
				if (entry.getValue() instanceof List) {
					List<String> values = (List<String>)entry.getValue();
					for (String value : values) {
						parameterizedWebTarget = parameterizedWebTarget.queryParam(entry.getKey(), new Object[] { URIUtil.encodeQuery(value, "UTF-8") });
					} 
				} 
			}  
		Invocation.Builder builder = parameterizedWebTarget.request();
		return builder;
	}

	private Invocation.Builder getRequestBuilder(String path) {
		Invocation.Builder builder = this.rootTarget.path(path).request();
		return builder;
	}

	private <T> String getJSONRepresentation(T input) throws JAXBException {
		Map<String, Object> properties = new HashMap<>();
		properties.put("eclipselink.media-type", "application/json");
		properties.put("eclipselink.json.include-root", Boolean.valueOf(true));
		properties.put("eclipselink.json.namespace-separator", Character.valueOf('#'));
		properties.put("eclipselink.oxm.metadata-source", MOXyContextResolver.BINDINGS_MAP);
		JAXBContext jc = null;
		if (contextHashMap.containsKey(input.getClass())) {
			log.debug("retreiving the JAXBContext from the cache");
			jc = contextHashMap.get(input.getClass());
		} else {
			log.debug("Creating the new JAXBContext");
			jc = JAXBContextFactory.createContext(new Class[] { input
					.getClass() }, properties);
			contextHashMap.put(input.getClass(), jc);
		} 
		StringWriter sw = new StringWriter();
		Marshaller m = jc.createMarshaller();
		m.marshal(input, sw);
		return sw.toString();
	}

	private void writeAssetDetailsFile(String stagingDirectory, AssetDetails assetDetails) throws DeliveryServiceException {
		log.debug(">>writeAssetDetailsFile");
		File file = new File(stagingDirectory, ".assetDetails");
		FileWriter writer = null;
		try {
			Properties props = new Properties();
			props.setProperty("id", assetDetails.getAssetId());
			props.setProperty("contenttype", assetDetails.getContentType());
			props.setProperty("mimetype", assetDetails.getMimeType());
			props.setProperty("assetname", assetDetails.getAssetName());
			if (assetDetails.getVersion() != null)
				props.setProperty("version", assetDetails.getVersion() + ""); 
			if (assetDetails.getCropInfo() != null)
				props.setProperty("cropInfo", assetDetails.getCropInfo()); 
			writer = new FileWriter(file);
			props.store(writer, "asset details");
		} catch (FileNotFoundException ex) {
			log.error(ex.getMessage());
			DeliveryServiceException.logStackTrace(log, ex);
			throw new DeliveryServiceException("Exception while storing asset details ", ex);
		} catch (IOException ex) {
			log.error("IO Exception" + ex.getMessage());
			DeliveryServiceException.logStackTrace(log, ex);
			throw new DeliveryServiceException("Exception while storing asset details", ex);
		} finally {
			if (writer != null)
				try {
					writer.close();
				} catch (IOException e) {
					log.error("Exception while closing the writer ", e);
				}  
		} 
		log.debug("<<writeAssetDetailsFile");
	}

	private void applyWaterMark(String inputFile, String outputFile) {
		log.debug(">>applyWaterMark");
		String waterMarkingEnabled = this.tenant.getSetting("WATER_MARKING_ENABLED");
		if (waterMarkingEnabled != null && waterMarkingEnabled.equalsIgnoreCase("Y")) {
			log.debug("Water_Marking_Enabled is Enabled");
			String waterMarkingCommand = this.tenant.getSetting("WATER_MARKING_COMMAND");
			waterMarkingCommand = waterMarkingCommand.replaceAll("\\$\\{OUTPUT_FILE\\}", 
					Matcher.quoteReplacement("\"" + outputFile + "\""));
			waterMarkingCommand = waterMarkingCommand.replaceAll("\\$\\{INPUT_FILE\\}", 
					Matcher.quoteReplacement("\"" + inputFile + "\""));
			if (waterMarkingCommand != null)
				try {
					Future<ProcessThreadOuput> future = ProcessUtils.execute(this.tenant.getId(), waterMarkingCommand, DeliveryServiceSettings.DELIVERY_SERVICE_HOME + "/data/tmp", 120000L);
					while (!future.isDone())
						Thread.sleep(250L); 
				} catch (IOException e) {
					log.error("Error water marking the file " + e.getMessage());
					DeliveryServiceException.logStackTrace(log, e);
				} catch (InterruptedException e) {
					log.error("Error water marking the file " + e.getMessage());
					DeliveryServiceException.logStackTrace(log, e);
				}  
		} else {
			log.debug("Water_Marking_Enabled is Disabled");
		} 
		log.debug("<<applyWaterMark");
	}

	private boolean validateAssetForMetadata(Asset asset) {
		Map<String, Set<String>> allowedMetadataValues = this.tenant.getMetadatafilters();
		boolean isValid = true;
		String metadataJoinCondition = this.tenant.getSetting("METADATA_JOIN_CONDITION");
		boolean orCondition = "OR".equalsIgnoreCase(metadataJoinCondition);
		if (allowedMetadataValues.size() == 0)
			return true; 
		if (asset.getMetadata() == null)
			return true; 
		MetadataField[] assetMetadataFields = asset.getMetadata().getAllMetadataFields();
		for (MetadataField metadataField : assetMetadataFields) {
			String metadataFieldIdStr = metadataField.getId().asString();
			if (allowedMetadataValues.get(metadataFieldIdStr) != null)
				if (metadataField instanceof MetadataTableField) {
					List<?> metadataValues = ((MetadataTableField)metadataField).getListOfValues();
					if (metadataValues == null && !orCondition)
						return false; 
					boolean metadataValueAvailable = false;
					for (Object metadataValue : metadataValues) {
						MetadataValue metaValue = (MetadataValue)metadataValue;
						if (((Set)allowedMetadataValues.get(metadataFieldIdStr)).contains(metaValue.getStringValue())) {
							metadataValueAvailable = true;
							if (orCondition)
								return true; 
							break;
						} 
					} 
					if (!metadataValueAvailable && !orCondition)
						return metadataValueAvailable; 
					isValid = false;
				} else if (metadataField.isDomained()) {
					MetadataValue metadataValue = metadataField.getValue();
					DomainValue domainValue = (DomainValue)metadataValue.getValue();
					String domainValueStr = null;
					if (domainValue != null)
						domainValueStr = domainValue.getDisplayValue(); 
					if (!((Set)allowedMetadataValues.get(metadataFieldIdStr)).contains(domainValueStr)) {
						if (!orCondition)
							return false; 
						isValid = false;
					} else if (orCondition) {
						return true;
					} 
				} else {
					MetadataValue metadataValue = metadataField.getValue();
					if (!((Set)allowedMetadataValues.get(metadataFieldIdStr)).contains(metadataValue.getStringValue())) {
						if (!orCondition)
							return false; 
						isValid = false;
					} else if (orCondition) {
						return true;
					} 
				}  
		} 
		return isValid;
	}

	public AssetDetails getAssetDetails(RenditionRequest renditionRequest) throws DeliveryServiceException {
		AssetDetails assetDetails = null;
		try {
			assetDetails = getLatestAssetId(renditionRequest);
		} catch (URIException e) {
			log.error("URI Exception when invoking OTMM Funciton  " + e.getMessage());
			DeliveryServiceException.logStackTrace(log, (Exception)e);
			throw new DeliveryServiceException("Exception while getting asset details ", e);
		} catch (JAXBException e) {
			log.error("JAXBException when invoking OTMM Funciton  " + e.getMessage());
			DeliveryServiceException.logStackTrace(log, (Exception)e);
			throw new DeliveryServiceException("Exception while getting asset details ", e);
		} catch (BaseTeamsException e) {
			log.error("BaseTeams when invoking OTMM Funciton  " + e.getMessage());
			DeliveryServiceException.logStackTrace(log, (Exception)e);
			throw new DeliveryServiceException("Exception while getting asset details ", e);
		} 
		return assetDetails;
	}

	private class RestSessionLogoutHook extends Thread {
		private RestSessionLogoutHook() {}

		public void run() {
			if (DBSRESTContentServiceImpl.this.session != null)
				try {
					log.debug(">>logout");
					Response response = DBSRESTContentServiceImpl.this.rootTarget.path("/sessions").request().header("X-Requested-By", Integer.valueOf(DBSRESTContentServiceImpl.this.session.getId())).delete();
					if (response.getStatus() == 200)
						log.warn("Session Deleted Successfully"); 
					log.debug("<<login");
				} catch (Exception e) {
					log.error("Error Logging out OTMM session");
				}  
		}
	}

}
