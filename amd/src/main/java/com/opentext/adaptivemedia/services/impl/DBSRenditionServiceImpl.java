package com.opentext.adaptivemedia.services.impl;

import com.opentext.adaptivemedia.common.Tenant;
import com.opentext.adaptivemedia.common.exception.DeliveryServiceException;
import com.opentext.adaptivemedia.interfaces.AssetDetails;
import com.opentext.adaptivemedia.interfaces.BaseTenantService;
import com.opentext.adaptivemedia.interfaces.CacheService;
import com.opentext.adaptivemedia.interfaces.ContentService;
import com.opentext.adaptivemedia.interfaces.RenditionService;
import com.opentext.adaptivemedia.interfaces.RenditionStorageManagerService;
import com.opentext.adaptivemedia.interfaces.RenditionStorageObject;
import com.opentext.adaptivemedia.interfaces.RenditionStorageObjectId;
import com.opentext.adaptivemedia.interfaces.TransformerService;
import com.opentext.adaptivemedia.renditions.RenditionRequest;
import com.opentext.adaptivemedia.services.AdaptiveMediaServiceFactory;
import com.opentext.adaptivemedia.tranformation.TransformationRequest;
import com.opentext.adaptivemedia.utils.DeliveryServiceUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DBSRenditionServiceImpl extends BaseTenantService implements RenditionService {
  public DBSRenditionServiceImpl(Tenant tenant) {
    super(tenant);
  }
  
  private static Log logger = LogFactory.getLog(DBSRenditionServiceImpl.class);
  
  public RenditionStorageObject getRendition(RenditionRequest request) throws DeliveryServiceException {
    logger.debug(">>getRendition");
    RenditionStorageObjectId renditionObjectId = null;
    String[] profileCollectionIds = null;
    AssetDetails assetDetails = null;
    CacheService cacheService = AdaptiveMediaServiceFactory.getInstance().getServiceFactoryInstance(this.tenant).getCacheServices().get(0);
    RenditionStorageManagerService renditionStorageService = AdaptiveMediaServiceFactory.getInstance().getServiceFactoryInstance(this.tenant).getStorageServices().get(0);
    if (request.getRenditionAttributes().get("prclid") != null)
      profileCollectionIds = ((String)request.getRenditionAttributes().get("prclid")).split(","); 
    if (request.getRenditionAttributes() != null && request.getRenditionAttributes().size() > 0) {
      String renditionHash = DeliveryServiceUtils.getOutputFileName(request.getRenditionAttributes(), this.tenant);
      Integer intVersion = null;
      if (request.getRenditionAttributes().get("version") != null) {
        intVersion = new Integer((String)request.getRenditionAttributes().get("version"));
        renditionObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), renditionHash, intVersion);
      } else if (request.getRenditionAttributes().get("vid") != null) {
        String assetId = (String)request.getRenditionAttributes().get("vid");
        renditionObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), renditionHash, assetId);
      } else {
        renditionObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), renditionHash);
      } 
    } else {
      renditionObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), null);
    } 
    if (request.getRenditionAttributes().get("smartcrop") != null && ((String)request.getRenditionAttributes().get("smartcrop")).equalsIgnoreCase("Y"))
      renditionObjectId.setSmartCrop(Boolean.valueOf(true)); 
    RenditionStorageObject renditionObject = null;
    try {
      if (profileCollectionIds != null) {
        assetDetails = stageOriginalandGenerateRendition(request);
        if (request.getRenditionAttributes().get("prid") == null) {
          RenditionStorageObject dummyObject = new RenditionStorageObject();
          dummyObject.setAssetDetails(assetDetails);
          dummyObject.setId(renditionObjectId);
          return dummyObject;
        } 
      } 
      if (cacheService.lookupRendition(renditionObjectId) == null) {
        renditionObject = renditionStorageService.getRenditionStorageObject(renditionObjectId, RenditionStorageObject.RenditionObjectDataSource.NO_CONTENT);
        if (renditionObject == null) {
          logger.debug("Rendition object is not found for : " + renditionObjectId);
          assetDetails = stageOriginalandGenerateRendition(request);
        } 
        renditionObject = renditionStorageService.getRenditionStorageObject(renditionObjectId, RenditionStorageObject.RenditionObjectDataSource.NO_CONTENT);
        if (renditionObject == null) {
          renditionObjectId.setRenditionId(null);
          renditionObject = renditionStorageService.getRenditionStorageObject(renditionObjectId, RenditionStorageObject.RenditionObjectDataSource.NO_CONTENT);
        } 
        if (assetDetails == null) {
          assetDetails = new AssetDetails();
          assetDetails.setOriginalAssetID(renditionObjectId.getOriginalAssetId());
        } 
        if (assetDetails.getAssetId() == null || assetDetails.getContentType() == null || assetDetails.getAssetName() == null)
          loadAssetDetails(renditionObjectId, this.tenant.getRepositoryRoot(), request, assetDetails); 
        renditionObject.setAssetDetails(assetDetails);
        if (renditionObject != null)
          cacheService.addRendition(renditionObjectId, cacheService
              .resolveRenditionObjectToCacheValue(renditionObject)); 
      } else {
        renditionObject = cacheService.resolveValueToRenditionObject(cacheService
            .lookupRendition(renditionObjectId));
      } 
      renditionObject.setId(renditionObjectId);
    } catch (IOException e) {
      logger.error("IO Error " + e.getMessage());
      throw new DeliveryServiceException("IO Error retrieving rendition Object ", e);
    } 
    logger.debug("<<getRendition");
    return renditionObject;
  }
  
  private AssetDetails stageOriginalandGenerateRendition(RenditionRequest request) throws DeliveryServiceException, IOException {
    logger.debug(">>stageOriginalandGenerateRendition");
    RenditionStorageObjectId renditionObjectId = null;
    RenditionStorageObjectId originalObjectId = null;
    AssetDetails assetDetails = null;
    RenditionStorageManagerService renditionStorageService = AdaptiveMediaServiceFactory.getInstance().getServiceFactoryInstance(this.tenant).getStorageServices().get(0);
    if (request.getRenditionAttributes() != null && request.getRenditionAttributes().size() > 0) {
      String renditionHash = DeliveryServiceUtils.getOutputFileName(request.getRenditionAttributes(), this.tenant);
      Integer intVersion = null;
      if (request.getRenditionAttributes().get("vid") != null) {
        renditionObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), renditionHash, (String)request
            .getRenditionAttributes().get("vid"));
        originalObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), null, (String)request
            .getRenditionAttributes().get("vid"));
      } else if (request.getRenditionAttributes().get("version") != null) {
        intVersion = new Integer((String)request.getRenditionAttributes().get("version"));
        renditionObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), renditionHash, intVersion);
        originalObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), null, intVersion);
      } else {
        renditionObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), renditionHash);
        originalObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), null);
      } 
    } else {
      renditionObjectId = RenditionStorageObjectId.buildRenditionStorageObjectId(request.getId(), null);
      originalObjectId = renditionObjectId;
    } 
    if (request.getRenditionAttributes().get("smartcrop") != null && ((String)request.getRenditionAttributes().get("smartcrop")).equalsIgnoreCase("Y")) {
      renditionObjectId.setSmartCrop(Boolean.valueOf(true));
      originalObjectId.setSmartCrop(Boolean.valueOf(true));
    } 
    RenditionStorageObject originalFile = renditionStorageService.getRenditionStorageObject(originalObjectId, RenditionStorageObject.RenditionObjectDataSource.NO_CONTENT);
    if (originalFile == null) {
      ContentService contentService = AdaptiveMediaServiceFactory.getInstance().getServiceFactoryInstance(this.tenant).getContentServices().get(0);
      File file = new File(renditionObjectId.getDirectoryForOriginal(this.tenant.getRenditionRepositoryRoot(), this.tenant.getRepositoryRoot()));
      if (!file.exists())
        file.mkdirs(); 
      assetDetails = contentService.stageContentForID(request, file.getAbsolutePath());
    } 
    if (assetDetails == null) {
      assetDetails = new AssetDetails();
      assetDetails.setOriginalAssetID(renditionObjectId.getOriginalAssetId());
    } 
    if (assetDetails.getAssetId() == null || assetDetails.getContentType() == null || assetDetails
      .getAssetName() == null)
      loadAssetDetails(renditionObjectId, this.tenant.getRepositoryRoot(), request, assetDetails); 
    if (request.getRenditionAttributes().size() == 0)
      return assetDetails; 
    String inputFile = DeliveryServiceUtils.getRenditionFileFromID(this.tenant.getRenditionRepositoryRoot(), this.tenant
        .getRepositoryRoot(), originalObjectId);
    logger.debug("Requested to generate rendition from profile collection");
    List<TransformerService> transformerSerices = (List<TransformerService>)AdaptiveMediaServiceFactory.getInstance().getServiceFactoryInstance(this.tenant).getTransformerServices().get(assetDetails.getContentType());
    if (transformerSerices != null) {
      TransformationRequest transformationRequest = new TransformationRequest();
      String outPutFile = renditionObjectId.getDirectoryForRendition(this.tenant.getRenditionRepositoryRoot(), this.tenant
          .getRepositoryRoot());
      transformationRequest.setInputFile(inputFile);
      transformationRequest.setOutputFile(outPutFile);
      transformationRequest.setTransformationAttributes(request.getRenditionAttributes());
      TransformerService transformerService = transformerSerices.get(0);
      if (transformerService != null)
        transformerService.generateRenditionFile(transformationRequest, assetDetails); 
    } 
    logger.debug("Generated rendition from profile collection");
    logger.debug("<<stageOriginalandGenerateRendition");
    return assetDetails;
  }
  
  private void loadAssetDetails(RenditionStorageObjectId id, String rootFolderName, RenditionRequest renditionRequest, AssetDetails assetDetails) throws DeliveryServiceException {
    logger.debug(">>loadAssetDetails");
    String stagingDirectory = id.getDirectoryForOriginal(this.tenant.getRenditionRepositoryRoot(), rootFolderName);
    File assetDetailsFile = new File(stagingDirectory, ".assetDetails");
    if (assetDetailsFile.exists()) {
      FileReader reader = null;
      try {
        reader = new FileReader(assetDetailsFile);
        Properties props = new Properties();
        props.load(reader);
        assetDetails.setAssetId(props.getProperty("id"));
        assetDetails.setContentType(props.getProperty("contenttype"));
        assetDetails.setMimeType(props.getProperty("mimetype"));
        assetDetails.setAssetName(props.getProperty("assetname"));
        if (props.getProperty("version") != null)
          assetDetails.setVersion(Integer.valueOf(Integer.parseInt(props.getProperty("version")))); 
        if (props.getProperty("cropInfo") != null)
          assetDetails.setCropInfo(props.getProperty("cropInfo")); 
        reader.close();
      } catch (FileNotFoundException ex) {
        logger.error(ex.getMessage());
        throw new DeliveryServiceException("Error while loading asset details ", ex);
      } catch (IOException ex) {
        logger.error(ex.getMessage());
        throw new DeliveryServiceException("Error while loading asset details ", ex);
      } finally {
        if (reader != null)
          try {
            reader.close();
          } catch (IOException e) {
            logger.error("Error while closing the reader", e);
          }  
      } 
    } 
    if ((assetDetails != null && assetDetails.getAssetName() == null) || !assetDetailsFile.exists()) {
      File assetIdFile = new File(stagingDirectory, ".assetId");
      if (assetIdFile.exists())
        assetIdFile.delete(); 
      ContentService contentService = AdaptiveMediaServiceFactory.getInstance().getServiceFactoryInstance(this.tenant).getContentServices().get(0);
      assetDetails = contentService.getAssetDetails(renditionRequest);
      FileWriter writer = null;
      try {
        Properties props = new Properties();
        props.setProperty("id", assetDetails.getAssetId());
        props.setProperty("contenttype", assetDetails.getContentType());
        props.setProperty("mimetype", assetDetails.getMimeType());
        props.setProperty("assetname", assetDetails.getAssetName());
        if (assetDetails.getVersion() != null)
          props.setProperty("version", assetDetails.getVersion() + ""); 
        writer = new FileWriter(assetDetailsFile);
        props.store(writer, "asset details");
      } catch (FileNotFoundException ex) {
        logger.error(ex.getMessage());
        DeliveryServiceException.logStackTrace(logger, ex);
        throw new DeliveryServiceException("Exception while storing asset details ", ex);
      } catch (IOException ex) {
        logger.error("IO Exception" + ex.getMessage());
        DeliveryServiceException.logStackTrace(logger, ex);
        throw new DeliveryServiceException("Exception while storing asset details", ex);
      } finally {
        if (writer != null)
          try {
            writer.close();
          } catch (IOException e) {
            logger.error("Exception while closing the writer", e);
          }  
      } 
    } 
    logger.debug("<<loadAssetDetails");
  }
}