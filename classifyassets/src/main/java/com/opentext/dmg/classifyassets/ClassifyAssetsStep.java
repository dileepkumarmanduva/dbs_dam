package com.opentext.dmg.classifyassets;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataTableField;
import com.artesia.metadata.admin.LookupValue;
import com.artesia.security.SecuritySession;
import com.opentext.activity.ActionProperty;
import com.opentext.activity.PropertyDataType;
import com.opentext.activity.PropertyType;
import com.opentext.dmg.classifyassets.util.ClassificationUtils;
import com.opentext.job.JobContext;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;

public class ClassifyAssetsStep extends BaseStep {

	private static final Log LOGGER = LogFactory.getLog(ClassifyAssetsStep.class);
	private static final String LD_TABLE = "DBS_LDT_ASSET_CLASSIFICATION";
	private static final String ASSET_TAGS_FIELD_ID = "ARTESIA.FIELD.TAG";
	private static final String STOCK_ORPHANS = "STOCK_ORPHANS";

	@Override
	public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession session)
			throws BaseTeamsException {

		if (LOGGER.isDebugEnabled()) {
			Map<String, Object> dataMap = jobContext.getData();
			LOGGER.debug(">>>ClassifyAssetsStep Begin. DataMap::" + dataMap);
			LOGGER.debug("beanProperties::" + getBeanProperties());
			LOGGER.debug("Job Parameters:: " + jobData.getJobParameters());
		}

		String assetIdStr = getBeanProperties().get("assetIds");
		LOGGER.debug("assetIdsStr" + ":" + assetIdStr);
		Object assetIdsObject = getDataFromContext(assetIdStr, jobContext);
		List<AssetIdentifier> assetIdList = ClassificationUtils.getAssetIds(assetIdsObject);

		List<LookupValue> classificationLookupValues = ClassificationUtils.getSystemLookUpValuesList(LD_TABLE, session);

		String stockOrphansFolderId =null;

		for (LookupValue lookupValue : classificationLookupValues) {
			if(STOCK_ORPHANS.equalsIgnoreCase(lookupValue.getDescription())) {
				stockOrphansFolderId = lookupValue.getId();
				continue;
			}
		}

		TeamsIdentifier fieldId = new TeamsIdentifier(ASSET_TAGS_FIELD_ID);

		for (AssetIdentifier assetId : assetIdList) {
			boolean isclassified = false;
			LOGGER.debug("assetId: " + assetId);
			String assetTagsCsv = "";
			MetadataField metadataField = AssetMetadataServices.getInstance()
					.retrieveMetadataFieldForAsset(assetId, fieldId, session);
			if (metadataField.isTabular()) {
				assetTagsCsv = ClassificationUtils.getTabularData((MetadataTableField) metadataField);
				LOGGER.debug("metadataValue: tags>>" + assetTagsCsv);
			}
			for (LookupValue lookupValue : classificationLookupValues) {

				String lookupTagsCsv = lookupValue.getDescription();
				List<String> commonTags = ClassificationUtils.findCommonWords(assetTagsCsv, lookupTagsCsv);
				LOGGER.debug("commonTags" + commonTags);
				if (commonTags != null && !commonTags.isEmpty()) {
					String folderId = lookupValue.getId();
					ClassificationUtils.assignAssetToFolder(assetId.getId(), folderId, session);
					isclassified=true;
				}
			}

			if(!isclassified) {
				LOGGER.debug("Asset with assetid "+ assetId +" moved to stock orpans");
				ClassificationUtils.assignAssetToFolder(assetId.getId(), stockOrphansFolderId, session);
			}
		}

		return StepStatus.COMPLETED;
	}

	@Override
	protected void initializeProperties(Set<ActionProperty> actionProperties) {
		ActionProperty assetIdsProperty = new ActionProperty("assetIds", "The ids of the assets to be classified",
				"Assets", true, PropertyDataType.STRING, "{assetIds}", PropertyType.INPUT);
		actionProperties.add(assetIdsProperty);

	}


}
