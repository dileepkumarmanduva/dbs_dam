package com.opentext.dmg.classifyassets.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.container.ContainerPosition;
import com.artesia.container.services.ContainerServices;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataTableField;
import com.artesia.metadata.MetadataValue;
import com.artesia.metadata.admin.LookupTable;
import com.artesia.metadata.admin.LookupValue;
import com.artesia.metadata.admin.services.MetadataAdminServices;
import com.artesia.security.SecuritySession;
import com.opentext.job.context.ListContextWrapper;

public class ClassificationUtils {

	private static final Log log = LogFactory.getLog(ClassificationUtils.class);
	
	
	public static List<LookupValue> getSystemLookUpValuesList(String tableName, SecuritySession securitySession) {
		List<LookupValue> lookupValueList = new ArrayList<>();
		LookupTable lookup;
		try {
			lookup = MetadataAdminServices.getInstance().retrieveLookupDomainTableByName(tableName, false,
					securitySession);
			LookupValue[] lookupValues = lookup.getLookupValues();
			for (LookupValue lookupValue : lookupValues) {
				lookupValueList.add(lookupValue);
			}

		} catch (BaseTeamsException e) {
			log.error("Error occur while retriving system look up values from table [ " + tableName + "]", e);
		}

		return lookupValueList;

	}

	public static List<String> findCommonWords(String input1, String input2) {
		String words1[] = input1.trim().toLowerCase().split(",");
		String words2[] = input2.trim().toLowerCase().split(",");
		List<String> list1 = new ArrayList<>(Arrays.asList(words1));
		log.debug(list1);
		List<String> list2 = Arrays.asList(words2);
		log.debug(list2);
		list1.retainAll(list2);
		return list1;
	}

	public static void assignAssetToFolder(String assetId, String folderId, SecuritySession session)
			throws BaseTeamsException {
		log.debug("Assigning Asset " + assetId + " to Folder " + folderId);
		List<TeamsIdentifier> insertChildIds = new ArrayList<>();
		insertChildIds.add(new TeamsIdentifier(assetId));
		ContainerServices.getInstance().insertChildren((ContainerPosition) ContainerPosition.LAST, insertChildIds,
				new TeamsIdentifier(folderId), session);

	}

	public static String getTabularData(MetadataTableField field) {
		if (field == null)
			return "";
		int row_count = field.getRowCount();
		StringBuilder data = new StringBuilder();
		String rowValue = "";
		for (int loop = 0; loop < row_count; loop++) {
			rowValue = field.getValueAt(loop).getStringValue();
			if (rowValue != null && !rowValue.equals("")) {
				data.append(rowValue).append(",");
			}
		}
		String returnString = data.toString();
		if (!returnString.equals("") && returnString.endsWith(",")) {
			returnString = returnString.substring(0, returnString.length() - 1);
		}
		return returnString;
	}

	public static List<AssetIdentifier> getAssetIds(Object assetIdsObject) {
		log.trace(">>getAssetIds()");
		List<AssetIdentifier> assetIds = null;
		if (assetIdsObject != null) {

			assetIds = new ArrayList<>();
			if (assetIdsObject instanceof ListContextWrapper) {

				ListContextWrapper<Object> assetIdListContextWrapper = (ListContextWrapper<Object>) assetIdsObject;
				List<Object> assetIdObjects = assetIdListContextWrapper.getListData();

				if (assetIdObjects != null) {
					for (Object assetIdObject : assetIdObjects) {
						if (assetIdObject instanceof TeamsIdentifier) {

							assetIds.add(new AssetIdentifier(assetIdObject.toString()));

							continue;
						}
						assetIds.add(new AssetIdentifier(assetIdObject.toString()));
					}

				}
			} else if (assetIdsObject instanceof TeamsIdentifier || assetIdsObject instanceof AssetIdentifier) {

				assetIds.add(new AssetIdentifier(assetIdsObject.toString()));
			} else {

				String[] assetIdsArray = assetIdsObject.toString().split(",");
				for (String assetId : assetIdsArray) {
					assetIds.add(new AssetIdentifier(assetId));
				}
			}
		}
		log.trace("<<getAssetIds()");
		return assetIds;
	}

	public static String getMetadataFieldStringValue(String assetId, String fieldId, SecuritySession session)
			throws BaseTeamsException {
		log.debug(">> getMetadataFieldStringValue() start");
		String metadatValueString = null;
		MetadataField metadataField = AssetMetadataServices.getInstance()
				.retrieveMetadataFieldForAsset(new AssetIdentifier(assetId), new TeamsIdentifier(fieldId), session);
		if (metadataField != null) {
			MetadataValue metadatValue = metadataField.getValue();
			metadatValueString = metadatValue.getStringValue();
			log.debug(metadataField.getName() + ": " + metadatValueString);
		}
		log.debug(">> getMetadataFieldStringValue() end");
		return metadatValueString;
	}

	private ClassificationUtils() {
		log.debug("Initializing ClassificationUtils");
	}

}
