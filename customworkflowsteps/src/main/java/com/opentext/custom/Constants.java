package com.opentext.custom;

import java.io.File;

public class Constants {
    /**
     * <p><b>private constructor</b></p>
     */
    private Constants() {
    }
    /**
     * <p><b>constant variable CUSTOM_ASSET_DATE_EXPIRY_WITH_CUSTOM_ASSIGNMENT</b></p>
     */
    public static final String CUSTOM_ASSET_DATE_EXPIRY_WITH_CUSTOM_ASSIGNMENT = "CustomAssetDateExpirWithCustomAssignment";
    /**
     * <p><b>constant variable CUSTOM_EXPIRY_USER_FORM_STEP</b></p>
     */
    public static final String CUSTOM_EXPIRY_USER_FORM_STEP = "customExpiryCustomUserStep";
    /**
     * <p><b>constant variable OTMM_ASSIGNMENT_DURATION_FORM_FIELD</b></p>
     */
    public static final String OTMM_ASSIGNMENT_DURATION_FORM_FIELD = "otmm.assignment.duration";

    /**
     * email configuration
     */
    public static final String TEXT_HTML_CHARSET_UTF_8 = "text/html; charset=utf-8";
    public static final String MAIL_SMTP_HOST = "mail.smtp.host";
    public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    public static final String FALSE = "false";

    public static final String CUSTOM_EXPIRY_INPUT_PROPERTIES_FILE_NAME = "customExpiryInput.properties";
    public static final String PROP_FILE_PATH = File.separator + "plugins" + File.separator + "conf" + File.separator;

    /**
     * custom asset expiry fields
     */
    public static final String COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_FLAG = "COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.FLAG";
    public static final String COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_DATE = "COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.DATE";
    public static final String COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_COUNT = "COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.COUNT";

    /**
     * contact sheet constants
     */
    public static final String ADAPTIVEMEDIA_RENDITION_ID_API_URL = "/adaptivemedia/rendition?id=";
    public static final String FILE = "file";
    public static final String CONTACT_SHEET_PDF_TIMESTAMP_FORMAT = "yyyy.MM.dd.HH.mm.ss";
    public static final String CONTACT_SHEET_DOC_NAME = "iDoc-";
    public static final String CONTACT_SHEET_PARA_NAME = "Contact Sheet";
    public static final String FONT_SIZE = "20f";
    public static final String CONTACT_SHEET_CLICK_LABLE = "Click here to open";
    public static final String CONTACT_SHEET_ENCRYPTED_PDF_FILE_NAME = "encryptedPdf.pdf";

    public static final String EXCEL_FORM_ASSETS = "excelForm.assets";
    public static final String EXCEL_FORM_SOURCE_EXCEL_ASSET_ID = "metadataassetId";
    public static final String LOG_CUSTOM_ASSET_METADATA_UPDATE = "CUSTOM_ASSET_METADATA_UPDATE -> ";
    public static final String CUSTOM_ASSET_EXPIRY = "CustomAssetExpiry";
    public static final String FREQUENCY_PROP = "frequency";
    public static final String JOBS_PROP = "jobs";
    public static final String SCHEDULE_PROP = "schedule";
    public static final String INTERVAL_PROP = "interval";
    public static final String JOB = "Job";
    public static final String USERNAME_PROP = "username";
    public static final String PASSWORD_PROP = "password";
    public static final String SESSION_RESOURCE = "session_resource";
    public static final String SESSION_KEY = "session";
    public static final String OTMMAUTHTOKEN_KEY = "otmmauthtoken";
    public static final String HOST_NAME_PROP = "hostName";
    public static final String SESSIONS = "/otmmapi/v6/sessions";
    public static final String MESSAGE_DIGEST = "message_digest";
    public static final String OTMMAPI_V_6_ASSETS = "/otmmapi/v6/assets/";
    public static final String CONTENTS_DISPOSITION_ATTACHMENT = "/contents?disposition=attachment";
    public static final String DAYS_7 = "7Days";
    public static final String DAYS_30 = "30Days";
    public static final String YESTERDAY = "yesterday";
    public static final String TODAY = "today";
    public static final String TEAMS_HOME = "TEAMS_HOME";
    public static final String DURATION_PROP = "duration";
    public static final String ASSET_TYPE_PROP = "assetType";
    public static final String JOB_KEY = "Job";
    public static final String FAKE_RECEIVER_EMAIL = "fake-receiver@fake-host.com";
    public static final String OTMM_GO_ASSET_API_URL = "/otmm/go/asset/";
    public static final String BODY_PROP = "body";
    public static final String RECEIVER_EMAIL_PROP = "receiverEmail";
    public static final String SUBJECT_PROP = "subject";
    public static final String ADMIN_EMAIL_ADDRESS = "ADMIN_EMAIL_ADDRESS";
    public static final String FAKE_SENDER_EMAIL = "admin@opentext.com";
    public static final String SERVER_KEY = "SERVER";
    public static final String COMMON = "COMMON";
    public static final String CONTACT_SHEET_EMAIL_SUBJECT = "Contact Sheet";
    public static final String CONTACT_SHEET_EMAIL_BODY = "<p>Hi Team,</p><br>PFA summary of contact sheet document.<p></p><p>Thanks & Regards,<br>OpenText Support Team.</p>";
    public static final String EMAIL_BODY = "<style>td{border:1px solid;padding:3px;}</style> <html><body><p>Hi Team,</p><p>Below assets are being expired for the duration of {0}. please take the necessary action.</p><br><table><tr><td>ASSET ID</td><td>ASSET NAME</td><td>Expirty Date</td><td>Imported By</td><td>Next attempt date</td><!--<td>Current attempt count</td>--><td>Total notify attempt</td></tr>{1}</table><br><br><p>Regards,<br>OpenText Team</p>";
    public static final String CONTACT_SHEET_EMAIL_SENDER = "admin@opentext.com";
    public static final String CONTACT_SHEET_EMAIL_PASSWORD = "xxxxx";
    public static final String COMMON_COMPONENT = "COMMON";
    public static final String ASSETS_CONTEXT = "assetIds";
    public static final String PDF_SHEET_ASSETS_CONTEXT = "assetIds";
    public static final String EMAIL_RECIPIENTS = "emailRecipients";
    public static final String FILE_CONTEXT = "file";
    public static final String EMAIL_SERVER = "EMAIL_SERVER";
    public static final String LOCAL_USER_NAME = "tsuper";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
}
