package com.opentext.custom.job;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.common.prefs.PrefData;
import com.artesia.common.prefs.PrefDataId;
import com.artesia.security.SecuritySession;
import com.artesia.system.services.SystemServices;
import com.opentext.custom.model.ConfigProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Properties;

import static com.opentext.custom.Constants.*;
import static com.opentext.custom.util.OTUtils.securitySession;

/**
 * BaseExample is a super class to all the sample activity related
 * example classes. It just reads the data from the property file.
 */
public class BaseCustomAssetExpiry {
    private static final Log log = LogFactory.getLog(BaseCustomAssetExpiry.class);
    public static final ConfigProperties configProperties;

    private BaseCustomAssetExpiry() {
    }

    static {
        if (null == System.getProperty(TEAMS_HOME)) {
            Properties props = System.getProperties();
            props.setProperty(TEAMS_HOME, System.getenv(TEAMS_HOME));
        }
        configProperties = new ConfigProperties();
        try {
            configProperties.setBody(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, BODY_PROP, securitySession));
            configProperties.setSubject(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, SUBJECT_PROP, securitySession));
            configProperties.setRecipients(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, RECEIVER_EMAIL_PROP, securitySession));
            configProperties.setDuration(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, DURATION_PROP, securitySession));
            configProperties.setFrequency(null != getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, FREQUENCY_PROP, securitySession) ? Integer.parseInt(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, FREQUENCY_PROP, securitySession)) : 0);
            configProperties.setInterval(null != getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, INTERVAL_PROP, securitySession) ? Integer.parseInt(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, FREQUENCY_PROP, securitySession)) : 0);
            configProperties.setSchedule(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, SCHEDULE_PROP, securitySession));
            configProperties.setJobs(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, JOBS_PROP, securitySession));
            configProperties.setType(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, ASSET_TYPE_PROP, securitySession));
            configProperties.setHostName(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, HOST_NAME_PROP, securitySession));
            log.info("CUSTOM_ASSET_EXPIRY -> utility configuration loaded" + configProperties);
            //configProperties.setPassword(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, PASSWORD_PROP, securitySession));
            //configProperties.setUsername(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, USERNAME_PROP, securitySession));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    public static String getSystemInformation(final String comp, final String key, final String name, final SecuritySession session)
            throws BaseTeamsException {
        SystemServices systemServices = SystemServices.getInstance();
        PrefDataId prefDataId = new PrefDataId(comp, key, name);
        PrefData systemSettings = systemServices.retrieveSystemSettingsByPrefDataId(prefDataId, session);
        String value = null;
        if (systemSettings != null) {
            value = systemSettings.getValue();
        }
        return value;
    }
}
