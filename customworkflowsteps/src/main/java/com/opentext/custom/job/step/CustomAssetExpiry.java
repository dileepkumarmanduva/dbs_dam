package com.opentext.custom.job.step;



import com.artesia.common.exception.BaseTeamsException;
import com.artesia.security.SecuritySession;

public interface CustomAssetExpiry
{
   /**
    * Executes the example code.
    */
   public void doAssetExpiryCheck(SecuritySession session) throws BaseTeamsException;
}
