package com.opentext.custom.job.step;

import com.artesia.asset.Asset;
import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.asset.services.AssetDataLoadRequest;
import com.artesia.asset.services.AssetServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.*;
import com.artesia.metadata.services.MetadataServices;
import com.artesia.security.SecuritySession;
import com.opentext.custom.step.ReadExcel;
import com.opentext.custom.util.OTUtils;
import com.opentext.job.JobContext;
import com.opentext.job.context.ListContextWrapper;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.opentext.custom.Constants.EXCEL_FORM_SOURCE_EXCEL_ASSET_ID;

public class CustomAssetUtil extends BaseStep {
    private static final Log log = LogFactory.getLog(CustomAssetUtil.class);

    public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession securitySession) {
        byte[] fileContent = null;
        try {
            Object assetIdsObject = jobContext.getData().get("htmlHightailAssets");
            List<AssetIdentifier> assetIdsList = ((ListContextWrapper<AssetIdentifier>) assetIdsObject).getListData();
            String fileId = "";
            if (null != jobContext && null != jobContext.getData()) {
                Object dataFromContext = jobContext.getData().get(EXCEL_FORM_SOURCE_EXCEL_ASSET_ID);
                if (null != dataFromContext) {
                    List<AssetIdentifier> temp = ((ListContextWrapper<AssetIdentifier>) dataFromContext).getListData();
                    fileId = temp.get(0).asString();
                    log.info("Metadata Asset ID is : " + fileId);
                }
            }

            final Map<String, Map<TeamsIdentifier, String>> assetsMetadataDetails = new HashMap<>();
            final File file = OTUtils.getOriginalAsset(fileId, securitySession);
            try {
                fileContent = Files.readAllBytes(file.toPath());
            } catch (Exception e) {
                log.info("An error occcured while trying to read the file content", e);
            } finally {
                if (null != file && file.exists()) {
                    file.delete();
                }
            }

            if (null != fileContent) {
                InputStream is = new ByteArrayInputStream(fileContent);
                Workbook book = WorkbookFactory.create(is);
                assetsMetadataDetails.putAll(ReadExcel.readXlsxFileToTeamsIdentifierMapNew(book, jobContext));
            } else {
                log.warn("Unable to read the metadata tag sheet, metadata update will not happen.");
                return StepStatus.COMPLETED_WITH_ERRORS;
            }
            invokeAssetUpdate(assetIdsList, assetsMetadataDetails, securitySession);
        } catch (Exception e) {
            log.error("An error occured while trying to attach the asset metadata, unable to download the spreadsheet or create a workbook", e);
            return StepStatus.COMPLETED_WITH_ERRORS;
        }
        return StepStatus.COMPLETED;
    }

    /**
     * @param assetIdsList
     * @param assetsMetadataDetails
     * @param securitySession
     */
    private void invokeAssetUpdate(List<AssetIdentifier> assetIdsList, final Map<String, Map<TeamsIdentifier, String>> assetsMetadataDetails, SecuritySession securitySession) {
        assetIdsList.stream().forEach(id -> {
            AssetDataLoadRequest dataRequest = new AssetDataLoadRequest();
            dataRequest.setLoadMetadata(true);
            try {
                Asset asset = AssetServices.getInstance().retrieveAsset(id, dataRequest, securitySession);
                if (asset != null) {
                    updateAssetMetadata(securitySession, asset, assetsMetadataDetails.get(id.asString()));
                }
            } catch (BaseTeamsException e) {
                log.error("An error occured while updating the asset with metadata: failed to retreive asset with ID : " + id.asString(), e);
            }
        });
    }

    private static void updateAssetMetadata(SecuritySession session, Asset asset, Map<TeamsIdentifier, String> teamsIdentifiers) {
        try {
            AssetServices.getInstance().lockAsset(asset.getAssetId(), session);
            List<MetadataField> metadataFields = new ArrayList<>();
            if (null != teamsIdentifiers)
                teamsIdentifiers.keySet().stream().forEach(ti -> {
                    try {

                        MetadataModel metadataModel = MetadataServices.getInstance().retrieveModelById(new TeamsIdentifier("DBS.GENERAL.ASSET"), session);

                        MetadataElement field = metadataModel.findElementById(ti);
                        if (field instanceof MetadataTableField) {
                            MetadataTableField tableField = (MetadataTableField) field;
                            String valuesToAddString = teamsIdentifiers.get(ti);
                            MetadataTableField cloneField = (MetadataTableField) MetadataUtils.createCloneForField(tableField);
                            String[] tablularFieldValuesArray = valuesToAddString.split(",");
                            for (String aValue : tablularFieldValuesArray) {
                                if (!aValue.isBlank() || !aValue.isEmpty() || aValue != null) {
                                    cloneField.addValue(new MetadataValue(aValue.trim()));
                                }
                            }
                            metadataFields.add(cloneField);
                        } else if (field instanceof MetadataField) {
                            MetadataField temp = (MetadataField) field;
                            String valueToSet = teamsIdentifiers.get(ti).trim();
                            if (!valueToSet.isBlank() || !valueToSet.isEmpty() || valueToSet != null) {
                                MetadataField cloneScalarField = MetadataUtils.createCloneForField(temp);
                                cloneScalarField.setValue(teamsIdentifiers.get(ti).trim());
                                metadataFields.add(cloneScalarField);
                            }
                        }
                    } catch (BaseTeamsException e) {
                        log.error("Failed to retrieve metadata model : DBS.GENERAL.ASSET", e);
                    }
                });

            if (!metadataFields.isEmpty()) {
                MetadataField[] mfs = new MetadataField[metadataFields.size()];
                metadataFields.toArray(mfs);
                AssetMetadataServices.getInstance().saveMetadataForAssets(asset.getAssetId().asAssetIdArray(), mfs, session);
                AssetServices.getInstance().unlockAsset(asset.getAssetId(), session);
            }
        } catch (Exception e) {
            log.error("Unable to update the metadata for asset from the tag sheet:" + asset.getAssetId().asString(), e);
        }
    }
}