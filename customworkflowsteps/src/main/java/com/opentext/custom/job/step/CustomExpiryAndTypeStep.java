package com.opentext.custom.job.step;

import com.artesia.common.MetadataFieldConstants;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.security.SecuritySession;
import com.opentext.custom.job.model.ExpiringAssetInfo;
import com.opentext.custom.util.OTUtils;
import com.opentext.job.JobContext;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.opentext.custom.Constants.*;
import static com.opentext.custom.util.OTUtils.getSystemInformation;

/**
 * It moves an asset from source folder to destination folder
 */
public class CustomExpiryAndTypeStep extends BaseStep {
    private static final Log log = LogFactory.getLog(CustomExpiryAndTypeStep.class);

    public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession securitySession) {
        String duration = null;
        try {
            duration = getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, DURATION_PROP, securitySession);
        } catch (BaseTeamsException e) {
            e.printStackTrace();
        }
        List<ExpiringAssetInfo> assetIdentifiers = null;

        try {
            TeamsIdentifier[] fieldIds = new TeamsIdentifier[]{
                    MetadataFieldConstants.METADATA_FIELD_ID__ASSET_DESCRIPTION,
                    MetadataFieldConstants.METADATA_FIELD_ID__SOURCE_REFERENCE,
                    MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE,
                    new TeamsIdentifier(COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_FLAG),
                    new TeamsIdentifier(COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_DATE),
                    new TeamsIdentifier(COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_COUNT)
            };

            for (String type : getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, ASSET_TYPE_PROP, securitySession).split(","))
                if (null != type && !type.isEmpty())
                    assetIdentifiers = OTUtils.searchExpiringAssetsForDuration(fieldIds, duration, type, securitySession);
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (BaseTeamsException e) {
            e.printStackTrace();
            log.error("CUSTOM_ASSET_EXPIRY -> " + e.getMessage());
        }

        try {
            if (null != assetIdentifiers && assetIdentifiers.size() > 0) {
                Map<String, Object> data = new HashMap<>();
                data.put(ASSETS_CONTEXT, assetIdentifiers);
                jobContext.setData(data);
            } else {
                Map<String, Object> data = new HashMap<>();
                data.put(ASSETS_CONTEXT, "");
                jobContext.setData(data);
            }
        } catch (Exception e) {
            log.error("CUSTOM_ASSET_EXPIRY -> exception: " + e.getMessage());
        }

        return StepStatus.COMPLETED;
    }

}