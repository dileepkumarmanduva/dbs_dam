package com.opentext.custom.job.step;

import com.artesia.common.MetadataFieldConstants;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.security.SecuritySession;
import com.artesia.security.session.services.AuthenticationServices;
import com.google.gson.Gson;
import com.opentext.custom.job.model.AssetsData;
import com.opentext.custom.job.model.ExpiringAssetInfo;
import com.opentext.custom.util.OTUtils;
import com.opentext.job.JobContext;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static com.opentext.custom.Constants.*;
import static com.opentext.custom.util.OTUtils.getSystemInformation;

/**
 * It moves an asset from source folder to destination folder
 */
public class CustomExpiryStep extends BaseStep {
    private static final Log log = LogFactory.getLog(CustomExpiryStep.class);

    public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession securitySession) {
        if (null == System.getProperty(TEAMS_HOME)) {
            Properties props = System.getProperties();
            props.setProperty(TEAMS_HOME, System.getenv(TEAMS_HOME));
        }
        String duration = null;
        try {
            duration = getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, DURATION_PROP, securitySession);

        } catch (BaseTeamsException e) {
            e.printStackTrace();
        }
        List<ExpiringAssetInfo> assetIdentifiers = null;
        try {
            TeamsIdentifier[] fieldIds = new TeamsIdentifier[]{
                    MetadataFieldConstants.METADATA_FIELD_ID__ASSET_DESCRIPTION,
                    MetadataFieldConstants.METADATA_FIELD_ID__SOURCE_REFERENCE,
                    MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE,
                    new TeamsIdentifier(COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_FLAG),
                    new TeamsIdentifier(COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_DATE),
                    new TeamsIdentifier(COM_DBS_METADATA_CUSTOM_EXPIRY_NOTIFICATION_COUNT)
            };
            assetIdentifiers = OTUtils.searchExpiringAssetsForDuration(fieldIds, duration, "", securitySession);
        } catch (BaseTeamsException e) {
            e.printStackTrace();
            log.error("CUSTOM_ASSET_EXPIRY -> " + e.getMessage());
        }
        try {
            Map<String, Object> data = new HashMap<>();
            AssetsData assetsData = new AssetsData();
            if (null != assetIdentifiers && assetIdentifiers.size() > 0) {
                assetsData.setExpiringAssetInfoList(assetIdentifiers);
                data.put(ASSETS_CONTEXT, new Gson().toJson(assetsData));
                jobContext.setData(data);
            } else {
                data.put(ASSETS_CONTEXT, new Gson().toJson(assetsData));
                jobContext.setData(data);
            }
        } catch (Exception e) {
            log.error("CUSTOM_ASSET_EXPIRY -> exception: " + e.getMessage());
        }
        return StepStatus.COMPLETED;
    }

    public static void main(String[]a) throws BaseTeamsException {
        if (null == System.getProperty(TEAMS_HOME)) {
            Properties props = System.getProperties();
            props.setProperty(TEAMS_HOME, "C:\\Users\\Home\\Documents\\OpenText\\OTMM5");
        }
        CustomExpiryStep customExpiryStep = new CustomExpiryStep();
        Map<String, Object> map = new HashMap<>();
        JobData jobData = new JobData();
        jobData.setJobExecutionId(123L);
        jobData.setStepId(123L);
        jobData.setStepName("CustomExpiry");
        jobData.setInitiatorLoginId("tsuper");
        jobData.setJobExecutionId(123L);
        jobData.setJobParameters(map);
        JobContext jobContext = new JobContext();
        jobContext.setData(map);
        SecuritySession session = AuthenticationServices.getInstance().login("tsuper", "OpenText123$");
        customExpiryStep.executeStep(jobData, jobContext, session);
    }
}