package com.opentext.custom.job.step;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.common.prefs.Preferences;
import com.artesia.security.SecuritySession;
import com.artesia.security.UserCriteria;
import com.opentext.custom.util.OTUtils;
import com.opentext.job.JobContext;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Arrays;
import java.util.Properties;

import static com.opentext.custom.Constants.*;

/**
 * This class is used to send notification after completion of custom activity
 *
 * @author umeshkumars
 */
public class EmailFileNotification extends BaseStep {
    private static final Log log = LogFactory.getLog(Notification.class);

    public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession securitySession)
            throws BaseTeamsException {
        boolean isError = false;
        String senderEmail = OTUtils.getSystemInformation(COMMON_COMPONENT, SERVER_KEY, ADMIN_EMAIL_ADDRESS, securitySession);

        UserCriteria userCriteria = new UserCriteria();
        userCriteria.setLoginName(jobData.getInitiatorLoginId());
        File attachment = null;
        String emailRecipients = "";

        if (jobContext.getData() != null) {
            try {
                emailRecipients = (String) jobContext.getData().get(EMAIL_RECIPIENTS);
                if (emailRecipients.isEmpty()) {
                    log.error("CUSTOM_CONTACT_SHEET -> exception: no expiry asset available");
                } else {
                    isError = true;
                }
            } catch (Exception e) {
                log.error("CUSTOM_CONTACT_SHEET -> exception: no access to process, ", e);
            }
            try {
                attachment = (File) jobContext.getData().get(FILE_CONTEXT);
            } catch (Exception e) {
                isError = true;
                log.error("CUSTOM_CONTACT_SHEET -> exception: no access to process, ", e);
            }
        }

        log.info("CUSTOM_CONTACT_SHEET -> email data: " + CONTACT_SHEET_EMAIL_SUBJECT + ", " + senderEmail + ", " + emailRecipients + ", " + CONTACT_SHEET_EMAIL_BODY);
        log.info("CUSTOM_CONTACT_SHEET -> " + attachment.getAbsolutePath());

        if (isError) {
            if (null == senderEmail)
                senderEmail = FAKE_SENDER_EMAIL;
            if (null == emailRecipients)
                emailRecipients = FAKE_RECEIVER_EMAIL;
            if (null != attachment && !emailRecipients.isEmpty()) {
                final StringBuilder mailBody = new StringBuilder();
                mailBody.append(CONTACT_SHEET_EMAIL_BODY);
                try {
                    sendEmail(senderEmail, emailRecipients, CONTACT_SHEET_EMAIL_SUBJECT, CONTACT_SHEET_EMAIL_BODY, attachment);
                    log.error("CUSTOM_CONTACT_SHEET -> email sent");
                } catch (MessagingException e) {
                    e.printStackTrace();
                    log.error("CUSTOM_CONTACT_SHEET -> ", e);
                    return StepStatus.FAILED;
                }
            }
        } else
            log.info("CUSTOM_CONTACT_SHEET -> no asset found to log an email");

        return StepStatus.COMPLETED;
    }

    public void sendEmail(String sender, String recepients, String subject, String CONTACT_SHEET_EMAIL_BODY, File file) throws MessagingException {
        String smtpServer = Preferences.getPreference(COMMON_COMPONENT, SERVER_KEY, EMAIL_SERVER);
        Properties properties = System.getProperties();

        properties.setProperty(MAIL_SMTP_HOST, smtpServer);
        properties.put(MAIL_SMTP_AUTH, FALSE);

        Session session = Session.getDefaultInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(CONTACT_SHEET_EMAIL_SENDER, CONTACT_SHEET_EMAIL_PASSWORD);
                    }
                });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender));
            message.setSubject(subject);

            if (null != recepients) {
                Arrays.stream(recepients.split(",")).forEach(recepient -> {
                    try {
                        message.addRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                });
            }

            //Setup email body content
            MimeBodyPart emailBodyContent = new MimeBodyPart();
            emailBodyContent.setContent(CONTACT_SHEET_EMAIL_BODY, TEXT_HTML_CHARSET_UTF_8);
            MimeBodyPart emailAttachment = new MimeBodyPart();

            //Attach a file
            DataSource source = new FileDataSource(file.getAbsolutePath());
            emailAttachment.setDataHandler(new DataHandler(source));
            emailAttachment.setFileName(file.getName());

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(emailBodyContent);
            multipart.addBodyPart(emailAttachment);
            message.setContent(multipart);

            Transport.send(message);
            log.info("CUSTOM_EMAIL_SEND_FOR_CONTACT_SHEET -> message sent for the contact sheet attachment....");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("CUSTOM_EMAIL_SEND_FOR_CONTACT_SHEET-> ", e);
        }
    }
}