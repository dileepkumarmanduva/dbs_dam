package com.opentext.custom.job.step;

import com.artesia.common.email.EmailHelper;
import com.artesia.common.email.EmailMessage;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.security.SecuritySession;
import com.artesia.security.UserCriteria;
import com.google.gson.Gson;
import com.opentext.custom.job.model.AssetsData;
import com.opentext.custom.util.OTUtils;
import com.opentext.job.JobContext;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.opentext.custom.Constants.*;
import static com.opentext.custom.util.OTUtils.getSystemInformation;

/**
 * This class is used to send notification after completion of custom activity
 *
 * @author umeshkumars
 */
public class Notification extends BaseStep {
    private static final Log log = LogFactory.getLog(Notification.class);

    public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession securitySession) {
        boolean isAssetAvailable = false;
        UserCriteria userCriteria = new UserCriteria();
        userCriteria.setLoginName(jobData.getInitiatorLoginId());
        AssetsData assetsData = null;
        if (jobContext.getData() != null) {
            try {
                String data = (String) jobContext.getData().get(ASSETS_CONTEXT);
                assetsData = new Gson().fromJson(data, AssetsData.class);
                if (assetsData.getExpiringAssetInfoList().isEmpty()) {
                    log.error("CUSTOM_ASSET_EXPIRY -> exception: no expiry asset available");
                } else {
                    isAssetAvailable = true;
                }
            } catch (Exception e) {
                log.error("CUSTOM_ASSET_EXPIRY -> exception: no access to process, ", e);
            }
        }
        if (isAssetAvailable) {
            try {
                searchData(assetsData, securitySession);
            } catch (BaseTeamsException e) {
                e.printStackTrace();
                log.error("CUSTOM_ASSET_EXPIRY -> exception: unable to send email,][ ", e);
                return StepStatus.FAILED;
            }
        } else {
            log.info("CUSTOM_ASSET_EXPIRY -> no asset found to log an email");
            return StepStatus.COMPLETED;
        }
        return StepStatus.COMPLETED;
    }

    public static void searchData(AssetsData data, SecuritySession securitySession)
            throws BaseTeamsException {
        UserCriteria userCriteria = new UserCriteria();
        userCriteria.setLoginName(securitySession.getLoginName());
        if (null != data && data.getExpiringAssetInfoList().size() > 0) {
            final StringBuilder mailBody = new StringBuilder();
            final List<String> recipient = new ArrayList();
            final EmailMessage message = new EmailMessage();
            final String hostName = getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, HOST_NAME_PROP, securitySession);
            data.getExpiringAssetInfoList().stream().forEach(asset -> {
                mailBody.append("<tr>");
                mailBody.append("<td><a href='" + hostName + OTMM_GO_ASSET_API_URL + asset.getAssetId() + "'>".concat(asset.getAssetName().concat("<a/></td>")));
                mailBody.append("<td>".concat(asset.getExpiryDate().concat("</td>")));
                mailBody.append("<td>".concat(asset.getAssetOwner().concat("</td>")));
                mailBody.append("</tr>");
            });

            if (recipient.size() == 0) {
                recipient.add(FAKE_RECEIVER_EMAIL);
            }
            String updatedEmailBody = OTUtils.getProp().getProperty(BODY_PROP).replace("{0}",
                    getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, DURATION_PROP, securitySession));
            if (null != getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, RECEIVER_EMAIL_PROP, securitySession) && !getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, RECEIVER_EMAIL_PROP, securitySession).isEmpty())
                message.setRecipientList(Arrays.asList(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, RECEIVER_EMAIL_PROP, securitySession).split(",")));
            else
                message.setRecipientList(recipient);
            message.setSender(getSystemInformation(COMMON, SERVER_KEY, ADMIN_EMAIL_ADDRESS, securitySession));
            message.setSubject(getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, SUBJECT_PROP, securitySession));
            try {
                message.setBody(updatedEmailBody.replace("{1}", mailBody));
            } catch (Exception e) {
                e.printStackTrace();
                log.error("CUSTOM_ASSET_EXPIRY -> exception: ", e);
            }
            try {
                EmailHelper emailHelper = new EmailHelper();
                emailHelper.sendEmail(message);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("CUSTOM_ASSET_EXPIRY -> exception:", e);
            }
        } else
            log.info("CUSTOM_ASSET_EXPIRY -> no asset found to log an email");
    }
}