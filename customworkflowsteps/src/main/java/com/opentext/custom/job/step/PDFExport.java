package com.opentext.custom.job.step;

import com.artesia.asset.Asset;
import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.services.AssetDataLoadRequest;
import com.artesia.asset.services.AssetServices;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.security.SecuritySession;
import com.google.gson.Gson;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.opentext.custom.job.model.AssetData;
import com.opentext.custom.util.OTUtils;
import com.opentext.job.JobContext;
import com.opentext.job.context.ListContextWrapper;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;

import static com.opentext.custom.Constants.*;
import static com.opentext.custom.util.OTUtils.*;

public class PDFExport extends BaseStep {
    private static final Log log = LogFactory.getLog(PDFExport.class);

    /**
     * @param document
     * @param datas
     * @param fileName
     * @throws IOException
     * @throws DocumentException
     * @throws URISyntaxException
     */
    public void generatePdf(Document document, List<Map<String, Object>> datas, String fileName) throws IOException, DocumentException, URISyntaxException {
        PdfWriter.getInstance(document, new FileOutputStream(fileName));
        document.open();

        Font heading = FontFactory.getFont(FontFactory.COURIER, 20, BaseColor.BLACK);
        Paragraph paragraph = new Paragraph(CONTACT_SHEET_PARA_NAME);
        heading.setStyle(FONT_SIZE);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setFont(heading);
        document.add(paragraph);
        document.add(Chunk.NEWLINE);

        datas.stream().forEach(data -> {
            try {
                Image img = (Image) data.get(FILE);
                img.setAlignment(Element.ALIGN_LEFT);
                document.add(img);
                document.add(Chunk.NEWLINE);
            } catch (DocumentException | NullPointerException e) {
                log.error(" -> " + e.getMessage());
            }
            Map<String, String> dataMap = (Map<String, String>) data.get("data");
            PdfPTable table = new PdfPTable(2);
            dataMap.keySet().stream().forEach(dataKey -> {
                PdfPCell horizontalAlignCell = new PdfPCell(new Phrase(dataKey));
                horizontalAlignCell.setHorizontalAlignment(Element.ALIGN_BOTTOM);
                table.addCell(horizontalAlignCell);
                PdfPCell verticalAlignCell = null;
                if (dataKey.contains("AMD URL")) {
                    Anchor anchor = new Anchor(CONTACT_SHEET_CLICK_LABLE);
                    anchor.setReference(dataMap.get(dataKey));
                    verticalAlignCell = new PdfPCell(anchor);
                } else
                    verticalAlignCell = new PdfPCell(new Phrase(dataMap.get(dataKey)));
                verticalAlignCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
                table.addCell(verticalAlignCell);
            });

            try {
                document.add(table);
                document.add(Chunk.NEWLINE);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        });
        document.close();
    }

    /**
     * @param fileName
     * @param pass
     * @throws IOException
     * @throws DocumentException
     */
    public void fileEncryption(String fileName, String pass) throws IOException, DocumentException {
        PdfReader pdfReader = new PdfReader(fileName);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(CONTACT_SHEET_ENCRYPTED_PDF_FILE_NAME));
        pdfStamper.setEncryption(pass.getBytes(), "".getBytes(), 0, PdfWriter.ENCRYPTION_AES_256);
        pdfStamper.close();
    }

    /**
     * @param path
     * @return
     */
    private Image getImage(Path path) {
        Image img = null;
        try {
            img = Image.getInstance(path.toAbsolutePath().toString());
        } catch (BadElementException | IOException ex) {
            log.error("CUSTOM_CONTACT_SHEET -> ", ex);
        }
        if (null != img) {
            img.scalePercent(35);
            img.setIndentationLeft(52);
            img.setBottom(10);
        }
        return img;
    }

    /**
     * @param assetIds
     * @param securitySession
     * @return
     */
    public List<AssetData> prepareAssetDataList(String assetIds, String adaptiveUrl, SecuritySession securitySession) {
        List<AssetData> assetDatas = new ArrayList<>();
        boolean isAssetAvailable = false;
        List<String> assetIdentifiers = new ArrayList<>();
        try {
            if (assetIds.isEmpty()) {
                log.error("CUSTOM_CONTACT_SHEET -> exception: no expiry asset available");
            } else {
                isAssetAvailable = true;
                assetIdentifiers = Arrays.asList(assetIds.split(","));
            }
        } catch (Exception e) {
            log.error("CUSTOM_CONTACT_SHEET -> exception: no access to process, " + e.getMessage());
        }
        if (isAssetAvailable) {
            if (null != assetIdentifiers) {
                for (String assetIdentifier : assetIdentifiers) {
                    final AssetDataLoadRequest assetDataLoadRequest = new AssetDataLoadRequest();
                    assetDataLoadRequest.setLoadMetadata(true);
                    assetDataLoadRequest.setLoadAssetContentInfo(true);
                    Asset asset = null;
                    try {
                        final AssetIdentifier ai = new AssetIdentifier();
                        ai.setId(assetIdentifier);
                        ai.setTeamsId(assetIdentifier);

                        asset = AssetServices.getInstance().retrieveAsset(ai, assetDataLoadRequest, securitySession);

                        final AssetData assetData = new AssetData();
                        assetData.setPath(adaptiveUrl + asset.getOriginalAssetId().getId());
                        assetData.setAssetName(asset.getName());
                        assetData.setAssetId(asset.getAssetId().getId());
                        assetData.setAssetOwner(asset.getImportUserName());
                        assetData.setCreateDate(new SimpleDateFormat("dd/mm/yyyy").format(asset.getDateCreated()));

                        assetDatas.add(assetData);
                    } catch (BaseTeamsException e) {
                        e.printStackTrace();
                        log.error("CUSTOM_SAMPLE_SHEET -> " + e.getMessage());
                    }
                }
            }
        } else
            log.info("CUSTOM_CONTACT_SHEET -> no asset found to log an email");
        return assetDatas;
    }

    /**
     * @throws BaseTeamsException
     */
    public File preparePdf(String assetIds, String adaptiveMediaURL) throws BaseTeamsException {
        final Document document = new Document();
        final String fileName = CONTACT_SHEET_DOC_NAME + new SimpleDateFormat(CONTACT_SHEET_PDF_TIMESTAMP_FORMAT).format(new Date());
        final String HOST_URL = getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB_KEY, HOST_NAME_PROP, OTUtils.securitySession);
        final List<Map<String, Object>> datas = new ArrayList<>();
        final List<AssetData> assetDatas = prepareAssetDataList(assetIds, adaptiveMediaURL, OTUtils.securitySession);

        if (null == System.getProperty(TEAMS_HOME)) {
            Properties props = System.getProperties();
            props.setProperty(TEAMS_HOME, System.getenv(TEAMS_HOME));
            log.info("SYSTEM PROP: " + System.getProperty(TEAMS_HOME));
        }

        assetDatas.stream().forEach(assetData -> {
            Image img = null;
            try {
                final Path path = OTUtils.getOriginalAsset(assetData.getAssetId(), OTUtils.securitySession).toPath();

                if (null != path) {
                    img = getImage(path);
                    img.setAlignment(Element.ALIGN_LEFT);
                }
            } catch (Exception e) {
                log.error("CUSTOM_CONTACT_SHEET -> ", e);
            }
            Map<String, Object> data = new HashedMap<>();
            Map<String, String> dataMap = new HashedMap<>();
            dataMap.put("Asset ID: ", assetData.getAssetId());
            dataMap.put("Asset Name: ", assetData.getAssetName());
            dataMap.put("Owner: ", assetData.getAssetOwner());
            dataMap.put("Created Date: ", assetData.getCreateDate());
            dataMap.put("AMD URL: ", adaptiveMediaURL + assetData.getAssetId());
            data.put(FILE, img);
            data.put("data", dataMap);
            datas.add(data);
        });

        try {
            document.open();
            generatePdf(document, datas, fileName + ".pdf");
        } catch (DocumentException | IOException | URISyntaxException e) {
            log.error("CUSTOM_CONTACT_SHEET_ERROR -> ", e);
        } finally {
            document.close();
        }
        return new File(fileName + ".pdf");
    }

    /**
     * @param jobData
     * @param jobContext
     * @param securitySession
     * @return
     */
    @Override
    public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession securitySession) throws BaseTeamsException {
        final String adaptiveMediaURL = OTUtils.HOST_URL + ADAPTIVEMEDIA_RENDITION_ID_API_URL;
        File file = null;
        if (jobContext.getData() != null) {
            try {
                log.info("CUSTOM_CONTACT_SHEET -> job context data: " + new Gson().toJson(jobContext));
                final String emailRecipients = (String) jobContext.getData().get(EMAIL_RECIPIENTS);

                Object dataFromContext = jobContext.getData().get(ASSETS_CONTEXT);
                List<AssetIdentifier> assetIdsList = ((ListContextWrapper<AssetIdentifier>) dataFromContext).getListData();
                StringBuilder assetIdsString = new StringBuilder();
                Iterator<AssetIdentifier> assetsIterator = assetIdsList.iterator();

                while (assetsIterator.hasNext()) {
                    assetIdsString.append(assetsIterator.next().asString());
                    if (assetsIterator.hasNext()) {
                        assetIdsString.append(",");
                    }
                }
                String assetIds = assetIdsString.toString();
                final Map<String, Object> data = new HashMap<>();
                if (assetIds.isEmpty()) {
                    log.error("CUSTOM_CONTACT_SHEET -> exception: no asset available");
                } else {
                    file = preparePdf(assetIds, adaptiveMediaURL);
                }
                if (null != file) {
                    data.put(FILE, file);
                    jobContext.setData(data);
                } else {
                    data.put(FILE, "");
                    jobContext.setData(data);
                }
                data.put(EMAIL_RECIPIENTS, emailRecipients);
                return StepStatus.COMPLETED;
            } catch (Exception e) {
                log.error("CUSTOM_CONTACT_SHEET -> exception: no access to process, " + e.getMessage());
                return StepStatus.FAILED;
            }
        } else
            return StepStatus.FAILED;
    }
}