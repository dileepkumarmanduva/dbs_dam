package com.opentext.custom.step;

import com.artesia.asset.Asset;
import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.services.AssetDataLoadRequest;
import com.artesia.asset.services.AssetServices;
import com.artesia.asset.services.RetrieveAssetsCriteria;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.security.SecuritySession;
import com.opentext.custom.util.OTUtils;
import com.opentext.job.JobContext;
import com.opentext.job.context.ListContextWrapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.opentext.custom.Constants.LOG_CUSTOM_ASSET_METADATA_UPDATE;

public class ReadExcel {
    private ReadExcel() {
    }

    private static final Log log = LogFactory.getLog(ReadExcel.class);

    /**
     * @param file
     * @return
     * @throws IOException
     */
    public static Map<String, Map<TeamsIdentifier, String>> readXlsxFileToTeamsIdentifierMap(Workbook file){
        Map<String, Map<TeamsIdentifier, String>> teamsIdentifiersMaps = new HashMap<>();
        Iterator<Row> rowIterator = file.getSheetAt(0).iterator();
        int rowOne = 0;
        List<TeamsIdentifier> teamsIdentifiers = new ArrayList<>();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            int indx = 0;
            Map<TeamsIdentifier, String> teamsIdentifiersMap = new HashMap<>();
            String assetId = "";
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                String data = "";
                try {
                    data = null != cell ? cell.getStringCellValue() : "";
                    log.error(LOG_CUSTOM_ASSET_METADATA_UPDATE + " -> data: " + data);
                } catch (Exception e) {
                    log.error(LOG_CUSTOM_ASSET_METADATA_UPDATE + e.getMessage(), e);
                }
                if (indx == 0)
                    assetId = data;
                if (rowOne == 0 && !data.isEmpty()) {
                    teamsIdentifiers.add(new TeamsIdentifier(data));
                }

                teamsIdentifiersMap.put(teamsIdentifiers.get(indx++), data);
            }
            teamsIdentifiersMaps.put(assetId, teamsIdentifiersMap);
        }
        return teamsIdentifiersMaps;
    }
    
    public static Map<String, Map<TeamsIdentifier, String>> readXlsxFileToTeamsIdentifierMapNew(Workbook file, JobContext context) throws BaseTeamsException{
        Map<String, Map<TeamsIdentifier, String>> teamsIdentifiersMaps = new HashMap<String, Map<TeamsIdentifier,String>>();
        Iterator<Row> rowIterator = file.getSheetAt(0).iterator();
        int rowOne = 0;
        List<TeamsIdentifier> teamsIdentifiers = new ArrayList<>();
        Map<String, AssetIdentifier> nameToIDMap = getNameToAssetIDMap(context, OTUtils.securitySession);
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            int indx = 0;
            Map<TeamsIdentifier, String> teamsIdentifiersMap = new HashMap();
    		
    		String assetId = "";
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                
                String data = null != cell ? cell.getStringCellValue() : "";
                if (indx == 0) {
                	if (nameToIDMap.get(data) != null) {
                		assetId = nameToIDMap.get(data).asString();
            		}else {
            			assetId = "";
            		}
                }
                if (rowOne == 0 && !data.isEmpty()) {
                    teamsIdentifiers.add(new TeamsIdentifier(data));
                }
                teamsIdentifiersMap.put(teamsIdentifiers.get(indx++), data);
            }
            	teamsIdentifiersMaps.put(assetId, teamsIdentifiersMap);
            	rowOne = 1;
        }
        return teamsIdentifiersMaps;
    }

	private static Map<String, AssetIdentifier> getNameToAssetIDMap(JobContext jobContext, SecuritySession securitySession) throws BaseTeamsException {

		log.info("getNameToAssetIDMap start>>>" );
		Object assetIdsObject = jobContext.getData().get("htmlHightailAssets");
		Map<String, AssetIdentifier> returnValue = new HashMap<>();
        List<AssetIdentifier> assetIdsList = ((ListContextWrapper<AssetIdentifier>)assetIdsObject).getListData();
        
		RetrieveAssetsCriteria retrieveAssetsCriteria = new RetrieveAssetsCriteria(assetIdsList.toArray(new AssetIdentifier[assetIdsList.size()]));
		
        Asset[] retrievedAssets = AssetServices.getInstance().retrieveAssets(retrieveAssetsCriteria, new AssetDataLoadRequest(), securitySession);
        log.info("getNameToAssetIDMap end>>>" );
        
        if (retrievedAssets !=null || retrievedAssets.length >0) {
        	for (Asset anAsset : retrievedAssets) {
        		returnValue.put(anAsset.getName(), anAsset.getAssetId());	
        	}
        }
		return returnValue;
	}
}
