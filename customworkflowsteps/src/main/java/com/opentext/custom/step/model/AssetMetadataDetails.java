package com.opentext.custom.step.model;

public class AssetMetadataDetails {
    private String assetId;
    private String format;
    private String components;
    private String demographics;
    private String purpose;
    private String topic;
    private String style;
    private String content;
    private String location;

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    public String getDemographics() {
        return demographics;
    }

    public void setDemographics(String demographics) {
        this.demographics = demographics;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "AssetMetadataDetails{" +
                "assetId='" + assetId + '\'' +
                ", format='" + format + '\'' +
                ", components='" + components + '\'' +
                ", demographics='" + demographics + '\'' +
                ", purpose='" + purpose + '\'' +
                ", topic='" + topic + '\'' +
                ", style='" + style + '\'' +
                ", content='" + content + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
