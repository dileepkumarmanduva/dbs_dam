package com.opentext.custom.util;

import com.artesia.asset.Asset;
import com.artesia.asset.AssetContentInfo;
import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.content.services.AssetContentLoadRequest;
import com.artesia.asset.content.services.AssetContentServices;
import com.artesia.asset.metadata.services.AssetMetadataServices;
import com.artesia.asset.services.AssetDataLoadRequest;
import com.artesia.asset.services.AssetServices;
import com.artesia.asset.services.RetrieveAssetsCriteria;
import com.artesia.common.MetadataFieldConstants;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.common.prefs.PrefData;
import com.artesia.common.prefs.PrefDataId;
import com.artesia.content.ContentData;
import com.artesia.content.ContentDataRequest;
import com.artesia.content.ContentInfo;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataCollection;
import com.artesia.metadata.MetadataField;
import com.artesia.metadata.MetadataValue;
import com.artesia.search.Search;
import com.artesia.search.SearchConstants;
import com.artesia.search.SearchScalarCondition;
import com.artesia.security.SecuritySession;
import com.artesia.security.session.services.LocalAuthenticationServices;
import com.artesia.system.services.SystemServices;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opentext.custom.job.model.ExpiringAssetInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.opentext.custom.Constants.*;


/**
 * @author umeshkumars
 */
public class OTUtils {
    private static final Log log = LogFactory.getLog(OTUtils.class);
    public static final String MASTER_CONTENT = "masterContent";
    public static SecuritySession securitySession;
    public static String HOST_URL;
    public static String username = "";
    public static String password = "";

    /**
     * static block for static initialization
     */
    static {
        if (null == System.getProperty(TEAMS_HOME)) {
            Properties props = System.getProperties();
            props.setProperty(TEAMS_HOME, "C:\\Users\\Home\\Documents\\OpenText\\OTMM5");
        }
        try {
            if (securitySession == null) {
                securitySession = LocalAuthenticationServices.getInstance().createSession(LOCAL_USER_NAME);
            }
            HOST_URL = getSystemInformation(CUSTOM_ASSET_EXPIRY, JOB, HOST_NAME_PROP, securitySession);
        } catch (BaseTeamsException e) {
            e.printStackTrace();
        }
    }

    private static HttpHeaders requestHeaders = new HttpHeaders();
    private static RestTemplate restTemplate = new RestTemplate();

    /**
     * private constructor
     */
    private OTUtils() {
    }

    /**
     * search expiring assets for the given duration
     *
     * @param fieldIds
     * @param duration
     * @param securitySession
     * @return
     * @throws BaseTeamsException
     */
    public static List<ExpiringAssetInfo> searchExpiringAssetsForDuration(TeamsIdentifier[] fieldIds, String duration, SecuritySession securitySession) throws BaseTeamsException {
        TeamsIdentifier durationConstant = getDurationCons(duration);
        AssetDataLoadRequest dataRequest = new AssetDataLoadRequest();
        dataRequest.setLoadThumbnailAndKeywords(true, true);
        dataRequest.setLoadDestinationLinks(true);
        dataRequest.setLoadMetadata(true);
        dataRequest.setMetadataFieldsToRetrieve(fieldIds);

        // Build a search to return assets imported today
        SearchScalarCondition searchScalarCondition = new SearchScalarCondition(
                MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE,
                durationConstant, null);

        Search search = new Search(searchScalarCondition);

        // Build the criteria object
        RetrieveAssetsCriteria criteria = new RetrieveAssetsCriteria();
        criteria.setSearchInfo(search, 5);
        List<Asset> searchAssets = new ArrayList<>();
        searchAssets.addAll(List.of(AssetServices.getInstance().retrieveAssets(criteria, dataRequest, securitySession)));
        return getExpiringAssetList(fieldIds, searchAssets, securitySession);
    }

    /**
     * get duration constant for the given duration property
     *
     * @param duration
     * @return
     */
    private static TeamsIdentifier getDurationCons(String duration) {
        TeamsIdentifier durationConstant;
        if (duration.equalsIgnoreCase(DAYS_7))
            durationConstant = SearchConstants.OPERATOR_ID__DATE_IS_IN_LAST_7_DAYS;
        else if (duration.equalsIgnoreCase(DAYS_30))
            durationConstant = SearchConstants.OPERATOR_ID__DATE_IS_IN_LAST_30_DAYS;
        else if (duration.equalsIgnoreCase(YESTERDAY))
            durationConstant = SearchConstants.OPERATOR_ID__DATE_IS_YESTERDAY;
        else if (duration.equalsIgnoreCase(TODAY))
            durationConstant = SearchConstants.OPERATOR_ID__DATE_IS_TODAY;
        else
            durationConstant = SearchConstants.OPERATOR_ID__DATE_IS_IN_LAST_7_DAYS;
        return durationConstant;
    }

    /**
     * get duration constant for the given duration property
     *
     * @param duration
     * @return
     */
    private static Integer getDurationDays(String duration) {
        Integer durationConstant;
        if (duration.equalsIgnoreCase(YESTERDAY))
            durationConstant = -1;
        else if (duration.equalsIgnoreCase(TODAY))
            durationConstant = 0;
        else {
            try {
                durationConstant = (NumberFormat.getInstance().parse(duration)).intValue();
            } catch (ParseException e) {
                durationConstant = 7;
                e.printStackTrace();
            }
        }
        return durationConstant;
    }

    /**
     * get expiring asset list
     *
     * @param fieldIds
     * @param searchAssets
     * @param securitySession
     * @return
     * @throws BaseTeamsException
     */
    private static List<ExpiringAssetInfo> getExpiringAssetList(final TeamsIdentifier[] fieldIds, List<Asset> searchAssets, SecuritySession securitySession) throws BaseTeamsException {
        List<ExpiringAssetInfo> expiringAssetsInfo = new ArrayList<>();
        for (Asset assetData : searchAssets) {
            // Load a single asset
            if (assetData != null) {
                String flag = assetData.getMetadata().getValueForField(new TeamsIdentifier("COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.FLAG")).getStringValue();
                MetadataValue countFieldValue = assetData.getMetadata().getValueForField(new TeamsIdentifier("COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.COUNT"));
                Integer frequency = null != countFieldValue.getValue() ? countFieldValue.getIntValue() : 0;
                MetadataValue dateFieldValue = assetData.getMetadata().getValueForField(new TeamsIdentifier("COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.DATE"));
                MetadataValue expiryDateFieldValue = assetData.getMetadata().getValueForField(MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE);
                Date expiryDate = (Date) (null != expiryDateFieldValue ? expiryDateFieldValue.getValue() : new Date());
                Date notifiedDate = (Date) (null != dateFieldValue ? dateFieldValue.getValue() : new Date());
                String expiryDateString = assetData.getMetadata().getValueForField(MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE).getStringValue();
                LocalDate nextNotifiedDate = null;
                LocalDate currentNotifiedDate = null;
                Integer interval = 0;
                if (null != notifiedDate) {
                    currentNotifiedDate = notifiedDate.toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDate();
                    try {
                        interval = Integer.parseInt(getSystemInformation("CustomAssetExpiry", "Job", "interval", securitySession));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    nextNotifiedDate = currentNotifiedDate.plusDays(interval);
                } else {
                    nextNotifiedDate = LocalDate.now();
                }
                LocalDate currentDate = new Date().toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
                LocalDate expiryLocalDate = expiryDate.toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();

                boolean isAfter = false;
                if (!currentDate.isBefore(nextNotifiedDate))
                    isAfter = true;
                log.info("CUSTOM_EXPIRY_ASSETS -> Dates: currentNotifiedDate - " + currentNotifiedDate + ", nextNotifiedDate - " + nextNotifiedDate + ", currentDate - " + currentDate);
                if (((flag == null || (null != flag && (!flag.equalsIgnoreCase("true") || flag.isEmpty()))) && isAfter)
                        || expiryLocalDate.plusDays(1).isEqual(currentDate)) {
                    String assetName = assetData.getName();
                    String assetOwner = assetData.getImportUserName();

                    ExpiringAssetInfo expiringAssetInfo = new ExpiringAssetInfo();
                    expiringAssetInfo.setAssetName(assetName);
                    expiringAssetInfo.setAssetOwner(assetOwner);
                    expiringAssetInfo.setFlag(flag);
                    expiringAssetInfo.setExpiryDate(expiryDateString);
                    expiringAssetInfo.setAssetId(assetData.getAssetId().getId());
                    expiringAssetInfo.setHostName(getSystemInformation("CustomAssetExpiry", "Job", "hostName", securitySession));
                    expiringAssetInfo.setNextNotifyDate(currentDate.plusDays(interval).toString());
                    expiringAssetInfo.setCurrentAttempt(frequency.toString());
                    Integer systemFrequency = null != getSystemInformation("CustomAssetExpiry", "Job", "frequency", securitySession) ? Integer.parseInt(getSystemInformation("CustomAssetExpiry", "Job", "frequency", securitySession)) : 0;
                    expiringAssetInfo.setTotalAttempt(systemFrequency.toString());
                    expiringAssetsInfo.add(expiringAssetInfo);

                    //Assume variables assetIds and session have already been set
                    if (!expiryLocalDate.plusDays(1).isEqual(currentDate))
                        updateAssetMetadata(assetData, frequency, securitySession);
                }

            }
        }

        log.info("CUSTOM_ASSEST_EXPIRY -> search asset for duration: " + expiringAssetsInfo);
        return expiringAssetsInfo;
    }

    /**
     * update asset metadata
     *
     * @param asset
     * @param frequency
     * @param securitySession
     */
    private static void updateAssetMetadata(Asset asset, Integer frequency, SecuritySession securitySession) {
        TeamsIdentifier flagId = new TeamsIdentifier("COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.FLAG");
        TeamsIdentifier countId = new TeamsIdentifier("COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.COUNT");
        TeamsIdentifier dateId = new TeamsIdentifier("COM.DBS.METADATA.CUSTOM.EXPIRY.NOTIFICATION.DATE");

        //save the new value
        try {
            AssetServices.getInstance().lockAsset(asset.getAssetId(), securitySession);

            //set flag-field value
            MetadataField flagField = new MetadataField(flagId);

            //get count-field value
            MetadataCollection[] countCol = AssetMetadataServices.getInstance().retrieveMetadataForAssets(asset.getAssetId().asAssetIdArray(),
                    countId.asTeamsIdArray(), null, securitySession);

            MetadataField countField = (MetadataField) countCol[0].findElementById(countId);
            countField.setValue(++frequency);

            //get date-field value
            MetadataCollection[] dateCol = AssetMetadataServices.getInstance().retrieveMetadataForAssets(asset.getAssetId().asAssetIdArray(),
                    dateId.asTeamsIdArray(), null, securitySession);

            MetadataField dateField = (MetadataField) dateCol[0].findElementById(dateId);
            Format f = new SimpleDateFormat("MM/dd/yyyy");
            String currentDate = f.format(new Date());
            log.info("CUSTOM_ASSET_EXPIRY -> Current Date : " + currentDate);
            dateField.setValue(currentDate);

            List<MetadataField> metadataFields = new ArrayList<>();
            metadataFields.add(dateField);
            metadataFields.add(countField);
            Integer systemFrequency = null != getSystemInformation("CustomAssetExpiry", "Job", "frequency", securitySession) ? Integer.parseInt(getSystemInformation("CustomAssetExpiry", "Job", "frequency", securitySession)) : 0;
            if (frequency >= systemFrequency) {
                MetadataCollection[] flagCol = AssetMetadataServices.getInstance().retrieveMetadataForAssets(asset.getAssetId().asAssetIdArray(),
                        flagId.asTeamsIdArray(), null, securitySession);
                flagField = (MetadataField) flagCol[0].findElementById(flagId);
                flagField.setValue("TRUE");
                metadataFields.add(flagField);
            }

            MetadataField[] metadataFieldsToSave = new MetadataField[metadataFields.size()];
            for (int i = 0; i < metadataFields.size(); i++)
                metadataFieldsToSave[i] = metadataFields.get(i);

            AssetMetadataServices.getInstance().saveMetadataForAssets(asset.getAssetId().asAssetIdArray(), metadataFieldsToSave, securitySession);
            AssetServices.getInstance().unlockAsset(asset.getAssetId(), securitySession);
            log.info("CUSTOM_ASSEST_EXPIRY -> updated asset metadata: \ncount field: " + countField + "\ndate-field: " + dateField + "\nflag-field: " + flagField);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("CUSTOM_ASSET_EXPIRY -> unable to unlock");
        }
    }

    /**
     * get expiring assets list for the given duration
     *
     * @param fieldIds
     * @param duration
     * @param type
     * @param securitySession
     * @return
     * @throws BaseTeamsException
     */
    public static List<ExpiringAssetInfo> searchExpiringAssetsForDuration(TeamsIdentifier[] fieldIds, String duration, String type, SecuritySession securitySession) throws BaseTeamsException {
        TeamsIdentifier between = SearchConstants.OPERATOR_ID__DATE_IS_BETWEEN;
        TeamsIdentifier yesterday = SearchConstants.OPERATOR_ID__DATE_IS_YESTERDAY;

        Integer durationConstant = getDurationDays(duration);
        AssetDataLoadRequest dataRequest = new AssetDataLoadRequest();
        dataRequest.setLoadThumbnailAndKeywords(true, true);
        dataRequest.setLoadDestinationLinks(true);
        dataRequest.setLoadMetadata(true);
        dataRequest.setMetadataFieldsToRetrieve(fieldIds);

        LocalDate today = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/uuuu");
        String todayDate = today.format(formatter);

        String constantDate = today.plusDays(durationConstant).format(formatter);

        String datesString = "";
        if (durationConstant < 0)
            datesString = constantDate + " and " + todayDate;
        else
            datesString = todayDate + " and " + constantDate;
        log.info(datesString);
        // Build a search to return assets imported today
        SearchScalarCondition searchAssetsInBetween = new SearchScalarCondition(
                MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE,
                between, datesString);

        SearchScalarCondition searchAssetsForYesterday = new SearchScalarCondition(
                MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE,
                yesterday, null);


        Search search = new Search();
        search.addCondition(searchAssetsInBetween);

        // Build the criteria object
        List<Asset> searchAssets = new ArrayList<>();
        RetrieveAssetsCriteria criteria = new RetrieveAssetsCriteria();
        criteria.setSearch(search);
        searchAssets.addAll(List.of(AssetServices.getInstance().retrieveAssets(criteria, dataRequest, securitySession)));

        search = new Search();
        search.addCondition(searchAssetsForYesterday);
        criteria = new RetrieveAssetsCriteria();
        criteria.setSearch(search);
        searchAssets.addAll(List.of(AssetServices.getInstance().retrieveAssets(criteria, dataRequest, securitySession)));

        List<ExpiringAssetInfo> expiringAssetsData = getExpiringAssetList(fieldIds, searchAssets, securitySession);
        return expiringAssetsData;
    }

    /**
     * @param comp
     * @param key
     * @param name
     * @param session
     * @return
     * @throws BaseTeamsException
     */
    public static String getSystemInformation(final String comp, final String key, final String name, final SecuritySession session) throws BaseTeamsException {
        SystemServices systemServices = SystemServices.getInstance();
        PrefDataId prefDataId = new PrefDataId(comp, key, name);
        PrefData systemSettings = systemServices.retrieveSystemSettingsByPrefDataId(prefDataId, session);
        String value = null;
        if (systemSettings != null) {
            value = systemSettings.getValue();
        }
        return value;
    }

    /**
     * @param unm
     * @param pwd
     * @param securitySession
     * @return
     */
    public static HttpEntity<MultiValueMap<String, String>> initializeSession(String unm, String pwd, SecuritySession securitySession) {
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add(USERNAME_PROP, unm);
        requestBody.add(PASSWORD_PROP, pwd);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(requestBody, headers);
        try {
            ResponseEntity<String> entity = restTemplate.exchange(HOST_URL + SESSIONS, HttpMethod.POST, request, String.class);
            JsonObject jsonObject = new JsonParser().parse(entity.getBody()).getAsJsonObject();
            String id = "";
            if (jsonObject.getAsJsonObject(SESSION_RESOURCE) != null && jsonObject.getAsJsonObject(SESSION_RESOURCE).getAsJsonObject(SESSION_KEY) != null && jsonObject.getAsJsonObject(SESSION_RESOURCE).getAsJsonObject(SESSION_KEY).get(MESSAGE_DIGEST) != null)
                id = jsonObject.getAsJsonObject(SESSION_RESOURCE).getAsJsonObject(SESSION_KEY).get(MESSAGE_DIGEST).getAsString();
            requestHeaders.add(OTMMAUTHTOKEN_KEY, id);
            //Remove session entry form json object
            jsonObject.remove(SESSION_RESOURCE);
            request = new HttpEntity<>(null, requestHeaders);
        } catch (Exception e) {
            log.error(LOG_CUSTOM_ASSET_METADATA_UPDATE, e);
        }
        return request;
    }

    /**
     * @param session
     * @param assetId
     * @return
     * @throws BaseTeamsException
     */
    public static File getOriginalAsset(String assetId, SecuritySession session) throws BaseTeamsException {
        ContentInfo masterContent = null;
        AssetContentInfo contentInfo = null;
        ContentDataRequest dataRequest = null;
        AtomicReference<File> fileContent = new AtomicReference<>();
        File assetFile = null;
        File destLoc = new File(System.getenv(TEAMS_HOME) + File.separator + "temp");
        try {
            AssetContentLoadRequest loadRequest = new AssetContentLoadRequest();
            loadRequest.setLoadMasterContent(true);
            loadRequest.setRequestedFormat(ContentData.ContentDataSource.FILE);
            loadRequest.setDestinationDirectory(destLoc);
            dataRequest = new ContentDataRequest();
            contentInfo = AssetContentServices.getInstance().retrieveAssetContent(new AssetIdentifier(assetId), loadRequest, dataRequest, session);
            masterContent = contentInfo.getMasterContent();
        } catch (Exception e) {
        }
        if (null == masterContent) {
            Arrays.stream(destLoc.list()).forEach(file -> {
                log.info("!!!! " + file);
            });
            Asset asset = AssetServices.getInstance().retrieveAsset(new AssetIdentifier(assetId), new AssetDataLoadRequest(), session);
            Arrays.stream(new File(destLoc.getAbsolutePath() + File.separator + MASTER_CONTENT).listFiles()).
                    forEach(file -> {
                        if (file.getName().equalsIgnoreCase(asset.getName())) {
                            fileContent.set(file);
                        }
                        log.info(file.getAbsolutePath());
                        log.info(file.getName());
                    });
            assetFile = fileContent.get();
        } else {
            assetFile = new File(destLoc.getAbsolutePath() + File.separator + MASTER_CONTENT + File.separator + masterContent.getName());
        }
        return assetFile;
    }

    public static Properties getProp() {
        log.info("PATH: " + System.getProperty("TEAMS_HOME") + File.separator + "plugins" + File.separator + "conf" + File.separator + "customExpiryInput.properties");
        FileReader reader = null;
        try {
            reader = new FileReader(System.getProperty("TEAMS_HOME") + File.separator + "plugins" + File.separator + "conf" + File.separator + "customExpiryInput.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Properties p = new Properties();
        try {
            p.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p;
    }
}

