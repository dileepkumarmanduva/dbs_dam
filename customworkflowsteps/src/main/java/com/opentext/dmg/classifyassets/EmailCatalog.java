package com.opentext.dmg.classifyassets;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.security.SecuritySession;
import com.opentext.activity.ActionProperty;
import com.opentext.activity.PropertyDataType;
import com.opentext.activity.PropertyType;
import com.opentext.job.JobContext;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;

public class EmailCatalog extends BaseStep {

	private static final Log log = LogFactory.getLog(EmailCatalog.class);

	@Override
	public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession session)
			throws BaseTeamsException {

		log.info("Executing EmailCatalog step.");
		return StepStatus.COMPLETED;
	}

	@Override
	protected void initializeProperties(Set<ActionProperty> actionProperties) {
		var assetIdsProperty = new ActionProperty("assetIds", "The ids of the assets to be published",
				"Assets", true, PropertyDataType.STRING, "{assetIds}", PropertyType.INPUT);
		actionProperties.add(assetIdsProperty);
	}
}
