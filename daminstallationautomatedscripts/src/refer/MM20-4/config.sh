### System settings
## Integration settings
# GLOB_LMNT - directory for prerequisites
# GLOB_DSMNT - directory for OTDS version
# GLOB_OMNT - directory for OTMM version
# GLOB_CONFDIR - directory for other configuration files
TMPDIR="/tmp"
GLOB_LMNT="/lf"
GLOB_DSMNT="/otds"
GLOB_OMNT="/mm"

## Shared service DNS name, certificate name, reverse proxy name
GLOB_SVC_HOSTNAME=""

GLOB_COCKPIT_ENABLE="install"
GLOB_MOTD_TEMPL="motd_templ"

## Oracle settings
# OTMM database settings
#
# GLOB_HOST_ORACLE_STORAGE (manual|<var>): where the tablespace dbf files go
#   manual: DBA predefines tablespaces
#    <var>: Root directory for dbf and index files, e.g. /u01/{index,data}
GLOB_HOST_ORACLE_SID="orcl"
GLOB_HOST_ORACLE="oracle12c"
GLOB_HOST_ORACLE_PORT="1521"
GLOB_HOST_ORACLE_SYS_USER="system"
GLOB_HOST_ORACLE_SYS_USER_PW="password"
GLOB_HOST_ORACLE_STORAGE="manual"

## SQL server settings
# the SQL server settings assume a shared database for all components and
# therefore are site wide settings in this iteration of the installer
#Note: Current installer does not fully configure the Data and Log dirs as SQL 
#      server is not popular for Unix installation, see 'mediamanagement_config.txt.tmpl'
GLOB_HOST_SQLSERVER="*undef*"
GLOB_HOST_SQLSERVER_USER="*undef*"
GLOB_HOST_SQLSERVER_USER_PW="*undef*"
GLOB_HOST_SQLSERVER_PORT="*undef*"

##Postgresql server settings
#Note: Current installer expects to install Postgres locally. A shared Postgres
#      installation is not currently supported. The configurable DbType is 
#      'local_postgres' for that very reason. Shared Postgres installation should
#       use 'postgres' in the future.
# Package from https://yum.postgresql.org
GLOB_PGSQL_ENABLE='ignore'
GLOB_PGADMIN_ENABLE='ignore'
GLOB_PGADMIN_APACHE_TEMPL="apache_pgadmin_conf_templ"
GLOB_HOST_PGSQL_SYS_USER="postgres"
GLOB_HOST_PGSQL_SYS_USER_PW="postgres1"
GLOB_HOST_PGSQL_PORT="5432"
GLOB_HOST_PGSQL="localhost"; #should be the postgres host
GLOB_HOST_PGSQL_DATADIR="$GLOB_INST/postgres"
GLOB_HOST_PGSQL_VERSION_MAJOR="12"
GLOB_HOST_PGSQL_VERSION_MINOR=""
GLOB_HOST_PGSQL_VERSION_SUBVERSION="2"
GLOB_HOST_PGSQL_REPO_RPM="pgdg-redhat-repo-latest.noarch.rpm"

## Site settings
# GLOB_OTMM_DB_TYPE (oracle,local_postgres,sqlserver): database choice
# GLOB_OTMM_DB_NAME (<var>): database name, ignored for Oracle
# GLOB_OTMM_ENABLE (jboss|tomee|tomee_preinstalled|ignore): OTMM AS server choice
#    ignore: do not install OTMM on this node
# GLOB_OTMM_PROTO (http|https): OTMM protocol
#     https: uses https on the AS, tested on wildfly only
# GLOB_OTMM_INSTALL_SCHEMA (install|ignore): configure database
#   install: this configures the OTMM DB schema and OTDS schema
#            since the OTMM installer works that way. 
#            Indicates the master for both OTMM and OTMM upgrades
#    ignore: for all resillience OTMM installations
# GLOB_OTMM_DESTROY_SCHEMA (ignore|destroy): destroy existing schema
#   destroy: removes a previous OTMM schema in the database and OTDS.
#            Useful for test installations. Use with great care elsewhere! 
GLOB_OTMM_DB_TYPE="*undef*"
GLOB_OTMM_DB_NAME="otmmdb"
GLOB_OTMM_DB_USER="towner"
GLOB_OTMM_DB_USER_PW="towner"
GLOB_OTMM_USER='tsuper' #fixed UI/tsuper access
GLOB_OTMM_USER_PW='tsuper' #for UI/tsuper access
GLOB_OTMM_ENABLE="ignore"
GLOB_OTMM_INSTALL_SCHEMA="ignore"
GLOB_OTMM_PROTO="http"
GLOB_OTMM_PORT="11090"
GLOB_OTMM_SEC_PORT="11443"
GLOB_OTMM_EXT_PORT="11090"
GLOB_OTMM_DESTROY_SCHEMA="ignore"
GLOB_OTMM_VERSION_UPGRADE_PATCH_NO="5"
GLOB_OTMM_VERSION_UPGRADE=""
GLOB_OTMM_DB_HOST_PORT="*undef*"
GLOB_OTMM_ORACLE_STORAGE_DESTROY="0"
GLOB_OTMM_ORACLE_USER_DESTROY="0"
GLOB_OTMM_ORACLE_DESTROY_SQLTEMPL="purgeoracle.sql.tmpl"
GLOB_OTMM_SERVICE_TMPL="otmm.service.tmpl"


# GLOB_USER, GLOB_SO_USER, GLOB_FF_USER, GLOB_GROUP - OTMM user, Solr user, Ffmpeg user, default group
GLOB_USER="otmm"
GLOB_SO_USER="otmm"
GLOB_FF_USER="otmm"
GLOB_GROUP="otdam"

# GLOB_INST - default installation root directory
GLOB_INST="${GLOB_INST:-/vagrant}"

## SMTP settings
# GLOB_SMTP_TYPE (ignore|maildrop): Configure OTMM SMTP authentication
GLOB_SMTP_TYPE="ignore"
GLOB_SMTP_SERVER="smtp.mailtrap.io"
GLOB_SMTP_USER="6cb9bebfeba194"
GLOB_SMTP_USER_PW="dba253d7d90474"

# User settings
# GLOB_LOGIN_CONF - .profile include file template
GLOB_LOGIN_CONF="bashlogin.templ"

##Java
# GLOB_JDKARC - JDK rpm, relative to GLOB_LMNT
# GLOB_JAVAHOME - resulting JAVA_HOME 
GLOB_JAVAHOME="/usr/lib/jvm/java-11"
GLOB_JDKARC="java-11-openjdk-devel"
GLOB_JDK_INSTALL="ignore"

##Tomcat (for OTDS)
# GLOB_TCARC (<var>): Tomcat archive relative to GLOB_LMNT
# GLOB_TCARC_TDIR (<var>): root directory in GLOB_TCARC
# GLOB_TCINS_LEAFDIR (<var>): tomcat target installation directory name
# GLOB_TCINS (<var>): full target installation directory, TOMCAT_ROOT
# GLOB_TCINS_SSL (enable|ignore): ignore or enable SSL port and certificate
GLOB_TOMCAT_VERSION="9.0.21"
GLOB_TCARC="apache-tomcat-9.0.21.tar.gz"
GLOB_TCARC_TDIR="apache-tomcat-9.0.21"
GLOB_TCINS_LEAFDIR="tomcat_otds"
GLOB_TCINS="$GLOB_INST/$GLOB_TCINS_LEAFDIR"
GLOB_TCINS_SSL='ignore'

##TomEE
# GLOB_EEARC - Tomee archive relative to GLOB_LMNT
# GLOB_EEINS - full Tomee installation directory, TOMEE_ROOT
GLOB_EEARC="*undef*"
GLOB_EEINS="$GLOB_INST/tomee_otmm"

#SUPORT
GLOB_SUP_HOME="$GLOB_INST/support"
GLOB_SUPP_DB_SID="orcl"
GLOB_SUPP_DB_USER="support"
GLOB_SUPP_DB_USER_PW="db_support_pw"
GLOB_SUPP_DB_NAME="db_support"
GLOB_SUPP_DB_HOST="*undef*"
GLOB_SUPP_DB_JDBC="*undef*"
GLOB_SUPP_DB_HOST_PORT="*undef*"
GLOB_SUPP_DB_SID="*undef*"
GLOB_SUPP_DB_TYPE="local_postgres"
GLOB_SUP_HOSTPORT="9091"
GLOB_SUP_CONFTMPL="supportmgr_config_txt.templ"
GLOB_SUP_SYSTEMD_TEMPL="support.service.tmpl"
GLOB_SUP_SERVICE_NAME="SupportManagerService"; #fixed by installer
GLOB_SUPP_ENABLE="ignore"
GLOB_SUPP_DB_INSTALL_SCHEMA="install"
GLOB_SUPP_DB_DESTROY_SCHEMA="destroy"
GLOB_DB_SUPP_USER="support"
GLOB_DB_SUPP_USER_PW="support"

##OTDS
# GLOB_OTDS_ENABLE (master|slave|ignore): type of OTDS installation
# GLOB_OTDSARC - tar file for OTDS
# GLOB_DSINS - full OTDS installation directory
# GLOB_OTDS_CONF, GLOB_OTDS_REPL_CONF - silent installer response file
# GLOB_OTDS_PASS, GLOB_OTDS_ADMINDOMAIN - OTDS Administrator
# GLOB_OTDSLOCALDIR - full OTDS install/uninstall spool directory
GLOB_OTDS_ENABLE='ignore'
GLOB_OTDS_VERSION_ID="1643"
#GLOB_OTDS_VERSION_ID="1662"
GLOB_OTDS_VERSION="20.3.1"
GLOB_OTDS_VERSIONID="2031"
GLOB_OTDS_VERSIONHFID="002"
GLOB_OTDS_HF_ARC="${GLOB_DSMNT}/otds-${GLOB_OTDS_VERSIONID}-hotfix${GLOB_OTDS_VERSIONHFID}.zip"
GLOB_OTDS_VERSION_HOTFIX="OTDS-${GLOB_OTDS_VERSIONID}-Hotfix${GLOB_OTDS_VERSIONHFID}"
GLOB_OTDSARC="OTDS-2031-LNX.tar"
GLOB_DSINS="$GLOB_INST/otds"
GLOB_OTDSLOCALDIR="/etc/opentext/unpacked/otds"
GLOB_OTDS_CONF="otdsresponse_tmpl"
GLOB_OTDS_REPL_CONF="otdsresponse_repl_tmpl"
GLOB_OTDS_PASS='*undef*'
GLOB_OTDS_ADMIN="otadmin"
GLOB_OTDS_DOMAIN="otds.admin"
GLOB_OTDS_ADMINDOMAIN="${GLOB_OTDS_ADMIN}@${GLOB_OTDS_DOMAIN}"
GLOB_HOST_OTDS='*undef*'
GLOB_HOST_OTDS_PORT_EXT='80'
GLOB_HOST_OTDS_PROTO=http
GLOB_HOST_OTDS_PORT=8080
GLOB_HOST_OTDS_AJP_PORT=8009
GLOB_HOST_OTDS_SHUTDOWN_PORT=8005
GLOB_OTDS_SERVICE_TMPL="otds.service.tmpl"

#DMTS
GLOB_DMTSINS="$GLOB_INST/dmts"
GLOB_DMTS_ROOT="$GLOB_INST/shared"
GLOB_DMTS_WORKING="/var/staging"
GLOB_DMTS_FEATURES="DMTS-Service,DMTS-Image,DMTS-Video"
GLOB_DMTS_CONFTEMPL="dmts_config.txt_templ"
GLOB_DMTS_SERVICES="All"
GLOB_DMTS_RMQ_INSTALL="yes"
GLOB_DMTS_RMQ_OTHERSERVER="no"
GLOB_DMTS_ERLANG_NODE="*undef*"
GLOB_DMTS_ERLANG_NODE_PORT="15672"
GLOB_DMTS_RMQ_HOST="localhost"
GLOB_DMTS_RMQ_HOST_PORT="5672"
#GLOB_DMTS_NODE_USERNAME="*undef*"
#GLOB_DMTS_NODE_USERNAME_PW="*undef*"
GLOB_DMTS_ENABLE_FFMPEG="ignore"
GLOB_DMTS_ENABLE_IMAGEMAGICK="ignore"
GLOB_DMTS_IM_ARCHDIR="linux/centos8"
GLOB_DMTS_IM_ARCHIVES="ufraw-*x86_64.rpm ImageMagick-7* ImageMagick-libs-7*"
GLOB_DMTS_IM_REPOS="$GLOB_LMNT/epel-release-latest-8* $GLOB_LMNT/remi-release-8* $GLOB_LMNT/nux-dextop-release*el7.nux.noarch.rpm"
GLOB_DMTS_MI_ARCHDIR="libmediainfo-*.x86_64.RHEL_7.rpm  libzen-*.x86_64.RHEL_7.rpm  mediainfo-*.x86_64.RHEL_7.rpm"
GLOB_DMTS_NODE_USERNAME="otadmin"
GLOB_DMTS_NODE_USERNAME_PW="otadmin"
GLOB_DMTS_LOCAL_NODE="*undef*"
GLOB_DMTS_NODE_PORT="18090"
GLOB_DMTS_SOFFICE_ARCDIR="linux/soffice-4.1.7"
GLOB_DMTS_SOFFICE_INSTDIR="/opt/openoffice4"
GLOB_DMTS_BLENDER_ARC="blender-2.82a-linux64.tar.xz"
GLOB_DMTS_BLENDER_INS="$GLOB_INST/blender"
GLOB_DMTS_BLENDER_GTIFARC="glTF-Blender-Exporter-master.zip"
GLOB_DMTS_NODE_USERNAME="otadmin"
GLOB_DMTS_NODE_USERNAME_PW="otadmin"

##Apache
# GLOB_APACHE_CERT* - certificate key, scr and crt files and CN definition
# GLOB_APACHEINS - apache configuration directory
# GLOB_APACHE_*_CONFIG_TEMPL - openssl.cnf and apache.conf file
# GLOB_APACHE_STREAMING (enable|ignore): configures Apache as streaming server for OTMM
GLOB_APACHE_CERT_CRTNAME="/etc/pki/tls/certs/$(hostname)_otmm.crt"
GLOB_APACHE_CERT_CSRNAME="/etc/pki/tls/certs/$(hostname)_otmm.csr"
GLOB_APACHE_CERT_KEYNAME="/etc/pki/tls/private/$(hostname)_otmm.key"
GLOB_APACHE_CERT_EMAIL="root@localhost"
GLOB_APACHE_CERT_COUNTRY="UK"
GLOB_APACHE_CERT_STATE="London"
GLOB_APACHE_CERT_LOC="London"
GLOB_APACHE_CERT_ORG="OpenText"
GLOB_APACHE_CERT_ORGUNIT="OpenText Test"
GLOB_APACHE_CERT_CAPEM="$GLOB_CONFDIR/ot.testca.pem"
GLOB_APACHE_CERT_CAKEY="$GLOB_CONFDIR/ot.testca.key"
GLOB_APACHE_CERT_CAPWD="ot.testca.password"
GLOB_APACHEINS="/etc/httpd"
GLOB_APACHE_PROXY_DOCROOT="/var/www/html/htdocs"
GLOB_APACHE_SSL_CONFIG_TEMPL="apache_ssl_cert.ini_tmpl"
GLOB_APACHE_HTTPS_CONFIG_TEMPL="apache_os_ssl.conf_tmpl"
GLOB_APACHE_PROXY_CONFIG_TEMPL="apache_os_proxy.conf_templ"
GLOB_APACHE_STREAMING='ignore'
GLOB_APACHE_HTTP_PORT="80"
GLOB_APACHE_HTTPS_PORT="443"

##OTMM
# GLOB_MMARCDIR - temporary mount point
# GLOB_MMARC - iso filename, relative to GLOB_MMARCDIR
# GLOB_OTMM_DB_TYPE - OTMM database kind
# various shared directories
GLOB_OTMM_VERSION="20.3"
GLOB_OTMM_UPDATE=""
GLOB_MMARCDIR="/tmp/mnt"
GLOB_MMARC="mediamgmt_20.4_linux.iso"
GLOB_MM_CONFTMPL="mediamanagement_config.txt_tmpl"
GLOB_MMINS="$GLOB_INST/MediaManagement"
GLOB_REPOSITORY_HOME="$GLOB_INST/shared/teams_respository_home"
#GLOB_IMPORT_WORKING="$GLOB_INST/shared/import_working_area"
#GLOB_DEFAULT_EXPORT="$GLOB_INST/shared/default_export_area"
#GLOB_VIDEO_STAGING="$GLOB_INST/shared/video_file_staging_area"
#GLOB_BULKEDIT_WORKING="$GLOB_INST/shared/bulkedit_workg_area"
GLOB_DMTS_WORKING_AREA="$GLOB_INST/shared/working"
GLOB_EXPORT_WORKING_AREA="$GLOB_INST/shared/working/export"
GLOB_IMPORT_WORKING_AREA="$GLOB_INST/shared/working/import"
GLOB_VIDEO_FILE_STAGING_AREA="$GLOB_INST/shared/transcodeinput"
GLOB_TARGET_PATH="$GLOB_INST/shared/transcodeoutput"
GLOB_OTMM_INDEX_SERVICE_TMPL="index.service.tmpl"
GLOB_OTMM_OTDS_BASEURL="${GLOB_HOST_OTDS_PROTO}://\${GLOB_HOST}:${GLOB_HOST_OTDS_PORT}"
GLOB_OTMM_SUPPORTMGR_URL="http://\${GLOB_HOST}:${GLOB_SUP_HOSTPORT}"
GLOB_OTMM_DMTS_BASEURL="http://\$GLOB_HOST:18090"
GLOB_OTMM_BASEURL="http://\$GLOB_HOST:11090"
GLOB_OTMM_RMQ_HOST="\$GLOB_HOST"
GLOB_OTMM_RMQ_HOST_PORT="5672"


##SOLR
# DBMAP, GLOB_SOLR_CONFTMPL, GLOB_SODB - database mapping for silent installer configuration
# GLOB_SOINS, SEARCH_HOME - full installation path for Solr
# GLOB_SODATA - full Solr index directory
declare -A DBMAP=([local_postgres]="POSTGRESQL" [postgres]="POSTGRESQL" [oracle]="ORACLE" [sqlserver]="SQLSERVER" [unknown]="HANA" [*undef*]="" [ignore]="")
GLOB_SOARC="*undef*"
GLOB_SOARC_LEAF="solr-7.3.1"
GLOB_SOINS="$GLOB_INST/solr-index"
GLOB_SODATA="$GLOB_INST/solr-index/solr-data"
SEARCH_HOME=$GLOB_SOINS
#GLOB_SODB=${DBMAP[$GLOB_OTMM_DB_TYPE]}
GLOB_SOLR_CONFTMPL="${GLOB_CONFDIR}/search_config.txt.tmpl"
GLOB_ZK_CONFTMPL="zookeeper_config.txt_templ"
GLOB_ZKINS="$GLOB_INST/zookeeper"
GLOB_ZKDATA="$GLOB_INST/zookeeper/data"
GLOB_ZKPORT="2181"
GLOB_ZKNODE_ID="1"
GLOB_SEARCH_ZK_NODES="server.1=$(hostname):2888:3888" ; #hostname:<leader comms port>:<leader elect port>;
GLOB_CLIENT_ZK_NODES="$(hostname):2181"; #hostname:port <comma> hostname:port
GLOB_ZK_CLUSTERED="YES"
GLOB_SOLR_REGISTER_WOTMM="YES"
GLOB_SOLR_SERVICE_TEMPL="solr.service.tmpl"
GLOB_CONF_SOLR="standalone"
GLOB_CONF_SOPORT="8983"

##FFMPEG
# GLOB_FFHOSTPORT (localhost|<var>): configure Ffmpeg in OTMM
#    localhost: start a subprocess without DB and service
#    <var>: listening $host:$port of the service
# GLOB_FFMPEG_DB_TYPE (oracle|local_postgres): Ffmpeg database server
GLOB_FF_FFMPEGARC="ffmpeg-4.0.3-64bit-static.tar.xz"
GLOB_TRANSCODE_ENABLE='ignore'
#GLOB_FF_CONFTMPL="ffmpeg_config.txt_tmpl"
GLOB_FF_INSDIR="$GLOB_INST/FFmpegBin"
GLOB_FFMPEG_DB_TYPE="ignore"
#use 'localhost' to work without server
GLOB_FF_DB=${DBMAP[$GLOB_FFMPEG_DB_TYPE]}

##Vantage
GLOB_HOST_TRANSC=localhost
GLOB_HOST_TRANSC_PORT=8676
GLOB_HOST_TRANSC_CATEGORY=XXXX

#
#AMD
#
GLOB_AMDINS="${GLOB_INST}/AMD"
GLOB_AMD_USER="amd"
GLOB_AMD_USER_PW="amd"
GLOB_AMD_REPO_HOME="$GLOB_INST/shared/amd_repository"
GLOB_AMD_PORT="8888"
GLOB_AMD_KAFKA_PORT="9092"
GLOB_AMD_ZK_PORT="2181"
GLOB_AMD_PROTO="http"
GLOB_DB_AMD_DBNAME="*undef*"
GLOB_AMD_ID="XXXX"
GLOB_DB_AMD_SID="*undef*"
GLOB_DB_AMD_PORT="*undef*"
GLOB_DB_AMD_HOST="*undef*"
GLOB_DB_AMD_USER="*undef*"
GLOB_DB_AMD_USER_PW="*undef*"
GLOB_DB_AMD_SYS_USER="*undef*"
GLOB_DB_AMD_SYS_USER_PW="*undef*"
GLOB_AMDCONFTEMPL="adaptivemedia_config.txt_templ"
GLOB_AMD_SERVICE_TMPL="adaptivemedia.service.tmpl"

##ImageMagick
GLOB_IKARCS="$GLOB_MMINS/install/ImageMagick/*rpm"
GLOB_ICARCS2="$GLOB_MMINS/install/xorg/*rpm"

##Rich Media Analysis
GLOB_RMA_SUBKEY="0032c120-08f0-4cff-927a-bb9e3241465d"
GLOB_RMA_AZURL="https://westcentralus.api.cognitive.microsoft.com/vision/v1.0"
GLOB_RMAINS="${GLOB_INST}/RMA"
GLOB_RMACONFTEMPL="richmediaanalysis_config.txt_templ"

#Digital Hub
GLOB_DH_ENABLE="ignore"
GLOB_DHINS="$GLOB_INST/DHub"
GLOB_WILDFLY_VERSION="11.0.0.Final"
GLOB_DHARC="wildfly-11.0.0.Final.tar.gz"
GLOB_DHINS_TDIR="wildfly-11.0.0.Final"
GLOB_DHINS_LEAFDIR="tomcat_dh"
GLOB_DHINS="$GLOB_INST/$GLOB_DHINS_LEAFDIR"


