#!/bin/bash

set -u
set -o pipefail


YumFlags="-q"
Verbose="no"
PauseAt="never"
while [ -n "${1:-}" -a -n "${2:-}" ]; do
        read "${1#*-}" <<< "$2"
        echo "*D* CLI: ${1#*-}='$2'"
        shift
        shift
done
if [ -n "${1:-}" ]; then
        echo "*F* remaining argument: $1"
        exit 1
fi


#GLOBALS
set -a

if [ \! "$Verbose" = "no" ]; then
	#BASH_XTRACEFD sets the output of set -x to 'GREEN' in vagrant up
	exec 20<> >( while IFS= read -r line; do printf '\033[0;32m[%s] %s\033[0m\n' "$(date +%T)" "$line"; done )
	export BASH_XTRACEFD="20"
	set -x
	YumFlags="-q"
fi


AddHosts="${AddHosts:-}"

if [ -f "/tmp/dnf.conf" ]; then
	echo "*D* DNF caching already configured"
else
	echo "*D* Enable DNF caching"
	cp /etc/dnf/dnf.conf /tmp/dnf.conf
	echo 'keepcache=1' >> /etc/dnf/dnf.conf
fi

# Include files, bootstrap directory is /vagrant or commandline -- nothing else
# Use config.sh as default config file
GLOB_DRYRUN="${DryRun:-no}"
GLOB_INST="${GlobalInstDir:-/opt/mm}"
GLOB_CONFDIR=${IncludeDir:-/vagrant}
GLOB_CONFFILE="${ConfigFileName:-config.sh}"
case "$GLOB_CONFFILE" in
	/*)
		. $GLOB_CONFFILE
	;;
	*)
		if [ -f "$GLOB_CONFDIR/$GLOB_CONFFILE" ]; then
			. $GLOB_CONFDIR/$GLOB_CONFFILE
		else
			. $GLOB_CONFFILE
		fi
	;;
esac

set -a
if [ \! "$Verbose" = "no" ]; then
	set -x
fi

GLOB_MOTD_ENABLE="${Motd:-ignore}"
if [ "GLOB_MOTD_ENABLE" = "ignore" ]; then
	GLOB_MOTD_TEMPL="*undef*"
fi

GLOB_LMNT="${FilesDir:-$GLOB_LMNT}"
GLOB_DSMNT="${FilesDir:-$GLOB_DSMNT}"
GLOB_OMNT="${FilesDir:-$GLOB_OMNT}"
GLOB_HOST="${Host:-$(hostname)}"
GLOB_SVC_HOSTNAME="${GLOB_SVC_HOSTNAME:-$GLOB_HOST}"
GLOB_IPADDR="${HostIp:-$(hostname --ip-address)}"
GLOB_HOST_HOSTSCONF="${ConfigureHostsFile:-no}"
GLOB_HOST_IPV6CONF="${ConfigureIpv6:-disable}"
GLOB_HOST_APACHE="${GLOB_APACHE:-$GLOB_HOST}"
GLOB_HOST_JAVA_CACERTS_UPDATE="${UpdateJavaCaCerts:-ignore}"
#GLOB_HOST_APACHE_IP="${GLOB_HOST_APACHE_IP:-$GLOB_IPADDR}"
GLOB_HOST_OTDS="${OtdsLbHost:-$GLOB_HOST}"
#GLOB_HOST_OTDS_IP="${GLOB_HOST_OTDS_IP:-$GLOB_IPADDR}"
GLOB_HOST_OTDS_PRIMARY="${OtdsMasterHost-${GLOB_HOST_OTDS_PRIMARY-$GLOB_HOST}}"
#GLOB_HOST_OTDS_REP1="${OtdsReplicate1}"
#GLOB_HOST_OTDS_REP1_IP="${OtdsReplicate1Ip}"
#GLOB_HOST_OTDS_PRIMARY_IP="${OtdsMasterHostIp-${GLOB_HOST_OTDS_PRIMARY_IP-$GLOB_HOST}}"
GLOB_HOST_OTMM="${OtmmHost:-$GLOB_HOST}"
#GLOB_HOST_OTMM_IP="${GLOB_HOST_OTMM_IP:-$GLOB_IPADDR}"
GLOB_HOST_TRANSC="${GLOB_HOST_TRANSC:-$GLOB_HOST}"
#GLOB_HOST_TRANSC_IP="${GLOB_HOST_TRANSC_IP:-$GLOB_IPADDR}"
GLOB_HOST_CLEANUP="${CleanupAfterFinish:-no}"

GLOB_PRIVATE_CA_ENABLE="${PrivateCa:-ignore}"
  GLOB_APACHE_CERT_CAPEM="${CaCert:-$GLOB_APACHE_CERT_CAPEM}"

GLOB_JDK_INSTALL="${ConfigureJdk:-ignore}"

GLOB_OTMM_DB_HOST="${OtmmDbHost:-*undef*}"
GLOB_OTMM_DB_HOST_PORT="${OtmmDbHostPort:-*undef*}"
GLOB_DB_SYS_USER="${OtmmDbSysAdm:-*undef*}"
GLOB_DB_SYS_PASS="${OtmmDbSysAdmPw:-*undef*}"

GLOB_HOST_ORACLE_SID="${OtmmOracleSid:-$GLOB_HOST_ORACLE_SID}"
GLOB_HOST_ORACLE_STORAGE="${OtmmOracleSetStorage-$GLOB_HOST_ORACLE_STORAGE}"

GLOB_HOST_SQLSERVER="${GLOB_HOST_SQLSERVER:-$GLOB_HOST}"
GLOB_HOST_SQLSERVER_USER="${GLOB_HOST_SQLSERVER_USER:-sa}"
GLOB_HOST_SQLSERVER_USER_PW="${GLOB_HOST_SQLSERVER_USER_PW:-sa1}"
GLOB_HOST_SQLSERVER_PORT="${GLOB_HOST_SQLSERVER_PORT:-1433}"

GLOB_PGSQL_ENABLE="${ConfigurePostgres-${GLOB_PGSQL_ENABLE-install}}"
 GLOB_PGADMIN_ENABLE="${ConfigurePgAdminUI-$GLOB_PGADMIN_ENABLE}"
 GLOB_PGSQL_PGADMIN_USER="${PgAdminWebUser:-admin@localhost}"
 GLOB_PGSQL_PGADMIN_USER_PW="${PgAdminWebUserPw:-letmein}"
 GLOB_HOST_PGSQL="${GLOB_HOST_PGSQL:-$GLOB_HOST}"
 GLOB_HOST_PGSQL_PORT="${GLOB_HOST_PGSQL_PORT:-5432}"
 GLOB_HOST_PGSQL_DATADIR="${GLOB_HOST_PGSQL_DATADIR:-$GLOB_INST/psql}"
 GLOB_OTMM_DB_NAME="${OtmmPostgresDbName:-${GLOB_OTMM_DB_NAME}}"

GLOB_OTMM_ENABLE="${ConfigureOtmm-${GLOB_OTMM_ENABLE}}"
 GLOB_OTMM_PROTO="${OtmmAppProto:-$GLOB_OTMM_PROTO}"
 GLOB_OTMM_EXT_PROTO="${OtmmAppProtoExt:-$GLOB_OTMM_PROTO}"
 GLOB_OTMM_PORT="${OtmmAppPort:-$GLOB_OTMM_PORT}"
 GLOB_OTMM_EXT_PORT="${OtmmAppPortExt:-$GLOB_OTMM_PORT}"
 GLOB_OTMM_DB_USER="${OtmmDBTeams:-${GLOB_OTMM_DB_USER:-towner}}"
 GLOB_OTMM_DB_USER_PW="${OtmmDBTeamsPw:-${GLOB_OTMM_DB_USER_PW:-towner}}"
 GLOB_OTMM_DB_NAME="${GLOB_OTMM_DB_NAME:-otmmdb}"
 GLOB_OTMM_DB_TYPE="${OtmmDbType:-$GLOB_OTMM_DB_TYPE}"
 GLOB_OTMM_USER_PW="${OtmmTeamsUserPw:-$GLOB_OTMM_USER_PW}"
 GLOB_REPOSITORY_HOME="${OtmmRepositoryHome:-$GLOB_REPOSITORY_HOME}"
 GLOB_OTMM_UPGRADE="${UpgradeOtmm-${GLOB_OTMM_UPGRADE-ignore}}"
 GLOB_OTMM_SSL_CERT_DNS_ALIASES="${OtmmSslCertAliases:-$GLOB_HOST}"
 GLOB_OTMM_OTDS_SOURCE_WHITELIST="${OtmmOtdsWhiteListNodes:-}"; #Don't fixup otmm.xml if emtpy
 GLOB_OTMM_SERVER_DEBUG_LEVEL="${OtmmServerDebugLevel:-1}"
 GLOB_OTMM_AMD_ENABLE="${OtmmConfigureAmd:-ignore}"
 GLOB_DB_AMD_USER="${OtmmAmdDbUser:-amd}"
 GLOB_DB_AMD_USER_PW="${OtmmAmdDbUserPw:-amd}"
 GLOB_DB_AMD_DBNAME="${OtmmAmdDbName:-amd}"
 GLOB_AMD_PORT="${OtmmAmdHostPort:-$GLOB_AMD_PORT}"
 GLOB_OTMM_ENABLE_SYSTEMD="${ConfigureSystemd:-ignore}"
 GLOB_OTMM_OTDS_BASEURL="${OtmmOtdsBaseUrl:-$GLOB_OTMM_OTDS_BASEURL}"
 GLOB_OTMM_BASEURL="${OtmmBaseUrl:-$GLOB_OTMM_BASEURL}"
 GLOB_OTMM_SUPPORTMGR_URL="${OtmmSupportManagerUrl:-$GLOB_OTMM_SUPPORTMGR_URL}"
 GLOB_OTMM_DMTS_BASEURL="${OtmmDmtsBaseUrl:-$GLOB_OTMM_DMTS_BASEURL}"

 if [ "$GLOB_MM_CONFTMPL" = "ignore" ]; then
	GLOB_MM_CONFTMPL="*undef*"
 fi
 if [ "$GLOB_OTMM_AMD_ENABLE" = "ignore" ]; then
	GLOB_AMD_SERVICE_TMPL="*undef*"
 fi

GLOB_DMTS_ENABLE="${ConfigureDmts:-ignore}"
 GLOB_DMTS_ENABLE_FFMPEG="${DmtsEnableFfmpeg:-ignore}"
 GLOB_DMTS_ENABLE_IMAGEMAGICK="${DmtsEnableImageMagick:-ignore}"
 GLOB_DMTS_NODE_USERNAME="${RqNodeUser:-$GLOB_DMTS_NODE_USERNAME}"
 GLOB_DMTS_NODE_USERNAME_PW="${RqNodeUserPw:-$GLOB_DMTS_NODE_USERNAME_PW}"
 GLOB_DMTS_ADD_OPENOFFICE="${DmtsAddOpenOffice:-ignore}"
 GLOB_DMTS_ADD_BLENDER="${DmtsAddBlender:-ignore}"
 if [ "$GLOB_DMTS_ENABLE" = "ignore" ]; then
	 GLOB_DMTS_CONFTEMPL="*undef*"
	 GLOB_DMTS_SOFFICE_ARCDIR="*undef*"
	 GLOB_DMTS_BLENDER_ARC="*undef*"
 fi

 if [ "$GLOB_DMTS_ADD_OPENOFFICE" = "ignore" ]; then
	 GLOB_DMTS_SOFFICE_ARCDIR="*undef*"
 fi

 if [ "$GLOB_DMTS_ADD_BLENDER" = "ignore" ]; then
	 GLOB_DMTS_BLENDER_ARC="*undef*"
 fi

GLOB_RMA_ENABLE="${ConfigureRma:-ignore}"
 GLOB_RMA_SUBKEY="${RmaSubscription:-$GLOB_RMA_SUBKEY}"
 if [ "$GLOB_RMA_ENABLE" = "ignore" ]; then
	 GLOB_RMACONFTEMPL="*undef*"
 fi

GLOB_APACHE_ENABLE="${ConfigureApache:-ignore}"
 GLOB_APACHE_STREAMING="${ApacheStreaming:-$GLOB_APACHE_STREAMING}"
 GLOB_APACHE_LBHOST="${ApacheLbHost:-$GLOB_HOST}"
 GLOB_APACHE_SSL_CERT_DNS_ALIASES="${ApacheSslCertAliases:-$GLOB_HOST}"
 if [ "$GLOB_APACHE_ENABLE" = "ignore" ]; then
	 GLOB_APACHE_PROXY_CONFIG_TEMPL="*undef*"
	 GLOB_APACHE_SSL_CONFIG_TEMPL="*undef*"
	 GLOB_APACHE_HTTPS_CONFIG_TEMPL="*undef*"
 elif [ "$GLOB_APACHE_ENABLE" = "proxy" ]; then
	 GLOB_APACHE_SSL_CONFIG_TEMPL="*undef*"
         GLOB_APACHE_HTTPS_CONFIG_TEMPL="*undef*"
 fi

GLOB_OTDS_ENABLE="${ConfigureOtds-${GLOB_OTDS_ENABLE-ignore}}"
 GLOB_OTDS_WORKAROUND_LOCALHOST=${OtdsWorkaroundLocalhost:-ignore}
 GLOB_OTDS_UPGRADE="${UpgradeOtds-${GLOB_OTDS_UPGRADE-ignore}}"
 GLOB_OTDS_VERSION_ID=${OtdsVersion:-1643}
 GLOB_OTDS_PURGE_INSTALL="${DestroyOtdsInstallation:-ignore}"
 GLOB_OTDS_PASS="${OtdsAdminUserPw:-$GLOB_OTDS_PASS}"
 GLOB_HOST_OTDS_PROTO="${OtdsAppProto:-$GLOB_HOST_OTDS_PROTO}"
 GLOB_HOST_OTDS_PROTO_EXT="${OtdsAppProtoExt:-$GLOB_HOST_OTDS_PROTO}"
 GLOB_HOST_OTDS_PORT_EXT="${OtdsLbHostPort:-$GLOB_HOST_OTDS_PORT}"
if [ "$GLOB_OTDS_ENABLE" = "ignore" ]; then
	GLOB_OTDSARC="*undef*"
	GLOB_UPDATE_OTDSARC1643hf14_ARC="*undef*"
fi

GLOB_DH_ENABLE="${ConfigureDh:-ignore}"
if [ "$GLOB_DH_ENABLE" = "ignore" ]; then
	GLOB_DHARC="*undef*"
fi

GLOB_SUPP_ENABLE="${ConfigureSupportMgr:-ignore}"

 #map the alternative OTDS configuration
 case $GLOB_OTDS_VERSION_ID in
	"1662")
		GLOB_OTDS_VERSION=$GLOB_OTDS_VERSION_2
		GLOB_OTDSARC=$GLOB_OTDSARC_2
		GLOB_OTDS_CONF=$GLOB_OTDS_CONF_2
		;;
	*) 
		;;
 esac

#pass required file check...
GLOB_OTDS_VERSION_2="*undef*"
GLOB_OTDSARC_2="*undef*"
GLOB_OTDS_CONF_2="*undef*"

 #Database schema must only be installed once! This setting chooses the server to
 #run the schmema installation.
 GLOB_OTMM_INSTALL_SCHEMA="${InstallOtmmSchema-${GLOB_OTMM_INSTALL_SCHEMA}}"

GLOB_CONF_SOLR="${ConfigureSolr:-ignore}"
GLOB_REGISTER_SOLR="${RegisterSolr-${GLOB_REGISTER_SOLR:-ignore}}"
 GLOB_HOST_SOLR_NODE="${SolrNodeHost-$GLOB_HOST}"
# GLOB_HOST_SOLR_NODE_IP="${SolrNodeHostIp-$GLOB_IPADDR}"
 GLOB_HOST_SOLR_NODE_PORT="${SolrNodePort-$GLOB_CONF_SOPORT}"
 GLOB_HOST_SOLR_MASTER="${SolrMasterHost:-$GLOB_HOST}"
# GLOB_HOST_SOLR_MASTER_IP="${SolrMasterHostIp:-$GLOB_IPADDR}"
 GLOB_HOST_SOLR_MASTER_PORT="${SolrMasterPort:-$GLOB_CONF_SOPORT}"
 GLOB_ZKNODE_ID="${GLOB_ZKNODE_ID:-1}"
 GLOB_SEARCH_ZK_NODES="${GLOB_SEARCH_ZK_NODES:-server.1=$GLOB_HOST:2888:3888}"
 GLOB_CLIENT_ZK_NODES="${GLOB_CLIENT_ZK_NODES-${GLOB_HOST:2181}}"
 GLOB_SOLR_REGISTER_WOTMM="${RegisterWithOtmm:-${GLOB_SOLR_REGISTER_WOTMM:-YES}}"
 GLOB_SOLR_REGISTER_WOTMM="${GLOB_SOLR_REGISTER_WOTMM^^}"; #make this upper case
 GLOB_SOLR_SETUP_ULIMIT="${SolrConfigureLimits:-no}"
 GLOB_SOLR_SOURCE_WHITELIST="${SolrWhiteListNodes:-}"; #Don't touch jetty.xml if emtpy
 GLOB_SODB=${DBMAP[$GLOB_OTMM_DB_TYPE]}

if [ "$GLOB_CONF_SOLR" = "ignore" ]; then
	GLOB_SOLR_CONFTMPL="*undef*"
	GLOB_ZK_CONFTMPL="*undef*"
elif [ "$GLOB_CONF_SOLR" = "standalone" ]; then
	GLOB_ZK_CONFTMPL="*undef*"
fi

if [ "$GLOB_OTMM_ENABLE" = "ignore" ] && \
   [ "$GLOB_CONF_SOLR" = "ignore" ] && \
   [ "$GLOB_DMTS_ENABLE" = "ignore" ] \
	; then 
	 GLOB_MMARC="*undef*"
	 GLOB_DMTS_SOFFICE_ARC="*undef*"
fi

GLOB_DMTS_ROOT="${TranscodeRootFolder:-$GLOB_DMTS_ROOT}"
GLOB_DMTS_WORKING="${TranscodeWorkingFolder:-$GLOB_DMTS_WORKING}"
GLOB_TRANSCODE_ENABLE="${OtmmSetTranscoder:-ignore}"


GLOB_SMTP_TYPE="${GLOB_SMTP_TYPE-ignore}"

if [ "$GLOB_JDK_INSTALL" = "default" ]; then
	GLOB_JDKARC="$GLOB_JDKARC"
elif [ "$GLOB_JDK_INSTALL" = "ignore" ]; then
	GLOB_JDKARC="*undef*"
else
	GLOB_JDKARC="$GLOB_JDK_INSTALL"
fi

GLOB_OTMM_DESTROY_SCHEMA="${DestroyExistingSchema:-${GLOB_OTMM_DESTROY_SCHEMA:-ignore}}"

GLOB_SMTP_TYPE="${GLOB_SMTP_TYPE-ignore}"

. $GLOB_CONFDIR/scripts/m_install_jdk.sh
. $GLOB_CONFDIR/scripts/m_install_user.sh
. $GLOB_CONFDIR/scripts/m_install_wildfly.sh
. $GLOB_CONFDIR/scripts/m_install_tomcat.sh
. $GLOB_CONFDIR/scripts/m_install_otds.sh
. $GLOB_CONFDIR/scripts/m_install_postgresql.sh
. $GLOB_CONFDIR/scripts/m_install_otmm.sh
. $GLOB_CONFDIR/scripts/m_install_solr.sh
. $GLOB_CONFDIR/scripts/m_install_ffmpeg.sh
. $GLOB_CONFDIR/scripts/m_install_apache.sh
. $GLOB_CONFDIR/scripts/m_rma_conf_install.sh
. $GLOB_CONFDIR/scripts/m_amd_conf_install.sh
. $GLOB_CONFDIR/scripts/m_dmts_install.sh
. $GLOB_CONFDIR/scripts/m_install_support.sh

. $GLOB_CONFDIR/scripts/m_config_common.sh
. $GLOB_CONFDIR/scripts/m_config_usage.sh
. $GLOB_CONFDIR/scripts/m_common_db.sh

GLOB_DB_AMD_HOST="${OtmmAmdDbHost:-$GLOB_OTMM_DB_HOST}"
GLOB_DB_AMD_PORT="${OtmmAmdDbHostPort:-$GLOB_OTMM_DB_HOST_PORT}"

GLOB_STOPATINSTALLER=${StopAtInstaller:-no}

if [ -n "${Help:-}" ]; then
	case "$Help" in
		"ConfigureHostsFile")
			echo 'ConfigureHostsFile "yes" adds the following entries to the /etc/hosts file:'
			echo "$AddHosts" | awk 'BEGIN{RS=";";FS=",";};{printf("%s	%s\n",$1,$2);}' 
		;;
		*)
			usage
		;;
	esac
        exit 1
fi

if grep -q '^SELINUX=permissive' /etc/selinux/config; then
	echo "*D* Selinux system wide disabled"
else
	echo "*D* Disabling SeLinux system wide"
	setenforce permissive
	test -f /etc/selinux/config.orig || \
		cp /etc/selinux/config /etc/selinux/config.orig
	sed -e 's/^SELINUX=.*/SELINUX=permissive/' <  /etc/selinux/config.orig > /etc/selinux/config
	diff -Nau /etc/selinux/config.orig /etc/selinux/config || /bin/true
fi



#
# Confirm archives exist, possibly download?
#
if [ "$GLOB_OTDS_UPGRADE" != "ignore" ]; then
	echo "*D* Installing Hotfix $GLOB_OTDS_HF_ARC for OTDS"
else
	echo "*D* OTDS hotfix ignored"
fi

set +x

fail=""
for archive in $(compgen -e); do
        case "$archive" in
                *_SOARC)
			echo "*D* check *_SOARC $archive"
                        echo "*D* $archive = ${!archive} not checked, directory in $GLOB_MMARC ISO"
                ;;
                *ARC)
			echo "*D* check *ARC $archive"
                        if 	[ "${!archive}" = "*undef*" ] || \
				[ "${!archive}" = "ignore" ] || \
				[ "${!archive}" = "default" ] || \
				[ "${!archive}" = "java-11-openjdk" ] || \
				[ "${!archive}" = "java-11-openjdk-devel" ]; then
                                echo "*D* $archive = ${!archive} not set, test ignored"
                        elif [ -f "${!archive}" -o -f "${GLOB_LMNT}/${!archive}" -o \
                                -f "${GLOB_OMNT}/${!archive}" -o \
                                -f "${GLOB_LMNT}/linux/${!archive}" -o \
                                -f "${GLOB_DSMNT}/${!archive}" ]; then
                                echo "*D* $archive = ${!archive} exists"
                        else
                                echo "*E* $archive = ${!archive} does not exist"
                                fail=1
                        fi
                ;;
		*TEMPL|*TMPL)
			echo "*D* check *TEMPL templates"
			if 	[ "${!archive}" = "*undef*" ] || \
				[ "${!archive}" = "ignore" ]; then
				echo "*D* $archive = ${!archive} not set, test ignored"
			elif [ -f "${GLOB_CONFDIR}/${!archive}" ] || \
				[ -f "${!archive}" ] ; then
				echo "*D* $archive = ${!archive} exists"
			else
                                echo "*E* $archive = ${!archive} does not exist"
                                fail=1
			fi
		;;
                *)
        esac
done
if [ -n "$fail" ]; then
        exit
fi

set +a
if [ \! "$Verbose" = "no" ]; then
	set -x
fi
set -e

GLOB_HOST_OTDS_IP="${GLOB_HOST_OTDS_IP:-$GLOB_IPADDR}"
GLOB_HOST_OTMM="${GLOB_HOST_OTMM:-$GLOB_HOST}"
GLOB_HOST_OTMM_IP="${GLOB_HOST_OTMM_IP:-$GLOB_IPADDR}"
GLOB_HOST_TRANSC="${GLOB_HOST_TRANSC:-$GLOB_HOST}"
GLOB_HOST_TRANSC_IP="${GLOB_HOST_TRANSC_IP:-$GLOB_IPADDR}"

#Sanity check
echo "*T* GLOB_HOST: ${GLOB_HOST:?-Host not set}"

#
# Add our own ca authority file, typically ot.testca
#
if [ "$GLOB_PRIVATE_CA_ENABLE" = "ignore" ]; then

	echo "*D* GLOB_PRIVATE_CA_ENABLE=$GLOB_PRIVATE_CA_ENABLE, private CA files not installed" 
else

	echo "*D* GLOB_PRIVATE_CA_ENABLE=$GLOB_PRIVATE_CA_ENABLE, install private CA fils" 

	if [ -f "$GLOB_APACHE_CERT_CAPEM" ]; then
		echo "*D* Install $GLOB_APACHE_CERT_CAPEM ca file"
		add_ca_cert_centos -CaPemFile "$GLOB_APACHE_CERT_CAPEM"
	else
		echo "*D* Unable to install $GLOB_APACHE_CERT_CAPEM, not found"
	fi

fi

#
# This emulates some DNS settings:
#
if [ "$GLOB_HOST_HOSTSCONF" = "yes" ]; then
	host_conf_hosts -Hostname "$GLOB_HOST" -Ipaddr "$GLOB_IPADDR" -AddHosts "${AddHosts:-''}"
elif [ -n "$AddHosts" ]; then
	echo "*E* Ignore /etc/hosts configuration but assigned AddHosts property. Add "
	echo "*E* -ConfigureHostsFile "yes" and restart to change the hosts file"
	exit 1
else
	echo "*D* Ignore /etc/hosts configuration"
fi

#
# This disables ipv6 for a node
#
if [ "$GLOB_HOST_IPV6CONF" = "disable" ]; then
	echo "*D* Disable IPV6"
	host_disable_ipv6
fi

#
# Prepare the OS
# > 20.2 - OpenJDK 11.0.1
#
if [ "$GLOB_JDK_INSTALL" = "ignore" ]; then

	echo "*D* GLOB_JDK_INSTALL=$GLOB_JDK_INSTALL, JDK install administratively denied"

elif [ -x /usr/bin/java ] && compare_jdk_version -MinVers 11.0.1_0; then
	echo "*D* Quick check for JDK 11.0.1 passed, JDK installed"
else
	case "$GLOB_JDKARC" in
		"default"|java-11-openjdk-*) 
			host_install_jdk -Package "$GLOB_JDKARC" -YumFlags "$YumFlags" 

		;;
		*rpm) 
			host_install_jdk -Rpm "$GLOB_LMNT/linux/$GLOB_JDKARC"
		;;
		*tar.gz) 
			host_install_jdk -Archive "$GLOB_LMNT/linux/$GLOB_JDKARC"
		;;
		ignore)
			echo "*D* JDK rollout ignored, use existing..."
		;;
		*)
			echo "*F* Unsupported JDK format for $GLOB_JDKARC"
			exit 1
		;;
	esac

	#ensure active JDK and link to cacerts if permitted
	config_alternatives \
		-JavaHome "$GLOB_JAVAHOME" \
		-UpdateJavaCaCerts "$GLOB_HOST_JAVA_CACERTS_UPDATE" \
		-JavaPkiCacerts "/etc/pki/java/cacerts"

	# Ensure this JVM is a minimum usable 
	# Make sure Java Home is pointing to a good version too
	if compare_jdk_version -MinVers 11.0.1_0 -Path "$GLOB_JAVAHOME"; then
		echo "*D* compare_jdk_version: Detected recent enough java version"
	else
		echo "*F* compare_jdk_version: This java version is not recommended"
		exit 1
	fi
fi

if [ "$GLOB_DRYRUN" = "no" ]; then
	#slow, so check if perl is installed as the canary
#	test -x /usr/bin/perl || \
		bash -c  "BASH_XTRACEFD= yum install -y $YumFlags augeas perl unzip lsof ant strace tcpdump"
else
	echo "*D* DRYRUN($GLOB_DRYRUN) yum install -y $YumFlags perl augeas net-tools unzip lsof ant strace tcpdump"
fi

#
# Prepare installation
#
if [ -d $GLOB_INST ]; then
        echo "*D* $GLOB_INST exists, skipping general setup"
else

        install_user \
                -ConfigTemplate $GLOB_CONFDIR/$GLOB_LOGIN_CONF \
                -User $GLOB_USER \
                -Group $GLOB_GROUP \
                -UserHome $GLOB_INST \
                -PasswordSeed "secret"

	#cope with DryRun
        id -u $GLOB_USER 1>/dev/null 2>&1 && install -m 0775 -g $GLOB_GROUP -o $GLOB_USER -d $GLOB_INST
fi

if [ "$PauseAt" = "ONE" ]; then
	echo "*D* ONE"
	exit 1
fi

#
# OTDS in Tomcat
#
if [ -d "$GLOB_DSINS" ] && [ "$GLOB_OTDS_PURGE_INSTALL" != "purge" ]; then
        echo "*D* GLOB_DSINS='$GLOB_DSINS' already exists, skipping OTDS install..."

elif [ "$GLOB_OTDS_ENABLE" = "ignore" ]; then
        echo "*D* GLOB_OTDS_ENABLE=$GLOB_OTDS_ENABLE, OTDS installation administratively denied..."

else

	if [ -z "$GLOB_TCARC_TDIR" -o -z "$GLOB_TCINS_LEAFDIR" ]; then

		echo "*E* Invalid configuration GLOB_TCARC_TDIR($GLOB_TCARC_TDIR) GLOB_TCINS_LEAFDIR($GLOB_TCINS_LEAFDIR)"

	elif [ ! -d $GLOB_TCINS ]; then 
		#InstallDir is GLOB_TCINS minus GLOB_TCARC_TDIR!
		install_conf_tomcat \
			-ServiceTemplate "$GLOB_CONFDIR/$GLOB_OTDS_SERVICE_TMPL" \
			-HeapSpace "512" \
			-HttpPort "$GLOB_HOST_OTDS_PORT" \
			-ShutdownPort "$GLOB_HOST_OTDS_SHUTDOWN_PORT" \
			-AjpPort "$GLOB_HOST_OTDS_AJP_PORT" \
			-InstallDir "$GLOB_INST" \
			-Archive "$GLOB_LMNT/$GLOB_TCARC" \
			-ArchiveTopDir "$GLOB_TCARC_TDIR" \
			-TomcatHomeLeafDir "$GLOB_TCINS_LEAFDIR" \
			-User "$GLOB_USER" \
			-JavaHome "$GLOB_JAVAHOME" \
			-Group "$GLOB_GROUP"

		if [ "$GLOB_TCINS_SSL" = 'ignore' ]; then
			echo "Tomcat SSL enablement administratively ignored"
		else
			enable_https_tomcat \
				-Alias "$GLOB_HOST-otds" \
				-Keystore "/etc/pki/tls/certs/tomcat_keystore.jks" \
				-Storepass "changeit" \
				-Keypass "changeit" \
				-JavaHome "$GLOB_JAVAHOME" \
				-InstallDir "$GLOB_INST/$GLOB_TCINS_LEAFDIR" \
				-Dname "cn=$GLOB_HOST, ou=$GLOB_APACHE_CERT_ORGUNIT, o=$GLOB_APACHE_CERT_ORG, c=$GLOB_APACHE_CERT_COUNTRY"
			create_csr_tomcat \
				-Alias "$GLOB_HOST-otds" \
				-Keystore "/etc/pki/tls/certs/tomcat_keystore.jks" \
				-Storepass "changeit" \
				-Keypass "changeit" \
				-CsrFile "/etc/pki/tls/certs/otds_otmm165.csr" \
				-JavaHome "$GLOB_JAVAHOME"
			sign_ot_test_csr \
				-SslCertAliases "$GLOB_HOST-otds" \
				-CsrFile "/etc/pki/tls/certs/otds_otmm165.csr" \
				-CrtFile "/etc/pki/tls/certs/otds_otmm165.crt" \
				-CaKeyFile "$GLOB_APACHE_CERT_CAKEY" \
				-CaPemFile "$GLOB_APACHE_CERT_CAPEM" \
				-JavaHome "$GLOB_JAVAHOME" \
				-CaPemFilePw "$GLOB_APACHE_CERT_CAPWD" \
				-CaSrlFile "/etc/pki/tls/certs/otds_otmm165.srl"
			install_crt_tomcat \
				-Alias "$GLOB_HOST-otds" \
				-CrtFile "/etc/pki/tls/certs/otds_otmm165.crt" \
				-Keystore "/etc/pki/tls/certs/tomcat_keystore.jks" \
				-Storepass "changeit" \
				-JavaHome "$GLOB_JAVAHOME" \
				-CaPemFile "$GLOB_APACHE_CERT_CAPEM"
		fi
	fi

	#cope with DryRun
	id -u $GLOB_USER 1>/dev/null 2>&1 && install -o $GLOB_USER -g $GLOB_GROUP -d /etc/opentext

	if [ -z "$GLOB_OTDS_PASS" ] || [ "$GLOB_OTDS_PASS" = "*undef*" ]; then
		echo "GLOB_OTDS_PASS=$GLOB_OTDS_PASS is not valid, use 'OtdsAdminUserPw' to add"
		exit 1
	fi

	if [ "$GLOB_OTDS_ENABLE" = "first" ]; then
		install_conf_otds \
			-OtdsWorkaroundLocalhost "$GLOB_OTDS_WORKAROUND_LOCALHOST" \
			-StopAtInstaller "$GLOB_STOPATINSTALLER" \
			-DestroyInstall "$GLOB_OTDS_PURGE_INSTALL" \
			-Archive "$GLOB_DSMNT/$GLOB_OTDSARC" \
			-User $GLOB_USER \
			-Group $GLOB_GROUP \
			-ConfigTemplate "$GLOB_CONFDIR/$GLOB_OTDS_CONF" \
			-LocalDir "$GLOB_OTDSLOCALDIR" \
			-OtdsInstallDir "$GLOB_DSINS" \
			-InstallDir "$GLOB_TCINS"
	elif [ "$GLOB_OTDS_ENABLE" = "slave" ]; then
		install_conf_otds \
			-StopAtInstaller "$GLOB_STOPATINSTALLER" \
			-Archive "$GLOB_DSMNT/$GLOB_OTDSARC" \
			-DestroyInstall "$GLOB_OTDS_PURGE_INSTALL" \
			-User $GLOB_USER \
			-Group $GLOB_GROUP \
			-ConfigTemplate "$GLOB_CONFDIR/$GLOB_OTDS_REPL_CONF" \
			-OtdsInstallDir "$GLOB_DSINS" \
			-LocalDir "$GLOB_OTDSLOCALDIR" \
			-InstallDir "$GLOB_TCINS"
	else 
		echo "*E* Invalid OTDS mode GLOB_OTDS_ENABLE=$GLOB_OTDS_ENABLE, abort"
		exit 1
	fi

	
	#TC is running but this function also waits for the complete startup of tomcat...
	tomcat_startup \
		-Service "$GLOB_TCINS_LEAFDIR" \
		-ShutdownPort "$GLOB_HOST_OTDS_SHUTDOWN_PORT" \
		-User "$GLOB_USER" \
		-PidDir "/var/run/tomcat_otds" \
        	-PidFile "tomcat_otds.pid" \
		-InstallDir "$GLOB_TCINS" 
fi

if [ -n "$GLOB_OTDS_ENABLE" ] && [ "$GLOB_OTDS_UPGRADE" = "ignore" ]; then
	GLOB_OTDS_VERSION_HOTFIX="" ;#empty for MOTD
	echo "*D* GLOB_OTDS_ENABLE='$GLOB_OTDS_ENABLE', GLOB_OTDS_UPGRADE='$GLOB_OTDS_UPGRADE' OTDS " \
	     "not required or hotfix not selected"
elif [ -n "$GLOB_OTDS_UPGRADE" ]; then
	echo "*D* Upgrade OTDS to $GLOB_OTDS_UPGRADE"
	#GLOB_OTDS_VERSION_HOTFIX
	tomcat_shutdown \
		-User "$GLOB_USER" \
		-PidDir "/var/run/tomcat_otds" \
        	-PidFile "tomcat_otds.pid" \
		-InstallDir "$GLOB_TCINS" 

	install_otds_patch \
		-HotfixFile "${GLOB_OTDS_VERSION_HOTFIX}.txt" \
		-InstallDir "$GLOB_DSINS" \
		-Archive "$GLOB_OTDS_HF_ARC" \
		-User $GLOB_USER 

	tomcat_startup \
		-Service "$GLOB_TCINS_LEAFDIR" \
		-ShutdownPort "$GLOB_HOST_OTDS_SHUTDOWN_PORT" \
		-User "$GLOB_USER" \
		-PidDir "/var/run/tomcat_otds" \
        	-PidFile "tomcat_otds.pid" \
		-InstallDir "$GLOB_TCINS" 
else
	echo "*F* Upgrade OTDS to $GLOB_OTDS_UPGRADE not configured"
	exit 1
fi

if [ "$PauseAt" = "TWO" ]; then
        echo "*D* TWO"
        exit 1
fi

#
# Postgresql server
# * Download and install Postgresql's PGPD repo package
#

if [ "$GLOB_OTMM_DB_TYPE" != "local_postgres" ]; then
        echo "*D* $GLOB_OTMM_DB_TYPE is not postgres, ignoring Postgresql installer..."

elif [ "$GLOB_PGSQL_ENABLE" = "ignore" ]; then
	GLOB_HOST_PGSQL_VERSION_MAJOR="not installed"; #For MOTD
        echo "*I* GLOB_PGSQL_ENABLE='$GLOB_PGSQL_ENABLE' Postgres installation administratively denied..."
elif [ -f "/usr/lib/systemd/system/postgresql-12.service" ]; then
       echo "Postgres installed, skipping"
else
	echo "*D* Postgres installation for ${GLOB_HOST_PGSQL_VERSION_MAJOR}.${GLOB_HOST_PGSQL_VERSION_MINOR}"
        install_conf_postgresql \
		-YumFlags "$YumFlags" \
		-EnablePgAgent 'ignore' \
                -PgAgentName "pgagent_${GLOB_HOST_PGSQL_VERSION_MAJOR}${GLOB_HOST_PGSQL_VERSION_MINOR}" \
                -PgAgentSystemDConf "/usr/lib/systemd/system/pgagent_${GLOB_HOST_PGSQL_VERSION_MAJOR}${GLOB_HOST_PGSQL_VERSION_MINOR}.service" \
                -PgAgentConfigTemplate "$GLOB_CONFDIR/pgagent_${GLOB_HOST_PGSQL_VERSION_MAJOR}${GLOB_HOST_PGSQL_VERSION_MINOR}.conf_templ" \
                -PgAgentHostUser "pgagent" \
                -PgAgentDbName $GLOB_OTMM_DB_NAME \
                -PgAgentDbUser $GLOB_OTMM_DB_USER \
                -PgAgentDbUserPw $GLOB_OTMM_DB_USER_PW \
                -PgAgentDbHost $GLOB_HOST_PGSQL \
                -PgAgentDbHostPort $GLOB_HOST_PGSQL_PORT \
                -PgConfigDir "/var/lib/pgsql/${GLOB_HOST_PGSQL_VERSION_MAJOR}/data" \
                -PgRepoRpm "$GLOB_LMNT/$GLOB_HOST_PGSQL_REPO_RPM" \
                -PgRepoGroup "PostgreSQL Database Server ${GLOB_HOST_PGSQL_VERSION_MAJOR} PGDG" \
                -PgSetup "/usr/pgsql-${GLOB_HOST_PGSQL_VERSION_MAJOR}/bin/postgresql-${GLOB_HOST_PGSQL_VERSION_MAJOR}-setup" \
                -PgService "postgresql-${GLOB_HOST_PGSQL_VERSION_MAJOR}.service" \
                -DbSysUser $GLOB_HOST_PGSQL_SYS_USER \
                -DbSysUserPass $GLOB_HOST_PGSQL_SYS_USER_PW
fi

if [ "$GLOB_PGADMIN_ENABLE" = "ignore" ]; then
	echo "*D* GLOB_PGADMIN_ENABLE=$GLOB_PGADMIN_ENABLE, PGAdmin installation administratively denied"

elif [ -f "/etc/httpd/conf.d/pgadmin4.conf.standalone" ]; then
	echo "*D* PGADMIN installed, skipping"
else
	echo "*D* GLOB_PGADMIN_ENABLE=$GLOB_PGADMIN_ENABLE, install as PgAdminWebUser=admin@localhost"

	install_conf_pgadmin \
		-PgAdminConfigTemplate "$GLOB_PGADMIN_APACHE_TEMPL" \
                -PgAdminConfigTemplate "$GLOB_CONFDIR/apache_pgadmin_conf_templ" \
                -PgAdminHost "$GLOB_HOST" \
                -PgAdminWebUser "admin@localhost" \
                -PgAdminWebUserPw "letmein" \
       		-YumFlag ="$YumFlags" 
fi

if [ "$PauseAt" = "THREE" ]; then
        echo "*D* THREE"
        exit 1
fi

# Support Manager

if [ "$GLOB_SUPP_ENABLE" = "ignore" ]; then

	echo "*D* GLOB_SUPP_ENABLE=$GLOB_SUPP_ENABLE,"`
		`" Support Manager install administratively denied" 

elif [ -d "$GLOB_SUP_HOME" ]; then

	echo "*D* GLOB_SUP_HOME=$GLOB_SUP_HOME directory exists, skipping..." 

else

	echo "*D* GLOB_SUPP_ENABLE=$GLOB_SUPP_ENABLE, installing Support Manager..." 

	install_support_service \
                -TmpMount "$GLOB_MMARCDIR" \
		-Archive "$GLOB_OMNT/$GLOB_MMARC" \
		-InstallDir "$GLOB_SUP_HOME" \
		-User "$GLOB_USER" \
		-Group "$GLOB_GROUP" \
		-ServiceTemplate "$GLOB_CONFDIR/$GLOB_SUP_SYSTEMD_TEMPL" \
		-ServerName "$GLOB_SUP_SERVICE_NAME" \
		-JdbcSysConnection "$GLOB_DB_SYS_JDBC" \
		-ConfigTemplate "$GLOB_CONFDIR/$GLOB_SUP_CONFTMPL" \
        	-DbSysUser "$GLOB_DB_SYS_USER" \
        	-DbSysUserPw "$GLOB_DB_SYS_PASS" \
        	-DbUser "$GLOB_SUPP_DB_USER" \
		-DbUserPw "$GLOB_SUPP_DB_USER_PW" \
        	-DbName "$GLOB_SUPP_DB_NAME" \
		-DbHost "$GLOB_SUPP_DB_HOST" \
		-DbHostPort "$GLOB_SUPP_DB_HOST_PORT" \
        	-DbType "$GLOB_SUPP_DB_TYPE" \
        	-DoInstallSchema "$GLOB_SUPP_DB_INSTALL_SCHEMA" \
        	-DoDestroySchema "$GLOB_SUPP_DB_DESTROY_SCHEMA" \
        	-SqlTool "java -Djava.security.egd=file:/dev/./urandom -jar $GLOB_CONFDIR/sqltool.jar" 

	support_install_service_helper \
		-User "$GLOB_USER" \
                -ServiceName "$GLOB_SUP_SERVICE_NAME" \
                -InstallDir "${GLOB_SUP_HOME}"

fi

if [ "$PauseAt" = "FOUR" ]; then
        echo "*D* FOUR"
        exit 1
fi

# DMTS

if [ "$GLOB_DMTS_ENABLE" = "ignore" ]; then

	echo "*D* GLOB_DMTS_ENABLE=$GLOB_DMTS_ENABLE, DMTS install administratively denied" 

elif [ -d "$GLOB_DMTSINS" ]; then

	echo "*D* GLOB_DMTSINS=$GLOB_DMTSINS, DMTS directory already installed, skipping..." 
else

	echo "*D* GLOB_DMTS_ENABLE=$GLOB_DMTS_ENABLE, DMTS service installation" 

#	echo "*D* Root required to configure sudoers"
#	dmts_configure_sudoers \
#		-SudoFile "dmts.${GLOB_GROUP}" \
#		-Group "$GLOB_GROUP" \

	echo "*D* Install DMTS from installer CD"
	install_dmts \
		-YumFlags "$YumFlags" \
		-User "$GLOB_USER" \
		-Group "$GLOB_GROUP" \
		-StopAtInstaller "$GLOB_STOPATINSTALLER" \
		-JavaHome "$GLOB_JAVAHOME" \
		-InstallDir "$GLOB_DMTSINS" \
        	-ConfigTemplate "$GLOB_CONFDIR/$GLOB_DMTS_CONFTEMPL" \
        	-FeatureList "$GLOB_DMTS_FEATURES" \
        	-ServiceList "$GLOB_DMTS_SERVICES" \
        	-RqInstall "$GLOB_DMTS_RMQ_INSTALL" \
        	-RqOtherServer "$GLOB_DMTS_RMQ_OTHERSERVER" \
        	-RqNode "$GLOB_DMTS_RMQ_HOST" \
        	-RqNodePort "$GLOB_DMTS_RMQ_HOST_PORT" \
        	-RqNodeUser "$GLOB_DMTS_NODE_USERNAME" \
        	-RqNodeUserPw "$GLOB_DMTS_NODE_USERNAME_PW" \
                -TmpMount "$GLOB_MMARCDIR" \
        	-Archive "$GLOB_OMNT/$GLOB_MMARC"

	echo "*D* Install RabbitMQ from installer CD"
	install_dmts_rabbitmq \
		-DmtsHome "$GLOB_DMTSINS" \
        	-RqAdmUser "$GLOB_DMTS_NODE_USERNAME" \
        	-RqAdmUserPw "$GLOB_DMTS_NODE_USERNAME_PW" \

	case "$GLOB_DMTS_SERVICES" in
		All|*DMTS-Service*)
			configure_dmts \
				-User "$GLOB_USER" \
				-Group "$GLOB_GROUP" \
				-ConfigureFile "$GLOB_DMTSINS/dmts-service/conf/application.properties" \
				-DmtsHome "$GLOB_DMTSINS" \
				-JavaHome "$GLOB_JAVAHOME" \
				-ServiceName "DMTS-Service" \
				-DmtsNode "$GLOB_DMTS_LOCAL_NODE" \
				-DmtsNodePort "$GLOB_DMTS_NODE_PORT" \
				-FirstRmqUser "$GLOB_DMTS_NODE_USERNAME" \
				-FirstRmqUserPw "$GLOB_DMTS_NODE_USERNAME_PW" \
				-FirstDmtsNode "$GLOB_HOST" \
				-FirstDmtsNodePort "$GLOB_DMTS_NODE_PORT" \
				-TranscodeWorkingArea "$GLOB_DMTS_ROOT" \
				-TranscoderWorkFolder "$GLOB_DMTS_WORKING"
		;;
		*)
			echo "*D* Skipping DMTS-Service install on this node"
		;;
	esac


	case "$GLOB_DMTS_SERVICES" in
		All|*DMTS-Image*)
			echo "*D* Configure DMTS-Image"
			configure_dmts \
				-User "$GLOB_USER" \
				-Group "$GLOB_GROUP" \
				-ConfigureFile "$GLOB_DMTSINS/dmts-image/conf/application.properties" \
				-DmtsHome "$GLOB_DMTSINS" \
				-JavaHome "$GLOB_JAVAHOME" \
				-ServiceName "DMTS-Image" \
				-FirstRmqUser "$GLOB_DMTS_NODE_USERNAME" \
				-FirstRmqUserPw "$GLOB_DMTS_NODE_USERNAME_PW" \
				-FirstDmtsNode "$GLOB_HOST" \
				-FirstDmtsNodePort "$GLOB_DMTS_NODE_PORT" \
				-TranscodeWorkingArea "$GLOB_DMTS_ROOT" \
				-TranscoderWorkFolder "$GLOB_DMTS_WORKING"
		;;
		*)
			echo "*D* Skipping DMTS-Image install on this node"
		;;
	esac


	case "$GLOB_DMTS_SERVICES" in
		All|*DMTS-Video*)
			echo "*D* Configure DMTS-Video"
			configure_dmts \
				-User "$GLOB_USER" \
				-Group "$GLOB_GROUP" \
				-ConfigureFile "$GLOB_DMTSINS/dmts-video/conf/application.properties" \
				-MediaInfoRpms "$GLOB_DMTS_MI_ARCHDIR" \
				-DmtsHome "$GLOB_DMTSINS" \
				-JavaHome "$GLOB_JAVAHOME" \
				-ServiceName "DMTS-Video" \
				-FirstRmqUser "$GLOB_DMTS_NODE_USERNAME" \
				-FirstRmqUserPw "$GLOB_DMTS_NODE_USERNAME_PW" \
				-FirstDmtsNode "$GLOB_HOST" \
				-FirstDmtsNodePort "$GLOB_DMTS_NODE_PORT" \
				-TranscodeWorkingArea "$GLOB_DMTS_ROOT" \
				-TranscoderWorkFolder "$GLOB_DMTS_WORKING"
		;;
		*)
			echo "*D* Skipping DMTS-Video install on this node"
		;;
	esac

	#Change the DMTS service user to otmm
	case "$GLOB_DMTS_SERVICES" in
		All)
			postinstall_configure_dmts_service \
				-DmtsHome "$GLOB_DMTSINS" \
				-FfmpegHome "$GLOB_FF_INSDIR" \
				-User "$GLOB_USER" \
				-Service "All"
			systemctl start dmts-image dmts-video dmts-service
		;;
		*DMTS-Video*)
			postinstall_configure_dmts_service \
				-DmtsHome "$GLOB_DMTSINS" \
				-User "$GLOB_USER" \
				-FfmpegHome "$GLOB_FF_INSDIR" \
				-Service "dmts-video"
			systemctl start dmts-video 
		;;
		*DMTS-Image*)
			postinstall_configure_dmts_service \
				-DmtsHome "$GLOB_DMTSINS" \
				-User "$GLOB_USER" \
				-FfmpegHome "$GLOB_FF_INSDIR" \
				-Service "dmts-image"
			systemctl start dmts-image 
		;;
		*DMTS-Servcie*)
			postinstall_configure_dmts_service \
				-DmtsHome "$GLOB_DMTSINS" \
				-User "$GLOB_USER" \
				-FfmpegHome "$GLOB_FF_INSDIR" \
				-Service "dmts-service"
			systemctl start dmts-service 
		;;
		*)
			echo "*D* GLOB_DMTS_SERVICES=$GLOB_DMTS_SERVICES Invalid argument"
		;;
	esac

fi

if [ "$GLOB_DMTS_ENABLE_FFMPEG" = "ignore" ]; then

	echo "*D* GLOB_DMTS_ENABLE_FFMPEG=$GLOB_DMTS_ENABLE_FFMPEG, FFMPEG binary installation denied"

else

	echo "*D* GLOB_DMTS_ENABLE_FFMPEG=$GLOB_DMTS_ENABLE_FFMPEG, DMTS video installation" 

	install_dmts_ffmpeg_binaries \
	        -Archive "$GLOB_LMNT/$GLOB_FF_FFMPEGARC" \
		-MediaInfoRpms "$GLOB_DMTS_MI_ARCHDIR" \
        	-InstallDir "$GLOB_FF_INSDIR" \
        	-User "$GLOB_USER" \
        	-DmtsHome "$GLOB_DMTSINS"
fi

if [ "$GLOB_DMTS_ENABLE_IMAGEMAGICK" = "ignore" ]; then

	echo "*D* GLOB_DMTS_ENABLE_IMAGEMAGICK=$GLOB_DMTS_ENABLE_IMAGEMAGICK, ImageMagick binary installation denied"

else

	echo "*D* GLOB_DMTS_ENABLE_IMAGEMAGICK=$GLOB_DMTS_ENABLE_IMAGEMAGICK, DMTS image installation" 

	install_dmts_imagemagick \
		-ReposRpm "GLOB_DMTS_IM_REPOS" \
        	-ArchiveDir="$GLOB_DMTS_IM_ARCHDIR" \
        	-Archives="$GLOB_DMTS_IM_ARCHIVES" 
fi

if [ "$GLOB_DMTS_ENABLE_IMAGEMAGICK" = "ignore" ]; then

	:noop

elif [ -d "$GLOB_DMTS_SOFFICE_INSTDIR" ]; then

	echo "*D* GLOB_DMTS_SOFFICE_INSTDIR=$GLOB_DMTS_SOFFICE_INSTDIR exists, "\
		"OpenOffice already installed"

else

	if [ "$GLOB_DMTS_ADD_OPENOFFICE" = "ignore" ]; then
		echo "*D* Installation of Openoffice administratively denied"
	else
		echo "*D* GLOB_DMTS_ADD_OPENOFFICE=$GLOB_DMTS_ADD_OPENOFFICE," \
			"Installation of Openoffice archives "

        	install_dmts_openoffice \
			-DmtsHome "$GLOB_DMTSINS" \
	                -User "$GLOB_USER" \
       	         	-ArchivesDir "$GLOB_DMTS_SOFFICE_ARCDIR"

		systemctl restart dmts-image
	fi

fi

if [ "$GLOB_DMTS_ENABLE_IMAGEMAGICK" = "ignore" ]; then

	:noop

elif [ -d "$GLOB_DMTS_BLENDER_INS" ]; then

	echo "*D* GLOB_DMTS_BLENDER_INS=$GLOB_DMTS_BLENDER_INS exists, Blender already installed"

else

	if [ "$GLOB_DMTS_ADD_BLENDER" = "ignore" ]; then

		echo "*D* Installation of Blender administratively denied"

	else

		echo "*D* GLOB_DMTS_ADD_BLENDER=$GLOB_DMTS_ADD_BLENDER, Installation of Blender"

        	install_dmts_blender \
			-DmtsHome "$GLOB_DMTSINS" \
	                -User "$GLOB_USER" \
			-Group "$GLOB_GROUP" \
			-InstallDir "$GLOB_DMTS_BLENDER_INS" \
       	         	-Archive "$GLOB_LMNT/linux/$GLOB_DMTS_BLENDER_ARC" \
			-GitfArchive "$GLOB_LMNT/$GLOB_DMTS_BLENDER_GTIFARC"

		systemctl restart dmts-image
	fi

fi

if [ "$PauseAt" = "FIVE" ]; then
	echo "*D* FIVE"
	exit 1
fi

#
# OTMM in wildfly
# * Install Wildfly and rollout OTMM ontop of it
#
if [ -d "$GLOB_MMINS" ] && [ -z "${Skip_install_detection_otmm:-}" ]; then
        echo "*D* $GLOB_MMINS exists, ignoring MediaManager installer."

elif [ "$GLOB_OTMM_ENABLE" = "ignore" ]; then
	echo "*D* OTMM installation administratively denied..."

elif 	[ "$GLOB_OTMM_ENABLE" = "tomee" ] || \
	[ "$GLOB_OTMM_ENABLE" = "tomee_embed" ]; then

	if [ -z "$OtmmSetTranscoder" ] || \
	   [ "$OtmmSetTranscoder" = "ignore" ]; then 
		echo "*E* OtmmSetTranscoder='$OtmmSetTranscoder', Invalid argument" 
		exit 1
	fi

	#sanity check - for multinode rollout
	#fail if not reachable
	if 	[ "$GLOB_OTMM_DB_TYPE" = "local_postgres" ] || \
		[ "$GLOB_OTMM_DB_TYPE" = "postgres"]; then
		otmm_validate_postgres \
			-DbHost "$GLOB_HOST_PGSQL" \
			-DbHostPort "$GLOB_HOST_PGSQL_PORT"
	fi

	# 
	# this mainly calls the OTDS function for
	# purging Resource and Partition from a 
	# previous installation attempt. That is
	# only needed if OTMM installs the Schema
	#
	otdspurge='ignore'
	if [ "$GLOB_OTMM_INSTALL_SCHEMA" = "install" ] && \
	   [ "$GLOB_OTMM_DESTROY_SCHEMA" = "destroy" ]; then
	   echo "*D* Installer configures GLOB_OTMM_INSTALL_SCHEMA=$GLOB_OTMM_INSTALL_SCHEMA, "\
		   "GLOB_OTMM_DESTROY_SCHEMA=$GLOB_OTMM_DESTROY_SCHEMA "\
		   "so destroy existing OTDS Resource and Partition" 
	   otdspurge='purge'
	fi
	preinstall_otmm_otds \
	        -OtdsPreinstallPurge "$otdspurge" \
                -OtdsAdminUser "$GLOB_OTDS_ADMINDOMAIN" \
                -OtdsAdminUserPw "$GLOB_OTDS_PASS" \
                -OtdsUrlNode  "${GLOB_HOST_OTDS_PROTO_EXT}://${GLOB_HOST_OTDS_PRIMARY}:${GLOB_HOST_OTDS_PORT_EXT}"


	#
	# if I am the first install server, install the schema
	# otherwise just install OTMM without schema
	#
	# This logfile check makes this method rerunnable to force 
	# reconfiguration of a bad install
	#
	# TODO: refactor installer to perform incremental changes
	#       for minor tasks, like shares and log level for example
	#
	if [ -d "$GLOB_MMINS/ddl" ]; then
		echo "*W* Rerun of install_otmm() administratively denied, skipping..."
	else

		# safer and double safer checks before
		# destruction of exsting user, schema and data
		# on the database
		if [ "$OtmmDbType" = "oracle" ]; then

			DestroyOtmmDBUser="ignore"
			DestroyOtmmDBTablespace="ignore"

			if [ "$GLOB_OTMM_DESTROY_SCHEMA" = "destroy" ]; then
				echo "*D* GLOB_OTMM_ORACLE_USER_DESTROY=$GLOB_OTMM_ORACLE_USER_DESTROY, "`
					`"destroying existing $GLOB_OTMM_DB_USER"
				GLOB_OTMM_ORACLE_USER_DESTROY="1"
				DestroyOtmmDBUser="destroy"
				if [ "$GLOB_HOST_ORACLE_STORAGE" = "manual" ] || \
					[ -z "$GLOB_HOST_ORACLE_STORAGE" ] || \
					[ "$GLOB_HOST_ORACLE_STORAGE" = "*undef*" ] ; then 
					echo "*D* GLOB_OTMM_ORACLE_STORAGE_DESTROY=$GLOB_OTMM_ORACLE_STORAGE_DESTROY, "`
						`"destroying existing tablespace"
					GLOB_OTMM_ORACLE_STORAGE_DESTROY="0"
				else
					GLOB_OTMM_ORACLE_STORAGE_DESTROY="1"
					DestroyOtmmDBTablespace="destroy_otmm_ts"
				fi
			else
				echo "*D* GLOB_OTMM_ORACLE_USER_DESTROY=$GLOB_OTMM_ORACLE_USER_DESTROY, "`
					`"leaving database untouched."
				GLOB_OTMM_ORACLE_USER_DESTROY="0"
				GLOB_OTMM_ORACLE_STORAGE_DESTROY="0"
			fi

		elif [ "$OtmmDbType" = "local_postgres" ] || \
		     [ "$OtmmDbType" = "postgres" ]; then

			DestroyOtmmDBUser="ignore"
			DestroyOtmmDBTablespace="ignore"

		else 
			echo "*D* OtmmDbType=$OtmmDbType, invalid argument"
			exit 1

		fi


		install_otmm \
			-OtmmRabbitNode "$GLOB_OTMM_RMQ_HOST" \
                        -OtmmRabbitNodePort "$GLOB_OTMM_RMQ_HOST_PORT" \
                        -OtmmRabbitUser "$GLOB_DMTS_NODE_USERNAME" \
                        -OtmmRabbitUserPw "$GLOB_DMTS_NODE_USERNAME_PW" \
			-OtmmBaseUrl "$GLOB_OTMM_BASEURL" \
			-OtdsBaseUrl "$GLOB_OTMM_OTDS_BASEURL" \
			-OtdsPassword "$GLOB_OTDS_PASS" \
			-SupportManagerUrl "$GLOB_OTMM_SUPPORTMGR_URL" \
                        -DmtsBaseUrl "$GLOB_OTMM_DMTS_BASEURL" \
			-DmtsShare "$GLOB_DMTS_ROOT" \
			-YumFlags "$YumFlags" \
			-ServiceTemplate "$GLOB_CONFDIR/$GLOB_OTMM_SERVICE_TMPL" \
			-IndexServiceTemplate "$GLOB_CONFDIR/$GLOB_OTMM_INDEX_SERVICE_TMPL" \
			-StopAtInstaller "$GLOB_STOPATINSTALLER" \
			-Archive "$GLOB_OMNT/$GLOB_MMARC" \
			-InstallDir $GLOB_MMINS \
			-ConfigTemplate $GLOB_CONFDIR/$GLOB_MM_CONFTMPL \
			-TmpMount $GLOB_MMARCDIR \
			-DoDestroyExistingSchema "$GLOB_OTMM_DESTROY_SCHEMA" \
			-DoInstallSchema "$GLOB_OTMM_INSTALL_SCHEMA" \
			-Host $GLOB_HOST \
			-DbType $GLOB_OTMM_DB_TYPE \
			-PgInstallDir "$GLOB_HOST_PGSQL_DATADIR" \
			-Group $GLOB_GROUP \
			-SqlTool "java -Djava.security.egd=file:/dev/./urandom -jar $GLOB_CONFDIR/sqltool.jar" \
			-DbSysUser "$GLOB_DB_SYS_USER" \
			-DbSysUserPass "$GLOB_DB_SYS_PASS" \
			-OtmmDbTeamsUser $GLOB_OTMM_DB_USER \
			-OtmmDbName $GLOB_OTMM_DB_NAME \
			-JdbcConnection "$GLOB_DB_SYS_JDBC" \
			-ConfigureOtmm "$GLOB_OTMM_ENABLE" \
			-ConfigureTranscode "$GLOB_TRANSCODE_ENABLE" \
			-TeamsRepository "$GLOB_REPOSITORY_HOME" \
			-TeamsHome "$GLOB_MMINS" \
			-Tomee_Home "$GLOB_EEINS" \
			-User "$GLOB_USER" \
			-TranscodeWorkingArea "$GLOB_DMTS_ROOT" \
			-AsInstallDir "$GLOB_EEINS" \
			-TeamsDbDir "$GLOB_HOST_ORACLE_STORAGE" \
			-DestroyOtmmDBUser "$DestroyOtmmDBUser" \
			-DestroyOtmmDBTablespace "$DestroyOtmmDBTablespace" \
			-DestroySqlTemplate "$GLOB_CONFDIR/$GLOB_OTMM_ORACLE_DESTROY_SQLTEMPL" \
			-JavaHome "$GLOB_JAVAHOME" 
	fi

	#
	# perform the jboss configuration in otmm.xml
	#
	#postinstall_otmm_jboss \
	#	-Host "$GLOB_HOST" \
	#	-User "$GLOB_USER" \
	#	-Group $GLOB_GROUP \
	#	-TeamsHome $GLOB_MMINS \
	#	-InstallDir $GLOB_WFINS

	if 	[ "$GLOB_OTMM_ENABLE" = "tomee" ] || \
		[ "$GLOB_OTMM_ENABLE" = "tomee_embed" ]; then
		otmm_tomee_postinstall \
			-TomeeHome "$GLOB_EEINS" \
			-TeamsHome "$GLOB_MMINS" \
			-Host "$GLOB_HOST"

	fi

	#
	# OTMM hardening as per hardening guide
	#
	#if [ -n "$GLOB_OTMM_OTDS_SOURCE_WHITELIST" ]; then
	#	configure_otmm_otds_whitelist \
	#		-InstallDir "$GLOB_WFINS" \
	#		-FilterList "$GLOB_OTMM_OTDS_SOURCE_WHITELIST"
	#fi

	#
	# Configure certificate
	#
	# Note: the following assumes the default Alias and keystore 
	#       names as per installer. Do not change GLOB_WFINS_KEYSTORE
	#       and GLOB_WFINS_KEYSTORE_ALIAS without changing the
	#       otmm.xml file
	#	

	if [ "$GLOB_OTMM_PROTO" = "https" ]; then

		echo "*D* HTTPS not configured, running the default self signed certificate on 11443"
		exit 1

	fi

	#Simplify names as OTDS doesn't recognise 443 automatically for https 
	if 	[ "$GLOB_OTMM_EXT_PROTO" = "https" -a "$GLOB_OTMM_EXT_PORT" = "443" ] || \
		[ "$GLOB_OTMM_EXT_PROTO" = "https" -a "$GLOB_OTMM_EXT_PORT" = "80" ]; then
		WHITELIST="$GLOB_OTMM_EXT_PROTO://$GLOB_HOST/ $WhitelistNodes"
	else
		WHITELIST="$GLOB_OTMM_EXT_PROTO://$GLOB_HOST:$GLOB_OTMM_EXT_PORT/ ${WhitelistNodes:-}"
	fi
	if [ "$GLOB_APACHE_ENABLE" = "proxy" -a -n "${ApacheLbHost:-}" ]; then
		WHITELIST="$WHITELIST http://$ApacheLbHost/"
	fi

	postinstall_otmm_otds \
		-WhitelistNodes "$WHITELIST" \
        	-OtdsAdminUser "$GLOB_OTDS_ADMINDOMAIN" \
        	-OtdsAdminUserPw "$GLOB_OTDS_PASS" \
        	-OtdsUrlNode  "${GLOB_HOST_OTDS_PROTO_EXT}://${GLOB_HOST_OTDS_PRIMARY}:${GLOB_HOST_OTDS_PORT_EXT}"


	#This step is only needed for the Solr Cloud but runs on the
	#OTMM server. It requires the pgagent to be installed with the
	#Postgres installation.
	if [ $GLOB_OTMM_DB_TYPE = 'local_postgres' ] && \\
	   [ $GLOB_CONF_SOLR = 'cloud' ] ; then
		postinstall_pgsql_otmm_solrcloud \
			-TeamsHome "$GLOB_MMINS" \
			-SqlTool "java -Djava.security.egd=file:/dev/./urandom -jar $GLOB_CONFDIR/sqltool.jar" \
			-JdbcConnection "$GLOB_DB_OTMM_JDBC" \
       			-DbSysUser "$GLOB_HOST_PGSQL_SYS_USER" \
	                -DbSysUserPw "$GLOB_HOST_PGSQL_SYS_USER_PW" \
			-DbOtmmUser "$GLOB_OTMM_DB_USER" \
	                -PgAgentService "pgagent_10.service" \
	                -PgAgentDbName "$GLOB_OTMM_DB_NAME" \
	                -PgAgentDbUser "$GLOB_OTMM_DB_USER" \
	                -PgAgentDbUserPw "$GLOB_OTMM_DB_USER_PW" \
			-User "$GLOB_USER" \
			-IsPrimaryNode "yes"
	fi

	#
	# Setup RC files after OTMM is rolled out.
	#
	setup_bashrc_otmm \
		-Aliases "mm_log=less \${TEAMS_HOME}/logs/mediamanager-appserver.log" \
		-InstallDir $GLOB_INST \
		-User $GLOB_USER \
		-FfmpegHome $GLOB_FF_INSDIR \
		-TeamsHome $GLOB_MMINS

        #
        # Configure outbound SMTP server
        #
        if [ "$GLOB_SMTP_TYPE" = 'ignore' ]; then
                echo "*D* SMTP configuration administratively denied..."
        elif [ "$GLOB_SMTP_TYPE" = 'mailtrap' ]; then
                echo "*D* SMTP configuration for $GLOB_SMTP_SERVER ..."
                smtp_outbound_otmm  \
                        -SmtpUser $GLOB_SMTP_USER \
                        -SmtpUserPw $GLOB_SMTP_USER_PW \
                        -SmtpGateway $GLOB_SMTP_SERVER \
                        -TeamsHome $GLOB_MMINS \
                        -DbType $GLOB_OTMM_DB_TYPE \
                        -DbUser $GLOB_OTMM_DB_USER \
                        -DbUserPw $GLOB_OTMM_DB_USER_PW \
                        -SqlTool "java -jar $GLOB_CONFDIR/sqltool.jar" \
                        -JdbcConnection "$GLOB_DB_OTMM_JDBC"
        else
                echo "*F* SMTP configuration invalid : $GLOB_SMTP_TYPE"
                exit 1
        fi

	echo "Manual Steps: using the http://$GLOB_HOST:11090/teams interface, create a Normal property template"
	systemctl start otmm.service

else

        echo "*E* OTMM installation for GLOB_OTMM_ENABLE=$GLOB_OTMM_ENABLE not configured..."
        exit 1

fi


#
# Configure AMD
#
if [ "$GLOB_OTMM_ENABLE" = "ignore" ]; then
	echo "*D* OTMM not configured here, so skipping installing AMD"

elif [ "$GLOB_OTMM_AMD_ENABLE" = "ignore" ]; then
	echo "*D* AMD installation administratively denied..."

elif systemctl status adaptivemediadelivery.service; then

	echo "*D* AMD installation already intalled, skipping..."

else
	echo "*D* AMD installation running..."
	amd_install \
		-StopAtInstaller "$GLOB_STOPATINSTALLER" \
		-DoInstallSchema "$GLOB_OTMM_INSTALL_SCHEMA" \
		-DoDestroyExistingSchema "$GLOB_OTMM_DESTROY_SCHEMA" \
		-DbSysUser $GLOB_DB_SYS_USER \
		-DbSysUserPw $GLOB_DB_SYS_PASS \
		-SqlTool "java -Djava.security.egd=file:/dev/./urandom -jar $GLOB_CONFDIR/sqltool.jar" \
		-ConfigTemplate "$GLOB_CONFDIR/$GLOB_AMDCONFTEMPL" \
		-TmpMount "$GLOB_MMARCDIR" \
		-Archive "$GLOB_OMNT/$GLOB_MMARC" \
		-InstallDir "$GLOB_AMDINS" \
		-DbType "$GLOB_OTMM_DB_TYPE" \
		-AmdDbUser "$GLOB_DB_AMD_USER" \
		-AmdDbUserPw "$GLOB_DB_AMD_USER_PW" \
		-AmdDbName "$GLOB_DB_AMD_DBNAME" \
		-User "$GLOB_USER" \
		-Group "$GLOB_GROUP" \
		-JavaHome $GLOB_JAVAHOME \
		-JdbcAmdConnection "$GLOB_DB_AMD_JDBC" \
		-JdbcSysConnection "$GLOB_DB_SYS_JDBC"
	echo "*D* AMD installation done."

	amd_enable_in_otmm \
		-TeamsHome "$GLOB_MMINS" \
		-DbType "$GLOB_OTMM_DB_TYPE" \
	        -DbUser "$GLOB_OTMM_DB_USER" \
        	-DbTeamsUserPw "$GLOB_OTMM_DB_USER_PW" \
        	-JdbcConnection "$GLOB_DB_OTMM_JDBC" \
        	-AmdIdentifier "$GLOB_AMD_ID" \
        	-AmdProto "$GLOB_AMD_PROTO" \
		-AmdHost "$GLOB_HOST" \
        	-AmdPort "$GLOB_AMD_PORT" \
        	-AmdUser "$GLOB_AMD_USER" \
        	-AmdUserPw "$GLOB_AMD_USER_PW" \
        	-User "$GLOB_USER" \
		-Group "$GLOB_GROUP"

	amd_install_service \
        	ServiceTemplate "$GLOB_CONFDIR/$GLOB_AMD_SERVICE_TMPL" \
        	InstallDir "$GLOB_AMDINS"
fi


#
# Install SOLR
# this should only run on the Solr nodes
#

if [ -d "$GLOB_SOINS" ]; then
        echo "*D* $GLOB_SOINS exists, ignoring SOLR installer...."

elif [ "$GLOB_CONF_SOLR" = "ignore" ]; then
        echo "*D* Search installation administratively denied..."

elif [ "$GLOB_CONF_SOLR" = "standalone" ]; then
        echo "*D* GLOB_CONF_SOLR = $GLOB_CONF_SOLR"

        install_user \
                -ConfigTemplate $GLOB_CONFDIR/$GLOB_LOGIN_CONF \
                -User "$GLOB_SO_USER" \
                -Group "$GLOB_GROUP" \
                -UserHome "$GLOB_INST" \
                -PasswordSeed "secret"

        install_solr_service \
		-ServiceTemplate "$GLOB_CONFDIR/$GLOB_SOLR_SERVICE_TEMPL" \
		-RegisterWithOtmm "$GLOB_SOLR_REGISTER_WOTMM" \
		-StopAtInstaller "$GLOB_STOPATINSTALLER" \
                -OtmmHost "$GLOB_HOST_OTMM" \
                -OtmmInstallDir "$GLOB_MMINS" \
                -SolrInstallationType "STANDALONE" \
                -JavaHome "$GLOB_JAVAHOME" \
                -TmpMount "$GLOB_MMARCDIR" \
                -Archive "$GLOB_OMNT/$GLOB_MMARC" \
                -ConfigTemplate "$GLOB_SOLR_CONFTMPL" \
                -OtmmDbType "$GLOB_OTMM_DB_TYPE" \
                -InstallDir "$GLOB_SOINS" \
                -InstallIndexDir "$GLOB_SODATA" \
                -Group "$GLOB_GROUP"

	if [ "$GLOB_SOLR_SETUP_ULIMIT" = "yes" ]; then
		configure_solr_limit \
			-User "$GLOB_SO_USER"
	fi

	# Configured as per hardening guide
	if [ -n "$GLOB_SOLR_SOURCE_WHITELIST" ]; then
		configure_solr_source_whitelist \
			-SolrLeaf "$GLOB_SOARC_LEAF" \
			-InstallDir "$GLOB_SOINS" \
			-FilterList "$GLOB_SOLR_SOURCE_WHITELIST"
	fi

	systemctl restart solr.service
	if [ -x $GLOB_SOINS/bin/OpenText-Media-Management-Search-process ]; then
	        su -l -c "$GLOB_SOINS/bin/OpenText-Media-Management-Search-process start" $GLOB_SO_USER
	fi

elif [ "$GLOB_CONF_SOLR" = "slave" ]; then
        echo "*D* GLOB_CONF_SOLR = $GLOB_CONF_SOLR"

        if [ -d $GLOB_SODATA ]; then
                echo "*D* $GLOB_SODATA exists, skipping install"
        else
                install_solr_service \
			-ServiceTemplate "$GLOB_CONFIDIR/$GLOB_SOLR_SERVICE_TEMPL" \
			-RegisterWithOtmm "$GLOB_SOLR_REGISTER_WOTMM" \
			-StopAtInstaller "$GLOB_STOPATINSTALLER" \
                	-OtmmHost "$GLOB_HOST_OTMM" \
                        -OtmmInstallDir "$GLOB_MMINS" \
                        -SolrUsrHome "$GLOB_INST/$GLOB_SO_USER" \
                        -SolrInstallationType "SLAVE" \
                        -JavaHome "$GLOB_JAVAHOME" \
                        -TmpMount "$GLOB_MMARCDIR" \
                        -Archive "$GLOB_OMNT/$GLOB_MMARC" \
			-ArchiveFile "*undef*" \
			-ArchiveLeaf "$GLOB_SOARC_LEAF" \
                        -ConfigTemplate "$GLOB_SOLR_CONFTMPL" \
                        -DbPsqlName "$GLOB_OTMM_DB_NAME" \
                        -DbOraSid "$GLOB_HOST_ORACLE_SID" \
                        -User "$GLOB_SO_USER" \
                        -UserPw "secret" \
                        -InstallDir "$GLOB_SOINS" \
                        -InstallIndexDir "$GLOB_SODATA" \
                        -Group "$GLOB_GROUP"

	        if [ "$GLOB_SOLR_SETUP_ULIMIT" = "yes" ]; then
			configure_solr_limit \
				-User "$GLOB_SO_USER"
		fi

		# Configured as per hardening guide
		if [ -n "$GLOB_SOLR_SOURCE_WHITELIST" ]; then
			configure_solr_source_whitelist \
				-InstallDir "$GLOB_SOINS" \
				-FilterList "$GLOB_SOLR_SOURCE_WHITELIST"
		fi

	        /etc/init.d/solr start
		if [ -x $GLOB_SOINS/bin/OpenText-Media-Management-Search-process ]; then
		        su -l -c "$GLOB_SOINS/bin/OpenText-Media-Management-Search-process start" $GLOB_SO_USER
		fi

        fi


elif [ "$GLOB_CONF_SOLR" = "master" ]; then
	echo "*D* GLOB_CONF_SOLR = $GLOB_CONF_SOLR"

	if [ -d $GLOB_SODATA ]; then
		echo "*D* $GLOB_SODATA exists, skipping install"
	else
		install_solr_service \
			-ServiceTemplate "$GLOB_CONFIDIR/$GLOB_SOLR_SERVICE_TEMPL" \
			-RegisterWithOtmm "$GLOB_SOLR_REGISTER_WOTMM" \
                	-OtmmHost "$GLOB_HOST_OTMM" \
			-OtmmInstallDir "$GLOB_MMINS" \
			-SolrUsrHome "$GLOB_INST/$GLOB_SO_USER" \
			-SolrInstallationType "MASTER" \
			-JavaHome "$GLOB_JAVAHOME" \
			-TmpMount "$GLOB_MMARCDIR" \
			-Archive "$GLOB_OMNT/$GLOB_MMARC" \
			-ArchiveFile "*undef*" \
			-ArchiveLeaf "$GLOB_SOARC_LEAF" \
			-ConfigTemplate "$GLOB_SOLR_CONFTMPL" \
			-DbPsqlName "$GLOB_OTMM_DB_NAME" \
			-DbOraSid "$GLOB_HOST_ORACLE_SID" \
			-User "$GLOB_SO_USER" \
			-UserPw "secret" \
			-InstallDir "$GLOB_SOINS" \
			-InstallIndexDir "$GLOB_SODATA" \
			-Group "$GLOB_GROUP"
	
	        if [ "$GLOB_SOLR_SETUP_ULIMIT" = "yes" ]; then
			configure_solr_limit \
				-User "$GLOB_SO_USER"
		fi

		if [ -n "$GLOB_SOLR_SOURCE_WHITELIST" ]; then
			configure_solr_source_whitelist \
				-FilterList "$GLOB_SOLR_SOURCE_WHITELIST"
		fi

	        systemctl restart solr.service
		if [ -x $GLOB_SOINS/bin/OpenText-Media-Management-Search-process ]; then
		        su -l -c "$GLOB_SOINS/bin/OpenText-Media-Management-Search-process start" $GLOB_SO_USER
		fi

        fi

elif [ "$GLOB_CONF_SOLR" = "cloud" ]; then

        #the installer needs access to the database settings via EJB :(
        #startup OTMM now - somewhere
        #su -l -c "$GLOB_MMINS/bin/mediamanagement-process-manager start" $GLOB_USER
	systemctl start otmm.service

        install_user \
                -ConfigTemplate $GLOB_CONFDIR/$GLOB_LOGIN_CONF \
                -User "$GLOB_SO_USER" \
                -Group "$GLOB_GROUP" \
                -UserHome "$GLOB_INST" \
                -PasswordSeed "secret"

        install_zookeeper_service \
		-StopAtInstaller "$GLOB_STOPATINSTALLER" \
                -InstallDir "$GLOB_ZKINS" \
                -JavaHome "$GLOB_JAVAHOME" \
                -TmpMount "$GLOB_MMARCDIR" \
                -Archive "$GLOB_OMNT/$GLOB_MMARC" \
                -ConfigTemplate "$GLOB_CONFDIR/GLOB_ZK_CONFTMPL" \
                -SolrUsrHome "$GLOB_INST/$GLOB_SO_USER" \
                -User "$GLOB_SO_USER" \
                -UserPw "secret" \
                -Group "$GLOB_GROUP"

        install_solr_service \
		-RegisterWithOtmm "$GLOB_SOLR_REGISTER_WOTMM" \
                -StopAtInstaller "$GLOB_STOPATINSTALLER" \
                -OtmmHost "$GLOB_HOST_OTMM" \
                -OtmmInstallDir "$GLOB_MMINS" \
                -SolrUsrHome "$GLOB_INST/$GLOB_SO_USER" \
                -SolrInstallationType "CLOUD" \
                -ZookeeperNodes "$GLOB_CLIENT_ZK_NODES" \
                -JavaHome "$GLOB_JAVAHOME" \
                -TmpMount "$GLOB_MMARCDIR" \
                -Archive "$GLOB_OMNT/$GLOB_MMARC" \
		-ArchiveLeaf "$GLOB_SOARC_LEAF" \
		-ArchiveFile "*undef*" \
                -ConfigTemplate "$GLOB_SOLR_CONFTMPL" \
                -DbPsqlName "$GLOB_OTMM_DB_NAME" \
                -DbOraSid "$GLOB_HOST_ORACLE_SID" \
                -User "$GLOB_SO_USER" \
                -UserPw "secret" \
                -InstallDir "$GLOB_SOINS" \
                -InstallIndexDir "$GLOB_SODATA" \
                -Group "$GLOB_GROUP"

	if [ "$GLOB_SOLR_SETUP_ULIMIT" = "yes" ]; then
		configure_solr_limit \
			-User "$GLOB_SO_USER"
	fi

	if [ -n "$GLOB_SOLR_SOURCE_WHITELIST" ]; then
		configure_solr_source_whitelist \
			-FilterList "$GLOB_SOLR_SOURCE_WHITELIST"
	fi


else
        echo "*F* unknown SOLR configuration type GLOB_CONF_SOLR=$GLOB_CONF_SOLR"
        exit 1
fi


#
# Index registration in OTMM - this must run on the every OTMM server
#
if [ "$GLOB_REGISTER_SOLR" = 'ignore' ]; then

	echo "*D* Solr registration administratively denied"

elif [ "$GLOB_REGISTER_SOLR" = 'master' ] || [ "$GLOB_REGISTER_SOLR" = 'standalone' ]; then

	#Solr must be running for the registration but Solr could be elsewhere
	#so assume Solr is running otherwise
	if [ "$GLOB_CONF_SOLR" = "ignore" ]; then
		SolrInstallDir="/x"; #directory does not exists
	else
		SolrInstallDir="$GLOB_SOINS"
	fi

        post_configure_solr \
		-SolrInstallDir "$SolrInstallDir" \
                -SolrType "$GLOB_REGISTER_SOLR" \
                -SolrMasterHost "$GLOB_HOST_SOLR_MASTER" \
                -SolrMasterPort "$GLOB_HOST_SOLR_MASTER_PORT" \
                -SolrNodeHost "$GLOB_HOST_SOLR_NODE" \
                -SolrNodePort "$GLOB_HOST_SOLR_NODE_PORT" \
                -User "$GLOB_USER" \
                -OtmmInstallDir "$GLOB_MMINS" \
		-SolrInstallDir "$SolrInstallDir" \
                -NumShards "*undef*" \
                -NumReplicas "*undef*" \
                -MaxShardsPerNode "*undef*" \

elif [ "$GLOB_REGISTER_SOLR" = 'slave' ]; then

        post_configure_solr \
                -SolrType "$GLOB_REGISTER_SOLR" \
                -SolrMasterHost "$GLOB_HOST_SOLR_MASTER" \
                -SolrMasterPort "$GLOB_HOST_SOLR_MASTER_PORT" \
                -SolrNodeHost "$GLOB_HOST_SOLR_NODE" \
                -SolrNodePort "$GLOB_HOST_SOLR_NODE_PORT" \
                -User "$GLOB_USER" \
                -OtmmInstallDir "$GLOB_MMINS" \
		-SolrInstallDir "$SolrInstallDir" \
                -NumShards "*undef*" \
                -NumReplicas "*undef*" \
                -MaxShardsPerNode "*undef*" \

elif [ "$GLOB_REGISTER_SOLR" = 'cloud' ]; then

        post_configure_solr \
                -SolrType "$GLOB_REGISTER_SOLR" \
                -SolrMasterHost "$GLOB_HOST_SOLR_MASTER" \
                -SolrMasterPort "$GLOB_HOST_SOLR_MASTER_PORT" \
                -SolrNodeHost "$GLOB_HOST_SOLR_NODE" \
                -SolrNodePort "$GLOB_HOST_SOLR_NODE_PORT" \
                -User "$GLOB_USER" \
                -OtmmInstallDir "$GLOB_MMINS" \
		-SolrInstallDir "$SolrInstallDir" \
                -NumShards "*undef*" \
                -NumReplicas "*undef*" \
                -MaxShardsPerNode "*undef*" \
else
	echo "*F* invalid \$GLOB_REGISTER_SOLR = $GLOB_REGISTER_SOLR"
	exit 1
fi


#
# DigitalHub install
# * Install Tomcat and rollout DigitalHub ontop of it
#

if [ -d "$GLOB_DHINS" ] && [ -z "${Skip_install_dh:-}" ]; then
        echo "*D* $GLOB_DHINS exists, ignoring DigitalHub installer."

elif [ "$GLOB_DH_ENABLE" = "ignore" ]; then
        echo "*D* DigitalHub installation administratively denied..."

elif [ "$GLOB_DH_ENABLE" = "ignore" ]; then
        echo "*D* DigitalHub installation administratively denied..."

elif [ "$GLOB_DH_ENABLE" = "tomcat" ]; then
	echo "*D* DigitalHub installation for $GLOB_DH_ENABLE..."

else
        echo "*E* DigitalHub installation for GLOB_DH_ENABLE=$GLOB_DH_ENABLE not configured..."
        exit 1
fi


#
# Apply OTMM upgrade
#
#
#
if [ "GLOB_OTMM_UPGRADE" = 'ignore' ]; then
	:
else
	case "$GLOB_OTMM_UPGRADE" in 
		16*u3)
			GLOB_OTMM_VERSION_UPGRADE_PATCH_NO="3"
			;;
		16*u4)
			GLOB_OTMM_VERSION_UPGRADE_PATCH_NO="4"
			;;
		16*u5)
			GLOB_OTMM_VERSION_UPGRADE_PATCH_NO="5"
			;;
		16*u11)
			GLOB_OTMM_VERSION_UPGRADE_PATCH_NO="11"
			;;
		16*u10)
			GLOB_OTMM_VERSION_UPGRADE_PATCH_NO="10"
			;;
		ignore)
			;;
		*)
			echo "*F* Upgrade OTMM to $GLOB_OTMM_UPGRADE not configured"
			exit 1
	esac
	GLOB_OTMM_VERSION_UPGRADE_DIR="Update $GLOB_OTMM_VERSION_UPGRADE_PATCH_NO"
	GLOB_OTMM_VERSION_UPGRADE_DIRNO=$(printf "%03d" $GLOB_OTMM_VERSION_UPGRADE_PATCH_NO)
fi
	
if [ "$GLOB_OTMM_ENABLE" = "ignore" ]; then
	echo "*D* OTMM not configured here, so skipping installing update"
elif [ "$GLOB_OTMM_UPGRADE" = "ignore" ]; then
	echo "*D* No incremental upgrade selected for OTMM"
else
	echo "*F* Upgrade OTMM to $GLOB_OTMM_UPGRADE not configured"
	exit 1
fi

#
# Apache installer
# * must run on the OTMM node(s) for streaming
# * should run for SSL termination on Apache together with 
#   whitelists on OTMM, Solr, OTDS and everywhere else
#
if [ "$GLOB_APACHE_ENABLE" = "ignore" ]; then
	echo "*D* Apache installation administratively denied..."
elif [ "$GLOB_APACHE_STREAMING" != "ignore" ] && [ "$GLOB_OTMM_DB_TYPE" = "*undef*" ]; then
	echo "*D* GLOB_APACHE_STREAMING=$GLOB_APACHE_STREAMING," \
		"GLOB_OTMM_DB_TYPE=$GLOB_OTMM_DB_TYPE, unable to configure Streaming"
	exit 1
elif [ "$GLOB_APACHE_ENABLE" = "sslproxy" ]; then
#		-User "$GLOB_USER" \
#		-Group "$GLOB_GROUP" \
	apache_install_proxy \
		-Group "$GLOB_GROUP" \
		-ApacheSslCertAliases "$GLOB_APACHE_SSL_CERT_DNS_ALIASES" \
		-ApacheConfigurationType "$GLOB_APACHE_ENABLE" \
		-ConfigurePgAdminUI "$GLOB_PGADMIN_ENABLE" \
		-ApachePort "$GLOB_APACHE_HTTPS_PORT" \
		-InstallDir "$GLOB_APACHEINS" \
		-DbType "$GLOB_OTMM_DB_TYPE" \
		-DbUser "$GLOB_OTMM_DB_USER" \
		-DbUserPw "$GLOB_OTMM_DB_USER_PW" \
		-JdbcConnection "$GLOB_DB_OTMM_JDBC" \
		-SqlTool "java -Djava.security.egd=file:/dev/./urandom -jar $GLOB_CONFDIR/sqltool.jar" \
		-RepositoryHome "$GLOB_REPOSITORY_HOME" \
		-ApacheProxyDocRoot "$GLOB_APACHE_PROXY_DOCROOT" \
                -LbHost "$GLOB_APACHE_LBHOST" \
		-OtCaPem "$GLOB_APACHE_CERT_CAPEM" \
		-OtCaKey "$GLOB_APACHE_CERT_CAKEY" \
		-OtCaPw "$GLOB_APACHE_CERT_CAPWD" \
		-CrtName "$GLOB_APACHE_CERT_CRTNAME" \
		-CsrName "$GLOB_APACHE_CERT_CSRNAME" \
		-KeyName "$GLOB_APACHE_CERT_KEYNAME" \
		-SslCnf "/etc/pki/tls/openssl_ot.cnf" \
		-ApacheCrtAliases "$GLOB_APACHE_SSL_CERT_DNS_ALIASES" \
		-ConfigStreaming "$GLOB_APACHE_STREAMING" \
		-ConfigTemplateSsl "$GLOB_CONFDIR/$GLOB_APACHE_SSL_CONFIG_TEMPL" \
		-ConfigTemplateApache "$GLOB_CONFDIR/$GLOB_APACHE_PROXY_CONFIG_TEMPL"
	apachectl start
elif [ "$GLOB_APACHE_ENABLE" = "proxy" ]; then
	apache_install_proxy \
		-Group "$GLOB_GROUP" \
		-ApacheConfigurationType "$GLOB_APACHE_ENABLE" \
		-ConfigurePgAdminUI "$GLOB_PGADMIN_ENABLE" \
		-InstallDir "$GLOB_APACHEINS" \
		-ApachePort "$GLOB_APACHE_HTTP_PORT" \
                -LbHost "$GLOB_APACHE_LBHOST" \
		-ApacheProxyDocRoot "$GLOB_APACHE_PROXY_DOCROOT" \
		-ConfigStreaming "$GLOB_APACHE_STREAMING" \
		-RepositoryHome "$GLOB_REPOSITORY_HOME" \
		-ConfigTemplateApache "$GLOB_CONFDIR/$GLOB_APACHE_PROXY_CONFIG_TEMPL" \
		-SqlTool "java -Djava.security.egd=file:/dev/./urandom -jar $GLOB_CONFDIR/sqltool.jar" \
		-DbType "$GLOB_OTMM_DB_TYPE" \
                -DbUser "$GLOB_OTMM_DB_USER" \
                -DbUserPw "$GLOB_OTMM_DB_USER_PW" \
                -DbHost "$GLOB_OTMM_DB_HOST" \
                -JdbcConnection "$GLOB_DB_OTMM_JDBC"
	apachectl start
else
	echo "*F* Invalid configuration for GLOB_APACHE_ENABLE: $GLOB_APACHE_ENABLE"
	exit 1
fi


#
# Startup OTMM
#
if [ \! "$GLOB_OTMM_ENABLE" = "ignore" ]; then
	if [ \! -f "$GLOB_MMINS/bin/mediamanagement-process-manager" ]; then
		#for -DryRun, catch this error
		echo "*E* Cannot find $GLOB_MMINS/bin/mediamanagement-process-manager, verify OTMM"
        else
		if $GLOB_MMINS/bin/mediamanagement-process-manager status; then
                	echo "MM already running..."
		else
			systemctl enable otmm.service 
		fi
		if $GLOB_MMINS/bin/indexer-process-manager status; then
                	echo "Indexer already running..."
		else
			systemctl enable index.service 
		fi
        fi
	#start after upgrade
	if [ -d "$GLOB_AMDINS" ]; then
		systemctl start adaptivemediadelivery.service
	fi
fi

#
# Configure RMA
#
if [ "$GLOB_OTMM_ENABLE" = "ignore" ]; then
	echo "*D* OTMM not configured here, so skipping installing RMA"
elif [ "$GLOB_RMA_ENABLE" = "ignore" ]; then
	echo "*D* RMA installation administratively denied..."
else
	echo "*D* RMA installation running..."
	rma_install \
                -StopAtInstaller "$GLOB_STOPATINSTALLER" \
		-ConfigTemplate "$GLOB_CONFDIR/$GLOB_RMACONFTEMPL" \
		-TmpMount "$GLOB_MMARCDIR" \
		-InstallDir "$GLOB_RMAINS" \
		-Archive "$GLOB_OMNT/$GLOB_MMARC"
	echo "*D* RMA installation done."
fi

#
# Cleanup of sensitive files
#
if [ "$GLOB_HOST_CLEANUP" = "yes" ]; then
	cleanup \
		-Empty "no" \
		-TmpMount "$GLOB_MMARCDIR" \
		-DmtsHome "$GLOB_DMTSINS" \
		-SolrHome "$GLOB_SOINS" \
		-AmdHome "$GLOB_AMDINS" \
		-TeamsHome "$GLOB_MMINS"
fi

#overwrites motd!!
if [ "$GLOB_COCKPIT_ENABLE" = "install" ]; then
        systemctl enable --now cockpit.socket
        systemctl start cockpit.socket
fi

if [ "$GLOB_MOTD_ENABLE" = "info" ]; then
	if [ "$GLOB_OTMM_UPGRADE" = 'ignore' ]; then
		GLOB_OTMM_VERSION_UPGRADE=""
	fi
	motd_info \
		-MotdTemplate "$GLOB_CONFDIR/$GLOB_MOTD_TEMPL" \
		-ShowCockpit "$GLOB_COCKPIT_ENABLE"
fi


## example bulkloader supprt
#yum install -y $YumFlags epel-release ; #get the repo, then restart yum to use the repo
#yum install -y $YumFlags perl-CPAN perl-WWW-Mechanize perl-DateTime perl-XML-TreeBuilder perl-LWP-Protocol-https perl-File-Slurp
