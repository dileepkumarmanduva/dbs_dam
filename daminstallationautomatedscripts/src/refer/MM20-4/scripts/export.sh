read -r -d '' DESC << EOM
Host:
* otmmdemo 192.168.18.100

OTMM:
* http://otmmdemo/otmm
* http://otmmdemo/teams
Version : 20.3
Access: tsuper/tsuper
Start/Stop: systemctl start otmm.service index.service

AMD (status: install):
* http://otmmdemo/adminservices
Access: amd/amd

SOLR standalone:
* http://otmmdemo/solr
Start/Stop: systemctl start solr.service

OTDS standalone:
* OTMM/Teams http://otmmdemo/otds-admin
Version: 20.2.3 OTDS-2023-Hotfix005
Access: otadmin@otds.admin/otadmin
Start/Stop: systemctl start tomcat_otds.service

DMTS standalone: All
* DMTS service/RMQ port: 5672
* OpenOffice: install
* Blender: install
Start/Stop: systemctl start dmts-service.service dmts-video.service dmts-image.service

Postgres 12:
PgAdmin web address: http://otmmdemo/pgadmin4
PgAdmin access: admin@localhost/letmein
System Database access: postgres/postgres1
Otmm Database: otmmdb
Otmm Database access: towner/towner


CentOS Cockit on https:/otmmdemo:9090 (vagrant/vagrant)

EOM

VBoxManage export otmmdemo \
	-o /Users/UK05858/OpenText\ Core/OTMM\ Demo\ Systems/OTMM\ 20.3\ Linux.ova \
	--vsys 0 \
		--product "Media Management/OTDS/SOLR/DMTS/AMD" \
		--vendor "OpenText PS" \
		--version "20.3" \
		--eula "For internal use only" \
		--description "$DESC"
