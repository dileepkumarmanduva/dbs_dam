#
# Install Adaptive Media Delivery
#

function amd_preconfigure_postgresql () {
	SqlSysUser="*undef*"
	SqlSysUserPw="*undef*"
	SqldAdmDbUser="amd"
	SqldAdmDbUserPw="amd"
	SqlAmdDbName="amd"
	SqlTool="java -jar SqlTool.jar"
	DoInstallSchema="*undef*"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	db_common_drop_database	 \
        	-DbSysUser "$SqlSysUser" \
        	-DbSysUserPw "$SqlSysUserPw" \
        	-DbUser "$SqldAdmDbUser" \
        	-DbName "$SqlAmdDbName" \
        	-DbType "local_postgres" \
        	-DoInstallSchema "$DoInstallSchema" \
        	-SqlTool "$SqlTool"

	db_common_create_database \
        	-DbSysUser "$SqlSysUser" \
        	-DbSysUserPw "$SqlSysUserPw" \
        	-DbUser "$SqldAdmDbUser" \
        	-DbUserPw "$SqldAdmDbUserPw" \
        	-DbName "$SqlAmdDbName" \
        	-DbType "local_postgres" \
        	-DoInstallSchema "$DoInstallSchema" \
        	-SqlTool "$SqlTool" 

}

function amd_preconfigure_oracle () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	echo "Oracle support not configured, yet..."
	exit 1
}

function amd_otmm_user () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi
	
}

#
# Build the silent installer file for OTMM
#
function install_amd_build_amdtxt () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        localconf=$(mktemp -t tmp.installer.amdconf.XXXXXXX.txt)
        cp $ConfigTemplate $localconf

        if [ "$DoInstallSchema" = "install" ]; then
                perl -pi -e 's/^CHOSEN_INSTALL_SET=.*/CHOSEN_INSTALL_SET=ApplicationandSchema/g' $localconf
        elif [ "$DoInstallSchema" = "ignore" ]; then
                perl -pi -e 's/^CHOSEN_INSTALL_SET=.*/CHOSEN_INSTALL_SET=Application/g' $localconf
        else
                echo "*F* Invalid flag DoInstallSchema=$DoInstallSchema"
                exit 1
        fi

	if [ "AmdCollectorEnabled" = "install" ]; then
                perl -pi -e 's/^IS_DATACOLLECTOR_INSTALLED=.*/IS_DATACOLLECTOR_INSTALLED=1/g' $localconf
	else
                perl -pi -e 's/^IS_DATACOLLECTOR_INSTALLED=.*/IS_DATACOLLECTOR_INSTALLED=/g' $localconf
                perl -pi -e 's/^DATACOLLECTOR_HOSTNAME=.*/DATACOLLECTOR_HOSTNAME=*undef*/g' $localconf
                perl -pi -e 's/^DATACOLLECTOR_SERVER_PORT=.*/DATACOLLECTOR_SERVER_PORT=*undef*/g' $localconf
	fi

        >&2 echo "*D* DbType = $DbType, configuration"
        if [ "$DbType" = "local_postgres" ]; then
                >&2 echo "*D* Postgres database, GLOB_OTMM_DB_NAME=$GLOB_OTMM_DB_NAME "
                perl -pi -e 's/^DB_TYPE=.*/DB_TYPE=POSTGRESQL/g' $localconf
                perl -pi -e 's/^ORACLE_SERVICE_NAME=.*/ORACLE_SERVICE_NAME=*undef*/g' $localconf
                #perl -pi -e 's/^POSTGRESQL_DB_NAME=.*/POSTGRESQL_DB_NAME='*undef*'/g' $localconf

        elif [ "$DbType" = "oracle" ]; then
                >&2 echo "*D* Oracle database ORACLE_SERVICE_NAME=$ORACLE_SERVICE_NAME, GLOB_OTMM_DB_NAME=$GLOB_OTMM_DB_NAME "
                perl -pi -e 's/^DB_TYPE=.*/DB_TYPE=ORACLE/g' $localconf
                #perl -pi -e 's/^ORACLE_SERVICE_NAME=.*/ORACLE_SERVICE_NAME=*undef*/g' $localconf
                perl -pi -e 's/^POSTGRESQL_DB_NAME=.*/POSTGRESQL_DB_NAME=*undef*/g' $localconf

        elif [ "$DbType" = "sqlserver" ]; then
                >&2 echo "*D* Sqlserver database GLOB_DB_POSTGRES_DB=$GLOB_DB_POSTGRES_DB, GLOB_OTMM_DB_NAME=$GLOB_OTMM_DB_NAME "
                perl -pi -e 's/^DB_TYPE=.*/DB_TYPE=SQLSERVER/g' $localconf
                perl -pi -e 's/^ORACLE_SERVICE_NAME=.*/ORACLE_SERVICE_NAME=*undef*/g' $localconf
                perl -pi -e 's/^POSTGRESQL_DB_NAME=.*/POSTGRESQL_DB_NAME=*undef*/g' $localconf

        else
                echo "*F* Unsupported db type $DbType"
        fi

        perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $localconf

        echo $localconf
}

function amd_install () {
	#-StopAtInstaller "$GLOB_STOPATINSTALLER" \
	#-DoInstallSchema "$GLOB_OTMM_INSTALL_SCHEMA" \
	#-DoDestroyExistingSchema "$GLOB_OTMM_DESTROY_SCHEMA" \
	#-DbSysUser $GLOB_DB_SYS_USER \
	#-DbSysUserPw $GLOB_DB_SYS_PASS \
	#-SqlTool "java -Djava.security.egd=file:/dev/./urandom -jar $GLOB_CONFDIR/sqltool.jar" \
	#-ConfigTemplate "$GLOB_CONFDIR/$GLOB_AMDCONFTEMPL" \
	#-TmpMount "$GLOB_MMARCDIR" \
	#-Archive "$GLOB_OMNT/$GLOB_MMARC" \
	#-InstallDir "$GLOB_AMDINS" \
	#-DbType "$GLOB_OTMM_DB_TYPE" \
	#-AmdDbUser "$GLOB_DB_AMD_USER" \
	#-AmdDbUserPw "$GLOB_DB_AMD_USER_PW" \
	#-AmdDbName "$GLOB_DB_AMD_DBNAME" \
	#-User "$GLOB_USER" \
	#-Group "$GLOB_GROUP" \
	#-JavaHome $GLOB_JAVAHOME \
	#-JdbcAmdConnection "$GLOB_DB_AMD_JDBC" \
	#-JdbcSysConnection "$GLOB_DB_SYS_JDBC"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        localconf=`install_amd_build_amdtxt \
                -ConfigTemplate $ConfigTemplate \
                -DoInstallSchema $DoInstallSchema \
                -DbType $DbType`
        diff -Nau $ConfigTemplate $localconf || /bin/true


        if [ -d "$TmpMount/OTMM" ]; then
                echo "$TmpMount exists, ignoring mounting of iso..."
        else
                mkdir -p $TmpMount
                mount -o loop $Archive $TmpMount
        fi


        if [ \! "$DoDestroyExistingSchema" = "destroy" ]; then

                echo "*D* DoDestroyExistingSchema=$DoDestroyExistingSchema, "`
			`"leaving any existing AMD schema untouched. "`
			`"The installer may fail if this is not a new installation."

        elif [ "$DoInstallSchema" = "install" ]; then

		db_common_drop_database \
        		-DbSysUser "DbSysUser" \
        		-DbSysUserPw "DbSysUserPass" \
        		-DbUser "$AmdDbUser" \
        		-DbName "*undef*" \
        		-DbType "$DbType" \
        		-DoInstallSchema "$DoInstallSchema" \
        		-DoDestroyTablespace "ignore" \
        		-DestroySqlTemplate "*undef*" \
        		-JdbcSysConnection "$JdbcSysConnection" \
        		-SqlTool "$SqlTool"

        else
                echo "*D* Schema installation administratively denied (DoInstallSchema=$DoInstallSchema)."
        fi

        if [ \! "$DoInstallSchema" = "install" ]; then
		echo "*D* DoInstallSchema=$DoInstallSchema, leaving any existing AMD schema untouched." 
	elif [ -f "$InstallDir/logs/install/createadminuser.log" ]; then
		echo "*D* $InstallDir/logs/install/createadminuser.log exists, skipping user creation"
	else

		db_common_create_database \
			-DbSysUser "$DbSysUser" \
        		-DbSysUserPw "$DbSysUserPw" \
        		-DbUser "$AmdDbUser" \
        		-DbUserPw "$AmdDbUserPw" \
        		-DbName "$AmdDbName" \
        		-DbType "$DbType" \
        		-DoInstallSchema "$DoInstallSchema" \
        		-JdbcSysConnection "$JdbcSysConnection" \
        		-SqlTool "$SqlTool"

	fi

	if [ -d $InstallDir ]; then
		echo "*D* $InstallDir exists, skipping installer"
	else
		_d="$TmpMount/AMD/Disk1/InstData/NoVM"
		if [ "$StopAtInstaller" = "yes" ]; then
			echo "*D* stopped at AMD installer"
			echo "*D* set export LAX_DEBUG=1 for more debugging"
			echo "*D* set export JAVA_HOME=$JavaHome"
			echo "*D* run $_d/install.bin -i silent -f $localconf"
			echo "*D* post cleanup with chown -R $User:$Group $InstallDir"
			exit 1
		else
			#as root!
			export JAVA_HOME=$JavaHome
			export LAX_DEBUG="1"
			echo "################################################################################"
			#export JAVA_TOOL_OPTIONS='-Dos.name="Windows 7"'
			oldp=$PATH
			PATH="$PATH:$JavaHome/bin"
			if $_d/install.bin LAX_VM "$JavaHome/bin/java" -i silent -f $localconf ; then
				echo "*D* Installer succeeded"
			else
				echo "*F* Installer failed"
				exit 1
			fi
			JAVA_TOOL_OPTIONS=""
			PATH=$oldp;
			echo "################################################################################"
			chown -R $User:$Group $InstallDir
		fi
	fi

	#
	# Configure shared repository with TEAMS_REPOSITORY_HOME
	#
	$SqlTool \
		-n "$AmdDbUser" \
		-p "$AmdDbUserPw" \
		-u "$JdbcAmdConnection" \
		-r "update amd_tenant_settings set svalue='Y' "`
			`"where tenant_id = ("`
				`"select tenant_id "`
				`"from amd_tenants "`
				`"where tenant_name='Default'"`
			`") and sid = 'OTMM_SHARED_REPOSITORY' "


	if /bin/su -c "grep ADAPTIVE_MEDIA_HOME \$HOME/.bashrc" $User; then
		echo "*D* ADAPTIVE_MEDIA_HOME already configured"
	else
		/bin/su -c "echo 'export ADAPTIVE_MEDIA_HOME=\"$InstallDir\"' >> \$HOME/.bashrc"  $User
	fi

	su -c "cd $InstallDir/bin; ADAPTIVE_MEDIA_HOME=\"$InstallDir\" ./adaptivemediaservice start" $User

}

#TODO: some logic for 
# 	for single node runs once 
#      	for multi node runs once per shared repo/tenant/otmm server 
function amd_enable_in_otmm () {
	TeamsHome=""
	DbTeamsUser="towner"
	DbTeamsUserPw="towner"
	JdbcConnection=""
	AmdIdentifier="1"
	AmdProto="http"
	AmdPort="8888"
	AmdUser="amd"
	AmdHost="*undef*"
	AmdUserPw="amd"
	User="otmm"
	Group="otmm"
	DbType="local_postgres"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	### ADAPTIVE_MEDIA_ENABLED
	db_common_set_otmm_settings \
		-DbType "$DbType" \
		-DbUser "$DbTeamsUser" \
		-DbUserPw "$DbTeamsUserPw" \
		-JdbcConnection "$JdbcConnection" \
		-Key "CONFIG" \
		-Component "ADAPTIVE_MEDIA" \
		-Name "ADAPTIVE_MEDIA_ENABLED" \
		-Value "Y"

        q="cd $TeamsHome/install/ant; "`
		`"ant configure-adaptive-media "`
			`"-Dnotifier.identifier='$AmdIdentifier' "`
			`"-Dnotifier.hostname='$AmdProto://$AmdHost:$AmdPort' "`
			`"-Dnotifier.user='$AmdUser' "`
			`"-Dnotifier.password='$AmdUserPw'" 
	su -l -c "$q" $User
}

#TODO
function amd_install_imagemagick () {
        ArchiveDir="/lf/linux/centos8"
        ReposRpm="/lf/epel-release-latest-8* /lf/remi-release-8* /lf/nux-dextop-release*el7.nux.noarch.rpm"
        Archives="nux-dextop-release*el7.nux.noarch.rpm remi-release-8* ufraw-*x86_64.rpm ImageMagick-7* ImageMagick-libs-7*"
        OfficeArchives=""

	while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi


        echo "*D* yum install $YumFlags -y $ReposRpm"
        (BASH_XTRACEFD=; yum install $YumFlags -y $ReposRpm)

        pgs="exiv2-devel gimp gimp-devel gtkimageview-devel "`
                `"lensfun-devel libexif-devel GConf2 djvulibre-devel "`
                `"fftw-devel jbigkit-devel libwebp-devel openjpeg2-devel "`
                `"jasper-devel ghostscript-devel"
        echo "*D* dnf -y --enablerepo=PowerTools install $pgs"
        (BASH_XTRACEFD=; dnf $YumFlags -y --enablerepo=PowerTools install $pgs)

        echo "*D* cd $ArchiveDir; yum install -y $(echo $Archives)"
        ( BASH_XTRACEFD=;cd $ArchiveDir; yum install $YumFlags -y $(echo $Archives)  )

        echo "*D* ufraw: $(ufraw-batch --version)"
        echo "*D* ImageMagick: $(magick -version)"

}

function amd_install_service () {
	ServiceTemplate="/vagrant/adaptivemedia.service.tmpl"
	InstallDir="/opt/mm/AMD"

	while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	cp $ServiceTemplate "$InstallDir/bin/adaptivemediadelivery.service"
        perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' \
                "$InstallDir/bin/adaptivemediadelivery.service"
        diff -Nau $ServiceTemplate "$InstallDir/bin/adaptivemediadelivery.service" || /bin/true
        systemctl enable "$InstallDir/bin/adaptivemediadelivery.service"
}
