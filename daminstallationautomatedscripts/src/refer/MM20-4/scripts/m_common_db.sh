#
# utility function for the database management
#
function db_common_drop_database () {

	DbSysUser="*undef*"
	DbSysUserPw="*undef*"
	DbUser="*undef*"
	DbName="*undef*"
        DbType="ignore"
        DoInstallSchema="ignore"
	DoDestroyTablespace="*undef*"
        DestroySqlTemplate="*undef*"
	JdbcSysConnection="*undef*"
	SqlTool="java -jar sqltool.jar"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ "$DoInstallSchema" = "install" ] ; then

		q="$SqlTool "`
			`"-n '$DbSysUser' "`
			`"-p '$DbSysUserPw' "`
			`"-u '$JdbcSysConnection' "

                if [ "$DbType" = "local_postgres" ]; then

			echo "*D* Drop items for DbType=$DbType, DbName=$DbName, DbUser=$DbUser"

			q1="$q -r 'drop database if exists ${DbName}'"
			echo "*D* $q1"
			eval "$q1"

			if [ "$DoDestroyTablespace" = "destroy_otmm_ts" ]; then

				echo "*D* DoDestroyTablespace=$DoDestroyTablespace, destroy OTMM tablepace"

				for n in "teams_uois_data" "teams_pm_idx" "teams_pm_data" \
					"teams_objstacks_idx" "teams_objstacks_data" "teams_cs_idx" \
					"teams_cs_data" "teams_context_idx" "teams_context_data" \
					"teams_catalog_idx" "teams_catalog_data" "teams_uois_idx"; do
                       		         q1="$q -r 'drop tablespace if exists ${n}'"
					echo "*D* $q1"
					eval "$q1"
				done

			else

				echo "*D* DoDestroyTablespace=$DoDestroyTablespace, do not touch tablespaces"

			fi


			q1="$q -r 'drop owned by ${DbUser} cascade'"
			echo "*D* $q1"
			eval "$q1"

			q1="$q -r 'drop role if exists ${DbUser}'"
			echo "*D* $q1"
			eval "$q1"


                elif [ "$DbType" = "oracle" ]; then

			echo "*D* Drop items for DbType=$DbType, DbName=$DbName, DbUser=$DbUser"

			q1="$q -r 'drop user $DbUser CASCADE"
			echo "*D* $q1"
			eval "$q1"

			if [ "$DoDestroyTablespace" = "destroy_otmm_ts" ]; then

				echo "*D* DoDestroyTablespace=$DoDestroyTablespace, destroy OTMM tablepace"

				#used for the OTMM destroy script
				localsql=$(mktemp -t tmp.purgeoracle..XXXXXXXX)
				cp "${DestroySqlTemplate}" "${localsql}"

				perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $localsql
				diff -Nau "${DestroySqlTemplate}" "${localsql}" || /bin/true

				echo "*I* DbSysUser=$DbSysUser, do "`
					`"destroy DbUser=$DbUser user"

				q="$SqlTool "`
					`"-n '$DbSysUser' "`
					`"-p '$DbSysUserPw' "`
					`"-u '$JdbcSysConnection' "`
					`"-e ${localsql}"
				echo "*D* $q"
				eval $q

			else
				echo "*D* DoDestroyTablespace=$DoDestroyTablespace, do not touch tablespaces"

			fi

                else
                        echo "*F* Unsupported database type DbType='$DbType'"
			exit 1
                fi

        else

                echo "*D* Schema changes administratively denied (DoInstallSchema=$DoInstallSchema)."

	fi
}

#
# Create a database and database user
#
function db_common_create_database () {
        DbSysUser="*undef*"
        DbSysUserPw="*undef*"
        DbUser="*undef*"
        DbUserPw="*undef*"
        DbName="*undef*"
        DbType="ignore"
        DoInstallSchema="ignore"
	JdbcSysConnection="*undef*"
        SqlTool="java -jar sqltool.jar"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        if [ "$DoInstallSchema" = "install" ] ; then

                q="$SqlTool "`
                        `"-n '$DbSysUser' "`
                        `"-p '$DbSysUserPw' "`
                        `"-u '$JdbcSysConnection' "        

                if [ "$DbType" = "local_postgres" ]; then

                        echo "*D* Create user DbType=$DbType, DbName=$DbName, DbUser=$DbUser"

                        q1="CREATE ROLE ${DbUser} with login password '${DbUserPw}'"
                        echo "*D* $q1"
                        eval "$q -r \"$q1\""
		
                        q1="CREATE DATABASE ${DbName} ENCODING 'UTF-8' LC_COLLATE 'en_US.UTF-8' LC_CTYPE 'en_US.UTF-8'"
                        echo "*D* $q1"
                        eval "$q -r \"$q1\""

                        q1="GRANT CONNECT, CREATE on DATABASE ${DbName} to ${DbUser}"
                        echo "*D* $q1"
                        eval "$q -r \"$q1\""
		
                        q1="GRANT CREATE, USAGE on SCHEMA public to ${DbUser}"
                        echo "*D* $q1"
                        eval "$q -r \"$q1\""
		

                elif [ "$DbType" = "oracle" ]; then

                        echo "*F* Drop items for DbType=$DbType, DbName=$DbName, DbUser=$DbUser"
                        echo "*F* This method is not implemented"
			exit 1

		else

                        echo "*F* Unsupported database type DbType='$DbType'"
                        exit 1

		fi

	else
                echo "*D* Schema changes administratively denied (DoInstallSchema=$DoInstallSchema)."

	fi

}

function db_common_set_otmm_settings () {
        DbUser="*undef*"
        DbUserPw="*undef*"
        DbType="ignore"
	JdbcConnection=""
        DoInstallSchema="ignore"
	JdbcSysConnection="*undef*"
        SqlTool="java -jar sqltool.jar"
	Component="ADAPTIVE_MEDIA"
	Key="CONFIG"
	Name="ADAPTIVE_MEDIA_ENABLED"
	Value="Y"
	Type="String"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        if [ "$DoInstallSchema" = "install" ] ; then

                q="$SqlTool "`
                        `"-n '$DbUser' "`
                        `"-p '$DbUserPw' "`
                        `"-u '$JdbcConnection' "        

                if [ "$DbType" = "local_postgres" ]; then

                        echo "*D* Set $Key/$Component: $Name = $Vvalue"

                        q1="update otmm_sys_config_settings "`
				`"set config_value='$Value' "`
				`"where "`
					`"component_id = ("`
						`"select id "`
						`"from otmm_sys_config_components "`
						`"where "`
						`"component_name='$Component' and key_name = '$Key' "`
					`") and name = '$Name'"
                        echo "*D* $q1"
                        eval "$q -r \"$q1\""
		
                elif [ "$DbType" = "oracle" ]; then

                        echo "*F* Set $Key/$Component: $Name = $Vvalue"
                        echo "*F* This method is not implemented"
			exit 1

		else

                        echo "*F* Unsupported database type DbType='$DbType'"
                        exit 1

		fi

	else
                echo "*D* Schema changes administratively denied (DoInstallSchema=$DoInstallSchema)."

	fi

}
