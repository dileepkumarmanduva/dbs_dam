set -a

#
# OTMM  Database connectivity general settings
#
GLOB_OTMM_DB_HOST="*undef*"
GLOB_DB_OTMM_JDBC="*undef*"
GLOB_DB_AMD_JDBC="*undef*"
GLOB_OTMM_ENABLE="${GLOB_OTMM_ENABLE:-ignore}"
if [ "$GLOB_OTMM_DB_TYPE" = "postgres" ] || \
   [ "$GLOB_OTMM_DB_TYPE" = "local_postgres" ]; then

	GLOB_OTMM_ORACLE_DESTROY_SQLTEMPL="*undef*"
	GLOB_DB_POSTGRES_DB="$GLOB_OTMM_DB_NAME"
	if [ -z "$GLOB_OTMM_DB_HOST" ] || [ "$GLOB_OTMM_DB_HOST" = '*undef*' ]; then
		GLOB_OTMM_DB_HOST="$GLOB_HOST_PGSQL"
	fi
	if [ -z "$GLOB_OTMM_DB_HOST_PORT" ] || [ "$GLOB_OTMM_DB_HOST_PORT" = '*undef*' ]; then
		GLOB_OTMM_DB_HOST_PORT="$GLOB_HOST_PGSQL_PORT"
	fi
	if [ -z "$GLOB_DB_SYS_USER" ] || [ "$GLOB_DB_SYS_USER" = "*undef*" ]; then
		GLOB_DB_SYS_USER="$GLOB_HOST_PGSQL_SYS_USER"
	fi
	if [ -z "$GLOB_DB_SYS_PASS" ] || [ "$GLOB_DB_SYS_PASS" = "*undef*" ]; then
		GLOB_DB_SYS_PASS="$GLOB_HOST_PGSQL_SYS_USER_PW"
	fi
	GLOB_DB_OTMM_JDBC="jdbc:postgresql://$GLOB_OTMM_DB_HOST:$GLOB_OTMM_DB_HOST_PORT/$GLOB_DB_POSTGRES_DB"
	GLOB_DB_SYS_JDBC="jdbc:postgresql://$GLOB_OTMM_DB_HOST:$GLOB_OTMM_DB_HOST_PORT/template1"
	GLOB_DB_AMD_JDBC="jdbc:postgresql://$GLOB_OTMM_DB_HOST:$GLOB_OTMM_DB_HOST_PORT/$GLOB_DB_AMD_DBNAME"

	[ "$GLOB_SUPP_DB_HOST" = "*undef*" ] && GLOB_SUPP_DB_HOST=""
	[ "$GLOB_SUPP_DB_HOST_PORT" = "*undef*" ] && GLOB_SUPP_DB_HOST_PORT=""
	[ "$GLOB_SUPP_DB_JDBC" = "*undef*" ] && GLOB_SUPP_DB_JDBC=""
	[ "$GLOB_DB_SUPP_USER" = "*undef*" ] && GLOB_DB_SUPP_USER=""

	GLOB_SUPP_DB_HOST="${GLOB_SUPP_DB_HOST:-$GLOB_OTMM_DB_HOST}"
	GLOB_SUPP_DB_HOST_PORT="${GLOB_SUPP_DB_HOST_PORT:-$GLOB_OTMM_DB_HOST_PORT}"
	GLOB_DB_SUPP_NAME="${GLOB_DB_SUPP_NAME:-db_support}"
	GLOB_SUPP_DB_JDBC="jdbc:postgresql://$GLOB_SUPP_DB_HOST:$GLOB_SUPP_DB_HOST_PORT/$GLOB_SUPP_DB_NAME"

elif [ "$GLOB_OTMM_DB_TYPE" = "oracle" ]; then

	if [ -z "$GLOB_OTMM_DB_HOST" ] || [ "$GLOB_OTMM_DB_HOST" = '*undef*' ]; then
		GLOB_OTMM_DB_HOST="$GLOB_HOST_ORACLE"
	fi
	if [ -z "$GLOB_OTMM_DB_HOST_PORT" ] || [ "$GLOB_OTMM_DB_HOST_PORT" = '*undef*' ]; then
		GLOB_OTMM_DB_HOST_PORT="$GLOB_HOST_ORACLE_PORT"
	fi
	if [ -z "$GLOB_DB_SYS_USER" ] || [ "$GLOB_DB_SYS_USER" = "*undef*" ]; then
		GLOB_DB_SYS_USER="$GLOB_HOST_ORACLE_SYS_USER"
	fi
	if [ -z "$GLOB_DB_SYS_PASS" ] || [ "$GLOB_DB_SYS_PASS" = "*undef*" ]; then
		GLOB_DB_SYS_PASS="$GLOB_HOST_ORACLE_SYS_USER_PW"
	fi
	GLOB_DB_OTMM_JDBC="jdbc:oracle:thin:@//${GLOB_OTMM_DB_HOST}:${GLOB_OTMM_DB_HOST_PORT}/${GLOB_HOST_ORACLE_SID}"
	GLOB_DB_SYS_JDBC="jdbc:oracle:thin:@//$GLOB_OTMM_DB_HOST:$GLOB_OTMM_DB_HOST_PORT/${GLOB_HOST_ORACLE_SID}"
	GLOB_DB_AMD_JDBC="jdbc:oracle:thin:@//$GLOB_OTMM_DB_HOST:$GLOB_OTMM_DB_HOST_PORT/${GLOB_HOST_ORACLE_SID}"

	[ "$GLOB_SUPP_DB_HOST" = "*undef*" ] && GLOB_SUPP_DB_HOST=""
	[ "$GLOB_SUPP_DB_HOST_PORT" = "*undef*" ] && GLOB_SUPP_DB_HOST_PORT=""
	[ "$GLOB_SUPP_DB_JDBC" = "*undef*" ] && GLOB_SUPP_DB_JDBC=""
	GLOB_SUPP_DB_HOST="${GLOB_SUPP_DB_HOST:-GLOB_OTMM_DB_HOST}"
	GLOB_SUPP_DB_HOST_PORT="${GLOB_SUPP_DB_HOST_PORT:-GLOB_OTMM_DB_HOST_PORT}"
	GLOB_SUPP_DB_SID="${GLOB_SUPP_DB_SID:-$GLOB_HOST_ORACLE_SID}"
	GLOB_SUPP_DB_JDBC="jdbc:oracle:thin:@//$GLOB_SUPP_DB_HOST:$GLOB_SUPP_DB_HOST_PORT/$GLOB_SUPP_DB_NAME"

	GLOB_HOST_PGSQL="*undef*"
	GLOB_HOST_PGSQL_PORT="*undef*"
	GLOB_HOST_PGSQL_DATADIR="*undef*"
	GLOB_HOST_PGSQL_SYS_USER="*undef*"
	GLOB_HOST_PGSQL_SYS_USER_PW="*undef*"
	GLOB_OTMM_DB_NAME="*undef*"

elif [ "$GLOB_OTMM_ENABLE" = "ignore" ]; then
	: echo "*D* GLOB_OTMM_ENABLE=$GLOB_OTMM_ENABLE, ignoring database settings"

else

	echo "*F* Unsupported db type $GLOB_OTMM_DB_TYPE"	
	exit 1
fi

set +a

function add_ca_cert_centos () {
	CaBuildDir="/etc/pki/ca-trust/source/anchors"
	CaPemFile=""

	if [ "$GLOB_DRYRUN" != "no" ]; then
		( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
		return
	fi
	while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
                read -r "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	cp $CaPemFile $CaBuildDir/
	update-ca-trust extract
}

function restricted_root_permissions () {
	Path=""
        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ "nfs" = "$(stat -f -L -c %T $Path)" ]; then
		>&2 echo "*D* Directory is an NFS share and cannot use root permissions"
		echo "yes"
	else
		>&2 echo "*D* Directory is not an NFS share and root permissions apply"
		echo "no"
	fi
}

# Prevent Java from accessing ipv6 interfaces by default
# as this may render the port inaccessible by ipv4
function host_disable_ipv6 () {
	OtmmSysConf="20-otmm.conf"
        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done

        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
	fi


	if [ -f "/etc/sysctl.d/${OtmmSysConf}" ]; then
		echo "*D* /etc/sysctl.d/${OtmmSysConf} already configure, skipping..."
		return 
	else
		echo 'net.ipv6.conf.all.disable_ipv6 = 1' >> "/etc/sysctl.d/${OtmmSysConf}"
		sysctl --system
	fi
}

function host_conf_hosts () {
	AddHosts=""
        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done

        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	test -f "/etc/hosts.orig" || cp /etc/hosts /etc/hosts.orig
        sed -i "/localhost/d" /etc/hosts
        echo "127.0.0.1 localhost" >> /etc/hosts
        sed -i "/$Hostname/d" /etc/hosts
	echo "$Ipaddr $Hostname" >> /etc/hosts
	if [ -n "$AddHosts" ]; then
	        echo "${AddHosts}" | awk 'BEGIN{RS=";";FS=",";};{printf("%s       %s\n",$1,$2);}'  >> /etc/hosts
	fi
	diff -Nau /etc/hosts.orig /etc/hosts 2>&1 || /bin/true
}

function motd_info () {
	 while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done

        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	test -f /etc/motd.orig || cp /etc/motd /etc/motd.orig
	cp $MotdTemplate /etc/motd
	perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' /etc/motd

	if [ "$ShowCockpit" = "install" ]; then
		echo >> /etc/motd
		echo "CentOS Cockit on https:/otmmdemo:9090 (vagrant/vagrant)" >> /etc/motd 
	fi
	diff -Nau $MotdTemplate /etc/motd 2>&1 || /bin/true
}

# hibbety, hobbety, this is now my property (shamelessly stolen from https://github.com/boxcutter/centos.git)
function cleanup {

        TmpMount="/tmp/mnt"
	DmtsHome="/opt/mm/dmts" 
	SolrHome="/opt/mm/solr" 
	AmdHome="/opt/mm/AMD" 
	TeamsHome="/opt/mm/MediaManagement"

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done

	umount $TmpMount || /bin/true
	umount /var/cache/dnf || /bin/true

	DISK_USAGE_BEFORE_CLEANUP=$(df -h)

	echo "*D* Clean up yum cache of metadata and packages to save space"
	yum -y --enablerepo='*' clean all

	echo "*D* Rebuild RPM DB"
	rpmdb --rebuilddb
	rm -f /var/lib/rpm/__db*

	echo "*D* Removing temporary files used to build box"
	find /var/log/ -name *.log -exec rm -f {} \;
	rm -rf /tmp/*
	rm  -f /var/log/wtmp /var/log/btmp
	rm -rf /var/cache/* /usr/share/doc/*
	rm -rf /run/log/journal/*

	#
	#test -d "$DmtsHome" && rm -f "$DmtsHome/logs"


	echo '*D* Clear out swap and disable until reboot'
	set +e
	swapuuid=$(/sbin/blkid -o value -l -s UUID -t TYPE=swap)
	case "$?" in
		2|0) ;;
		*) exit 1 ;;
	esac
	set -e
	if [ "x${swapuuid}" != "x" ]; then
	    # Whiteout the swap partition to reduce box size
	    # Swap is disabled till reboot
	    swappart=$(readlink -f /dev/disk/by-uuid/$swapuuid)
	    /sbin/swapoff "${swappart}"
	    dd if=/dev/zero of="${swappart}" bs=1M || echo "dd exit code $? is suppressed"
	    /sbin/mkswap -U "${swapuuid}" "${swappart}"
	fi

	echo '*D* Zeroing out empty area to save space in the final image'
	# Zero out the free space to save space in the final image.  Contiguous
	# zeroed space compresses down to nothing.
	dd if=/dev/zero of=/EMPTY bs=1M || echo "dd exit code $? is suppressed"
	rm -f /EMPTY

	# Block until the empty file has been removed, otherwise, Packer
	# will try to kill the box while the disk is still full and that's bad
	sync

	echo "*D* Disk usage before cleanup"
	echo "${DISK_USAGE_BEFORE_CLEANUP}"

	echo "*D* Disk usage after cleanup $(df -h)"

}
