function usage() {
        set +x
	cat<<EOT
OTMM cluster Installer

SYNOPSIS 
	./install.sh [KEY VALUE ARGS]

ARGUMENTS

  -ConfigFileName <var>: Default is "config.sh"
                        Initialiser file for all settings of the application. It is
			located at an absolute location or relative to -IncludeDir 

  -IncludeDir <var>:  	Default is "/vagrant"
  			Include directory for all installer scripts and template files

  -FilesDir <var>:   	Default is blank, with various mount points for the installer.
  			/lf for all binaries
			/mm for all MediaManagement installer file
			/otds for all OTDS installer files
			/amd for all AMD installer files
  			Override directory for source and installer archives in a single
			place.

  -GlobalInstDir (<var>): Default is "/opt/mm"
  			Installation root directory hosting the subdirectories for all
			components.

  -Verbose (yes|no): 	Default is "no"
  			Increase logging output and add a timestamp for performance tuning

  -Motd (info|ignore): 	Default is "ignore"	
  			Set the /etc/motd file to show the configuration settings on ssh login

  -ConfigureHostsFile (yes|no): Default is "no"
  			Amend the node's /etc/hosts file with all neighborhood servers. Once
			enabled, the -Host and -HostIp are added to the /etc/hosts file.
  	-AddHosts <val>: Default is blank
			Content to add to /etc/hosts in the format "<ip>,<host>;<ip>,<host>"
                        Adds an entry for this Host to the local /etc/hosts file
			See also section: HOST FILE CONFIGURATION
  	-HostIp <NAME>: Default is blank
			Hostname IP of current node used by "-ConfigureHostsFile 'yes'"

  -Host <NAME>:         Default hostname as identified by the hostname command
  			Hostname of current node 
			Best practice is to set this flag everytime the installer runs

  -ConfigureSystemd (ignore|all): Default is "ignore"
  			Allow the installer to configure the systemd startup scripts.
			Warning: This excludes any scripts installed by the Opentext installers!


  -StopAtInstaller (yes|no): Default is 'no'
  			If set, the installer stops at the first installer. This flag
			is designed for tuning of the silent installer response files

  -ConfigureJdk (ignore|default|openjdk-11|<archive>): Default is ignore
  			Roll out a JVM installation on this node. 
			default: is the configured archive in the -ConfigFileName
			openjdk-11: install JDK from Centos using yum
			<archive>.tgz: install JDK from tar ball

*Postgres server configuration options*:

  -ConfigurePostgres (ignore|master): Default is "ignore"
                        Install an OTMM compatible version of Postgresql in this 
			configuration on this node. Postgres is automatically configured
			with pg_agent.

     -ConfigurePgAdminUI: (ignore|install): Default is "ignore"
                        Enable the PGAdmin WEB UI for the postgres server
			  
     -PgAdminWebUser (<var>): Default is "admin@localhost"
	                Username and password of the pagadmin interface, if selected

     -PgAdminWebUserPw (<var>): Default is "letmein"
	                Username and password of the pagadmin interface, if selected

*OTDS configuration options*: see also OTDS CONFIGURATION 

  -ConfigureOtds (ignore|first|second): default is ignore
  			Enable OTDS with given configuration on this node
                        For "secondary" installations set the -OtdsMasterHost and 
			-OtdsMasterHostPort.  For single server mode use "first". 
			For all cases, even for "ignore", select a -OtdsLbHost for 
			the OTDS worker node of the Apache proxy. OTMM uses he
			value of -OtdsLbHost.
			The installer skips an this step if the directory 
			GLOB_DSINS=${GLOB_DSINS:-} exists.

    -OtdsLbHost <var>    : Default is $GLOB_HOST
    			Hostname used in the load balancer access to OTDS. Set
			this to the apache reverse proxy name and port

    -OtdsLbHostPort <var> : Default is $GLOB_HOST_OTDS_PORT_EXT
    			Hostname used in the load balancer access to OTDS. Set
			this to the apache reverse proxy name and port

    -OtdsVersion (default): Default is 20.2.1
                        Select an alternative version for OTDS. Currently, no
			alternative version is available.

    -OtdsWorkaroundLocalhost (install|ignore): (16.2.4?) OT installer fails since its generated self
    			signed certificate does not include 'localhost' since opendj rejects
			the connection attempt. Temporarily set the linux hostname to localhost
			for the installation to force certificate to use localhost.

    -DestroyOtdsInstallation (destroy|ignore): Default is ignore
    			Designed for tuning an installer configuration.
    			Before installation, this flag causes purging of the the existing 
			(failed?) OTDS installation. Purges _all_ registration information 
			in /etc/opentext! Only suitable for OTMM/OTDS installations.

    -OtdsAdminUserPw (<val>): Set the otadmin user password
    			The silent installer in (<?) 20.2.1 does mangle password containing a '=', 
		  	do not use that character

    -WhitelistNodes (<val>): Default is blank
    			Whitespace separated list of trusted OTDS URLs

    -UpgradeOtds <ignore>: default is "ignore".
                        Apply OTDS hotfix on the existing installation or during the 
			new installation. The installer skips the installation if the directory
                        GLOB_DSINS=$GLOB_DSINS exists.

*OTMM configuration options*:

  -ConfigureOtmm (ignore|jboss|tomee_embed|tomee): Default is ignore
  			Enable OTMM and TEAMS on this node
			OTMM 20.2 supports only TomEE (pre installed or automatically installed)  and JBoss

    -OtmmHost <var>: Default is $GLOB_HOST
    			Hostname of the node hosting OTMM used for Apache and OTDS push connector

    -OtmmServerDebugLevel (<var>): Default is 1
                          Default server debug level in the range of (1..5)

    -OtmmTeamsUserPw <val>: Default is tsuper
    			Set the teams user password

    -OtmmRepositoryHome <val>: Default is $GLOB_REPOSITORY_HOME
                        Configure this shared directory for the Assets

    -DestroyExistingSchema (destroy|ignore): Purges the existing OTMM schema in the database and OTDS
                        This allows the installer to rerun on new installations. The flag calls procedures
                        that purge the database and OTDS settings. For safety, the flag only applies for 
                        databases if the tablespace is already prepared, i.e. OtmmOracleSetStorage 
                        must be "manual" and InstallOtmmSchema is "install". For OTDS, the flag only applies if
                        InstallOtmmSchema is also set to "install".

    -OtmmDbType (local_postgres|oracle|sqlplus): Default is "local_postgres"
                        Specify the database type to use for OTMM and use the DB details from \$ConfigFileName
			OtmmDbType is also setting the database for the search service.

    -InstallOtmmSchema (ignore|install): Default is "ignore"
                        Used on the initial node allowing the OTMM installer to create the database schema. 
			Subsequent nodes reuse the existing schema with a setting of 'ignore'

       -OtmmOracleSetStorage (<subdirectory>|manual): Default is "manual" 
       			Only applicable wth InstallOtmmSchema "install" and for Oracle only: allow the 
			installer to create the tablespaces for OTMM in the specified subdirectory. 
			The contstant "manual" indicates that a DBA already provided the required 
			tablespaces.

    -OtmmDbHost (<var>): Default is "$GLOB_OTMM_DB_HOST"
    			Hostname of the database service for OTMM and search.

    -OtmmDbHostPort (<var>): Default is "$GLOB_OTMM_DB_HOST_PORT"
    			Corresponding port on OtmmDbHost of the database service for OTMM and search.

    -OtmmOracleSid (<val>): Default is "$GLOB_HOST_ORACLE_SID"
			Only applicable for OtmmDbType "oracle".    			
                        Set the Oracle SID for the connection string 

    -OtmmPostgresDbName (<val>): Default is "$GLOB_OTMM_DB_NAME"
                          Set the Postgres database name for OTMM

    -OtmmDbSysAdm (<val>): Default is Oracle: "$GLOB_HOST_ORACLE_SYS_USER", Postgres: "$GLOB_HOST_PGSQL_SYS_USER"
    			Indentifies the database user with the installer permissions 

    -OtmmDbSysAdmPw (<val>): Default is Oracle: "$GLOB_HOST_ORACLE_SYS_USER_PW", Postgres: "$GLOB_HOST_PGSQL_SYS_USER_PW"
    			Corresponding password for the OtmmDbSysAdm user

    -OtmmDBTeams (<val>): Default is "towner"
                        Set the Teams OTMM database user

    -OtmmDBTeamsPw (<val>): Default is "towner"
                        Corresponding password for the OtmmDbTeams user

    -UpgradeOtmm (ignore): Default is "ignore"
                        No upgrade patch available at source 

    -OtmmSetTranscoder (ignore|ffmpeg): Default is "ignore"
    			Select the transcoder for the installer

RMA configuration (experimental):
  -ConfigureRma (ignore|install): default is ignore
                         Enable Rich Media Analytics
    -RmaSubscription (<var>): default is set in config.sh

Solr configuration options:

  -ConfigureSolr (ignore|standalone): default is ignore
                        Enable SOLR with given configuration on this node, MASTERSLAVE and CLOUD are untested 
                        This may run before the OTMM installation and then requires a corresponding
                        RegisterSolr with arguments on each OTMM and for each Solr node

    -SolrConfigureLimits (yes|no): default is "no"
    			Only for new installations, configure ulimit values for solr

    -RegisterWithOtmm: (YES|NO): Default is "YES"
                          Enable automatic registration with OTMM that requires running OTMM server

  -RegisterSolr (ignore|cloud|standalone|masterslave): 
                          On a two stage installation can register Solr with OTMM, see also ConfigureSolr.
                          Use ConfigureSolr = "ignore" to prevent rollout of unwanted SOLR installation on OTMM nodes
                          Not required with RegisterWithOtmm = "yes"

    -SolrMasterHost <NAME>: in SOLR_URL of Tresources
                          Defaults to the current nodename, useful for standalone installs

    -SolrMasterPort <NAME>: in SOLR_URL of Tresources
                          Sets the Tresources file up for search. Must run on all OTMM nodes

    -SolrNodeHost <NAME>: register this node hostname in OTMM

    -SolrNodePort <NAME>: register this node port in OTMM
                          Registers this node in OTMM. Must run once for each Solr node

Apache configuration options:

  -ConfigureApache (ignore|proxy): Default is "ignore"
  			Enable Apache this node. 

Transcoder configuration options:
  -ConfigureTranscode (ignore|ffmpeg|vantage): Default is "ignore"
                          Enable FFMPEG in this configuration on this node and must match the DMTS 
			  configuration

    -TranscodeRootFolder <var>: Default is $GLOB_DMTS_ROOT
    			The root directory shared between OTMM and all DMTS servers for 
			exchanging files.

    -TranscodeWorkingFolder <var>: Default is $GLOB_DMTS_WORKING
    			The local working directory on the DMTS nodes

DMTS configuration option:

  -ConfigureDmts (ignore|install): Default is "ignore"
  			Install the DMTS or one of its components on this node

    -DmtsEnableFfmpeg (install|ignore): Default is "ignore"
    			Install and enable FFMPEG on this node

    -DmtsEnableImageMagick (install|ignore): Default is "ignore"
    			Install and enable ImageMagick on this node

	-DmtsAddOpenOffice (ignore|install): Default is "ignore"
			Install OpenOffice and configure for this DMTS Image server
			
Configure private CA:  
  -PrivateCa (ignore|install): Default is ignore
  			Install a custom root certificate on this node
	-CaCert	<var>	Default is \$GLOB_APACHE_CERT_CAPEM
			Default root certificate PEM file for the private CA of this node
	

Experimental:

  -DryRun (otmm|no): Default is "no". 
  			WARNING: this is experimental and does perform some changes.
			Simulate some of the otmm processing steps. Using 'otmm' performs OS and node 
			configuration but prints high level commands for the  OTMM installations.

  -ConfigureIpv6 (ignore|disable): default is 'disable'
  			Used to system wide switch off ipv6 in sysconf

  -CleanupAfterFinish  (yes|no): Default is 'no'
  			If set, the installer cleans up installer log file after the installetion

  -UpdateJavaCaCerts (install|ignore): Default is ignore
  			Update JAVA_HOME/jre/lib/security/cacerts to include the -CaCert
  			This action will link the cacerts to /etc/pki/java/cacerts
	-CaCert	<var>	Default is \$GLOB_APACHE_CERT_CAPEM
			Default root certificate for a private CA of this cluster

  -OtdsAppProto (http|https): OTDS server protocol of the UI used for the proxy or direct access.
                        Setting this to https automatically configures wildfly to use a certificate

  -OtdsAppProtoExt (http|https): User facing/proxy hosted protocol of the UI used for trusted 
    			sites and OTMM installer. Must be https for -ConfigureApache=sslproxy
  -OtdsAppPortExt (80|443|<var>): Base URL port for OTMM. Must be the apache port for -ConfigureApache=sslproxy
  -OtdsFirstHost <NAME>: Hostname of the inital OTDS first node
  -OtdsReplicate1 <NAME>: Hostname of the replication OTDS node for the hosts file
    -OtmmAppProto (http|https): Application server protocol of the UI used for the proxy or direct
                          Setting this to https automatically configures wildfly to use a 
				certificate and sets the apache proxy to use https for internal connections
    -OtmmAppProtoExt (http|https): User facing/proxy hosted protocol of the UI used for trusted 
    			 sites and OTMM installer. Must be https for -ConfigureApache=sslproxy
    -OtmmAppPortExt (80|443|<var>): Base URL port for OTMM. Must be the external apache port for -ConfigureApache=sslproxy
    -OtmmAppPort (11090|11443|<var>): Base URL port for OTMM. Must be the internal apache port for -ConfigureApache=sslproxy. 
                          Does not (yet) specify the new OTMM port in wildfly
    -OtmmSslCertAliases (<var>): default is hostname
				Space separated alias names used in the CRT' 
                          Also set OtmmAppProtoExt and OtmmAppPortExt

    -OtmmOtdsWhiteListNodes (<var>): default is blank
    			 Sets the whitelist for OTMM's /artesia-ux-ws endpoint to only OTDS 
    -OtmmConfigureAmd (install|ignore): default is ignore
    			 Configure AMD
    -OtmmAmdDbUser (<var>): default is amd
    			 AMD database user
    -OtmmAmdDbUserPw (<var>): default is amd
    			 AMD database user password
    -OtmmAmdDbName (<var>): default is amd
    			 AMD database name (postgres only)
    -OtmmAmdDbSid (<var>): default is amd
    			 AMD database name (oracle only)
    -OtmmAmdDbHost (<var>): default is the used OTMM database host
    			 AMD database host name 
    -OtmmAmdDbHostPort (<var>): default is the used OTMM database port
    			 AMD database port name 
    -OtmmAmdHostPort (<var>): default is 8888
    			 AMD service port name 

      -ApacheSslCertAliases (<var>): default is hostname
				Space separated alias names used in the CRT' 
                          Also set OtmmAppProtoExt and OtmmAppPortExt
     -ApacheStreaming (ignore|stream): configure apache for streaming 
     -ApacheLbHost <NAME>: Node used in the OTMM configuration, OTDS trusted referrals and
                           Apache Servername on port 80/443
    -SolrHost <NAME>:     Hostname of search node in "cloud" or "slave" mode, defaults to Host

    -SolrHostIP <NAME>:   IP address of search node in "cloud" or "slave" mode used in /etc/hosts file, defaults to HostIP
    -SolrWhiteListNodes: <var>: space separated hostnames or IP addresses of permitted nodes for solr
  -ConfigureSupportMgr (install|ignore): default is ignore
			  Enable the Support Manager on this node


DESCRIPTION

HOST FILE CONFIGURATION

The installer can manage the hostnames for a cluster install using the /etc/hosts file.
Set the -ConfigureHostsFile to "yes" to enable this function. If enabled, the installer
deletes the -Host entry  and set replaces it with a new -HostIp -Host entry. Then the
installer adds/replaces all additional hosts configured with -AddHosts. The -AddHost
argument expects a semi-colon separated list of comma separated key value fields, e.g.
'127.0.0.2,hostA;127.0.0.3,hostB'.

This function should not be used if the hosts are managed by a DNS server.

TLS CONFIGURATION

The installer supports the following installation scenario:

- no TLS
- Reverse proxy with TLS termination
- all services TLS

The installer can create the certificates automatically provided a global CA authority
file exists. The configuration expects the following settings:

GLOB_APACHE_CERT_CRTNAME: absolute filename for the automatically created CRT file
GLOB_APACHE_CERT_CSRNAME: absolute filename for the automatically created CSR file
GLOB_APACHE_CERT_KEYNAME: absolute filename of the private key file
GLOB_APACHE_CERT_EMAIL  : Email address in the service certificate
GLOB_APACHE_CERT_COUNTRY: Country code of the service certificate
GLOB_APACHE_CERT_STATE  : State code of the service certificate
GLOB_APACHE_CERT_LOC    : Location code of the service certificate
GLOB_APACHE_CERT_ORG    : Organisation name in the service certificate


OTDS CONFIGURATION:

The implementation does provide two installation modes:
	* first) for the initial OTDS node
	* second) for all other OTDS nodes

OTDS replication is part of the product. The installer configuration sets the
needed configuration with one 'primary' node and many 'secondary' nodes. 

Important settings for the node:
	* -Host, GLOB_HOST_OTDS for the current node 
	* GLOB_HOST_OTDS_PORT for the current node port

Important settings for the primary node:
	* -OtdsPrimaryHost 
	* -OtdsPrimaryHostPort

Important settings for the load balancer/read access:
	* -OtdsLbHost
	* -OtdsLbHostPort

Minimal requirements for the installation:
	Tomcat,
	Java

INSTALL COMMANDS

First step for the rollout (BASE):

bash -x ./install.sh \\
	-Verbose no \\
	-Host otmm202 \\
	-HostIp "172.16.1.191" \\
	-ConfigureHostsFile yes \\
	-ConfigureJdk default

Base + Postgres 12:

bash ./install.sh \\
	-Verbose no \\
	-Host otmm202 \\
	-ConfigurePostgres install 

Base + OTDS

bash -x ./install.sh \\
	-Verbose no \\
	-Host otmm202 \\
	-ConfigureOtds first \\
	-OtdsAdminUserPw "otadmin" 

Base + DMTS = OTMM Ready

bash -x ./install.sh \\
	-Verbose yes \\
	-Host otmm202 \\
	-ConfigureDmts first \\
	-DmtsEnableImageMagick "install" \\
	-DmtsEnableFfmpeg "install"


OTMM Ready + OTMM

bash -x ./install.sh \\
	-Verbose no \\
        -Host otmm202 \\
        -ConfigureOtmm "tomee_embed" \\
		-InstallOtmmSchema "install" \\
		-OtmmDbType "local_postgres" \\
		-OtmmTeamsUserPw "tsuper"\\
		-OtmmTeamsUserPw "tsuper"\\
		-OtmmSetTranscoder "ffmpeg" \\
        -OtdsAdminUserPw "otadmin" 

OTMM Read + Solr
bash ./install.sh \\
	-Verbose yes \\
	-Host otmm202 \\
	-ConfigureSolr standalone \\
		-SolrConfigureLimits "yes" \\
		-OtmmDbType "local_postgres" \\
		-RegisterSolr standalone

Enable Apache proxy
bash ./install.sh \\
	-ConfigureApache "install" \\
	-OtmmDbType "local_postges" \\
	-ApacheStreaming "install"

Base + OTMM

Basic OTMM service (delete existing OTDS partition/resource!) for Oracle
sudo su -
cd /vagrant
bash ./install.sh \\
        -Verbose yes \\
        -ConfigureJdk default \\
        -Host otmm202 \\
	        -HostIp "172.16.1.191" \\
       		-ConfigureHostsFile yes \\
        -ConfigureOtmm "tomee_embed" \\
		-OtmmDbType "oracle" \\
			-OtmmOracleSetStorage "/var/tmp" \\
		-ConfigureHostsFile "yes" \\
			-AddHosts "172.16.1.251,oracle12c" \\
		-InstallOtmmSchema "install" \\
		-DestroyExistingSchema "destroy" \\
		-OtmmServerDebugLevel "5" \\
	-ConfigureOtds first \\
        	-OtdsAdminUserPw "otadmin" \\
	-ConfigureDmts first \\
       		-DmtsEnableImageMagick "install" \\
        	-DmtsEnableFfmpeg "install" \\
        	-DmtsEnableImageMagick "install" \\
		-OtmmSetTranscoder "ffmpeg" \\
        -ConfigureSolr standalone \\
                -SolrConfigureLimits "yes" \\
                -OtmmDbType "oracle" \\
                -RegisterSolr standalone


Basic OTMM service (delete existing OTDS partition/resource!) for Postgres
sudo su -
cd /vagrant
bash ./install.sh \\
        -Verbose yes \\
        -ConfigureJdk default \\
        -Host otmm202 \\
                -HostIp "172.16.1.191" \\
                -ConfigureHostsFile yes \\
        -ConfigureOtmm "tomee_embed" \\
		-OtmmDbType "local_postgres" \\
			-ConfigurePostgres install \\
		-InstallOtmmSchema "install" \\
		-DestroyExistingSchema "ignore" \\
		-OtmmServerDebugLevel "5" \\
	-ConfigureOtds first \\
        	-OtdsAdminUserPw "otadmin" \\
        -ConfigureDmts first \\
                -DmtsEnableImageMagick "install" \\
                -DmtsEnableFfmpeg "install" \\
                -DmtsEnableImageMagick "install" \\
                -OtmmSetTranscoder "ffmpeg" \\
        -ConfigureSolr standalone \\
                -SolrConfigureLimits "yes" \\
		-OtmmDbType "local_postgres" \\
                -RegisterSolr standalone



EOT

}
