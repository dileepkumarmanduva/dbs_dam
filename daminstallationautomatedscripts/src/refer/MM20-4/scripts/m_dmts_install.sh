function install_dmts_build_dmtstxt () {
	ConfigTemplate="dmts_config.txt_templ"
	FeatureList="DMTS-Service,DMTS-Image,DMTS-Video"
	ServiceList="All"; #Custom
	InstallDir="*undef*";
	RqInstall="1";
	RqOtherServer="0";
	RqNode="*undef*";
	RqNodePort="5672";
	RqNodeUser="*undef*";
	RqNodeUserPw="*undef*";

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2" 
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        localconf=$(mktemp -t tmp.installer.dmtsconf.XXXXXXX.txt)
        cp $ConfigTemplate $localconf

	perl -pi -e 's/^#* *CHOSEN_INSTALL_FEATURE_LIST=.*/CHOSEN_INSTALL_FEATURE_LIST='"${FeatureList}"'/g' $localconf
	perl -pi -e 's/^#* *CHOSEN_INSTALL_SET=.*/CHOSEN_INSTALL_SET='"${ServiceList}"'/g' $localconf
	perl -pi -e 's!^#* *USER_INSTALL_DIR=.*!USER_INSTALL_DIR='"${InstallDir}"'!g' $localconf
	perl -pi -e 's/^#* *RABBITMQ_INSTALL=.*/RABBITMQ_INSTALL='"${RqInstall}"'/g' $localconf
	perl -pi -e 's/^#* *RABBITMQ_CONFIGURE=.*/RABBITMQ_CONFIGURE='"${RqOtherServer}"'/g' $localconf
	perl -pi -e 's/^#* *RABBITMQ_HOST=.*/RABBITMQ_HOST='"${RqNode}"'/g' $localconf
	perl -pi -e 's/^#* *RABBITMQ_PORT=.*/RABBITMQ_PORT='"${RqNodePort}"'/g' $localconf
	perl -pi -e 's/^#* *RABBITMQ_USERNAME=.*/RABBITMQ_USERNAME='"${RqNodeUser}"'/g' $localconf
	perl -pi -e 's/^#* *RABBITMQ_PASSWORD=.*/RABBITMQ_PASSWORD='"${RqNodeUserPw}"'/g' $localconf

	perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $localconf
        >&2 diff -Nau $ConfigTemplate $localconf || /bin/true

        echo $localconf

}

function dmts_configure_sudoers () {
	SudoFile="dmts.otdam"
	Group="otdam"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -f "/etc/sudoers.d/$SudoFile" ]; then
		echo "*D*  /etc/sudoers.d/$SudoFile exists, skipping creation."
	else
		(
			echo "Cmnd_Alias DMTS = " 
			echo "/bin/systemctl enable dmts-*, "
			echo "/bin/systemctl start dmts-*, "
			echo "/bin/systemctl stop dmts-*, "
			echo "/bin/systemctl disable dmts-* "
			echo "%$Group ALL=(ALL) NOPASSWD: DMTS "
		) > /etc/sudoers.d/$SudoFile
		echo "*D* created /etc/sudoers.d/$SudoFile"
	fi
}

function install_dmts () {
	ConfigTemplate="dmts_config.txt_templ"
	FeatureList="DMTS-Service,DMTS-Image,DMTS-Video"
	ServiceList="All"
        InstallDir="*undef*";
        RqInstall="yes";
        RqOtherServer="no";
        RqNode="*undef*";
        RqNodePort="5672";
        RqNodeUser="*undef*";
        RqNodeUserPw="*undef*";
	TmpMount="/tmp/mnt"
	Archive="*undef*"
	User="*undef*"
	Group="*undef*"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ "$(getenforce)" = "Permissive" ]; then
		echo "*D* Confirmed selinux permissive"
	else
		echo "*F* Unconfirmed selinux permissive, found: $(getenforce)"
		exit 1
	fi
	
	if [ "$RqInstall" = "yes" ]; then RqInstall="1"; else RqInstall="0"; fi
	if [ "$RqOtherServer" = "no" ]; then 
		RqOtherServer="0"; 
	else 
		RqOtherServer="1"; 
		if [ -z "$RqNode" ] || [ "$RqNode" = "*undef*" ] || \
		   [ -z "$InstallDir" ] || [ "$InstallDir" = "*undef*" ] \
		   [ -z "$RqNodePort" ] || [ "$RqNodePort" = "*undef*" ] \
		   [ -z "$RqNodeUser" ] || [ "$RqNodeUser" = "*undef*" ] \
		   [ -z "$RqNodeUserPw" ] || [ "$RqNodeUserPw" = "*undef*" ] \
		   [ -z "$Archive" ] || [ "$Archive" = "*undef*" ]; then
		   	echo "*F* on or more arguments are invalid: " \
				"InstallDir=$InstallDir," \
				"Archive=$Archive," \
				"RqNode=$RqNode," \
				"RqNodePort=$RqNodePort," \
				"RqNodeUser=$RqNodeUser," \
				"RqNodeUserPw=$RqNodeUserPw, abort." \
		   exit 1
	   	fi

		if </dev/tcp/$RqNode/$RqNodePort; then
			echo "*D* Tcp connection confirmed to RabbitMQ" \
				"server on $RqNode:$RqNodePort"
		else
			echo "*F* Tcp connection RabbitMQ server failed" \
				"for $RqNode:$RqNodePort"
			exit 1
		fi
	fi

	#sanity check - detect failed installation not properly cleaned up
	if [ -f "/etc/systemd/system/dmts-service.service" ] ||
	   [ -f "/etc/systemd/system/dmts-video.service" ] ||
	   [ -f "/etc/systemd/system/dmts-image.service" ]; then
		echo "*F* Existing installation of failed previous installation attempt"
		echo "*F* Run systemctl disable dmts-service dmts-video dts-image as appropriate"
		exit 1  
	fi

        localconf=$(install_dmts_build_dmtstxt \
		-InstallDir "$InstallDir" \
		-FeatureList "$FeatureList" \
		-ServiceList "$ServiceList" \
		-InstallDir "$InstallDir" \
		-RqInstall "$RqInstall" \
		-RqOtherServer "$RqOtherServer" \
		-RqNode "$RqNode" \
		-RqNodePort "$RqNodePort" \
		-RqNodeUser "$RqNodeUser" \
		-RqNodeUserPw "$RqNodeUserPw" \
		-ConfigTemplate $ConfigTemplate) 

	if [ -d "$TmpMount/DMTS" ]; then
                echo "$TmpMount exists, ignoring mounting of iso..."
        else
                localarc=$(mktemp -t tmp.mmiso.XXXXXXXX)
                echo "*D* $TmpMount missing, mounting $Archive as $localarc to $TmpMount"
                cp $Archive $localarc
                mkdir -p $TmpMount
                mount -o loop $localarc $TmpMount
        fi

	#tidy
        test -d "$InstallDir" && rm -rf "$InstallDir"

        _d="$TmpMount/DMTS/Disk1/InstData/VM"
        if [ "$StopAtInstaller" = "yes" ]; then
                echo "*D* stopped at DMTS installer"
                echo "*D* set export LAX_DEBUG=1 for more debugging"
                echo "*D* set export JAVA_HOME=$JavaHome"
                echo "*D* set export TEAMS_HOME=$TeamsHome"
                echo "*D* run $_d/install.bin -i silent -f $localconf"
                exit 1
        else
                #as root!
                export JAVA_HOME=$JavaHome
                export LAX_DEBUG="1"
                echo "################################################################################"
		if $_d/install.bin -i silent -f $localconf; then
                        echo "*D* Installer succeeded"
		else
			echo "*F* Installer failed"
			exit 1
		fi
                echo "################################################################################"
	fi

	chown -R "$User:$Group" "$InstallDir"

}

# Function install_rabbitmq
# * only used if RabbitMq is installed here as server
# * and should not be called if RabbitMq is elsewhere.
function install_dmts_rabbitmq() {
	DmtsHome=""
	RqNodeUser="*undef*"
	RqNodeUserPw="*undef*"
	RqAdmUser="*undef*"
	RqAdmUserPw="*undef*"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -z "$RqAdmUser" ] || [ -z "$RqAdmUserPw" ]; then
	   	echo "*F* on or more arguments are invalid: " \
			"RqNodeUser=$RqNodeUser," \
			"RqNodeUserPw=$RqNodeUserPw, abort." 
	   exit 1
	fi

        echo "*D* install using yum install $DmtsHome/install/erlang/*.rpm"
        res=$( cd $DmtsHome/install/erlang && BASH_XTRACEFD= yum install -y -q *.rpm 2>&1 || /bin/true)
        case "${res}x" in
		"Error: Nothing to do"*|"x")
			echo "*D* Nothing to do"
			;;
		*)
			echo "*F* Error code: $? $res"
			exit 1
			;;
	esac

        echo "*D* install Socat"
        if bash -c "yum list installed socat"; then
                echo "*D* Socat already installed.."
        else
                echo "*D* yum install $YumFlags -y socat"
                bash -c "BASH_XTRACEFD= yum install $YumFlags -y socat"
        fi

        echo "*D* install RabbitMQ"
        res=$( cd $DmtsHome/install/rabbitmq && BASH_XTRACEFD= yum install -y -q *.rpm 2>&1 || /bin/true)
	case "${res}x" in
                "Error: Nothing to do"*|"x")
                        echo "*D* Nothing to do"
                        ;;
                *)
                        echo "*F* Error code: $? $res"
                        exit 1
        esac

	echo "*D* systemctl start rabbitmq-server"
	( BASH_XTRACEFD=; systemctl enable rabbitmq-server; systemctl start rabbitmq-server )

	echo "*D* rabbitmq-plugins enable rabbitmq_management"
	( BASH_XTRACEFD=; rabbitmq-plugins enable rabbitmq_management )

	if [ "$RqNodeUser" = "*undef*" ]; then
		:
	else
		echo "*D* create RabbitMQ administrator user"
		(BASH_XTRACEFD=; rabbitmqctl add_user "$RqNodeUser" "$RqNodeUserPw" )
		(BASH_XTRACEFD=; rabbitmqctl set_permissions --vhost "/" "$RqNodeUser" '.*' '.*' '.*' )
	fi

	echo "*D* create RabbitMQ node user"
	(BASH_XTRACEFD=; rabbitmqctl add_user "$RqAdmUser" "$RqAdmUserPw" )
	(BASH_XTRACEFD=; rabbitmqctl set_user_tags "$RqAdmUser" administrator )
	(BASH_XTRACEFD=; rabbitmqctl set_permissions --vhost "/" "$RqAdmUser" '.*' '.*' '.*' )
}

function configure_dmts () {
	User="otmm"
	Group="otdam"
	ConfigureFile="/opt/mm/DMTS/dmts-image/conf/application.properties"
	TranscodeWorkingArea="/opt/mm/shared"
	JavaHome=""
	DmtsHome="/opt/mm/DMTS"
	TranscoderWorkFolder="/var/tmp/working"
	ServiceName="DMTS-Image"
	FirstRmqUser="*undef*"
	FirstRmqUserPw="*undef*"
	FirstRmqNode="localhost"
	FirstRmqNodePort="5672"
	FirstDmstNode="localhost"
	FirstDmstNodePort="15672"
	DmtsNode="*undef*"
	DmtsNodePort="*undef*"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2" 
                shift           
                shift           
        done            
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi


	test -f "${ConfigureFile}.orig" || cp  "${ConfigureFile}" "${ConfigureFile}.orig"

	case "$ServiceName" in 
		"DMTS-Video")
			configure_dmts_video \
				-User "$User" \
				-Group "$Group" \
				-MediaInfoRpms "$MediaInfoRpms" \
				-ConfigureFile "$ConfigureFile" \
				-DmtsHome "$DmtsHome" \
				-WorkingArea "$TranscodeWorkingArea" \
				-StagingFolder "$TranscoderWorkFolder" \
				-Transcoder "ffmpeg" 
		;;
		"DMTS-Image")
			configure_dmts_image \
				-User "$User" \
				-Group "$Group" \
				-ConfigureFile "$ConfigureFile" \
				-DmtsHome "$DmtsHome" \
				-WorkingArea "$TranscodeWorkingArea" \
				-StagingFolder "$TranscoderWorkFolder" 
		;;
		"DMTS-Service")
			configure_dmts_service \
				-DmtsNodePort "$DmtsNodePort" \
				-ConfigureFile "$ConfigureFile" \
				-JavaHome "$JavaHome" \
				-DmtsHome "$DmtsHome" \
				-FirstRmqUser "$FirstRmqUser" \
				-FirstRmqUserPw "$FirstRmqUserPw" \
				-FirstRmqNode "$FirstRmqNode" \
				-FirstRmqNodePort "$FirstRmqNodePort" 
		;;
		*)
			echo "*F* Invalid argument ServiceName=$ServiceName"		
			exit 1
		;;
	esac

}

function configure_dmts_service () {
	FirstDmstNode="localhost"
	FirstDmstNodePort="15672"
	DmtsNodePort="18090"
	JavaHome=""

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        perl -pi -e 's/server.port=.*/server.port='"${DmtsNodePort}"'/g' $ConfigureFile
        perl -pi -e 's/spring.rabbitmq.host=.*/spring.rabbitmq.host='"${FirstRmqNode}"'/g' $ConfigureFile
        perl -pi -e 's/spring.rabbitmq.port=.*/spring.rabbitmq.port='"${FirstRmqNodePort}"'/g' $ConfigureFile

	test -f "${DmtsHome}/conf/RabbitMQ_SEC_Srv.cfg.orig" || \
		cp  "${DmtsHome}/conf/RabbitMQ_SEC_Srv.cfg" "${DmtsHome}/conf/RabbitMQ_SEC_Srv.cfg.orig"

	if [ "$FirstRmqUser" != "*undef*" ] &&  \
	   [ -n "$FirstRmqUser" ] &&  \
	   [ "$FirstRmqUserPw" != "*undef*" ] &&  \
	   [ -n "$FirstRmqUser" ]; then
	 	( echo "$FirstRmqUser" ; echo "$FirstRmqUserPw" ) > "${DmtsHome}/conf/RabbitMQ_SEC_Srv.cfg"
		export DMTS_HOME="$DmtsHome"
		export JAVA_HOME="$JavaHome"
		PATH="/usr/bin:$PATH"; #crypt.sh does not accept JAVA_HOME but insists on calculating it
		"$DmtsHome/bin/crypt.sh" ef "${DmtsHome}/conf/RabbitMQ_SEC_Srv.cfg"
	else 
		echo "*D* FirstRmqUser=$FirstRmqUser, FirstRmqUserPw=$FirstRmqUserPw not valid, skipping"
	fi

}


function configure_dmts_image () {
	User="otmm"
	Group="otdam"
	FirstRmqNode="localhost"
        FirstRmqNode="5672"
	FirstDmstNode="localhost"
	FirstDmstNodePort="15672"
	WorkingArea="/opt/mm/shared"
	StagingFolder="/var/staging/dmts-video"


        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	test -d "${StagingFolder}" || install -d -o "$User" -g "$Group" "$StagingFolder"
	test -d "${WorkingArea}" || install -d -o "$User" -g "$Group" "$WorkingArea"

#        perl -pi -e 's/server.port=.*/server.port='"${DmtsNodePort}"'/g' $ConfigureFile
#        perl -pi -e 's/spring.rabbitmq.host=.*/spring.rabbitmq.host='"${FirstRmqNode}"'/g' $ConfigureFile
#        perl -pi -e 's/spring.rabbitmq.port=.*/spring.rabbitmq.port='"${FirstRmqNodePort}"'/g' $ConfigureFile
        perl -pi -e 's!^[# ]*dmts.staging.root-folder=.*!dmts.staging.root-folder='"${StagingFolder}"'!g' $ConfigureFile
        perl -pi -e 's!^[# ]*dmts.fileshare.root-folder=.*!dmts.fileshare.root-folder='"${WorkingArea}"'!g' $ConfigureFile

}

function configure_dmts_video () {
	FirstDmstNode="localhost"
	FirstDmstNodePort="15672"
	FirstRmqNode="localhost"
        FirstRmqNode="5672"
	WorkingArea="/opt/mm/shared"
	StagingFolder="/var/staging/dmts-video"
	Transcoder="ffmpeg"


        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	test -d "${WorkingArea}" || install -d -o "$User" -g "$Group" "$WorkingArea"

#        perl -pi -e 's/server.port=.*/server.port='"${DmtsNodePort}"'/g' $ConfigureFile
#        perl -pi -e 's/spring.rabbitmq.host=.*/spring.rabbitmq.host='"${FirstNode}"'/g' $ConfigureFile
#        perl -pi -e 's/spring.rabbitmq.port=.*/spring.rabbitmq.port='"${FirstNodePort}"'/g' $ConfigureFile
        perl -pi -e 's!^[# ]*dmts.staging.root-folder=.*!dmts.staging.root-folder='"${StagingFolder}"'!g' $ConfigureFile
        perl -pi -e 's!^[# ]*dmts.fileshare.root-folder=.*!dmts.fileshare.root-folder='"${WorkingArea}"'!g' $ConfigureFile
	if [ "$Transcoder" = "ffmpeg" ]; then
	        perl -pi -e 's/dmts.transcode.provider=.*/dmts.transcode.provider='"${Transcoder}"'/g' $ConfigureFile
	else
		echo "*F* Transcoder=$Transcoder not yet configured"
		exit 1
	fi

	echo "*D* Installing MediaInfo $MediaInfoRpms"

        echo "*D* cd $DmtsHome; yum install -y $(cd $DmtsHome;echo $MediaInfoRpms)"
        ( cd $DmtsHome/install/mediainfo; yum install -y $MediaInfoRpms  )

	echo "*D* mediainfo: $(mediainfo --Version)"

}

function install_dmts_imagemagick () {
	ArchiveDir="/lf/linux/centos8"
	ReposRpm="/lf/linux/epel-release-8* /lf/linux/rpmfusion-free-release-8*"
#	Archives="nux-dextop-release*el7.nux.noarch.rpm remi-release-8* ufraw-*x86_64.rpm ImageMagick-7* ImageMagick-libs-7*"
	Archives="ImageMagick-7* ImageMagick-libs-7* ImageMagick-debuginfo-7* ufraw-*x86_64.rpm gtkimageview-devel gtkimageview-*"
	OfficeArchives=""

	q="yum install -y $ReposRpm"
	echo "*D* $q"
	(BASH_XTRACEFD=; eval "$q")
#	q="yum config-manager --set-enabled PowerTools"
	q="dnf config-manager --set-enabled powertools"
	echo "*D* $q"
	(BASH_XTRACEFD=; eval "$q")

#	echo "*D* yum install $YumFlags -y $ReposRpm"
#	(BASH_XTRACEFD=; yum install $YumFlags -y $ReposRpm)

#	pgs="exiv2-devel gimp gimp-devel gtkimageview-devel "`
#		`"lensfun-devel libexif-devel GConf2 djvulibre-devel "`
#		`"fftw-devel jbigkit-devel libwebp-devel openjpeg2-devel "`
#		`"jasper-devel ghostscript-devel"
#
#	q="dnf -y install $pgs"
#	echo "*D* $q"
#	(BASH_XTRACEFD=; eval "$q")

	q="cd $ArchiveDir; yum install -y $(echo $Archives)"
	echo "*D* $q"
	(BASH_XTRACEFD=; eval "$q")

	echo "*D* ufraw: $(ufraw-batch --version)"
	echo "*D* ImageMagick: $(magick -version)"
}

# pseudo code ot create the Centos8 ImageMagick RPMS
function build_dmts_imagemagick () {

	yum -y groupinstall 'Development Tools'	
	yum -y install bzip2-devel freetype-devel libjpeg-devel libpng-devel libtiff-devel zlib-devel ghostscript-devel libwmf-devel jasper-devel libtool-ltdl-devel libX11-devel libXext-devel libXt-devel libxml2-devel librsvg2-devel OpenEXR-devel php-devel giflib-devel LibRaw-devel
	yum -y install /lf/remi-release-8.rpm 
	yum -y install djvulibre-devel fftw-devel jbigkit-devel libwebp-devel openjpeg2-devel
	rpm -iv https://imagemagick.org/download/linux/SRPMS/ImageMagick-7.0.10-23.src.rpm
	cd .../SPEC; rpmbuild -bb ImageMagick.spec

	yum config-manager –set-enabled PowerTools


	dnf -y --enablerepo=PowerTools install exiv2-devel gimp gimp-devel gtkimageview-devel lensfun-devel libexif-devel GConf2 djvulibre-devel fftw-devel jbigkit-devel libwebp-devel openjpeg2-devel
	rpm -iv http://dl.fedoraproject.org/pub/epel/7/SRPMS/Packages/u/ufraw-0.22-14.el7.src.rpm  
	git clone https://github.com/sergiomb2/ufraw.git
	rm .../SOURCES/*
	
	cd .../SPEC; rpmbuild -bb ufraw.spec

}

function postinstall_configure_dmts_service () {
	User="otmm"
	Group="otdam"
	Service="All"
	DmtsHome="/opt/mm/dmts"
	FfmpegHome="/opt/mm"	

	while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [  "$Service" = "All" ]; then
		for n in "dmts-service" "dmts-video" "dmts-image"; do
			postinstall_configure_dmts_service \
				-Service "$n" \
				-Group "$Group" \
				-User "$User" \
				-FfmpegHome "$FfmpegHome" \
				-DmtsHome "$DmtsHome"
		done
		return
	elif 	[  "$Service" = "dmts-service" ] || \
		[  "$Service" = "dmts-image" ] || \
		[  "$Service" = "dmts-video" ]; then
			tmpl="${DmtsHome}/${Service}/conf/${Service}.service.template"
                	localcft=$(mktemp -t tmp.dmts-service.XXXXXXXX)
			test -f "${tmpl}.orig" || cp "${tmpl}" "${tmpl}.orig"
			cp -f "${tmpl}.orig" "${localcft}"

			perl -pi -e 's/^(User=).*/$1'"${User}"'/' "${localcft}"
			perl -pi -e 's!^(Environment=FFMPEG_HOME=).*!$1'"${FfmpegHome}"'!' "${localcft}"
			perl -pi -e 's!After=(.*)!After=$1 rabbitmq-server.service!' "${localcft}"
			diff -Nau "${tmpl}.orig" "${localcft}" || /bin/true

			#TODO/MAYBE: check the user has permissions to 
			#write the log - otherwise it won't run!

			#commit
			cp -f "${localcft}" "${tmpl}"
			"${DmtsHome}/bin/service-ctl.sh" "$Service" "stop"
			"${DmtsHome}/bin/service-ctl.sh" "$Service" install 

			# restore template as per installer
			cp -f "${tmpl}.orig" "${tmpl}"
			chown -R "${User}:${Group}" "${DmtsHome}"
			echo "*D* Configured $Service to run as $User"
			
	else
		echo "*E* Invalid arguments Service=$Service, abort"
		exit 1
	fi

}

function install_dmts_openoffice () {
	OfficeHome="/opt/openoffice4"
	DmtsHome="/opt/mm/dmts"
	ArchivesDir="linux/soffice-4.1.7"
	User="otmm"

	while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	ConfigureFile="$DmtsHome/dmts-image/conf/application.properties"

	echo "*D* install $Archive"
	eval 'BASH_XTRACEFD=;cd "/lf/$ArchivesDir" && yum -q -y install openoffice-*'

        OtmmRc=$( IFS=:; set -- $(getent passwd $User); echo $6 )"/.otmmrc"
        if [ \! -f "$OtmmRc" ]; then
                echo "*F* User=$User ($OtmmRc) does not have otmmrc file"
                exit 1
        fi

	echo "*D* configure DMTS"
	test -f "${ConfigureFile}.orig" || cp  "${ConfigureFile}" "${ConfigureFile}.orig"
	perl -pi -e 's!^[# ]*(jodconverter.local.office-home)=.*!$1='"${OfficeHome}"'!g' "$ConfigureFile"

	echo 'export OFFICE_HOME="'"$OfficeHome"'"' >> "$OtmmRc"
	echo 'PATH="$PATH:$OFFICE_HOME/program"' >> "$OtmmRc"

	echo "*D* configure systemd"
	echo "*D* OpenOffice: $($OfficeHome/program/soffice -h 2>&1 | head -1)"
}

function install_dmts_blender () {
	DmtsHome="/opt/mm/dmts"
	User="otmm"
	Group="otdam"
	InstallDir="/opt/mm/blender"
	Archive=""
	GitfArchive="$GLOB_DMTS_BLENDER_GTIFARC"

	while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        ConfigureFile="$DmtsHome/dmts-image/conf/application.properties"

        OtmmRc=$( IFS=:; set -- $(getent passwd $User); echo $6 )"/.otmmrc"
        if [ \! -f "$OtmmRc" ]; then
                echo "*F* User=$User ($OtmmRc) does not have otmmrc file"
                exit 1
        fi

	echo "*D* Install $Archive to $InstallDir"
	(BASH_XTRACEFD=; yum -y -q install mesa-libGLU)

	echo "*D* Install blender to $InstallDir"
	localarc=$(mktemp -d -t tmp.blender.XXXXXXXX)
	case $Archive in
		*xz)
			if which -s xzcat &>/dev/null; then
				echo "*D* Found required xzcat"
			else
				echo "*D* Did not find xzcat, installing"
				bash -c "BASH_XTRACEFD= yum install -q -y xz"
			fi
			 xzcat $Archive | tar -xC "$localarc"  -f -
		;;
		*)
			tar -jxC "$localarc" -f "$Archive"
		;;
	esac	
	filedir=$(echo $localarc/*)
	mv $filedir $InstallDir
	chown -R $User:$Group $InstallDir

#	echo "*D* Enable gITF Exporter"
#	localgtifarc=$(mktemp -d -t tmp.blender-gitf.XXXXXXXX)
#	chown $User $localgtifarc
#        su -c "unzip -B -d $localgtifarc $GitfArchive" $User
#	gtiffiledir=$(echo $localgtifarc/*)
#	mv $gtiffiledir/scripts/addons/io_scene_gltf2 $InstallDir/2.82/scripts/addons/.
#	chown -R $User:$Group $InstallDir

#	su -c "cd ${DmtsHome}/install/blender ; $InstallDir/blender -b -noaudio -P enable_gltf.py" $User 

        echo "*D* configure DMTS"
        test -f "${ConfigureFile}.orig" || cp  "${ConfigureFile}" "${ConfigureFile}.orig"
        perl -pi -e 's!^[# ]*(dmts.blender.path)=.*!$1='"${InstallDir}"'/blender!g' "$ConfigureFile"

        echo 'export BLENDER_HOME="'"$InstallDir"'"' >> "$OtmmRc"
        echo 'PATH="$PATH:$BLENDER_HOME/program"' >> "$OtmmRc"


	echo $( $InstallDir/blender --version | head -1 )
}
