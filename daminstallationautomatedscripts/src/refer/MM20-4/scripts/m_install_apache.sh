function apache_install_ssl () {

	ConfigVideoProxySubdir="Video"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

#       -InstallDir $GLOB_APACHEINS \
#       -User $GLOB_USER \
#       -Group $GLOB_GROUP \
#       -DbType $GLOB_OTMM_DB_TYPE \
#       -DbUser $GLOB_DB_TEAMS_USER \
#       -DbUserPw GLOB_DB_TEAMS_PASS \
#       -JdbcConnection $GLOB_DB_OTMM_JDBC \
#       -SqlTool "java -jar $GLOB_CONFDIR/sqltool.jar" \
#       -RepositoryHome $GLOB_REPOSITORY_HOME \
#	-ApacheProxyDocRoot $GLOB_APACHE_PROXY_DOCROOT \
#       -LbHost $GLOB_HOST_OTMM \
#       -OtCaPem $GLOB_APACHE_CERT_CAPEM \
#       -OtCaKey $GLOB_APACHE_CERT_CAKEY \
#       -OtCaPw "$GLOB_APACHE_CERT_CAPWD" \
#       -CrtName "$GLOB_APACHE_CERT_CRTNAME" \
#       -CsrName "$GLOB_APACHE_CERT_CSRNAME" \
#       -KeyName "$GLOB_APACHE_CERT_KEYNAME" \
#       -SslCnf "/etc/pki/tls/openssl_ot.cnf" \
#       -ConfigTemplateSsl "$GLOB_CONFDIR/$GLOB_APACHE_SSL_CONFIG_TEMPL" \
#       -ConfigTemplateApache "$GLOB_CONFDIR/$GLOB_APACHE_HTTPS_CONFIG_TEMPL"
#	-ConfigVideoProxySubdir "Video"



        yum install -q -y httpd

        #
        # Configure Apache for SSL termination
        #
        if test -f $InstallDir/conf.d/$LbHost.conf
        then
                echo "*D* Apache SSL termination already configured, skipping..."
        else

                #Apache streaming server
		#TODO: fixup streaming only when selected!
                #perl -pi -e "s/^User.*/User $User/" $InstallDir/conf/httpd.conf
                #perl -pi -e "s/^Group.*/Group $Group/" $InstallDir/conf/httpd.conf
		mkdir -p "$ApacheProxyDocRoot"
                ln -sf $RepositoryHome "$ApacheProxyDocRoot/$ConfigVideoProxySubdir"
		if [ "$DbType" = "local_postgres" ] || [ "$DbType" = "oracle" ]; then
                        $SqlTool -n "$DbUser" -p "$DbUserPw" -u "$JdbcConnection" -r "update otmm_sys_config_settings set config_value='https://$LbHost/$ConfigVideoProxySubdir' where name = 'VIDEO_BASE_URL'"
                        $SqlTool -n "$DbUser" -p "$DbUserPw" -u "$JdbcConnection" -r "update otmm_sys_config_settings set config_value='https://$LbHost/ConfigVideoProxySubdir' where name = 'AUDIO_BASE_URL'"
                else
                        echo "*E* Streaming for $DbType is not configured yet"
                fi

         #      for n in proxy proxy_http fdpass balancer slotmem_shm xml2enc proxy_html; do
         #            a2enmod mod_$n
         #      done

                yum install -q -y mod_ssl

                #default configuration file is in the way...
                test -f $InstallDir/conf.d/ssl.conf && \
			mv $InstallDir/conf.d/ssl.conf $InstallDir/conf.d/ssl.conf.disabled

                SslCertFields="/C=$GLOB_APACHE_CERT_COUNTRY/ST=$GLOB_APACHE_CERT_STATE/L=$GLOB_APACHE_CERT_LOC/O=$GLOB_APACHE_CERT_ORG/CN=$GLOB_SVC_HOSTNAME/emailAddress=$GLOB_APACHE_CERT_EMAIL"

                cp -f $ConfigTemplateSsl $SslCnf
                perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $SslCnf

                cp -f $ConfigTemplateApache $InstallDir/conf.d/$LbHost.conf
                perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $InstallDir/conf.d/$LbHost.conf

                openssl genrsa -out "$KeyName" 2048
                openssl req -new -sha256 -subj "$SslCertFields" -key "$KeyName" -out "$CsrName" -config "$SslCnf"
                openssl x509 -req -in "$CsrName" -CAkey "$OtCaKey" -CA "$OtCaPem" -out "$CrtName" -extfile "$SslCnf" -extensions v3_req -days 360 -sha256 -CAcreateserial -passin "pass:$OtCaPw"

                systemctl restart httpd

        fi
}

function apache_install_proxy () {

	ConfigVideoProxySubdir="Video"
	ConfigurePgAdminUI="ignore"
	ApachePort="80"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

#       -InstallDir $GLOB_APACHEINS \
#       (-RepositoryHome $GLOB_REPOSITORY_HOME \)
#	-ApacheProxyDocRoot $GLOB_APACHE_PROXY_DOCROOT \
#       -LbHost $GLOB_HOST_OTMM \
#       -ConfigStreaming "ignore" \
#	-RepositoryHome
#       -ConfigTemplateApache "$GLOB_CONFDIR/$GLOB_APACHE_PROXY_CONFIG_TEMPL"
#	-SqlTool
#	-JdbcConnection
#	-DbUserPw
#	-DbUser
#	-ConfigVideoProxySubdir "Video"
#	-ApachePort "80"


        yum install -q -y httpd
	usermod -a -G $Group "apache"
       	systemctl enable httpd


        #
        # Configure Apache for SSL termination
        #
        if test -f $InstallDir/conf.d/$LbHost.conf
        then
                echo "*D* Apache Proxy /$InstallDir/conf.d/$LbHost.conf already configured, skipping..."
        else

                #Apache streaming server
                #TODO: move apache configuration out into separate function apache instance/port, avoid symlink, etc..
                #TODO: move db setup out into m_install_otmm
		export GLOB_APACHE_SELECTED_PORT=$ApachePort
		export GLOB_APACHE_DEFINE_PGADMINUI="UnDefine"
		export GLOB_APACHE_DEFINE_COCKPIT="Define"
		if [ "$ConfigurePgAdminUI"  = "install" ]; then
			export GLOB_APACHE_DEFINE_PGADMINUI="Define"
		fi
		export GLOB_APACHE_DEFINE_SSLPROXY="UnDefine"
		ApacheProtocol="http"
		if [ "$ApacheConfigurationType" = "sslproxy" ]; then
			export GLOB_APACHE_DEFINE_SSLPROXY="Define"
			ApacheProtocol="https"
		fi

                if [ "$ConfigStreaming" = "ignore" ] || [ \! -d "$RepositoryHome" ] ; then

                        echo "*D* Configure proxy only: ConfigStreaming=$ConfigStreaming, RepositoryHome=$RepositoryHome, ApacheConfigurationType=$ApacheConfigurationType"

                	cp $ConfigTemplateApache $InstallDir/conf.d/$LbHost.conf
	                perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $InstallDir/conf.d/$LbHost.conf
			diff -Nau $ConfigTemplateApache $InstallDir/conf.d/$LbHost.conf 2>&1 || /bin/true

			if [ "$ApacheConfigurationType" = "sslproxy" ]; then
				apache_setup_mod_ssl \
					-InstallDir "$InstallDir" \
					-ConfigTemplateSsl "$ConfigTemplateSsl" \
					-SslCnf "$SslCnf" \
					-ApacheSslCertAliases "$ApacheSslCertAliases" \
					-ConfigTemplateApache "$ConfigTemplateApache" \
					-LbHost "$LbHost" \
					-KeyName "$KeyName" \
					-CsrName "$CsrName" \
					-CrtName "$CrtName" \
					-OtCaKey "$OtCaKey" \
					-OtCaPem "$OtCaPem" \
					-OtCaPw "$OtCaPw"
			else
				echo "*D* SSL configuration administratively denied"
			fi

       	        	systemctl restart httpd

                else
                        echo "*D* RepositoryHome configured, enable Apache Streaming"
			mkdir -p "$ApacheProxyDocRoot"
                        ln -sf $RepositoryHome "$ApacheProxyDocRoot/$ConfigVideoProxySubdir"

                	cp $ConfigTemplateApache $InstallDir/conf.d/$LbHost.conf
	                perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $InstallDir/conf.d/$LbHost.conf
			diff -Nau $ConfigTemplateApache $InstallDir/conf.d/$LbHost.conf 2>&1 || /bin/true

	                if [ "$DbType" = "local_postgres" ] || [ "$DbType" = "oracle" ]; then
				$SqlTool -n "$DbUser" -p "$DbUserPw" -u "$JdbcConnection" -r "update otmm_sys_config_settings set config_value='$ApacheProtocol://$LbHost/$ConfigVideoProxySubdir' where name = 'VIDEO_BASE_URL'"
				$SqlTool -n "$DbUser" -p "$DbUserPw" -u "$JdbcConnection" -r "update otmm_sys_config_settings set config_value='$ApacheProtocol://$LbHost/$ConfigVideoProxySubdir' where name = 'AUDIO_BASE_URL'"
			else
				echo "*E* Streaming for DbType=$DbType is not configured yet"
				exit 1
			fi

			if [ "$ApacheConfigurationType" = "sslproxy" ]; then
				apache_setup_mod_ssl \
					-InstallDir "$InstallDir" \
					-ConfigTemplateSsl "$ConfigTemplateSsl" \
					-SslCnf "$SslCnf" \
					-ApacheSslCertAliases "$ApacheSslCertAliases" \
					-ConfigTemplateApache "$ConfigTemplateApache" \
					-LbHost "$LbHost" \
					-KeyName "$KeyName" \
					-CsrName "$CsrName" \
					-CrtName "$CrtName" \
					-OtCaKey "$OtCaKey" \
					-OtCaPem "$OtCaPem" \
					-OtCaPw "$OtCaPw"
			else
				echo "*D* SSL configuration administratively denied"
			fi

       	        	systemctl restart httpd

                fi

        fi
}

#
# configure SSL with our own CA authority
#

function apache_setup_mod_ssl_test() {
	export GLOB_APACHE_DEFINE_PGADMINUI="UnDefine"
	export GLOB_APACHE_DEFINE_SSLPROXY="Define"
	export GLOB_APACHE_SELECTED_PORT="443"
	export GLOB_SVC_HOSTNAME="$(hostname)"
	export GLOB_HOST="$(hostname)"
	export GLOB_OTMM_PORT="11090"
	export GLOB_HOST_OTDS_PORT="8080"
	export GLOB_CONF_SOPORT="8983"
	export GLOB_APACHE_CERT_CRTNAME="/etc/pki/tls/certs/$(hostname)_otmm.crt"
	export GLOB_APACHE_CERT_KEYNAME="/etc/pki/tls/private/$(hostname)_otmm.key"
	export GLOB_APACHE_CERT_COMMON_NAME="https://$(hostname)"
	export GLOB_APACHE_CERT_EMAIL="root@localhost"
	export GLOB_APACHE_CERT_ORG="Open Text"
	export GLOB_APACHE_CERT_LOC="London"
	export GLOB_APACHE_CERT_COUNTRY="UK"
	export GLOB_APACHE_CERT_CAKEY="$GLOB_INST/ot.testca.key"
	export GLOB_APACHE_CERT_CAPEM="$GLOB_INST/ot.testca.pem"
	export GLOB_APACHE_CERT_CAPWD="ot.testca.password"
	apache_setup_mod_ssl -InstallDir /etc/httpd -ConfigTemplateSsl /vagrant/apache_ssl_cert.ini_tmpl -SslCnf /etc/pki/tls/openssl_ot.cnf -ApacheSslCertAliases 'aswapp01 aswotmm01' -ConfigTemplateApache /vagrant/apache_os_proxy.conf_temp -LbHost $GLOB_SVC_HOSTNAME -KeyName $GLOB_APACHE_CERT_KEYNAME -CsrName "/etc/pki/tls/certs/$(hostname)_otmm.csr" -CrtName $GLOB_APACHE_CERT_CRTNAME -OtCaKey $GLOB_APACHE_CERT_CAKEY -OtCaPem $GLOB_APACHE_CERT_CAPEM -OtCaPw "$GLOB_APACHE_CERT_CAPWD"
}

function apache_setup_mod_ssl () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	#-InstallDir
	#-ConfigTemplateSsl
	#-SslCnf
	#-ApacheSslCertAliases
	#-ConfigTemplateApache
	#-LbHost
	#-KeyName
	#-CsrName
	#-CrtName
	#-OtCaKey
	#-OtCaPem
	#-OtCaPw

	yum install -q -y mod_ssl

	#default configuration file is in the way...
	test -f $InstallDir/conf.d/ssl.conf && \
		mv $InstallDir/conf.d/ssl.conf $InstallDir/conf.d/ssl.conf.disabled

	SslCertFields="/C=$GLOB_APACHE_CERT_COUNTRY/ST=$GLOB_APACHE_CERT_STATE/L=$GLOB_APACHE_CERT_LOC/O=$GLOB_APACHE_CERT_ORG/CN=$GLOB_SVC_HOSTNAME/emailAddress=$GLOB_APACHE_CERT_EMAIL"

	cp $ConfigTemplateSsl $SslCnf
	perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $SslCnf

	#Append dns aliases to cnf file
	cnt=1
	for w in $ApacheSslCertAliases; do
		echo "DNS.$(( cnt ++ ))=$w" >> $SslCnf
	done
	diff -Nau $ConfigTemplateSsl $SslCnf 2>&1 || /bin/true

	cp $ConfigTemplateApache $InstallDir/conf.d/$LbHost.conf
	perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $InstallDir/conf.d/$LbHost.conf
	diff -Nau $ConfigTemplateApache $InstallDir/conf.d/$LbHost.conf 2>&1 || /bin/true

	openssl genrsa -out "$KeyName" 2048
	openssl req -new -sha256 -subj "$SslCertFields" -key "$KeyName" -out "$CsrName" -config "$SslCnf"
	openssl x509 -req -in "$CsrName" -CAkey "$OtCaKey" -CA "$OtCaPem" -out "$CrtName" -extfile "$SslCnf" -extensions v3_req -days 360 -sha256 -CAcreateserial -passin "pass:$OtCaPw"
}

# Create CSR request and, if necessary, sign with our test ca
function apache_setup_mod_ssl_create_csr () {

	${GLOB_CONFDIR:?"Invalid configuration GLOB_CONFDIR"}
	ConfigTemplateSsl="$GLOB_CONFDIR/$GLOB_APACHE_SSL_CONFIG_TEMPL"
	SslCnf="/etc/pki/tls/openssl_ot.cnf"
	KeyName="$GLOB_APACHE_CERT_KEYNAME" 
	CsrName="/etc/pki/tls/certs/$(hostname)_otmm.csr"
	CrtName="/etc/pki/tls/certs/$(hostname)_otmm.crt"
	SslCertFields="/C=$GLOB_APACHE_CERT_COUNTRY/ST=$GLOB_APACHE_CERT_STATE/L=$GLOB_APACHE_CERT_LOC/O=$GLOB_APACHE_CERT_ORG/CN=$GLOB_SVC_HOSTNAME/emailAddress=$GLOB_APACHE_CERT_EMAIL"
	ApacheSslCertAliases="$GLOB_SVC_HOSTNAME"
	OtCaPem="$GLOB_APACHE_CERT_CAPEM"
	OtCaKey="$GLOB_APACHE_CERT_CAKEY"
	OtCaPw="$GLOB_APACHE_CERT_CAPWD"

	while [ -n "$1" -a -n "$2" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "$1" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -f "$SslCnf" ]; then
		echo "*W* $SslCnf exists, reuse existing settings. Please check DNS entries match '$SslCertFields'!"
	else
		if [ -f "$ConfigTemplateSsl" ]; then
			cp $ConfigTemplateSsl $SslCnf
			perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $SslCnf
		
			#Append dns aliases to cnf file
			cnt=1
			for w in $ApacheSslCertAliases; do
				echo "DNS.$(( cnt ++ ))=$w" >> $SslCnf
			done
			diff -Nau $ConfigTemplateSsl $SslCnf || /bin/true
		else
			echo "*F* Cannot access ConfigTemplateSsl=$ConfigTemplateSsl"
			exit 1
		fi
	fi

	if [ -f "$KeyName" ]; then
		echo "*D* Found private key file"
	else
		echo "*W* Private key file is missing, creating new one!"
		openssl genrsa -out "$KeyName" 2048
	fi

	#backup existing CSR file
	if [ -f "$CsrName" ]; then
		localfile=$(mktemp -p $(dirname $CsrName) -t $(basename $CsrName).backup.XXX)
		mv -f $CsrName $localfile
	fi

	openssl req -new -sha256 -subj "$SslCertFields" -key "$KeyName" -out "$CsrName" -config "$SslCnf"
	if [ "$SignThisRequest" = "yes" ]; then
		if [ -f "$CrtName" ]; then
			localfile=$(mktemp -p $(dirname $CrtName) -t $(basename $CrtName).backup.XXX)
			mv -f $CrtName $localfile
		fi
                openssl x509 -req -in "$CsrName" -CAkey "$OtCaKey" -CA "$OtCaPem" -out "$CrtName" -extfile "$SslCnf" -extensions v3_req -days 360 -sha256 -CAcreateserial -passin "pass:$OtCaPw"
	fi

	echo -n "Certificate: "
	openssl x509 -modulus -noout -in $CrtName | openssl md5
	echo -n "Request: "
	openssl req -modulus -noout -in $CsrName | openssl md5
	echo -n "Key: "
	openssl rsa -noout -modulus -in $KeyName | openssl md5
	echo -n "Key integrity: "
	openssl rsa -check -noout -in $KeyName 
}


