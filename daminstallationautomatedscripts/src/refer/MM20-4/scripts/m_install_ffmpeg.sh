function install_dmts_ffmpeg_binaries () {
	Archive="*undef*"
	InstallDir="/opt/mm/ffmpegdir"
	User="otmm"
	DmtsHome="/opt/mm/dmts"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -d "$InstallDir" ]; then
		echo "*D* FFMPEG InstallDir=$InstallDir exists, skipping the installation"
		return
	fi

	if which -s ffmpeg &>/dev/null; then
		echo "*D* install_dmts_ffmpeg_binaries: Ffmpeg binares found, skipping the installation"
		return
	fi

	if which -s xzcat &>/dev/null; then
		:
	else
		bash -c "BASH_XTRACEFD= yum install -q -y xz"
	fi

	if [ -f "$Archive" ]; then
		echo "*D* install_dmts_ffmpeg_binaries: Found $Archive"
	else
		echo "*F* Invalid argument, cannot access Archive='$Archive'"
		exit 1
	fi

	OtmmRc=$( IFS=:; set -- $(getent passwd $User); echo $6 )"/.otmmrc"
	if [ \! -f "$OtmmRc" ]; then
		echo "*F* User=$User ($OtmmRc) does not have otmmrc file"
		exit 1
	fi

	echo "*D* install static linked binaries from $Archive to $InstallDir"
	ffmpegarcdir=$(su -c 'mktemp -d -t tmp.installer.ffmpeg.XXXXXX' $User)
        xzcat $Archive | tar -xC $ffmpegarcdir  -f -
	ffmpegdir=$(echo "$ffmpegarcdir"/*64bit-static )
	mv "$ffmpegdir" "$InstallDir"
	chown -R $User "$InstallDir"
#	install -d -o $User "$InstallDir/bin"
#        mv $ffmpegdir/ff* $ffmpegdir/qt* "$InstallDir/bin"
#	mv $ffmpegdir/* "$InstallDir"
	cp -r "$InstallDir/model" /usr/local/share/
        
        #add FFMPEG to the executable path
	if grep -q FFMPEG_HOME "$OtmmRc"; then
                echo "*D* FFMPEG_HOME already set in $OtmmRc"
        else
                echo "*D* set FFMPEG_HOME to $OtmmRc"
                echo 'export FFMPEG_HOME="'"$InstallDir"'"' >> $OtmmRc
        fi

	dmtsservice="$DmtsHome/dmts-video/conf/dmts-video.service"

	echo "*D* Configure DMTS Video service in $dmtsservice" 
	test -f "${dmtsservice}.orig" || cp "${dmtsservice}" "${dmtsservice}.orig"
	perl -pi -e 's!^Environment=FFMPEG_HOME=.*!Environment=FFMPEG_HOME='"$InstallDir"'!g' $dmtsservice
	perl -pi -e 's/^User=.*/User='"$User"'/g' $dmtsservice
	diff -Nau "${dmtsservice}.orig" "$dmtsservice" || /bin/true

	chown -R $User $InstallDir

}

function install_ffmpeg_shareddirs () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	#-Group -User -Stage -Target
	if [ "$(restricted_root_permissions -Path $Stage)" = "no" ]; then
		install -m 0777 -g $Group -o $User -d $Stage
	else
		2>& echo "*W* Ensure $Stage is writable by $User, NFS permissions apply"
	fi
	if [ "$(restricted_root_permissions -Path $Target)" = "no" ]; then
		install -m 0777 -g $Group -o $User -d $Target
	else
		2>& echo "*W* Ensure $Target is writable by $User, NFS permissions apply"
	fi

}

