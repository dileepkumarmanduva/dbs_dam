host_install_jdk () {
	YumFlags="-q"
        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi	

	if [ -n "${Package:-}" ]; then
		echo "*D* yum install $YumFlags -y $Package"
		yum install $YumFlags -y $Package
	elif [ -n "${Rpm:-}" ]; then
		(rpm -qa | grep $Rpm) || rpm -iv $Rpm
	else
                echo "*F* Invalid argument,  must be -Package or -Rpm"
                exit 1
	fi

	if [ -n "${Archive:-}" ]; then
		echo "*F* unsupported (for now)"
		exit 1
	fi
}

install_jdk () {
        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi	

	if [ 	-z "$UserHome" -o \
	    	-z "$User" -o \
	    	-z "$Group" -o \
	    	-z "$Archive" -o \
	    	-z "$InstallDir" -o \
	    	-z "$ArchiveTopDir" ]; then
		echo "*F* Invalid arguments:"
		echo "*F* User=$User, Group=$Group, UserHome=$UserHome -- new owner of the JDK files and path to .profile"
		echo "*F* Archive=$Archive, ArchiveTopDir=$ArchiveTopDir -- the tar ball and top directory"
		echo "*F* InstallDir=$InstallDir -- where to install the JDK, JAVA_HOME is then $InstallDir/jdk"
		exit 1
	fi

	if [ -d $InstallDir/jdk ]; then
		echo "*D* $InstallDir/jdk exists, ignoring $Archive install"
	else
		install -o $User -g $Group -d $InstallDir
		tar -xf $Archive -C $InstallDir
		chown -R $User:$Group $InstallDir
		ln -sf $InstallDir/$ArchiveTopDir $InstallDir/jdk
	
		echo "JAVA_HOME=\"$InstallDir/jdk\"" >> $UserHome/.profile
		echo "PATH=\"\$JAVA_HOME/bin:\$PATH\"" >> $UserHome/.profile
	fi
}

compare_jdk_version () {
	JavaHome=''

	MinVers="1.0.0_000"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi	

	if [ -n "$JavaHome" ]; then
		jver=$( "$JavaHome/bin/java" -version 2>&1 | head -1)
	else
		jver=$( java -version 2>&1 | head -1)
	fi
	echo "$jver" |\
		awk -F'"' -v minvers="$MinVers" \
			'/version/{\
				split($2,a,"[._]");\
				split(minvers,b,"[._]");\
				ax=sprintf("%04d%04d%04d%05d",a[1],a[2],a[3],a[4]);\
				bx=sprintf("%04d%04d%04d%05d",b[1],b[2],b[3],b[4]);\
				if (bx>ax){exit 1}else{exit 0}\
			}'
	comp=$?
	>&2 echo "*D* $jver > $MinVers => $comp"
	return $comp
}


function config_alternatives () {
	UpdateJavaCaCerts="ignore"

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi	

        #Oracle jdk is jdk1.
        id=$(echo '' | update-alternatives --config java 2>/dev/null|awk '/java-11-openjdk/{printf($1)}')
        if [ "$id" = "1" ] || [ "$id" = "2" ] || [ "$id" = "3" ]; then
                echo "$id" | update-alternatives --config java
        elif [ "$id" = '+' ] || [ "$id" = '*+' ]; then
                echo "*D* suitable java version already selected"
        else
                echo "*E* Cannot choose an alternative for java with id='$id'"
		exit 1
        fi

	#ensure the cacerts is managed in /etc/pki
	if [ "$UpdateJavaCaCerts" = "ignore" ]; then
		echo "*D* Update of certificate administratively denied..."	
	elif [ -h "$JavaHome/jre/lib/security/cacerts" ]; then
		echo "*D* Confirmed link `ls -l $JavaHome/jre/lib/security/cacerts`"
	elif [ -f "$JavaPkiCacerts" ]; then
		echo "*D* Confirmed cacerts is `ls -l $JavaPkiCacerts`"
		test -f "$JavaHome/jre/lib/security/cacerts.original" ||
			mv $JavaHome/jre/lib/security/cacerts{,.original}
		ln -sf "$JavaPkiCacerts" "$JavaHome/jre/lib/security/cacerts"
	else
		echo "*E* Cacerts not managed in $JavaPkiCacerts"
	fi

	
}
