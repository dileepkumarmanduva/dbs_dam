function install_conf_otds () {
	OtdsWorkaroundLocalhost="ignore"

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi	

	if [ "$OtdsInstallDir" != "$GLOB_DSINS" ]; then
		echo "*F* OtdsInstallDir must match GLOB_DSINS, $OtdsInstallDir ?= $GLOB_DSINS"
		echo "GLOB_DSINS is used in the otdsresponse file and elsewhere"
		exit 1
	fi

	if [ "$DestroyInstall" = "purge" ]; then
		localbackup=$(su -c "mktemp -t 'tmp.otdsinstdir.XXXXX' -d" $User)
		echo "*W* Destroying any existing OTDS installation, creating backup in ${localbackup}"
		mkdir "${localbackup}/otds" "${localbackup}/unixsetup" "${localbackup}/products"
		test -d "$OtdsInstallDir" && mv "$OtdsInstallDir" "$localbackup/otds"
		test -d "/etc/opentext/unixsetup" && mv "/etc/opentext/unixsetup" "$localbackup/unixsetup"
		test -d "/etc/opentext/products" && mv "/etc/opentext/products" "$localbackup/products"
	fi

	#Gotcha - old files may not get overwritten on retry
	if [ -d "/etc/opentext/unixsetup" ] || [ -d "/etc/opentext/products" ] ; then
		echo "*F* check for stale files found possible unwanted matches in /etc/opentext"
		exit 1
	fi

        localarc=$(su -c "mktemp -t tmp.installer.otds.XXXXXX" $User)
        su -c "cp $Archive $localarc" $User
        install -d $LocalDir -o $User -g $Group
        su -c "tar -x -C $LocalDir -f $localarc" $User
        install -o $User -g $Group $ConfigTemplate $LocalDir/otdsresponse
        perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $LocalDir/otdsresponse
	diff -Nau $ConfigTemplate $LocalDir/otdsresponse || /bin/true

        if [ "$StopAtInstaller" = "yes" ]; then
                echo "Stopped at Installer, command 'cd $LocalDir && ./setup -rf otdsresponse -qi' as user $User"
		echo "The install and start tomcat with something like: tomcat_startup -ShutdownPort $GLOB_HOST_OTDS_SHUTDOWN_PORT -User $GLOB_USER -PidDir /var/run/tomcat_otds -PidFile tomcat_otds.pid -InstallDir $GLOB_TCINS"
                exit 1
        else
		#the installer tries to validate a self signed certificate with the CN of the hostname
		#but fixes the call itself to localhost - work around this issue
		#NB: this script sets the ssl debug flag to inspect logfile for certificatel logging 
		if [ "$OtdsWorkaroundLocalhost" = "install" ]; then
			oldname=$(hostname)	
			oldenv=${GLOB_HOST_OTDS_PRIMARY}
			hostnamectl set-hostname localhost
			export GLOB_HOST_OTDS_PRIMARY="localhost"
			echo "*W* workaround! Changing hostname '$oldname' -> localhost, GLOB_HOST_OTDS_PRIMARY '$oldenv' -> localhost. If the installer errors, run 'hostnamectl set-hostname $oldname' to restore value manually."
                	su - -c "cd $LocalDir; export DEBUG=1; export OPENDJ_JAVA_ARGS="-Djavax.net.debug=ssl"; ./setup -rf otdsresponse -qi" $User
			hostnamectl set-hostname $oldname
			GLOB_HOST_OTDS_PRIMARY=$oldenv
			echo "*W* workaround! Reset hostname to '$(hostname)'"
		else
                	su - -c "cd $LocalDir; export DEBUG=1; export OPENDJ_JAVA_ARGS="-Djavax.net.debug=ssl"; ./setup -rf otdsresponse -qi" $User
		fi
        fi
}

function install_otds_patch () { 

	#-HotfixFile
	#-InstallDir
	#-Archive
	#-User

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

	while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -f "$InstallDir/$HotfixFile" ]; then
		echo "*D* Hotfix $InstallDir/$HotfixFile exists, skipping"
	else
        	echo "*D* unzip -B -d $InstallDir $Archive"
        	su -c "unzip -B -d $InstallDir $Archive" $User
	fi

	#echo "********************MANUAL STEPS********************"
	#echo "Resources > Media Management > Actions > Properties"
	#echo "Add Attribute to Mappings:"
	#echo "__USERNAME__ = oTExternalID1  %s
	#echo "__DOMAINNAME__ = oTDomainName  %s
	#echo "********************MANUAL STEPS********************"
	
	return 0
}

function get_login_token_otds () {
	#-Username
	#-Password
	#-RestUrlNode
	#-CookieFile

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	TOKEN=$(curl -k -s -X POST "$RestUrlNode/otdsws/rest/authentication/credentials" \
		-c "$CookieFile" \
		-H "accept: application/json;charset=utf-8" \
		-H "Content-Type: application/json" \
		-d "{ \"userName\": \"$Username\", \"password\": \"$Password\" }" |\
		python2 -c 'import json,sys;obj=json.load(sys.stdin);print(obj["ticket"])')

	echo $TOKEN
}

function set_trusted_sites_otds () {
	#-Token
	#-RestUrlNode
	#-TrustNode

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	local WHITELIST
	WHITELIST=""
	if [ -n "$TrustNode" ]; then
		local WL1
		local IFS=, 
		read -ra WL1 <<< "$TrustNode"
		declare -A WL2
		for i in "${WL1[@]}"; do 
			WHITELIST="$WHITELIST,\"$i\""
		done
		WHITELIST=${WHITELIST:1}
	fi

	curl -k -s -X PUT "$RestUrlNode/otdsws/rest/systemconfig/whitelist" \
		-H "accept: application/json;charset=utf-8" \
		-H "OTDSTicket: $Token" \
		-H "Content-Type: application/json" \
		-d "{ \"stringList\": [ $WHITELIST ]}"

}

function get_trusted_sites_otds () {

	#-Token
	#-RestUrlNode

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	WHITELIST=$(curl -k -s -X GET "$RestUrlNode/otdsws/rest/systemconfig/whitelist" \
		-H "accept: application/json;charset=utf-8" \
		-H "OTDSTicket: $Token" \
		-H "Content-Type: application/json" |\
		python2 -c 'import json,sys;obj=json.load(sys.stdin);print ",".join(obj["stringList"])')
	echo "$WHITELIST"

}

function add_trusted_sites_otds () {
	#-Token
	#-RestUrlNode
	#-TrustNode

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	WHITELIST=$(get_trusted_sites_otds -Token "$Token" -RestUrlNode "$RestUrlNode")
	if [ -z "$WHITELIST" ]; then
		WHITELIST="$TrustNode"
	else
		local WL1
		local IFS=, 
		read -ra WL1 <<< "$WHITELIST"
		declare -A WL2
		for i in "${WL1[@]}"; do 
			WL2["$i"]="1"
		done
		WL2[$TrustNode]="1"
		WHITELIST=$(IFS=, ;echo "${!WL2[*]}")
	fi
	set_trusted_sites_otds -Token "$Token" -RestUrlNode "$RestUrlNode" -TrustNode "$WHITELIST"
}

function get_mm_resources_otds () {
	ResourceName="Media%20Management"
	#-Token
	#-RestUrlNode
        #-Token
        #-RestUrlNode

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        $(curl -k -s -X GET "$RestUrlNode/otdsws/rest/resources/$ResourceName" \
                -H "accept: application/json;charset=utf-8" \
                -H "OTDSTicket: $Token" \
                -H "Content-Type: application/json" ) 
}

function put_mm_resources_otds () {
	ResourceName="Media%20Management"
	#-Token
	#-RestUrlNode
        #-Token
        #-RestUrlNode
	#-ResourceDefinition

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        $(curl -k -s -X PUT "$RestUrlNode/otdsws/rest/resources/$ResourceName" \
                -H "accept: application/json;charset=utf-8" \
                -H "OTDSTicket: $Token" \
                -H "Content-Type: application/json" \
		-d "$ResourceDefinition" ) 
}

function delete_mm_resource_otds () {
	ResourceName="Media%20Management"
	#-Token
	#-RestUrlNode

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        curl -k -s -X DELETE "$RestUrlNode/otdsws/rest/resources/$ResourceName" \
                -H "accept: application/json;charset=utf-8" \
                -H "OTDSTicket: $Token" \
		-H "Content-Type: application/json" 
}

function delete_mm_partition_otds () {
	PartitionName="OTMM"
	#-Token
	#-RestUrlNode

        if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        curl -k -s -X DELETE "$RestUrlNode/otdsws/rest/partitions/$PartitionName" \
                -H "accept: application/json;charset=utf-8" \
                -H "OTDSTicket: $Token" \
		-H "Content-Type: application/json" 
}

function add_mm_amd_user () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

}

function add_mm_user () {
	PartitionName="OTMM"
	#-Token
	#-RestUrlNode
	#-AccountName
	#-AccountNamePw
	#-AccountDepartment
	#-AccountEmail
	#-AccountFirstname
	#-AccountSurname
	#-AccountInitials

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	val=$(  curl -I -k -s -X GET "${RestUrlNode}otdsws/rest/users/$AccountName" \
                -H "accept: application/json;charset=utf-8" \
                -H "OTDSTicket: $Token" \
		-H "Content-Type: application/json" | awk 'FNR==1{print($2)}'
	)
	if [ "$val" = "404" ]; then
		echo "Creating user $User"
		read -r -d '' SUBMIT <<- EOM
		{"values":
		    [	 {"name":"accountDisabled","values":["false"]}
			,{"name":"UserMustChangePasswordAtNextSignIn","values":["true"]}
			,{"name":"UserCannotChangePassword","values":["false"]}
			,{"name":"PasswordNeverExpires","values":["false"]}
			,{"name":"displayName","values":["$AccountSurname, $AccountFirstname"]}
			,{"name":"userPassword","values":["$AccountNamePw"]}
			,{"name":"givenName","values":["$AccountFirstname"]}
			,{"name":"initials","values":["$AccountInitials"]}
			,{"name":"mail","values":["$AccountEmail"]}
			,{"name":"oTDepartment","values":["$AccountDepartment"]}
			,{"name":"sn","values":["$AccountName"]}
		    ]	
		    ,"customAttributes":[]
		    ,"userPartitionID":"$PartitionName"
		    ,"location":"ou=Root,ou=${PartitionName},ou=IdentityProviders,dc=identity,dc=opentext,dc=net"
		    ,"name":"$AccountName"
		    ,"description":""
	    }
EOM

	curl 	-k -s -X POST "${RestUrlNode}otdsws/rest/users" \
		-H "accept: application/json;charset=utf-8" \
		-H "OTDSTicket: $Token" \
		-H "Content-Type: application/json" "-d${SUBMIT}"

	else
		echo "User $User already exists, ignore..."
	fi

}
