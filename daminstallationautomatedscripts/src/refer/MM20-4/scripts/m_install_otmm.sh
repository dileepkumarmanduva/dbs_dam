#
# Build the silent installer file for OTMM
#
function install_otmm_build_mmtxt () {
	#-TranscodeExport
        local ConfigTemplate=""
	OtdsBaseUrl="*undef*"
	OtdsPassword="*undef*"
	SupportManagerUrl="*undef*"
	OtmmBaseUrl="*undef*"
	DmtsBaseUrl="*undef*"
	DmtsShare="*undef*"
	StopInstaller="no"
	InstallSchema="no"
	Asserver="tomee_embed"
	Database="local_postgres"
	OtmmDbStorage="manual"
	ClusterMode="*undef*"
	RabbitNode="*undef*"
	RabbitNodePort="*undef*"
	RabbitUser="*undef*"
	RabbitUserPw="*undef*"
	TomeeHome="*undef*"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	declare -A setg
	declare -A commentg

	setg=( ['OTDS_BASE_URL']="$OtdsBaseUrl" ['OTDS_ADMIN_PASSWORD']="$OtdsPassword" ['SUPPORT_MANAGER_BASE_URL']="$SupportManagerUrl" ['WEB_APPS_BASE_URL']="$OtmmBaseUrl" ['DMTS_SERVICE_BASE_URL']="$DmtsBaseUrl" ['RESTART_SYSTEM']="0" ['DMTS_WORKING_AREA']="$DmtsShare" )
        commentg=( ['UPGRADE_OTMM']="0" )

        if [ $StopInstaller = "yes" ]; then
                commentg['INSTALLER_UI']='ignore'
	else
                setg['INSTALLER_UI']='silent'
	fi

        #Primary is default, secondary by choice
	echo "*D* InstallSchema=$InstallSchema"
        if [ "$InstallSchema" = 'yes' ]; then
                setg['CHOSEN_INSTALL_SET']='Silent'
                setg['CREATE_USER_AND_PARTITION']=1
        else
                setg['CHOSEN_INSTALL_SET']='SilentAppOnly'
                setg['CREATE_USER_AND_PARTITION']=0
                commentg['OTMM_PRODUCER_USER_NAME']="*undef*"
                commentg['OTMM_PRODUCER_USER_PASSWORD']="*undef*"
                commentg['CONFIRM_OTMM_PRODUCER_USER_PASSWORD']="*undef*"
	fi

        #Choice of application server
        if [ $Asserver = 'tomee_embed' ]; then
                echo "App Server choice is '$Asserver'"
                setg['TOMEE_PLUS']='1'
                setg['EMBED_TOMEE']='1'
                setg['TOMEE_CUSTOM_PATH']="$TomeeHome" 
                commentg['JBOSS_EAP']='*undef*'
                commentg['JBOSS_CUSTOM_PATH']='*undef*'
	else
                echo "App Server $Asserver is not implemented"
		exit 1
	fi

        #Blank out all unwanted choices - as it saves loads of time tracking down spurious settings

        #Ignore HANA (for now)
        commentg['HANA_SERVER']="*undef*";
        commentg["HANA_PORT"]="*undef*";
        commentg["HANA_ADMIN"]="*undef*";
        commentg["HANA_ADMIN_PW"]="*undef*";
        commentg["HANA_USER"]="*undef*";
        commentg["HANA_USER_PW"]="*undef*";

        #No need for Oracle
        if [ "$Database" != 'oracle' ]; then
                commentg["ORACLE_SERVICE_NAME"]="*undef*";
                commentg["DB_SELECTED_ORACLE"]="0";
                commentg["ORACLE_SERVER"]="*undef*";
                commentg["ORACLE_PORT"]="*undef*";
                commentg["TEAMSDB_ADMIN"]="*undef*";
                commentg["TEAMSDB_ADMIN_PW"]="*undef*";
                commentg["CREATE_TABLE_SPACE"]="*undef*";
                commentg["TEAMSDB_DATA_DIR"]="*undef*";
                commentg["TEAMSDB_IDX_DIR"]="*undef*";
                commentg["ORACLE_DEFTAB"]="*undef*";
                commentg["ORACLE_SYSADMIN"]="*undef*";
                commentg["ORACLE_SYSADMIN_PW"]="*undef*";
                commentg["ORACLE_TEMPTAB"]="*undef*";
	fi

        if [ "$Database" != 'sqlserver' ]; then
                commentg["SQLSERVER_ADMIN"]="*undef*";
                commentg["DB_SELECTED_SQLSERVER"]="0";
                commentg["SQLSERVER_ADMIN_PW"]="*undef*";
                commentg["DATABASE_NAME"]="*undef*";
                commentg["SQLSERVER_DATA_DIR"]="*undef*";
                commentg["SQLSERVER_LOG_DIR"]="*undef*";
                commentg["SQLSERVER_DATA_SIZE"]="*undef*";
                commentg["SQLSERVER_LOG_SIZE"]="*undef*";
                commentg["SQLSERVER_SERVER"]="*undef*";
                commentg["SQLSERVER_PORT"]="*undef*";
                commentg["SQLSERVER_USER"]="*undef*";
                commentg["SQLSERVER_USER_PW"]="*undef*";
	fi

        #No need for Postgres
        if [ "$Database" != 'local_postgres' ] && [ "$Database" != 'postgres' ]; then
                commentg["POSTGRESQL_DATA_TABLESPACE_DIR"]="*undef*";
                commentg["DB_SELECTED_POSTGRESQL"]="0";
                commentg["POSTGRESQL_INDEX_TABLESPACE_DIR"]="*undef*";
                commentg["POSTGRESQL_DEFTAB"]="*undef*";
                commentg["POSTGRESQL_ADMIN"]="*undef*";
                commentg["POSTGRESQL_ADMIN_PW"]="*undef*";
                commentg["POSTGRESQL_USER"]="*undef*";
                commentg["POSTGRESQL_USER_PW"]="*undef*";
                commentg["POSTGRESQL_DATABASE_NAME"]="*undef*";
	fi

	#configure postgres as database
        if [ "$Database" = 'local_postgres' ] || [ "$Database" = 'postgres' ]; then
                echo "Database choice is '$Database'" 
                setg['DB_SELECTED_POSTGRESQL']='1'
                if [ "$OtmmDbStorage" != "manual" ]; then
                        setg['CREATE_POSTGRESQL_TABLE_SPACE']='1'
		else
                        setg['CREATE_POSTGRESQL_TABLE_SPACE']='0'
                        commentg['POSTGRESQL_DATA_TABLESPACE_DIR']='*undef*'
                        commentg['POSTGRESQL_INDEX_TABLESPACE_DIR']='*undef*'
		fi

        #configure sqlserver as database
        elif  [ "$Database" = "sqlserver" ]; then

                echo "Database choice is '$Database'" 
                $setg['DB_SELECTED_SQLSERVER']='1'

                if [ "$InstallSchema" != "yes" ]; then
                        echo "*E* OTMM (InstallSchema=$InstallSchema) for SQL Server, Ignore OtmmDbStorage settings" 
                        setg['SQLSERVER_DATA_DIR']="*undef*"
                        setg['SQLSERVER_LOG_DIR']="*undef*"
		elif [ "$OtmmDbStorage" -eq "manual" ]; then
                        echo "*E* OTMM (OtmmDbStorage=$OtmmDbStorage) for SQL Server always creates schema and user. Abort."
                        exit 1 
                elif [ "$OtmmDbStorage" -eq "default" ]; then
                        echo "*E* OTMM (OtmmDbStorage=$OtmmDbStorags) uses the preset values."
		else
                        setg['SQLSERVER_DATA_DIR']="$OtmmDbStorage\DATA"
                        setg['SQLSERVER_LOG_DIR']="$OtmmDbStorage\LOG"
		fi

        #configure oracle as database
	elif [ "$Database" = "oracle" ]; then

               echo "*D* Database choice is '$Database'" 
                setg['DB_SELECTED_SQLSERVER']='1'

                if [ "$OtmmDbStorage" = "manual" ]; then
                        setg['CREATE_TABLE_SPACE']='0'
                        commentg['TEAMSDB_DATA_DIR']='*undef*'
                        commentg['TEAMSDB_IDX_DIR']='*undef*'
		else
                        setg['CREATE_TABLE_SPACE']='1'
                        setg['TEAMSDB_DATA_DIR']="$OtmmDbStorage\DATA"
                        setg['TEAMSDB_IDX_DIR']="$OtmmDbStrorage\INDEX"
		fi


	else
                echo  "*E* Database $Database is not implemented" 
		exit 1
	fi

        setg['RABBITMQ_INSTALL_LATER']='0'
	commentg['RABBITMQ_INSTALL']='0'

        if [ "$ClusterMode" = 'standalone' ]; then
                setg['RABBITMQ_MESSAGE_BROADCAST']='0'
                commentg['JMS_MESSAGE_BROADCAST']='0'
                commentg['RABBITMQ_CONFIGURE']='0'
                commentg['RABBITMQ_SERVER']='*undef*'
                commentg['RABBITMQ_PORT']='*undef*'
                commentg['RABBITMQ_USERNAME']='*undef*'
                commentg['RABBITMQ_PASSWORD']='*undef*'
	elif [ $ClusterMode = 'rabbit_configure' ]; then
                commentg['JMS_MESSAGE_BROADCAST'] = '0'
                setg['RABBITMQ_MESSAGE_BROADCAST']='1'
                setg['RABBITMQ_INSTALL']=0
                setg['RABBITMQ_CONFIGURE']=1
                setg['RABBITMQ_SERVER']=$RabbitNode
                setg['RABBITMQ_PORT']=$RabbitNodePort
                setg['RABBITMQ_USERNAME']=$RabbitUser
                setg['RABBITMQ_PASSWORD']=$RabbitUserPw
	elif [ $ClusterMode = 'rabbit_install' ]; then
                commentg['JMS_MESSAGE_BROADCAST']='0'
                setg['RABBITMQ_MESSAGE_BROADCAST']='1'
                setg['RABBITMQ_INSTALL']=1
                setg['RABBITMQ_CONFIGURE']=0
                setg['RABBITMQ_SERVER']=$RabbitNode
                setg['RABBITMQ_PORT']=$RabbitNodePort
                setg['RABBITMQ_USERNAME']=$RabbitUser
                setg['RABBITMQ_PASSWORD']=$RabbitUserPw
	else
                echo "*E* ClusterMode $ClusterMode is not implemented"
		exit 1
	fi


#        if [ "$ConfigureOtmm" = "tomee_embed" ]; then
#                perl -pi -e 's/^#*JBOSS_EAP=.*/#JBOSS_EAP=0/g' $ConfigTemplate
#                perl -pi -e 's/^#*TOMEE_PLUS=.*/TOMEE_PLUS=1/g' $ConfigTemplate
#                perl -pi -e 's/^#*EMBED_TOMEE=.*/EMBED_TOMEE=1/g' $ConfigTemplate
#                perl -pi -e 's/^#*JBOSS_CUSTOM_PATH=.*/#JBOSS_CUSTOM_PATH=*undef*/g' $ConfigTemplate
#                #perl -pi -e 's/^#*TOMEE_CUSTOM_PATH=.*/#TOMEE_CUSTOM_PATH=*undef*/g' $ConfigTemplate
#	elif [ "$ConfigureOtmm" = "tomee" ]; then
#                perl -pi -e 's/^#*JBOSS_EAP=.*/#JBOSS_EAP=0/g' $ConfigTemplate
#                perl -pi -e 's/^#*TOMEE_PLUS=.*/TOMEE_PLUS=1/g' $ConfigTemplate
#                perl -pi -e 's/^#*EMBED_TOMEE=.*/EMBED_TOMEE=0/g' $ConfigTemplate
#                perl -pi -e 's/^#*JBOSS_CUSTOM_PATH=.*/#JBOSS_CUSTOM_PATH=*undef*/g' $ConfigTemplate
#                #perl -pi -e 's/^#*TOMEE_CUSTOM_PATH=.*/#TOMEE_CUSTOM_PATH=*undef*/g' $ConfigTemplate
#	elif [ "$ConfigureOtmm" = "jboss" ]; then
#                perl -pi -e 's/^#*JBOSS_EAP=.*/JBOSS_EAP=1/g' $ConfigTemplate
#                perl -pi -e 's/^#*TOMEE_PLUS=.*/#TOMEE_PLUS=0/g' $ConfigTemplate
#                perl -pi -e 's/^#*EMBED_TOMEE=.*/#EMBED_TOMEE=0/g' $ConfigTemplate
#                perl -pi -e 's/^#*TOMEE_CUSTOM_PATH=.*/#TOMEE_CUSTOM_PATH=*undef*/g' $ConfigTemplate
#                #perl -pi -e 's/^#*JBOSS_CUSTOM_PATH=.*/#JBOSS_CUSTOM_PATH=*undef*/g' $ConfigTemplate
#        else
#                echo "*F* ConfigureOtmm: $ConfigureOtmm is not valid or supported"
#		exit 1
#        fi
#
#        if [ "$DoInstallSchema" = "install" ]; then
##                echo "$Host is a backend server, install OTMM schema"
#                perl -pi -e 's/^CHOSEN_INSTALL_SET=.*/CHOSEN_INSTALL_SET=Silent/g' $ConfigTemplate
#        elif [ "$DoInstallSchema" = "ignore" ]; then
##                echo "$Host is a frontend server, ignore OTMM schema - requires existing schema in OTDS"
#                perl -pi -e 's/^CHOSEN_INSTALL_SET=.*/CHOSEN_INSTALL_SET=SilentAppOnly/g' $ConfigTemplate
#        else
#                echo "*F* Invalid flag DoInstallSchema=$DoInstallSchema"
#                exit 1
#        fi
#
#	>&2 echo "*D* DbType = $DbType, configuration"
#        if [ "$DbType" = "local_postgres" ]; then
#		>&2 echo "*D* Postgres database GLOB_DB_POSTGRES_DB=$GLOB_DB_POSTGRES_DB, GLOB_OTMM_DB_NAME=$GLOB_OTMM_DB_NAME "
#                perl -pi -e 's/#*DB_SELECTED_ORACLE=.*/#DB_SELECTED_ORALCE=0/g' $ConfigTemplate
#                perl -pi -e 's/#*DB_SELECTED_POSTGRESQL=.*/DB_SELECTED_POSTGRESQL=1/g' $ConfigTemplate
#                perl -pi -e 's/#*DB_SELECTED_SQLSERVER=.*/#DB_SELECTED_SQLSERVER=0/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_ADMIN=.*/SQLSERVER_ADMIN=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_ADMIN_PW=.*/SQLSERVER_ADMIN_PW=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_SERVER=.*/SQLSERVER_SERVER=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_PORT=.*/SQLSERVER_PORT=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_USER=.*/SQLSERVER_USER=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_USER_PW=.*/SQLSERVER_USER_PW=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/^#*DATABASE_NAME=.*/DATABASE_NAME=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_DATA_DIR=.*/SQLSERVER_DATA_DIR=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_LOG_DIR=.*/SQLSERVER_LOG_DIR=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_DATA_SIZE=.*/SQLSERVER_DATA_SIZE=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_LOG_SIZE=.*/SQLSERVER_LOG_SIZE=*undef*/g' $ConfigTemplate
#        elif [ "$DbType" = "oracle" ]; then
#		>&2 echo "*D* Oracle database GLOB_DB_POSTGRES_DB=$GLOB_DB_POSTGRES_DB, GLOB_OTMM_DB_NAME=$GLOB_OTMM_DB_NAME "
#                perl -pi -e 's/#*DB_SELECTED_ORACLE=.*/DB_SELECTED_ORACLE=1/g' $ConfigTemplate
#                perl -pi -e 's/#*DB_SELECTED_POSTGRESQL=.*/#DB_SELECTED_POSTGRESQL=0/g' $ConfigTemplate
#                perl -pi -e 's/#*DB_SELECTED_SQLSERVER=.*/#DB_SELECTED_SQLSERVER=0/g' $ConfigTemplate
#                if [ "$TeamsDbDir" = "manual" ]; then
#                	>2& echo "*D* TeamsDbDir=$TeamsDbDir, Oracle storage already prepared"
#                        perl -pi -e 's/CREATE_TABLE_SPACE=.*/CREATE_TABLE_SPACE=0/g' $ConfigTemplate
#                else
#                	>2& echo "*D* Automatically set storage to $TeamsDbDir"
#                        perl -pi -e 's/CREATE_TABLE_SPACE=.*/CREATE_TABLE_SPACE=1/g' $ConfigTemplate
#                        perl -pi -e 's!TEAMSDB_DATA_DIR=.*!TEAMSDB_DATA_DIR='$TeamsDbDir'/data!g' $ConfigTemplate
#                        perl -pi -e 's!TEAMSDB_IDX_DIR=.*!TEAMSDB_IDX_DIR='$TeamsDbDir'/index!g' $ConfigTemplate
#                fi
#                perl -pi -e 's/#*POSTGRESQL_DATABASE_NAME=.*/POSTGRESQL_DATABASE_NAME=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_USER_PW=.*/POSTGRESQL_USER_PW=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_USER=.*/POSTGRESQL_USER=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_DATA_TABLESPACE_DIR=.*/POSTGRESQL_DATA_TABLESPACE_DIR=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_DATA_TABLESPACE_DIR=.*/POSTGRESQL_INDEX_TABLESPACE_DIR=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*CREATE_POSTGRESQL_TABLE_SPACE=.*/CREATE_POSTGRESQL_TABLE_SPACE=0/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_ADMIN=.*/SQLSERVER_ADMIN=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_ADMIN_PW=.*/SQLSERVER_ADMIN_PW=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_SERVER=.*/SQLSERVER_SERVER=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_PORT=.*/SQLSERVER_PORT=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_USER=.*/SQLSERVER_USER=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_USER_PW=.*/SQLSERVER_USER_PW=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/^#*DATABASE_NAME=.*/DATABASE_NAME=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_DATA_DIR=.*/SQLSERVER_DATA_DIR=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_LOG_DIR=.*/SQLSERVER_LOG_DIR=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_DATA_SIZE=.*/SQLSERVER_DATA_SIZE=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*SQLSERVER_LOG_SIZE=.*/SQLSERVER_LOG_SIZE=*undef*/g' $ConfigTemplate
#        elif [ "$DbType" = "sqlserver" ]; then
#                perl -pi -e 's/#*DB_SELECTED_ORACLE=.*/#DB_SELECTED_ORACLE=0/g' $ConfigTemplate
#                perl -pi -e 's/#*DB_SELECTED_POSTGRESQL=.*/#DB_SELECTED_POSTGRESQL=0/g' $ConfigTemplate
#                perl -pi -e 's/#*DB_SELECTED_SQLSERVER=.*/DB_SELECTED_SQLSERVER=1/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_DATABASE_NAME=.*/POSTGRESQL_DATABASE_NAME=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_USER_PW=.*/POSTGRESQL_USER_PW=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_USER=.*/POSTGRESQL_USER=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_DATA_TABLESPACE_DIR=.*/POSTGRESQL_DATA_TABLESPACE_DIR=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*POSTGRESQL_DATA_TABLESPACE_DIR=.*/POSTGRESQL_INDEX_TABLESPACE_DIR=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*CREATE_POSTGRESQL_TABLE_SPACE=.*/CREATE_POSTGRESQL_TABLE_SPACE=0/g' $ConfigTemplate
#        else
#                >2& echo "*F* Unsupported db type $DbType"
#        fi
#
#        if [ "$ConfigureTranscode" = "ignore" ]; then
#                perl -pi -e 's/#*DMTS_WORKING_AREA=.*/DMTS_WORKING_AREA=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*Transcode_selected_FFmpeg=.*/#Transcode_selected_FFmpeg=0/g' $ConfigTemplate
#                perl -pi -e 's/#*Transcode_selected_Vantage=.*/#Transcode_selected_Vantage=0/g' $ConfigTemplate
#        elif [ "$ConfigureTranscode" = "vantage" ]; then
#                perl -pi -e 's/#*DMTS_WORKING_AREA=.*/DMTS_WORKING_AREA=*undef*/g' $ConfigTemplate
#                perl -pi -e 's/#*Transcode_selected_FFmpeg=.*/#Transcode_selected_FFmpeg=0/g' $ConfigTemplate
#                perl -pi -e 's/#*Transcode_selected_Vantage=.*/Transcode_selected_Vantage=1/g' $ConfigTemplate
#        elif [ "$ConfigureTranscode" = "ffmpeg" ]; then
#                perl -pi -e 's!#*DMTS_WORKING_AREA=.*!DMTS_WORKING_AREA='"$TranscodeWorkingArea"'!g' $ConfigTemplate
#                perl -pi -e 's/#*Transcode_selected_FFmpeg=.*/Transcode_selected_FFmpeg=1/g' $ConfigTemplate
#                perl -pi -e 's/#*Transcode_selected_Vantage=.*/#Transcode_selected_Vantage=0/g' $ConfigTemplate
#        else
#                >2& echo "*F* ConfigureTranscode=$ConfigureTranscode is not supported" 1>&2
#		exit 1
#        fi

        #set all values
	for k in "${!setg[@]}"; do 
		val=$(eval echo ${setg[$k]})
                echo "*D* Replace $k with $val"
		perl -pi -e 's!^#*('"$k"')=.*!\1='"$val"'!g' $ConfigTemplate
	done

        #comment all unwanted values
	for k in "${!commentg[@]}"; do 
		val=$(eval echo ${commentg[$k]})
                echo "*D* Comment out $k with $val"
		perl -pi -e 's!^#*('"$k"')=.*!#\1='"$val"'!g' $ConfigTemplate
	done

        perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $ConfigTemplate

        echo $ConfigTemplate
}

function install_otmm () {
	#-TeamsRepository
	ServiceTemplate=""
	IndexServiceTemplate=""

	YumFlags="-q"
	DestroyOtmmDBUser="ignore"
	DestroyOtmmDBTablespace="ignore"
	DestroySqlTemplate="purgeoracle.sql.tmpl"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	localconf=$(mktemp -t tmp.installer.otmmconf.XXXXXXX.txt)
        cat "$ConfigTemplate" > "$localconf"

	if [ "$DoInstallSchema" = "install" ]; then
		InstallSchema="yes"
	else
		InstallSchema="no"
	fi

        install_otmm_build_mmtxt \
		-ConfigTemplate "$localconf" \
		-OtdsBaseUrl "$OtdsBaseUrl" \
		-OtmmBaseUrl "$OtmmBaseUrl" \
		-OtdsPassword "$OtdsPassword" \
		-SupportManagerUrl "$SupportManagerUrl" \
		-DmtsBaseUrl "$DmtsBaseUrl" \
		-DmtsShare "$DmtsShare" \
		-StopInstaller "$StopAtInstaller" \
		-InstallSchema "$InstallSchema" \
		-Asserver "$ConfigureOtmm" \
		-TomeeHome "$Tomee_Home" \
		-Database "$DbType" \
		-OtmmDbStorage "$PgInstallDir" \
		-ClusterMode "standalone" \
		-RabbitNode "$OtmmRabbitNode" \
		-RabbitNodePort "$OtmmRabbitNodePort" \
		-RabbitUser "$OtmmRabbitUser" \
		-RabbitUserPw "$OtmmRabbitUserPw"

	diff -Nau $ConfigTemplate $localconf || /bin/true

        chown $User $localconf

	#dependency of the instantclient
        if [ "$DbType" = "oracle" ]; then
		yum install -y $YumFlags  libaio
	fi

        if [ "$DoInstallSchema" = "install" ]; then
                if [ $DbType = 'local_postgres' ]; then
                	echo "InstallOtmmSchema=$DoInstallSchema, prepare Postgres folders"
                        if [ -d $PgInstallDir/data/catalog ]; then
                                echo "$PgInstallDir/data is populated, skip creation"
                        else
                                for n in catalog context cs objstacks pm uois; do
                                        install -m 0775 -g $Group -o postgres -d $PgInstallDir/data/$n
                                        install -m 0775 -g $Group -o postgres -d $PgInstallDir/index/$n
                                done
                                chown postgres:$Group $PgInstallDir/data $PgInstallDir/index
                        fi
                elif [ $DbType = 'oracle' ]; then
                        echo "*D* DbType=$DbType is prepared by the DBA beforehand, skipping"
                elif [ $DbType = 'sqlserver' ]; then
                        echo "*D* DbType=$DbType is prepared by the DBA beforehand, skipping"
                else
                        echo "*F* DbType=$DbType is not fully configured, abort"
                        exit 1
                fi
        else
		echo "*D* Schema installation administratively denied (DbType=$DbType)."
        fi

        if [ -d "$TmpMount/OTMM" ]; then
                echo "$TmpMount exists, ignoring mounting of iso..."
        else
		localarc=$(mktemp -t tmp.mmiso.XXXXXXXX)
                echo "*D* $TmpMount missing, mounting $Archive as $localarc to $TmpMount"
		cp $Archive $localarc
                mkdir -p $TmpMount
                mount -o loop $localarc $TmpMount
        fi


        #tidy
        test -d "$InstallDir" && rm -rf "$InstallDir"

        if [ \! "$DoDestroyExistingSchema" = "destroy" ]; then
                echo "*D* DoDestroyExistingSchema=$DoDestroyExistingSchema, " \
		     "leaving any existing OTMM schema untouched. The installer " \
		     "may fail if this is not a new installation."

        elif [ "$DoInstallSchema" = "install" ]; then

		echo "InstallOtmmSchema=$DoInstallSchema selected, destroy existing $OtmmDbName schema"

		if [ "$DestroyOtmmDBUser" = "destroy" ]; then

			db_common_drop_database \
				-SqlTool "$SqlTool" \
				-DbSysUser "$DbSysUser" \
				-DbSysUserPass "$DbSysUserPass" \
				-JdbcSysConnection "$JdbcConnection" \
				-DbUser "$OtmmDbTeamsUser" \
				-DbName "$OtmmDbName" \
				-DestroyDBTablespace "$DestroyOtmmDBTablespace" 

		else
			echo "*D* DestroyOtmmDBUser=$DestroyOtmmDBUser DB User deletion administratively denied"

                fi
        else
		echo "*D* Schema installation administratively denied (DoInstallSchema=$DoInstallSchema)."
        fi

        _d="$TmpMount/OTMM/Disk1/InstData/NoVM"
        if [ "$StopAtInstaller" = "yes" ]; then
                echo "*D* stopped at OTMM installer"
                echo "*D* set export LAX_DEBUG=1 for more debugging"
                echo "*D* set export JAVA_HOME=$JavaHome"
                echo "*D* set export TEAMS_HOME=$TeamsHome"
                echo "*D* run as $User: cd $_d;$_d/install.bin -i silent -f $localconf"
                echo "*D* post clearnup with chown -R $User:$Group $InstallDir"
		#configure sshd X11 forwarding - for MacOS
		augtool -s -e -At "Sshd incl /etc/ssh/sshd_config" 'set /files/etc/ssh/sshd_config/X11UseLocalhost "yes"' 
		augtool -s -e -At "Sshd incl /etc/ssh/sshd_config" 'set /files/etc/ssh/sshd_config/AddressFamily "inet"' 
		systemctl restart sshd
                exit 1
        else
                #as root!
                export JAVA_HOME=$JavaHome
#               /usr/sbin/tcpdump -w /var/tmp/install.tcpdump -s4096 &
#               pid=$!
                export LAX_DEBUG="1"
                echo "################################################################################"
		cd $_d; #install.bin uses all jar file in the CWD?
                if (su -l -c \
			"BASH_XTRACEFD=; $_d/install.bin -i silent -f $localconf" \
		    $User); then
			echo "*D* Installer succeeded"
		else
			set -x
			if [ -f "$TeamsHome/Media Management_InstallLog.log" ]; then
				echo "*D*: Found '$TeamsHome/Media Management_InstallLog.log'"
				cnt1=$(egrep -c 'Status: ERROR' "$TeamsHome/Media Management_InstallLog.log") || /bin/true
				cnt2=$(awk '/NonFatalErrors/{print($1)}' "$TeamsHome/Media Management_InstallLog.log")
				if grep -i 'Aborting Install' "$TeamsHome/Media Management_InstallLog.log"; then
					echo "*F* Aborted installation, check $TeamsHome/Media Management_InstallLog.log"
					exit 1
				elif ! grep -q "0 FatalErrors" "$TeamsHome/Media Management_InstallLog.log"; then
					echo "*F* Found fatal errors, abort."
					exit 1
				elif [ "$cnt2" -gt "0" ] && [ "$cnt1" -eq "$cnt2" ]; then
					egrep '(Stack overflow|StackOverflow|NonFatalErrors)' "$TeamsHome/Media Management_InstallLog.log"
					echo "*I* Found $cnt2 non fatal (StackOverflow) errors, ignore."
				elif [ "$cnt2" -gt "0" ]; then
					echo "*F* Found $cnt2 non fatal errors, abort."
					egrep '(Stack overflow|StackOverflow|NonFatalErrors)' "$TeamsHome/Media Management_InstallLog.log"
					exit 1
				else
					echo "*D* No releveant errors found in the installer log file" 
				fi
			else
				echo "*E*: Installer failed: $?"
				exit 1
			fi
		fi
                echo "################################################################################"
#               kill $pid
                chown -R $User:$Group $InstallDir
		if [ \! -d "$TeamsRepository" ]; then
			install -o $User -g $Group -d $TeamsRepository
		elif [ "$(restricted_root_permissions -Path $TeamsRepository)" = "no" ]; then
	                chown -R $User:$Group $TeamsRepository
		else
			2>& echo "*W* NFS restrictions, please Make $TeamsRepository writeable for $User" 
		fi
        fi

	if [ "$ConfigureOtmm" = "tomee_embed" ] || [ "$ConfigureOtmm" = "tomee" ]; then
		echo "*D* Setting chown -R $User:$Group $AsInstallDir"
		chown -R $User:$Group $AsInstallDir
	fi

	cp $ServiceTemplate "$InstallDir/bin/otmm.service"
        perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' \
                "$InstallDir/bin/otmm.service"
        systemctl enable "$InstallDir/bin/otmm.service"

	cp $IndexServiceTemplate "$InstallDir/bin/index.service"
        perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' \
                "$InstallDir/bin/index.service"
        systemctl enable "$InstallDir/bin/index.service"
}


postinstall_otmm_otds () {
	#-WhitelistNodes "http://$GLOB_HOST:$GLOB_OTMM_PORT/"
	#-OtdsAdminUser
	#-OtdsAdminUserPw
	#-OtdsUrlNode

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	TOKEN=$(get_login_token_otds -Username "$OtdsAdminUser" -Password "$OtdsAdminUserPw" -RestUrlNode "$OtdsUrlNode" -CookieFile "/tmp/cookiejar" )

	for w in $WhitelistNodes; do
		add_trusted_sites_otds \
			-Token "$TOKEN" \
			-RestUrlNode "$OtdsUrlNode" \
			-TrustNode "$w"
	done
}

# Method to purge an existing Resource and Partition
preinstall_otmm_otds () {
	#-OtdsAdminUser
	#-OtdsAdminUserPw
	#-OtdsUrlNode
	#-OtdsPreinstallPurge

	while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ "$OtdsPreinstallPurge" = "purge" ]; then
		TOKEN=$(get_login_token_otds -Username "$OtdsAdminUser" -Password "$OtdsAdminUserPw" -RestUrlNode "$OtdsUrlNode" -CookieFile "/tmp/cookiejar" )
		delete_mm_resource_otds \
			-Token "$TOKEN" \
			-RestUrlNode "$OtdsUrlNode"
		delete_mm_partition_otds \
			-Token "$TOKEN" \
			-RestUrlNode "$OtdsUrlNode"
	else
		echo "*D* Administratively denied clearing of any exiting OTMM partition"
	fi	
	
}

function smtp_outbound_otmm  () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        #        -SmtpUser
        #        -SmtpUserPw
        #        -SmtpGateway
        #        -TeamsHome
        #        -DbType
        #        -DbUser
        #        -DbUserPw
        #        -SqlTool
        #        -JdbcConnection


        # Configure the outbound SMTP server
        if grep -q "SMTP_SERVER_USERNAME" $TeamsHome/servers/SMTP_SEC_Srv.cfg; then
                echo "Configuring SMTP"
                printf "${SmtpUser}\n${SmtpUserPw}\n" > $TeamsHome/servers/SMTP_SEC_Srv.cfg
                ( cd $TeamsHome/bin ; export TEAMS_HOME="$TeamsHome" ; PATH="$TeamsHome/bin:.:$PATH" ./crypt.sh ef ../servers/SMTP_SEC_Srv.cfg )

                if [ "$DbType" = "local_postgres" -o "$DbType" = "oracle" -o "$DbType" = "sqlserver" ]; then
                        $SqlTool -u "$JdbcConnection" -n "$DbUser" -p "$DbUserPw" -r "update otmm_sys_config_settings set CONFIG_VALUE='$SmtpGateway' where name='EMAIL_SERVER'"
                        $SqlTool -u "$JdbcConnection" -n "$DbUser" -p "$DbUserPw" -r "update otmm_sys_config_settings set CONFIG_VALUE='TRUE' where name='EMAIL_SERVER_AUTHENTICATION_ENABLED'"
                else
                        echo "*E* Database type not supported"
                        exit 1
                fi
        else
                echo "*I* SMTP is already configured"
        fi
}

function setup_bashrc_otmm () {
	FfmpegHome="/usr/bin"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        #       -InstallDir
        #       -User
        #       -FfmpegHome
        #       -TeamsHome
	#	-Aliases

        #prepare for utility functions, e.g. bulkimport
        #which req. JAVA_HOME, PATH and TEAMS_HOME at least
        if grep TEAMS_HOME $InstallDir/$User/.bashrc; then
                echo ".bashrc already configured"
        else
                echo "#Added for OTMM" >> $InstallDir/$User/.bashrc
                echo "set -a" >> $InstallDir/$User/.bashrc
                cat $TeamsHome/bin/artesia-process-manager-wrapper-unix.conf >> $InstallDir/$User/.bashrc
                echo "" >> $InstallDir/$User/.bashrc
                echo "PATH=\".:$FfmpegHome:\$PATH\"" >> $InstallDir/$User/.bashrc
                echo 'export FFMPEG_HOME="'${FfmpegHome}'"' >> $InstallDir/$User/.bashrc
                echo "set +a" >> $InstallDir/$User/.bashrc
                echo >> $InstallDir/$User/.bashrc

		if [ -n "$Aliases" ]; then
			#alias "sm_log"="less ~/CHPApplications/CHPData/log/CHPSysMgt/CHPSysMgt.log"
                	echo "${Aliases}" | awk 'BEGIN{RS=";"};{printf("alias %s\n",$1);}'  >> $InstallDir/$User/.bashrc
		fi
        	diff -Nau  $InstallDir/$User/.bashrc.orig $InstallDir/$User/.bashrcs 2>&1 || /bin/true
        fi
}

postinstall_pgsql_otmm_solrcloud () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        #Database is now available, start pgagent
        #NB: only needed for postgres
        systemctl restart $PgAgentService
        chown -R apache:apache /var/{lib,log}/pgadmin4

        #the SOLR configuration
        if [ "$IsPrimaryNode" = "yes" ]; then
                $SqlTool -u "$JdbcConnection" -p "$DbSysUserPw" -n "$DbSysUser" -r "grant all on schema pgagent to $DbOtmmUser"
                $SqlTool -u "$JdbcConnection" -p "$DbSysUserPw" -n "$DbSysUser" -r "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA pgagent to $DbOtmmUser"
                $SqlTool -u "$JdbcConnection" -p "$DbSysUserPw" -n "$DbSysUser" -r "grant all PRIVILEGES on all functions in schema pgagent to $DbOtmmUser"
                $SqlTool -u "$JdbcConnection" -p "$DbSysUserPw" -n "$DbSysUser" -r "grant all PRIVILEGES on all SEQUENCES in schema pgagent to $DbOtmmUser"
                $SqlTool -u "$JdbcConnection" -p "$DbSysUserPw" -n "$DbSysUser" -r "grant all PRIVILEGES on all FUNCTIONS in schema pgagent to $DbOtmmUser"
                su -l -c 'cd '$TeamsHome'/install/ant ; ant configure-indexer-ha; ant create-indexer-monitor-job' $User
        else
                su -c 'cd '$TeamsHome'/install/ant ; ant configure-indexer-ha' $User
        fi

}

function enable_https_wildfly () {

	Alias=''
	Realm=''
	InstallDir=''
	Keystore=''

	localarc=$(mktemp -t tmp.installer.wildfly.XXXXXXXX)
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        if [ -z "$Realm" ] || \
           [ -z "$InstallDir" ] || \
           [ -z "$Keystore" ] || \
           [ -z "$Keypass" ] || \
           [ -z "$Alias" ]; then
                echo "*F* Invalid argument"
                echo "*F* Alias=$Alias"
                echo "*F* Keystore=$Keystore"
                echo "*F* Realm=$Realm"
                echo "*F* Keypass=$Keypass"
        fi

       if [ -f "$Keystore" ]; then
               echo "*D* $Keystore exists, ignore keypair creation"
       else
               $JavaHome/bin/keytool -genkeypair \
                       -alias $Alias \
                       -keyalg RSA \
                       -keystore $Keystore \
                       -keypass $Keypass \
                       -storepass $Storepass \
                       -noprompt  \
                       -keysize 2048 \
                       -dname "$Dname"
       fi

	conffile=$InstallDir/standalone/configuration/otmm.xml
	cp $conffile $conffile.sslbackup
	if grep $Realm $conffile; then
		echo "*D* Realm $Realm already configured, skipping..."
	else
	        export __SSL_CONFIG
		__SSL_CONFIG=$(cat << EOM
<security-realm name="$Realm">
	<server-identities>
		<ssl>
			<keystore path="$Keystore" relative-to="/" keystore-password="$Keypass" alias="$Alias"/>
		</ssl>
	</server-identities>
</security-realm>
EOM
)
                sed -i 's/<security-realms>/<security-realms>\n@__SSL_CONFIG@/' $conffile
	fi

}


function install_upgrade_otmm_update () {

        localupgdir=$(mktemp -u -t tmp.installer.otmm165upN.XXXXXXXX)

        #-TeamsHome
        #-OtmmArchive
        #-FfmpegArchive
        #-User
	#-InstallOtmmSchema (install|ignore)
	#-ConfigureTranscode (ffmpeg|ffmpeg_local|other)
	#-OtmmVersion
	#-OtmmUpgrade

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	local _version=$(echo $OtmmVersion|tr -d ".")
	local _upgrade=$(printf "%03d" $OtmmUpgrade)

#        su -c 'cd '$TeamsHome'/bin; ./mediamanagement-process-manager stop' $User
#        su -c 'cd '$TeamsHome'/bin; ./indexer-process-manager stop' $User
	systemctl stop otmm.service index.service

        su -c "unzip -q -B -d $localupgdir '$OtmmArchive'" $User

	export ANT_OPTS="${ANT_OPTS:-} -d64"
	if [ -d "$TeamsHome/bak/MM${_version}-update-${_upgrade}" ]; then
		echo "*D* MM${_version}-update-${_upgrade} already installed"
	elif [ "$InstallOtmmSchema" = "install" ]; then
		su -l -c "( cd $localupgdir/MM${_version}-update-${_upgrade} ; ant install-patch )" $User
	elif [ "$InstallOtmmSchema" = "ignore" ]; then
		su -l -c "( cd $localupgdir/MM${_version}-update-${_upgrade} ; ant install-patch-secondary-node )" $User
	else 
		echo "*F* Invalid InstallOtmmSchema='$InstallOtmmSchema', abort"
		exit 1
	fi

	if [ -d "$localupgdir" ]; then
		rm -rf "$localupgdir"
	fi

        su -c "unzip -q -B -d $localupgdir '$FfmpegArchive'" $User
        export ANT_OPTS="${ANT_OPTS:-} -d64"

	if [ -d "$TeamsHome/bak/FFMPEG${_version}-update-${_upgrade}" ]; then
		echo "*D* FFMPEG${_version}-update-${_upgrade} already installed"
	elif [ "$ConfigureTranscode" = "ffmpeg_local" ]; then
		echo "*D* ffmpeg_local does not require upgrade, skipping..."
	elif [ "$ConfigureTranscode" = "ffmpeg"] && [ "$InstallOtmmSchema" = "install" ] ; then
		su -l -c "( cd $localupgdir/FFMPEG${_version}-update-${_upgrade} ; ant install-patch )" $User
	elif [ "$ConfigureTranscode" = "ffmpeg" ] && [ "$InstallOtmmSchema" = "ignore" ] ; then
		su -l -c "( cd $localupgdir/FFMPEG${_version}-update-${_upgrade} ; ant install-patch-secondary-node )" $User
	else 
		echo "*F* Invalid or unsupported ConfigureTranscode=$ConfigureTranscode, InstallOtmmSchema=$InstallOtmmSchema, abort"
		exit 1
	fi

        return 0
}

function otmm_cleanup () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

}

function otmm_hardening () {

	OtmmInstallDir=$GLOB_MMINS
	TaskFlags="dll_logs"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi
	
	for flag in $TaskFlags; do
		if [ "$flag" = "ddl_logs" ] && [ -d "$OtmmInstallDir" ]; then
			for n in $OtmmInstallDir/ddl_install/*.log; do
				echo "*D* Delete DDL log file='$n'"
				rm -f $n 
			done
		fi
		if [ "$flag" = "installer" ]; then
			rm -f /tmp/tmp.installer.*
		fi
	done
}

#
# IP list filtering for otmm /artesia-ux-ws endpoint
#
function configure_otmm_otds_whitelist () {
	FilterList='127.0.0.1'
	InstallDir="$GLOB_WFINS"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	_file="${InstallDir}/standalone/configuration/otmm.xml"
	_subsystem='server/profile/subsystem[#attribute/xmlns="urn:jboss:domain:undertow:4.0"]'
	_match='server/host/filter-ref/#attribute/name "ip-access-filter"'
	test -f ${_file}.orig || cp $_file{,.orig}
	if augtool -e -At "Xml incl $_file" match \
		"/files${_file}/${_subsystem}/${_match}" | grep -q "^/files"
	then
		echo "*D* IP access filter already configured, leaving in place"
	else
		echo "*D* Configure IP access filter in $_file"
		local localaug=$(mktemp -t tmp.installer.augeas5.XXXXXXX)
		(
		echo 'defnode NNEW /files'$_file
		echo 'defnode SUBSYSTEM $NNEW/server/profile/subsystem[#attribute/xmlns="urn:jboss:domain:undertow:4.0"]'
		echo 'set $SUBSYSTEM/server/host/filter-ref/#attribute/name "ip-access-filter"'
		echo 'set $SUBSYSTEM/server/host/filter-ref/#attribute/predicate "path-prefix('"'/artesia-ux-ws'"')"'
		echo 'set $SUBSYSTEM/filters/expression-filter/#attribute/name "ip-access-filter"'
		echo 'set $SUBSYSTEM/filters/expression-filter/#attribute/expression "ip-access-control(default-allow=true, acl={'"'127.0.0.1 allow'"'}"'
		echo "save" 
		echo "errors" ) > "$localaug"
		augtool -e -At "Xml incl $_file" -f "$localaug"  1>/dev/null 2>&1
		rm -f "$localaug"
	fi

	echo "*D* setting whitelist"

	#purge all
	augtool -s -e -At "Xml incl $_file" "rm /files$_file/$_subsystem/filters/expression-filter/#attribute/expression"
	#build entry 
	wl="" 
	for myip in 127.0.0.1 $FilterList; do
		#valid ip?
		o=`printf "%02x%02x%02x%02x" $(echo $myip | tr "." " ") 2>/dev/null`; r=$?
		if [ $(echo -n $o | wc -c) = "8" -a $r = "0" ]; then
			echo $myip
		else
			#no? assume hostname and try that
			#todo - catch any errors and do something
			getent ahostsv4 "$myip" 2>/dev/null || /bin/true
		fi
	done | \
	awk '{a[$1]=1};END{s="";for(n in a){s=s ",'"'"'" n " allow'"'"'"};print(substr(s,2));}' | \
	while read myips
	do
		echo "set" $myips
		augtool -s -e -At "Xml incl $_file" "set /files$_file/$_subsystem/filters/expression-filter/#attribute/expression \"ip-access-control(default-allow=false, acl={$myips})\""
	done

	diff -Nau $_file{.orig,} 2>&1 || /bin/true
}

function otmm_tomee_postinstall () {

	Host="localhost"
	MultiCastIp="235.25.203"
	TeamsHome=""
	TomeeHome=""

        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
                read -r "${1#*-}" <<< "$2"
                        shift
                shift
        done
        if [ -n "${1:-}" ]; then
                        echo "*F* remaining argument: $1"
                        exit 1
        fi

	test -f "${TomeeHome}/conf/conf.d/ejbd.properties.orig" || \
		cp "${TomeeHome}/conf/conf.d/ejbd.properties" \
		   "${TomeeHome}/conf/conf.d/ejbd.properties.orig"
        perl -pi -e 's!^( *bind *)=.*$!${1}= '${Host}'!' ${TomeeHome}/conf/conf.d/ejbd.properties
	diff -Nau "${TomeeHome}/conf/conf.d/ejbd.properties.orig" \
		"${TomeeHome}/conf/conf.d/ejbd.properties" 2>&1 || /bin/true

	test -f "${TomeeHome}/conf/multicast.properties.orig" || \
		cp "${TomeeHome}/conf/multicast.properties" \
		   "${TomeeHome}/conf/multicast.properties.orig"
        perl -pi -e 's!^( *bind *)=.*$!${1}="'${MultiCastIp}'"!' ${TomeeHome}/conf/multicast.properties
	diff -Nau "${TomeeHome}/conf/multicast.properties.orig" \
		"${TomeeHome}/conf/multicast.properties" 2>&1 || /bin/true

	test -f "${TeamsHome}/data/cs/global/Tresource.orig" || \
		cp "${TeamsHome}/data/cs/global/Tresource" \
		"${TeamsHome}/data/cs/global/Tresource.orig"
	perl -pi -e 's!^("PROVIDER_HOST")="[^:]*://([^:]*)(:.*)$!${1}="ejbd://'${Host}'${3}!' \
		"${TeamsHome}/data/cs/global/Tresource"
	diff -Nau "${TeamsHome}/data/cs/global/Tresource.orig" \
		"${TeamsHome}/data/cs/global/Tresource.orig" 2>&1 || /bin/true

	#add some missing jars to TomEE
	test -f "${TomeeHome}/conf/catalina.properties.orig" || \
                cp "${TomeeHome}/conf/catalina.properties" \
                   "${TomeeHome}/conf/catalina.properties.orig"
        perl -pi -e 's!^(shared.loader)=.*$!${1}="'${TeamsHome}'/deploy/spring/*.jar"!' ${TomeeHome}/conf/catalina.properties
        diff -Nau "${TomeeHome}/conf/catalina.properties.orig" \
                "${TomeeHome}/conf/catalina.properties" 2>&1 || /bin/true

}
