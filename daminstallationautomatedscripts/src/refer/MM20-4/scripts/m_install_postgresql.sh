#set PGAgentName only for solr cloud installations
#  that require the PGAgent workflow service
function install_conf_postgresql () {
	YumFlags="-q"

	 if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        localarc=$(mktemp -t tmp.installer.psqlarc.XXXXXX)
	local pgconf="$PgConfigDir/postgresql.conf"
	local pghba="$PgConfigDir/pg_hba.conf"

        if [ ! -f $pgconf ]; then
		echo "*D* dnf -qy module disable postgresql"
		dnf -qy module disable postgresql
		echo "*D* yum install --nogpgcheck -y $YumFlags $PgRepoRpm"
                yum install --nogpgcheck -y -q "$PgRepoRpm" || /bin/true
		echo "*D* yum install yum groupinstall -y $YumFlags \"$PgRepoGroup\""
                bash -c "BASH_XTRACEFD= yum groupinstall -y $YumFlags \"$PgRepoGroup\""
                bash -c "BASH_XTRACEFD= yum install -y $YumFlags xauth $PgAgentName"
                $PgSetup initdb
                systemctl start "$PgService"
                systemctl enable "$PgService"
        fi

	if [ \! -f $pgconf ]; then echo "*F* missing $pgconf" ; exit 1; fi
	if [ \! -f $pghba ]; then echo "*F* missing $pghba" ; exit 1; fi

	set -e
	localaug=$(mktemp -t tmp.installer.psqlaug.XXXXXXXX)
        augtool -e -At "Pg_Hba incl $pghba" match \
                "/files${pghba}/*[type = 'host'][database = 'all'][user = 'all'][method = 'md5'][address = '0.0.0.0/0']" | grep -q '(no matches)'  || /bin/true
        if [ "$?" = '0' ]; then
                cp $pghba $pghba.backup
                cp $pgconf $pgconf.backup
                ( echo "insert 01 before /files${pghba}/1" ; echo "set /files${pghba}/01/type host"; echo "set /files${pghba}/01/database all"; echo "set /files${pghba}/01/user all"; echo "set /files${pghba}/01/address 0.0.0.0/0"; echo "set /files${pghba}/01/method md5"; echo save ) > $localaug
                sudo sh -c "augtool -e -At 'Pg_Hba incl $pghba' -f $localaug 1>/dev/null 2>&1" ; rm $localaug

                ( echo "set /files${pgconf}/listen_addresses *"; echo "set /files${pgconf}/max_connections 100"; echo "set /files${pgconf}/max_prepared_transactions 100"; echo save ) > $localaug
                sudo sh -c "augtool -e -At 'Postgresql incl $pgconf' -f $localaug 1>/dev/null 2>&1" ; rm $localaug
                diff -Nau $pghba.backup $pghba ||  /bin/true
                diff -Nau $pgconf.backup $pgconf ||  /bin/true
                systemctl restart $PgService

        fi
        set -e

	su -l -c "echo ALTER USER "$DbSysUser" with password \'"${DbSysUserPass}"\' | psql -d template1" $DbSysUser
	#required for Solr Cloud only
	if [ -n "$PgAgentName" ] && [ "$EnablePgAgent" = "install" ]; then
		su -l -c "echo create extension pgagent | psql -d template1" $DbSysUser
#		pgagenthome=$( getent passwd pgagent | cut -d: -f6 )
#		test -d $pgagenthome || install -d $pgagenthome -o $PgAgentHostUser -g $PgAgentHostUser -m 0700 
#		echo "${PgAgentDbHost}:${PgAgentDbHostPort}:${PgAgentDbName}:${PgAgentDbUser}:${PgAgentDbUserPw}" > "$pgagenthome/.pgpass"
#		chown "${PgAgentHostUser}:${PgAgentHostUser}" "$pgagenthome/.pgpass"
#		chmod 0600 "$pgagenthome/.pgpass"
#		#su -l -c "pgagent hostaddr=$PgAgentDbHost dbname=$PgAgentDbName user=$PgAgentDbUser" "$PgAgentHostUser"

		#Configure systemd - preconfigure for OTMM DB even though it may not exists at this point. Startup of pgagent may therefore fail!
		if [ -f "/etc/systemd/system/$PgAgentName.service" ]; then
			echo "*D* /etc/systemd/system/$PgAgentName.service exists, skipping systemd configuration..."
		else
			echo "*D* /etc/systemd/system/$PgAgentName.service configuration..."
			echo "*D* cloning using $PgAgentSystemDConf" 
			install -m 0644 -o root -g root $PgAgentSystemDConf /etc/systemd/system/$PgAgentName.service
			perl -pi -e 's@(ExecStart=).*@${1}/usr/bin/'${PgAgentName}' -s \${LOGFILE} host=\${DBHOST} dbname=\${DBNAME} user=\${DBUSER} port=\${DBPORT} password=\${DBPASS}@' /etc/systemd/system/$PgAgentName.service
			diff -Nau $PgAgentSystemDConf /etc/systemd/system/$PgAgentName.service || /bin/true
			echo "*D* preparing using $PgAgentConfigTemplate"
			install -m 0600 -o $PgAgentHostUser -g $PgAgentHostUser "$PgAgentConfigTemplate" "/etc/pgagent/$PgAgentName.conf"
		        perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' "/etc/pgagent/$PgAgentName.conf"
			diff -Nau "$PgAgentConfigTemplate" "/etc/pgagent/$PgAgentName.conf" || /bin/true

		fi

	fi
}

function install_conf_pgadmin () {
	ConfigurePgAdminUI="ignore"
	ConfigTemplate=""
	YumFlags="-q"
	PgAdminConfigTemplate=""
	PgAdminWebUser=""
	PgAdminWebUserPw=""
	PgAdminHost=""

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	#EPEL is now a dependencies for some python packages	
	yum -y $YumFlags install epel-release
	bash -c "BASH_XTRACEFD= yum -y $YumFlags install pgadmin4-redhat-repo"
	bash -c "BASH_XTRACEFD= yum -y update"
	bash -c "BASH_XTRACEFD= yum -y $YumFlags install httpd pgadmin4-web"
#	yum -y $YumFlags install pgadmin4 python3-pip
	test -x /usr/pgadmin4/bin/setup-set.sh && /usr/pgadmin4/bin/setup-web.sh

	#To fix error: ImportError: No module named flask_compress
#	pip3 install --upgrade pip
#	pip3 install flask-compress psycopg2-binary
#	#should be part of hostname.conf if running on the same port
#	install -o apache -g apache \
#		$PgAdminConfigTemplate \
#		/etc/httpd/conf.d/pgadmin4.conf.standalone
#	install -o apache -g apache -m 2755 -d /var/lib/pgadmin4/
#	install -o apache -g apache -m 2755 -d /var/log/pgadmin4/
#	install -m 0775 -o apache -g apache -d /var/lib/pgadmin4/sessions 
#	install -m 0775 -o apache -g apache -d /var/lib/pgadmin4/storage 
#	touch /var/log/pgadmin4/pgadmin4.log 
#	touch /var/lib/pgadmin4/pgadmin4.db
#	chown apache:apache /var/log/pgadmin4/pgadmin4.log /var/lib/pgadmin4/pgadmin4.db
#
#	_conf="/usr/lib/python3.6/site-packages/pgadmin4-web"
#	cat>>$_conf/config_distro.py<<EOT
#LOG_FILE = '/var/log/pgadmin4/pgadmin4.log'
#SQLITE_PATH = '/var/lib/pgadmin4/pgadmin4.db'
#SESSION_DB_PATH = '/var/lib/pgadmin4/sessions'
#STORAGE_DIR = '/var/lib/pgadmin4/storage'
#EOT
#
#	PGADMIN_SETUP_EMAIL=$PgAdminWebUser \
#	PGADMIN_SETUP_PASSWORD=$PgAdminWebUserPw \
#	python3 $_conf/setup.py

	systemctl restart httpd.service
	echo "*D* Connect to PGAdmin on http://$PgAdminHost/pgadmin4 using $PgAdminWebUser and $PgAdminWebUserPw"
}

#
# Preliminary installation check for OTMM using postgres
#
function otmm_validate_postgres () {

	# -DbSysUser "$GLOB_DB_SYS_USER" \
	# -DbSysUserPass "$GLOB_DB_SYS_PASS" \
	# -DbName $GLOB_OTMM_DB_NAME \
	# -DbHost $GLOB_HOST_PGSQL \
	# -DbHostPort $GLOB_HOST_PGSQL_PORT \

	while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	</dev/tcp/$DbHost/$DbHostPort 1>/dev/null 2>&1
	if [ $? -gt 0 ]; then
		echo "*E* Postgres not listening on $DbHost:$DbHostPort"
		exit 1
	fi
}
