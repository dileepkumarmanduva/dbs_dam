#
# install_solr_configure_slave - configure a Solr slave server
# this function must run on an OTMM server, not just any server!
#
function install_solr_configure_slave () {
	#-OtmmHost -OtmmInstallDir -User

	set -x 
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi
	if grep 'BUILD_VERSION=16.3' $OtmmInstallDir/data/cs/global/build.properties; then
	        su -l -c "cd $OtmmInstallDir/install/ant; ant -Dsolr.hostname=$OtmmHost -Dsolr.port=8983 configure-solr-slave" $User
	else
	        su -l -c "cd $OtmmInstallDir/install/ant; ant -Dsolr.hostname=$OtmmHost -Dsolr.port=8983 configure-solr-standalone" $User
	fi
}

function configure_solr_limit () {
	
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -z "$User" ] || [ "$User" = "*undef*" ]; then
		echo "*E* Invalid arguments"
		echo "*E* User=$User"
	fi

	if [ -x /usr/bin/augtool ]; then
	        if augtool -e -At "Limits incl /etc/security/limits.conf" match \
	                "/files/etc/security/limits.conf/domain[.='$User']" | grep -q '(no matches)'
		then
			local localaug=$(mktemp -t tmp.installer.ulimit.XXXXXXXX)
			test -f /tmp/limits.conf || cp /etc/security/limits.conf /tmp/limits.conf
			( echo 'set /files/etc/security/limits.conf/domain[last()+1] "'$User'"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/type "hard"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/item "nproc"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/value "65535"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()+1] "'$User'"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/type "soft"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/item "nproc"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/value "65535"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()+1] "'$User'"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/type "hard"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/item "nofile"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/value "65535"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()+1] "'$User'"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/type "soft"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/item "nofile"'; \
			echo 'set /files/etc/security/limits.conf/domain[last()]/value "65535"'; \
			echo "save" ; \
			echo "errors" ) > "$localaug"
			augtool -e -At 'Limits incl /etc/security/limits.conf' -f "$localaug"
			rm -f "$localaug"
			diff -Nau /tmp/limits.conf /etc/security/limits.conf 2>&1 || /bin/true
		else
			echo "*D* Ulimit for $User already set, ignoring configuration "
		fi
	else
		if grep "$User" /etc/security/limits.conf; then
			test -f /tmp/limits.conf || cp /etc/security/limits.conf /tmp/limits.conf
			echo "$User hard nproc 65535" >> /etc/security/limits.conf
			echo "$User soft nproc 65535" >> /etc/security/limits.conf
			echo "$User hard nofile 65535" >> /etc/security/limits.conf
			echo "$User soft nofile 65535" >> /etc/security/limits.conf
			diff -Nau /tmp/limits.conf /etc/security/limits.conf 2>&1 || /bin/true
		else
			echo "*D* Ulimit for $User already set, ignoring configuration "
		fi
	fi
}

function install_otmm_build_solrtxt () {
	ConfigTemplate="/tmp/solr_config.txt"
	SolrInstallationType='STANDALONE'
	OtmmDbType='oracle'

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	cp ${ConfigTemplate} ${ConfigTemplate}.orig

	if [ "$SolrInstallationType" = 'CLOUD' ]; then
		echo "*F* SolrInstallationType=$SolrInstallationType not implemented yet"
		exit 1
		perl -pi -e 's/^(SEARCH_SELECTED).*/${1} = 1/g' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_STANDALONE_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_MASTER_SLAVE_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_CLOUD_SELECTED).*/${1} = 1/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_MASTER_SELECTED)=.*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_SLAVE_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(MASTER_HOST).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(MASTER_PORT).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(REGISTER_WITH_OTMM).*/${1} = @GLOB_SOLR_REGISTER_WOTMM@/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_CLUSTER_ZOOKEEPER_NODES_DETAILS).*/${1} = ???/' $ConfigTemplate
	elif [ "$SolrInstallationType" = 'STANDALONE' ]; then
		perl -pi -e 's/^(SEARCH_SELECTED).*/${1} = 1/g' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_STANDALONE_SELECTED).*/${1} = 1/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_MASTER_SLAVE_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_CLOUD_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_MASTER_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_SLAVE_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(MASTER_HOST).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(MASTER_PORT).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(REGISTER_WITH_OTMM).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_CLUSTER_ZOOKEEPER_NODES_DETAILS).*/${1} = *undef*/' $ConfigTemplate
	elif [ "$SolrInstallationType" = 'SLAVE' ]; then
		echo "*F* SolrInstallationType=$SolrInstallationType not implemented yet"
		exit 1
		perl -pi -e 's/^(SEARCH_SELECTED).*/${1} = 1/g' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_STANDALONE_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_MASTER_SLAVE_SELECTED).*/${1} = 1/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_CLOUD_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_MASTER_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_SLAVE_SELECTED).*/${1} = 1/' $ConfigTemplate
		perl -pi -e 's/^(MASTER_HOST).*/${1} = @GLOB_HOST_SOLR_MASTER@/' $ConfigTemplate
		perl -pi -e 's/^(MASTER_PORT).*/${1} = @GLOB_HOST_SOLR_MASTER_PORT@/' $ConfigTemplate
		perl -pi -e 's/^(REGISTER_WITH_OTMM).*/${1} = @GLOB_SOLR_REGISTER_WOTMM@/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_CLUSTER_ZOOKEEPER_NODES_DETAILS).*/${1} = *undef*/' $ConfigTemplate
	elif [ "$SolrInstallationType" = 'MASTER' ]; then
		echo "*F* SolrInstallationType=$SolrInstallationType not implemented yet"
		exit 1
		perl -pi -e 's/^(SEARCH_SELECTED).*/${1} = 1/g' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_STANDALONE_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_MASTER_SLAVE_SELECTED).*/${1} = 1/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_CLOUD_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_MASTER_SELECTED).*/${1} = 1/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_SLAVE_SELECTED).*/${1} = 0/' $ConfigTemplate
		perl -pi -e 's/^(MASTER_HOST).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(MASTER_PORT).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(REGISTER_WITH_OTMM).*/${1} = @GLOB_SOLR_REGISTER_WOTMM@/' $ConfigTemplate
		perl -pi -e 's/^(SEARCH_CLUSTER_ZOOKEEPER_NODES_DETAILS).*/${1} = *undef*/' $ConfigTemplate
	else
		echo "*E* Invalid Solr installation type: $SolrInstallationType, must be CLOUD, MASTER, SLAVE, STANDALONE"
		exit 1
	fi

	if [ "$SolrInstallationType" = 'MASTER' ] || \
	   [ "$SolrInstallationType" = 'CLOUD'  ] || \
	   [ "$SolrInstallationType" = 'SLAVE'  ]    \
	   ; then
		if [ "$OtmmDbType" = 'oracle' ]; then
			perl -pi -e 's/^(ORACLE_SERVICE_NAME).*/${1} = \@GLOB_HOST_ORACLE_SID\@/' $ConfigTemplate
			perl -pi -e 's/^(POSTGRESQL_DB_NAME).*/${1} = *undef*/' $ConfigTemplate
			perl -pi -e 's/^(DB_TYPE).*/${1} = ORACLE/' $ConfigTemplate
		elif [ "$OtmmDbType" = 'postgres' ]; then
			perl -pi -e 's/^(ORACLE_SERVICE_NAME).*/${1} = *undef*/' $ConfigTemplate
			perl -pi -e 's/^(POSTGRESQL_DB_NAME).*/${1} = \@GLOB_OTMM_DB_NAME\@/' $ConfigTemplate
			perl -pi -e 's/^(DB_TYPE).*/${1} = POSTGRESQL/' $ConfigTemplate
		else
			echo "*F* Invalid OtmmDbType=$OtmmDbType"
		fi
	else
		perl -pi -e 's/^(ORACLE_SERVICE_NAME).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(POSTGRESQL_DB_NAME).*/${1} = *undef*/' $ConfigTemplate
		perl -pi -e 's/^(DB_TYPE).*/${1} = *undef*/' $ConfigTemplate
	fi

	#figure this one out when you configure CLOUD
	perl -pi -e 's/^(ZOOKEEPER_SELECTED).*/${1} = 0/' $ConfigTemplate
	#fixed this to /tmp as it will be the log root in 20.2
	perl -pi -e 's!^(ZOOKEEPER_HOME).*!${1} = /tmp!' $ConfigTemplate
	perl -pi -e 's/^(ZOOKEEPER_PORT).*/${1} = *undef*/' $ConfigTemplate
	perl -pi -e 's/^(ZOOKEEPER_ADMIN_SERVER_PORT).*/${1} = *undef*/' $ConfigTemplate
	perl -pi -e 's/^(ZOOKEEPER_DATA_DIR).*/${1} = *undef*/' $ConfigTemplate
	perl -pi -e 's/^(IS_ZOOKEEPER_CLUSTERED).*/${1} = *undef*/' $ConfigTemplate
	perl -pi -e 's/^(ZOOKEEPER_UNIQUE_ID).*/${1} = *undef*/' $ConfigTemplate
	perl -pi -e 's/^(ZOOKEEPER_CLUSTER_NODES_DETAILS).*/${1} = *undef*/' $ConfigTemplate

	perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $ConfigTemplate
        diff -Nau ${ConfigTemplate}.orig $ConfigTemplate 2>&1 || /bin/true 

}

#
# install_solr_service - configure Search
# this function must run on an search server
#
function install_solr_service () {
	ServiceTemplate="solr.service.tmpl"

	#-OtmmHost -OtmmInstallDir -JavaHome -TmpMount -Archive -ConfigTemplate -OtmmDbType -DbPsqlName -DbOraSid -User -UserPw -InstallDir -InstallIndexDir -Group -RegisterWithOtmm
	SolrInstallationType="STANDALONE"
	SolrInSh='/etc/default/solr.in.sh'
	OtmmDbType='oracle'

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -z "$OtmmDbType" ] || [ "$OtmmDbType" = "*undef*" ] || \
	   [ -z "$SolrInstallationType" ] || [ "$SolrInstallationType" = "*undef*" ] || \
	   [ -z "$Archive" ] || [ "$Archive" = "*undef*" ] || \
	   [ -z "$TmpMount" ] || [ "$TmpMount" = "*undef*" ] || \
	   [ -z "$ConfigTemplate" ] || [ "ConfigTemplate" = "*undef*" ] || \
	   [ -z "1" ]; then
		echo "*E* Invalid arguments:"
		echo "*E* SolrInstallationType=$SolrInstallationType"
		echo "*E* ConfigTemplate=$ConfigTemplate"
		echo "*E* OtmmDbType=$OtmmDbType"
		echo "*E* Archive=$Archive"
		echo "*E* TmpMount=$TmpMount"
		exit 1
	fi

        if [ -d "$TmpMount/SEARCH" ]; then
                echo "$TmpMount/SEARCH exists, ignoring mounting of iso..."
        else
                mkdir -p $TmpMount
                mount -o loop $Archive $TmpMount
        fi

	echo "*D* OtmmDbType '$OtmmDbType'"
	localconf=$(mktemp -t tmp.installer.solrconf.XXXXXXXXXX)
        cp $ConfigTemplate $localconf
        install_otmm_build_solrtxt \
		-ConfigTemplate "$localconf" \
		-SolrInstallationType "$SolrInstallationType" \
		-OtmmDbType "$OtmmDbType" 

#        if id $User; then
#                echo "SOLR User $User exists"
#        else
#                echo "SOLR User $User does not exists, creating..."
#                PW=`perl -e "printf('%s',crypt('$UserPw','OT'))"`
#                useradd -k /tmp/skel -g $Group -d $SolrUsrHome -s /bin/bash -m -p $PW $User
#                #chown ${User}:${Group} $InstallDir
#        fi

	chmod 777 "$localconf" 

	# 20.2 uses a SOLR installer 
	_d="$TmpMount/SEARCH/Disk1/InstData/NoVM"
	if [ "$StopAtInstaller" = "yes" ]; then 
		#Use installer
		echo "export JAVA_HOME=$JavaHome"
		echo "export LAX_DEBUG=1"
		echo " $_d/install.bin -i silent -f $localconf "
		echo "cd $InstallDir;"
		echo "chown -R $User:$Group $InstallDir $InstallIndexDir"
		exit 1
	else
		#Use installer
		export JAVA_HOME=$JavaHome
		export LAX_DEBUG="1"
		echo "################################################################################"
		$_d/install.bin -i silent -f $localconf
		echo "################################################################################"
		#
		cd $InstallDir;
		chown -R $User:$Group $InstallDir $InstallIndexDir
	fi


	#link for systemd to work
	#_d=$(echo /opt/mm/solr-index/solr-*.*.*)
	#ln -sf ${_d}/bin/init.d/solr /etc/rc.d/init.d/
	#ln -sf ${_d}/bin/solr.in.sh $SolrInSh
        #perl -pi -e 's!^SOLR_INSTALL_DIR=.*!SOLR_INSTALL_DIR="'${_d}'"!g' /etc/rc.d/init.d/solr
        #perl -pi -e 's/^RUNAS=.*/RUNAS="'$User'"/g' /etc/rc.d/init.d/solr

	SolrInstall=$(echo /opt/mm/solr-index/solr-*.*.*)
	cp -f $SolrInstall/bin/init.d/solr /etc/rc.d/init.d/
        perl -pi -e 's!^SOLR_INSTALL_DIR=.*!SOLR_INSTALL_DIR="'$SolrInstall'"!g' /etc/rc.d/init.d/solr
        perl -pi -e 's/^RUNAS=.*/RUNAS="'$User'"/g' /etc/rc.d/init.d/solr
	perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' /etc/rc.d/init.d/solr
	diff -Nau $SolrInstall/bin/init.d/solr /etc/rc.d/init.d/solr || /bin/true

	ln -sf $SolrInstall/bin/solr.in.sh /etc/default/solr.in.sh 
	cp -f $ServiceTemplate $SolrInstall/bin/solr.service
	perl -pi -e 's!\@SOLR_INCLUDE\@!'"$SolrInstall/bin/solr.in.sh"'!' $SolrInstall/bin/solr.service
	perl -pi -e 's!\@SOLR_INSTALL_DIR\@!'"$SolrInstall"'!' $SolrInstall/bin/solr.service
	perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $SolrInstall/bin/solr.service
	diff -Nau $ServiceTemplate $SolrInstall/bin/solr.service || /bin/true
	systemctl enable $SolrInstall/bin/solr.service

	#configure 16.2
	#if grep 'SOLR_ENV=/var/solr' /etc/rc.d/init.d/solr; then
	#        perl -pi -e 's!^SOLR_ENV=.*!SOLR_ENV="'$SolrInSh'"!g' /etc/rc.d/init.d/solr
	#fi

	#force JVM to bind to the IPv4 interface (since it redirects to IPv6 in some occasions)
	#NB: sysconf may already take care of this - TODO, detect ipv6 IF
	if [ -x "/usr/bin/augtool" ]; then 
		test -f /tmp/solr.in.sh.backup || cp $SolrInSh /tmp/solr.in.sh.backup
		if augtool -e -At 'Shellvars incl '$SolrInSh match 'match /files/etc/default/solr.in.sh/SOLR_OPTS[.=~regexp(".*java.net.preferIPv4Stack.*")]' | grep -q '(no matches)'; then
			augtool -s -e -At 'Shellvars incl '$SolrInSh 'set /files'$SolrInSh'/SOLR_OPTS[last()+1] "\"$SOLR_OPTS -Djava.net.preferIPv4Stack=true\""'
#			augtool -s -e -At 'Shellvars incl '$SolrInSh 'set /files'$SolrInSh'/SOLR_HOME "\"'$InstallIndexDir'/data\""'
		fi
		diff -Nau /tmp/solr.in.sh.backup $SolrInSh 2>&1 || /bin/true
	else
		test -f /tmp/solr.in.sh.backup || cp $SolrInSh /tmp/solr.in.sh.backup
		grep java.net.preferIPv4Stack $SolrInSh || \
			(echo 'SOLR_OPTS="$SOLR_OPTS -Djava.net.preferIPv4Stack=true"' >> $SolrInSh)
	        perl -pi -e 's!^SOLR_HOME=.*!SOLR_HOME="'$InstallIndexDir'/data"!g' $SolrInSh
		diff -Nau /tmp/solr.in.sh.backup $SolrInSh || /bin/true
	fi

	#runs on the OTMM server only
        #perl -pi -e 's/^RUNAS=.*/RUNAS="'$User'"/g' /etc/rc.d/init.d/solr
        #su -l -c "cd $InstallDir; bin/OpenText-Media-Management-Search-process start" $User
}

function solr_systemd_config () {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi
}

#
# post_configure_solr - configure Solr in OTMM
# this procedure must be run on the OTMM node for every Solr node
#
function post_configure_solr () {

	SolrInstallDir='/x'; #does not exists by default

        #Solr master in Tresources -SolrMasterHost
        #Solr port  in Tresources  -SolrMasterPort
        #Solr node to register -SolrNodeHost
        #Solr port to register  -SolrNodePort
        #-OtmmInstallDir, where Otmm is
	#-SolrInstallDir, where Solr is
        #Otmm user   -User
        #Node type   -SolrType
        # this registers the SolrNodeHost:SolrNodePort as either master, slave, standalone or cloud
        #Cloud Shards -NumShards
        #Cloud MaxShards -MaxShardsPerNOde

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        #Setup Tresources to the correct URL/Node
        #this part needs to run on every OTMM node
	test -f $OtmmInstallDir/data/cs/global/Tresource.backup || cp $OtmmInstallDir/data/cs/global/Tresource{,.backup}
        perl -pi -e 's!^"SOLR_URL"=.*!"SOLR_URL"="'http://${SolrMasterHost}:${SolrMasterPort}/solr'"!g' \
                $OtmmInstallDir/data/cs/global/Tresource
        perl -pi -e 's/^"SOLR_HOST"=.*/"SOLR_HOST"="'$SolrMasterHost'"/g' \
                $OtmmInstallDir/data/cs/global/Tresource
	diff -Nau $OtmmInstallDir/data/cs/global/Tresource{.backup,} || /bin/true
	
	#registration can only run with Solr up - if OTMM does not host Solr, assume Solr is already running
	if [ -d "$SolrInstallDir" ]; then
		configure_solr_limit -User $User
		if [ -x $SolrInstallDir/bin/OpenText-Media-Management-Search-process ]; then 
	        	su -l -c "$SolrInstallDir/bin/OpenText-Media-Management-Search-process start" $User
		fi
	fi

        #Register the solr node appropriately
        #this part needs to run for every Solr Node but could run on the same OTMM node
        #this part only needs to run if the installer did not elect to configure automatically - which is when it runs
        #before the OTMM installation
        if [ "$SolrType" = "ignore" ]; then

                echo "*D* Nothing to be done"

        elif [ "$SolrType" = "cloud" ]; then

                echo "*F* review this step - this may not be correct"
                exit 1
                #extract configuration from OTMM
                su -l -c "cd $OtmmInstallDir/install/ant; ant create-solr-configset" $User
                #install configuration collections
                # maxshardspernode >= ( numshards * numreplicas ) / nodes
                su -l -c "cd $OtmmInstallDir/install/ant; ant -Dnumshards=$NumShards \
                        -Dnumreplicas=$NumReplicas \
                        -Dmaxshardspernode=$MaxShardsPerNode create\
                        -otmm-collection" $User
                su -l -c "cd $OtmmInstallDir; bin/indexer-process-manager status || bin/indexer-process-manager start" $User

        elif [ "$SolrType" = "standalone" ]; then

		if grep 'BUILD_VERSION=16.3' $OtmmInstallDir/data/cs/global/build.properties; then
		        echo "*D* solr standalone configuration for 16.3"
		else
                	su -l -c "cd $OtmmInstallDir/install/ant; ant -Dsolr.hostname=$SolrMasterHost -Dsolr.port=8983 configure-solr-standalone" $User
		fi
                su -l -c "cd $OtmmInstallDir; bin/indexer-process-manager status || bin/indexer-process-manager start" $User

        elif [ "$SolrType" = "master" ]; then

                su -l -c "cd $OtmmInstallDir/install/ant; ant -Dsolr.hostname=$SolrMasterHost -Dsolr.port=8983 configure-solr-master" $User
                su -l -c "cd $OtmmInstallDir; bin/indexer-process-manager status || bin/indexer-process-manager start" $User

        elif [ "$SolrType" = "slave" ]; then

                su -l -c "cd $OtmmInstallDir/install/ant; ant -Dsolr.hostname=$SolrNodeHost -Dsolr.port=8983 configure-solr-slave" $User
                su -l -c "cd $OtmmInstallDir; bin/indexer-process-manager status || bin/indexer-process-manager start" $User

        else
                echo "*E* Invalid Solr configuration type: $SolrType"
                exit 1
        fi
}

function install_zookeeper_service ()  {

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        if [ -d "$TmpMount/SEARCH" ]; then
                echo "$TmpMount/SEARCH exists, ignoring mounting of iso..."
        else
                mkdir -p $TmpMount
                mount -o loop $Archive $TmpMount
        fi

#        if id $User; then
#                echo "SOLR User $User exists"
#        else
#                echo "SOLR User $User does not exists, creating..."
#                PW=`perl -e "printf('%s',crypt('$UserPw','OT'))"`
#                useradd -k /tmp/skel -g $Group -d $SolrUsrHome -s /bin/bash -m -p $PW $User
#        fi

	localconf=$(mktemp -t tmp.installer.zookeeper.XXXXXXXXXX)

        rm -f $localconf; cp $ConfigTemplate $localconf
	perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $localconf
	echo "*D* Configuration file is $localconf,  populated using $ConfigTemplate"
        echo "*D* diff -Nau $ConfigTemplate $localconf"
        diff -Nau $ConfigTemplate $localconf || /bin/true

	chown -R $User:$Group $localconf

        _d="$TmpMount/SEARCH/Disk1/InstData/VM"
        export JAVA_HOME=$JavaHome
        export LAX_DEBUG="1"
        echo "################################################################################"
        su -c "cd $_d; ./install.bin -i silent -f $localconf" $User
        echo "################################################################################"
        #

	# Work around the UNIX/DOS file issue in zookeeper.sh
	perl -pi -e 's/\r\n/\n/g' $InstallDir/bin/zookeeper.sh
	
	su -c "cd $InstallDir/bin; export ZOOKEEPER_HOME=$InstallDir; ./OpenText-Media-Management-Zookeeper-process start" $User
}

#
# Fixup jetty to filter solr access
#
# Hint for  debugging:
# set /augeas/load/xml/lens "Xml.lns"
# set /augeas/load/xml/incl "/opt/mm/solr-index/solr-7.3.1/server/etc/jetty.xml"
# set /augeas/load/xml/incl "/opt/mm/solr-index/solr-7.3.1/server/etc/jetty.xml.orig"
# set /augeas/load/xml/incl "/opt/mm/solr-index/solr-7.3.1/server/etc/jetty.xml.tmp2"
# load
# defnode NNEW /files/opt/mm/solr-index/solr-7.3.1/server/etc/jetty.xml
# defnode ONEW /files/opt/mm/solr-index/solr-7.3.1/server/etc/jetty.xml.orig
# defnode TNEW /files/opt/mm/solr-index/solr-7.3.1/server/etc/jetty.xml.tmp2
# defnode HANDLER $TNEW/Configure/New/Set/New/Set/Array[#attribute/type="org.eclipse.jetty.server.Handler"]
# rm $HANDLER/Item[New/#attribute/class="org.eclipse.jetty.server.handler.ContextHandlerCollection"]
# ins Item before $HANDLER/Item[1]
# set $HANDLER/Item[1]/New/#attribute/id "IPAccessHandler"
# set $HANDLER/Item[1]/New/#attribute/class "org.eclipse.jetty.server.handler.IPAccessHandler"
# set $HANDLER/Item[1]/New/Set/#attribute/name "white"
# set $HANDLER/Item[1]/New/Set/Array/#attribute/type "String"
# set $HANDLER/Item[1]/New/Set/Array/Item/#text "127.0.0.1"
# set $HANDLER/Item[1]/New/Set/Array/Item[last()+1]/#text "172.16.2.100"
# set $HANDLER/Item[1]/New/Set[last()+1]/#attribute/name "whiteListByPath"
# set $HANDLER/Item[1]/New/Set[last()]/#text false
# set $HANDLER/Item[1]/New/Set[last()+1]/#attribute/name "handler"
# set $HANDLER/Item[1]/New/Set[last()]/New/#attribute/id "Contexts"
# set $HANDLER/Item[1]/New/Set[last()]/New/#attribute/class "org.eclipse.jetty.server.handler.ContextHandlerCollection
#
function configure_solr_source_whitelist () {
	FilterList='127.0.0.1'
	InstallDir="$GLOB_SOINS"
	SolrLeaf='solr-7.3.1'

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	_file="${InstallDir}/${SolrLeaf}/server/etc/jetty.xml"
	_handler='Configure/New/Set/New/Set/Array[#attribute/type="org.eclipse.jetty.server.Handler"]'
	_match='Item/New/#attribute/id "IPAccessHandler"'
	test -f ${_file}.orig || cp $_file{,.orig}
	if augtool -e -At "Xml incl $_file" match \
		"/files${_file}/${_handler}/${_match}" | grep -q "^/files"
	then
		echo "*D* IPAccessHandler already configured, leaving in place"
	else
		echo "*D* Configure IPAccessHandler in $_file"
		local localaug=$(mktemp -t tmp.installer.jetty.XXXXXXXX)
		( 
		echo 'defnode NNEW /files'$_file
		echo 'defnode HANDLER $NNEW/Configure/New/Set/New/Set/Array[#attribute/type="org.eclipse.jetty.server.Handler"]'
		echo 'rm $HANDLER/Item[New/#attribute/class="org.eclipse.jetty.server.handler.ContextHandlerCollection"]'
		echo 'ins Item before $HANDLER/Item[1]'
		echo 'set $HANDLER/Item[1]/New/#attribute/id "IPAccessHandler"'
		echo 'set $HANDLER/Item[1]/New/#attribute/class "org.eclipse.jetty.server.handler.IPAccessHandler"'
		echo 'set $HANDLER/Item[1]/New/Set/#attribute/name "white"'
		echo 'set $HANDLER/Item[1]/New/Set/Array/#attribute/type "String"'
		echo 'set $HANDLER/Item[1]/New/Set/Array/Item/#text "127.0.0.1"'
		echo 'set $HANDLER/Item[1]/New/Set[last()+1]/#attribute/name "whiteListByPath"'
		echo 'set $HANDLER/Item[1]/New/Set[last()]/#text false'
		echo 'set $HANDLER/Item[1]/New/Set[last()+1]/#attribute/name "handler"'
		echo 'set $HANDLER/Item[1]/New/Set[last()]/New/#attribute/id "Contexts"'
		echo 'set $HANDLER/Item[1]/New/Set[last()]/New/#attribute/class "org.eclipse.jetty.server.handler.ContextHandlerCollection"'
		echo "save" ; \
		echo "errors" ) > "$localaug"
		augtool -e -At "Xml incl $_file" -f "$localaug"
		rm -f "$localaug"
	fi
	
	echo "*D* setting whitelist"

	#purge all
	augtool -s -e -At "Xml incl $_file" "rm /files$_file/$_handler/Item/New/Set/Array/Item"
	#add first entry
	augtool -s -e -At "Xml incl $_file" "set /files$_file/$_handler/Item/New/Set/Array/Item/#text \"127.0.0.1\""
	#add the rest
	wl=""
	for myip in $FilterList; do
		#valid ip?
		o=`printf "%02x%02x%02x%02x" $(echo $myip | tr "." " ") 2>/dev/null`; r=$?
		if [ $(echo -n $o | wc -c) = "8" -a $r = "0" ]; then 
			echo $myip
		else 
			#no? assume hostname and try that
			#todo - catch any errors and do something
			getent ahostsv4 "$myip" 2>/dev/null || /bin/true
		fi
	done | \
	awk '{a[$1]=1};END{for(n in a){print(n)}}' | \
	while read myip
	do
		echo "set" $myip
		augtool -s -e -At "Xml incl $_file" "set /files$_file/$_handler/Item/New/Set/Array/Item[last()+1]/#text \"$myip\""
	done

	diff -Nau $_file{.orig,} || /bin/true
}
