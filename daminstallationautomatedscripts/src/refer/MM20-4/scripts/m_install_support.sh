#
# Build the silent installer file for OTMM
#
function install_support_build_mmtxt () {
#	DbName="db_suppprt"
#	DbHost="*undef*"
#	DbHostPort="*undef*"
	localconf="*undef*"
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	>&2 echo "*D* DbType = $DbType, configuration"
        if [ "$DbType" = "local_postgres" ]; then
		>&2 echo "*D* Postgres database GLOB_DB_POSTGRES_DB=$GLOB_DB_POSTGRES_DB, GLOB_OTMM_DB_NAME=$GLOB_OTMM_DB_NAME "
                perl -pi -e 's/^DB_TYPE=.*/DB_TYPE=POSTGRESQL/g' $localconf
                perl -pi -e 's/^SQLSERVER_DB_NAME=.*/SQLSERVER_DB_NAME=*undef*/g' $localconf
                perl -pi -e 's/^ORACLE_SERVICE_NAME=.*/ORACLE_SERVICE_NAME=*undef*/g' $localconf
#                perl -pi -e 's/^POSTGRESQL_DB_NAME=.*/POSTGRESQL_DB_NAME=$DbName/g' $localconf
#                perl -pi -e 's/^DB_HOST_NAME=.*/DB_HOST_NAME=$DbHost/g' $localconf
#                perl -pi -e 's/^DB_PORT=.*/DB_PORT=$DbHostPort/g' $localconf

        elif [ "$DbType" = "oracle" ]; then
		>&2 echo "*D* Oracle database GLOB_DB_POSTGRES_DB=$GLOB_DB_POSTGRES_DB, GLOB_OTMM_DB_NAME=$GLOB_OTMM_DB_NAME "
                perl -pi -e 's/^DB_TYPE=.*/DB_TYPE=ORACLE/g' $localconf
                perl -pi -e 's/^SQLSERVER_DB_NAME=.*/SQLSERVER_DB_NAME=*undef*/g' $localconf
                perl -pi -e 's/^POSTGRESQL_DB_NAME=.*/POSTGRESQL_DB_NAME=*undef*/g' $localconf
#                perl -pi -e 's/^DB_HOST_NAME=.*/DB_HOST_NAME=$DbHost/g' $localconf
#                perl -pi -e 's/^DB_PORT=.*/DB_PORT=$DbHostPort/g' $localconf

	else
                >2& echo "*F* Unsupported db type $DbType"
        fi

       perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $localconf
       diff -Nau $ConfigTemplate $localconf || /bin/true
}

function install_support_service () {
	Archive="*undef*"
	ConfigTemplate="supportmgr_config_txt.templ"
	ServiceTemplate="support.service.tmpl"
        DbSysUser="*undef*"
        DbSysUserPw="*undef*"
        DbUser="*undef*"
        DbUserPw="*undef*"
        DbName="*undef*"
        DbType="ignore"
	DbHost="*undef*"
	DbHostPort="*undef*"
        DoInstallSchema="ignore"
	DoDestroySchema="ignore"
        DoDestroyTablespace="*undef*"
        DestroySqlTemplate="*undef*"
        SqlTool="java -jar sqltool.jar"
	JdbcSysConnection="*undef*"
	InstallDir="/opt/mm/support"
	TmpMount="/tmp/mnt"
	StopAtInstaller="no"

	ServiceTemplate=""
	IndexServiceTemplate=""

#	YumFlags="-q"
	DestroySqlTemplate="purgeoracle.sql.tmpl"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	localconf=$(mktemp -t tmp.installer.supportconf.XXXXXXX.txt)
        cp $ConfigTemplate $localconf

       install_support_build_mmtxt \
                -localconf $localconf \
		-ConfigTemplate $ConfigTemplate \
                -DbType $DbType

	#drop my exiting database
	if [ "$DoInstallSchema" = "install" ]; then

		if [ "$DoDestroySchema" = "destroy" ]; then

			echo "*D* DoInstallSchema=$DoInstallSchema, DoDestroySchema=$DoDestroySchema destroy existing schema"

			db_common_drop_database \
				-JdbcSysConnection "$JdbcSysConnection" \
		       	 	-DbSysUser "$DbSysUser" \
		       	 	-DbSysUserPw "$DbSysUserPw" \
       			 	-DbUser "$DbUser" \
       			 	-DbName "$DbName" \
       			 	-DbType "$DbType" \
       			 	-DoInstallSchema "$DoInstallSchema" \
       			 	-SqlTool "$SqlTool"

		else

			echo "*D* DoInstallSchema=$DoInstallSchema, DoDestroySchema=$DoDestroySchema leave schema alone"

		fi

		db_common_create_database \
			-JdbcSysConnection "$JdbcSysConnection" \
	       	 	-DbSysUser "$DbSysUser" \
	       	 	-DbSysUserPw "$DbSysUserPw" \
       		 	-DbUser "$DbUser" \
       		 	-DbUserPw "$DbUserPw" \
       		 	-DbName "$DbName" \
       		 	-DbType "$DbType" \
       		 	-DoInstallSchema "$DoInstallSchema" \
       		 	-SqlTool "$SqlTool"

	else
		echo "*W* DoInstallSchema=$DoInstallSchema, administratively denied creating schema"
	fi
	
        chown $User $localconf

	#dependency of the instantclient
#        if [ "$DbType" = "oracle" ]; then
#		yum install -y $YumFlags libaio
#	fi

        if [ -d "$TmpMount/OTMM" ]; then
                echo "$TmpMount exists, ignoring mounting of iso..."
        else
		localarc=$(mktemp -t tmp.mmiso.XXXXXXXX)
                echo "*D* $TmpMount missing, mounting $Archive as $localarc on $TmpMount"
		cp $Archive $localarc
                mkdir -p $TmpMount
                mount -o loop $localarc $TmpMount
        fi

        #tidy
        test -d "$InstallDir" && rm -rf "$InstallDir"

        _d="$TmpMount/SUPPORTMANAGER/Disk1/InstData/NoVM"
        if [ "$StopAtInstaller" = "yes" ]; then
                echo "*D* stopped at SupportMgr installer"
                echo "*D* set export LAX_DEBUG=1 for more debugging"
                echo "*D* set export JAVA_HOME=$JavaHome"
                echo "*D* run as $User: cd $_d;$_d/install.bin -i silent -f $localconf"
                echo "*D* post cleanup with chown -R $User:$Group $InstallDir"
                exit 1
        else
                #as root!
                export JAVA_HOME=$JavaHome
                export LAX_DEBUG="1"
                echo "################################################################################"
		cd $_d; 
                ( BASH_XTRACEFD=; $_d/install.bin -i silent -f $localconf )
                echo "################################################################################"

                chown -R $User:$Group $InstallDir
        fi

}



function install_upgrade_support_update () {

        localupgdir=$(mktemp -u -t tmp.installer.otmm165upN.XXXXXXXX)

        #-TeamsHome
        #-OtmmArchive
        #-FfmpegArchive
        #-User
	#-InstallOtmmSchema (install|ignore)
	#-ConfigureTranscode (ffmpeg|ffmpeg_local|other)
	#-OtmmVersion
	#-OtmmUpgrade

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	local _version=$(echo $OtmmVersion|tr -d ".")
	local _upgrade=$(printf "%03d" $OtmmUpgrade)

#        su -c 'cd '$TeamsHome'/bin; ./mediamanagement-process-manager stop' $User
#        su -c 'cd '$TeamsHome'/bin; ./indexer-process-manager stop' $User
	systemctl stop otmm.service index.service

        su -c "unzip -q -B -d $localupgdir '$OtmmArchive'" $User

	export ANT_OPTS="${ANT_OPTS:-} -d64"
	if [ -d "$TeamsHome/bak/MM${_version}-update-${_upgrade}" ]; then
		echo "*D* MM${_version}-update-${_upgrade} already installed"
	elif [ "$InstallOtmmSchema" = "install" ]; then
		su -l -c "( cd $localupgdir/MM${_version}-update-${_upgrade} ; ant install-patch )" $User
	elif [ "$InstallOtmmSchema" = "ignore" ]; then
		su -l -c "( cd $localupgdir/MM${_version}-update-${_upgrade} ; ant install-patch-secondary-node )" $User
	else 
		echo "*F* Invalid InstallOtmmSchema='$InstallOtmmSchema', abort"
		exit 1
	fi

	if [ -d "$localupgdir" ]; then
		rm -rf "$localupgdir"
	fi

        su -c "unzip -q -B -d $localupgdir '$FfmpegArchive'" $User
        export ANT_OPTS="${ANT_OPTS:-} -d64"

	if [ -d "$TeamsHome/bak/FFMPEG${_version}-update-${_upgrade}" ]; then
		echo "*D* FFMPEG${_version}-update-${_upgrade} already installed"
	elif [ "$ConfigureTranscode" = "ffmpeg_local" ]; then
		echo "*D* ffmpeg_local does not require upgrade, skipping..."
	elif [ "$ConfigureTranscode" = "ffmpeg"] && [ "$InstallOtmmSchema" = "install" ] ; then
		su -l -c "( cd $localupgdir/FFMPEG${_version}-update-${_upgrade} ; ant install-patch )" $User
	elif [ "$ConfigureTranscode" = "ffmpeg" ] && [ "$InstallOtmmSchema" = "ignore" ] ; then
		su -l -c "( cd $localupgdir/FFMPEG${_version}-update-${_upgrade} ; ant install-patch-secondary-node )" $User
	else 
		echo "*F* Invalid or unsupported ConfigureTranscode=$ConfigureTranscode, InstallOtmmSchema=$InstallOtmmSchema, abort"
		exit 1
	fi

        return 0
}

#
# create and install systemd configuration files
#
function support_install_service_helper () {
        ServiceName="ignore"
        InstallDir="*undef*"
	User="otmm"

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        if      [ "$ServiceName" = "ignore" ] || \
                [ "$InstallDir" = "*undef*" ] ; then
                echo "*D* Systemd control ServiceName=$ServiceName, "`
                        `"InstallDir=$InstallDir "`
                        `"administratively denied"
        else
                echo "*D* Configure Systemd control ServiceName=$ServiceName in $InstallDir"
		perl -pi -e "s/^User=.*/User=$User/g" "${InstallDir}/bin/${ServiceName}.service"
		( cd $InstallDir/bin/; systemctl enable "$ServiceName" )
        fi

}

