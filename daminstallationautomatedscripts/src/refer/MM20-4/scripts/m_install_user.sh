
function install_user () {

	if [ "$GLOB_DRYRUN" != "no" ]; then
                ( set +x; echo -n "*D* DRYRUN($GLOB_DRYRUN): ${FUNCNAME[0]}"; printf ' %q' "$@"; echo)
                return
        fi
        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	#		-ConfigTemplate $GLOB_CONFDIR/$GLOB_LOGIN_CONF
	#		-User $GLOB_USER
	#		-Group $GLOB_GROUP
	#		-UserHome $GLOB_INST
	#		-PasswordSeed $GLOB_OTDS_PASS

	#
	# included rc files for all OTMM users
	#
	if [ -d /tmp/skel ]; then
		echo "*D* Skeleton files are already in place"
	else
		cp -r /etc/skel /tmp
		for n in /tmp/skel/.*; do
			case "$n" in
				*logout)
					echo "*D* $n is not a login script, skipping..."
				;;
				*)
				        test -f $n && echo '. $HOME/.otmmrc' >> $n
				;;
			esac
		done
		cp $ConfigTemplate /tmp/skel/.otmmrc
		perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' /tmp/skel/.otmmrc
	fi

	#
	# create group if not exists
	#
	getent group $Group || groupadd $Group

        if id $User; then
                echo "User $User exists"
                if [ -d $UserHome/$User ]; then
                        echo "$User home directory exists as well"
                else
                        install -m 0775 -g $Group -o $User -d $UserHome/$User
			for n in /tmp/skel/.*; do
				[ -f $n ] && install -m 0775 -g $Group -o $User $n "$UserHome/$User/$(basename $n)"
			done
                fi
        else
                PW=`perl -e 'printf("%s",crypt("'\'$PasswordSeed\''","OT"))'`
                install -d $UserHome -g $Group -m 0775
                useradd -k /tmp/skel -g $Group -d $UserHome/$User -s /bin/bash -m -p $PW $User
        fi

	#
	# 
	#
	#if [ -d "$UserHome" ]; then
	#	echo "*D* $UserHome exists, ignoring"
	#else
	#        install -m 0775 -g "$Group" -o "$User" -d "$UserHome"
	#fi
}

