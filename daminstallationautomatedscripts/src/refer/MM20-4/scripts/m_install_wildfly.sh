#function install_otmm_wildfly () {
#
#	#install_otmm_wildfly \
#        #	-Archive "$GLOB_LMNT/$GLOB_WFARC" \
#        #	-InstallDir $GLOB_INST \
#        #	-ArchiveTopDir "$GLOB_WFARC_TDIR" \
#	#	-WildflyHomeLeafDir "$GLOB_TCINS_LEAFDIR" \
#        #	-User $GLOB_USER
#
#	localarc=$(mktemp -t tmp.installer.wildfly.XXXXXXXXx)
#        while [ -n "${1:-}" -a -n "${2:-}" ]; do
#                read "${1#*-}" <<< "$2"
#                shift
#                shift
#        done
#        if [ -n "${1:-}" ]; then
#                echo "*F* remaining argument: $1"
#                exit 1
#        fi
#
#	cp $Archive $localarc
#	chmod 777 $localarc
#        test -f $InstallDir || install -d -m 0775 -o $User -g $Group $InstallDir
#	su -c "tar -zx -C $InstallDir -f $localarc" $User
#        mv $InstallDir/$ArchiveTopDir $InstallDir/$WildflyHomeLeafDir
#}


#function enable_https_wildfly () {
#        Alias=''
#        Keystore=''
#        Storepass=''
#        Keypass=''
#        Dname=''
#        JavaHome=''
#        InstallDir=''
#
#        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
#                read -r "${1#*-}" <<< "$2"
#                shift
#                shift
#        done
#        if [ -n "${1:-}" ]; then
#                echo "*F* remaining argument: $1"
#                exit 1
#        fi
#
#	if [ -z "$Alias" ] || \
#	   [ -z "$Keystore" ] || \
#	   [ -z "$Storepass" ] || \
#	   [ -z  "$Keypass" ] || \
#	   [ -z  "$Dname" ] || \
#	   [ -z  "$JavaHome" ] || \
#	   [ -z  "$InstallDir" ]; then
#		echo "*F* Invalid argument"
#		echo "*F* Alias=$Alias, FQDN of this server"
#		echo "*F* Keystore=$Keystore, location of keystore"
#		echo "*F* Storepass=$Storepass, write access password"
#		echo "*F* Keypass=$Keypass, read access password"
#		echo "*F* Dname=$Dname, domain owner"
#		echo "*F* InstallDir=$InstallDir, Tomcat home directory"
#		echo "*F* JavaHome=$JavaHome, Java home directory"
#		exit 1
#	fi
#
#	if [ -f "$Keystore" ]; then
#		echo "*D* $Keystore exists, ignore keypair creation"
#	else
#		$JavaHome/bin/keytool -genkeypair \
#			-alias $Alias \
#			-keyalg RSA \
#			-keystore $Keystore \
#			-keypass $Keypass \
#			-storepass $Storepass \
#			-noprompt  \
#			-keysize 2048 \
#			-dname "$Dname"
#	fi
#
#	if grep "OTConnector" "$InstallDir/conf/server.xml"
#	then
#		echo "*D* HTTPS Connector is already configured, ignoring"
#	else
#		cp $InstallDir/conf/server.xml $InstallDir/conf/server.xml.sslbackup
#
#	export __SSL_CONFIG
#        __SSL_CONFIG=$(cat << EOM
#<!-- Define a SSL/TLS HTTP/1.1 Connector on port 8443
#           This connector uses the NIO implementation that requires the JSSE
#           style configuration. When using the APR/native implementation, the
#           OpenSSL style configuration is required as described in the APR/native
#           documentation -->
#<!-- OTConnector is configured marker -->
#<Connector URIEncoding="UTF-8"
#           server="Apache Tomcat"
#           port="8443"
#           executor="tomcatThreadPool"
#           SSLEnabled="true"
#           maxPostSize="-1"
#           scheme="https"
#           protocol="org.apache.coyote.http11.Http11Nio2Protocol"
#           sslImplementationName="org.apache.tomcat.util.net.jsse.JSSEImplementation"
#           secure="true">
#  <SSLHostConfig sslProtocol="TLS"
#                 protocols="TLSv1.2"
#                 honorCipherOrder="true"
#                 certificateVerification="none"
#                 ciphers="TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
#                          TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
#                          TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384,
#                          TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384,
#                          TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
#                          TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
#                          TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256,
#                          TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256,
#                          TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,
#                          TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384,
#                          TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
#                          TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,
#                          TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384,
#                          TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384,
#                          TLS_ECDH_RSA_WITH_AES_256_CBC_SHA,
#                          TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA,
#                          TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,
#                          TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256,
#                          TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
#                          TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
#                          TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256,
#                          TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256,
#                          TLS_ECDH_RSA_WITH_AES_128_CBC_SHA,
#                          TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA" >
#    <Certificate type="RSA"
#                 certificateKeystoreFile="$Keystore"
#		 certificateKeyAlias="$Alias"
#                 certificateKeystorePassword="$Keypass" />
#  </SSLHostConfig>
#</Connector>
#EOM
#)
#
#		sed -i 's/Service name="Catalina">/Service name="Catalina">\n@__SSL_CONFIG@/' $InstallDir/conf/server.xml
#		perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $InstallDir/conf/server.xml
#
#		diff -Nau $InstallDir/conf/server.xml $InstallDir/conf/server.xml.sslbackup || /bin/true
#
#	fi
#}

#function create_csr_wildfly () {
#        Alias=''
#	Keystore=''
#        Storepass=''
#        Keypass=''
#        CsrFile=''
#        JavaHome=''
#
#        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
#                read -r "${1#*-}" <<< "$2"
#                shift
#                shift
#        done
#        if [ -n "${1:-}" ]; then
#                echo "*F* remaining argument: $1"
#                exit 1
#        fi
#        
#	if [ -z "$Alias" ] || \
#	   [ -z "$Keystore" ] || \
#	   [ -z "$Storepass" ] || \
#	   [ -z "$Keypass" ] || \
#	   [ -z "$CsrFile" ] || \
#	   [ -z "$JavaHome" ]; then
#		echo "*F* Invalid argument"
#		echo "*F* Alias=$Alias"
#		echo "*F* Keystore=$Keystore"
#		echo "*F* Storepass=$Storepass"
#		echo "*F* Keypass=$Keypass"
#		echo "*F* CsrFile=$CsrFile"
#		echo "*F* JavaHome=$JavaHome"
#	fi
#
#	$JavaHome/bin/keytool \
#		-certreq \
#		-alias $Alias \
#		-keystore $Keystore \
#		-storepass $Storepass \
#		-noprompt \
#		-file $CsrFile \
#		-ext "SAN=dns:otmm165.ot.test,dns:otmm165sv1,dns:otmm165sv2,dns:otmm165sv1.ot.test,dns:otmm165sv2.ot.test"
#
#}

function sign_ot_test_csr () {
        CsrFile=''
        CrtFile=''
        CaKeyFile=''
        CaPemFile=''
        JavaHome=''
        CaPemFilePw=''
        x503ExtFile=''
	CaSrlFile=''
	WildflySslCertAliases=''
        
        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
                read -r "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -z "$CsrFile" ] || \
	   [ -z "$CrtFile"  ] || \
	   [ -z "$CaKeyFile"  ] || \
	   [ -z "$CaPemFile"  ] || \
	   [ -z "$JavaHome"  ] || \
	   [ -z "$CaSrlFile"  ] || \
	   [ -z "$CaPemFilePw" ]
	then
		echo "*F* Invalid arguments"
		echo "*F* CsrFile=$CsrFile"
		echo "*F* CrtFile=$CrtFile"
		echo "*F* CaKeyFile=$CaKeyFile"
		echo "*F* CaPemFile=$CaPemFile"
		echo "*F* CaPemFilePw=$CaPemFilePw"
		echo "*F* CaSrlFile=$CaSrlFile"
		echo "*F* JavaHome=$JavaHome"
	fi

	x503ExtFile=$(mktemp -t tmp.installer.wfx503.XXXXXXXXXX)

cat << 'EOT' > $x503ExtFile
[req]
req_extensions = v3_req

# Extensions to add to a certificate request
[ v3_req ]
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
EOT

        #Append dns aliases to cnf file
        cnt=1
        for w in $WildflySslCertAliases; do
                echo "DNS.$(( cnt ++ ))=$w" >> $x503ExtFile
        done

	test -f "$CaSrlFile" || echo "ED4B4A80662B1B4C" > "$CaSrlFile"
	openssl x509 -req \
	        -in $CsrFile \
		-CAkey "$CaKeyFile" \
		-CA "$CaPemFile" \
		-CAserial "$CaSrlFile" \
		-extfile "$x503ExtFile" \
		-extensions v3_req \
		-out "$CrtFile" \
		-days 1825 \
		-sha256 \
		-passin "pass:$CaPemFilePw"
	rm "$x503ExtFile"
	openssl x509 -in "$CrtFile" -noout -text | egrep '(Issuer:|Subject:|DNS:)'

}

#
# Gotcha: if the alias used here is the same as the alias in the private key entry, 
#         the check for the existing CRT below does not work - use another alias or reinstall
#
function install_crt_wildfly () {
        Alias=''
        CrtFile=''
        Keystore=''
        Storepass=''
        JavaHome=''
        CaPemFile=''

        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
                read -r "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -z "$Alias" ] || \
  	   [ -z "$CrtFile" ] || \
	   [ -z "$Keystore" ] || \
	   [ -z "$Storepass" ] || \
	   [ -z "$JavaHome" ] || \
	   [ -z "$CaPemFile" ]; then
		echo "*F* Invalid arguments"
		echo "*F* Alias=$Alias"
		echo "*F* CrtFile=$CrtFile"
		echo "*F* Keystore=$Keystore"
		echo "*F* Storepass=$Storepass"
		echo "*F* CaPemFile=$CaPemFile"
		echo "*F* JavaHome=$JavaHome"
	fi

#should already be in /etc/pki/...
#	if $JavaHome/bin/keytool \
#		-list \
#		-alias "ot.testca" \
#		-storepass "$Storepass" \
#		-noprompt \
#		-keystore "$Keystore"; then
#		echo "*D* ot.testca exists, skipping"
#	else
#		$JavaHome/bin/keytool \
#			-import \
#			-trustcacerts \
#			-alias ot.testca \
#			-file "$CaPemFile" \
#			-keystore "$Keystore" \
#			-storepass "$Storepass" \
#			-noprompt
#	fi

	if $JavaHome/bin/keytool \
			-import \
			-alias "$Alias" \
			-file "$CrtFile" \
			-keystore "$Keystore" \
			-storepass "$Storepass" \
			-trustcacerts \
			-noprompt 
	then
		echo "*D* Installed CRT"
	else
		echo "*D* CRT already installed or other error, ignoring..."
	fi

}

function create_csr_wildfly () {
        Alias=''
        Keystore=''
        Storepass=''
        Keypass=''
        CsrFile=''
        JavaHome=''
	WildflySslCertAliases=''

        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
                read -r "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        if [ -z "$Alias" ] || \
           [ -z "$Keystore" ] || \
           [ -z "$Storepass" ] || \
           [ -z "$Keypass" ] || \
           [ -z "$CsrFile" ] || \
           [ -z "$JavaHome" ]; then
                echo "*F* Invalid argument"
                echo "*F* Alias=$Alias"
                echo "*F* Keystore=$Keystore"
                echo "*F* Storepass=$Storepass"
                echo "*F* Keypass=$Keypass"
                echo "*F* CsrFile=$CsrFile"
                echo "*F* JavaHome=$JavaHome"
        fi

        #Append dns aliases to cnf file
        cnt=1
	EXT=""
        for w in $WildflySslCertAliases; do
		if [ -z $EXT ]; then
	                EXT="SAN=dns:$w" 
		else
	                EXT="${EXT},dns:$w" 
		fi
        done

        $JavaHome/bin/keytool \
                -certreq \
                -alias $Alias \
                -keystore $Keystore \
                -storepass $Storepass \
                -noprompt \
                -file $CsrFile \
                -ext "$EXT"

}

function sign_ot_test_csr_wildfly () {
        CsrFile=''
        CrtFile=''
        CaKeyFile=''
        CaPemFile=''
        JavaHome=''
        CaPemFilePw=''
        x503ExtFile=''
        CaSrlFile=''
	WildflySslCertAliases=''

        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
                read -r "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

        if [ -z "$CsrFile" ] || \
           [ -z "$CrtFile"  ] || \
           [ -z "$CaKeyFile"  ] || \
           [ -z "$CaPemFile"  ] || \
           [ -z "$JavaHome"  ] || \
           [ -z "$CaSrlFile"  ] || \
           [ -z "$CaPemFilePw" ]
        then
                echo "*F* Invalid arguments"
                echo "*F* CsrFile=$CsrFile"
                echo "*F* CrtFile=$CrtFile"
                echo "*F* CaKeyFile=$CaKeyFile"
                echo "*F* CaPemFile=$CaPemFile"
                echo "*F* CaPemFilePw=$CaPemFilePw"
                echo "*F* CaSrlFile=$CaSrlFile"
                echo "*F* JavaHome=$JavaHome"
        fi

        x503ExtFile=$(mktemp -t tmp.installer.wfx503.XXXXXXXXXX)

cat << 'EOT' > $x503ExtFile
[req]
req_extensions = v3_req

# Extensions to add to a certificate request
[ v3_req ]
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
EOT

        #Append dns aliases to cnf file
        cnt=1
        for w in $WildflySslCertAliases; do
                echo "DNS.$(( cnt ++ ))=$w" >> $x503ExtFile
        done

        test -f "$CaSrlFile" || echo "ED4B4A80662B1B4C" > "$CaSrlFile"
        openssl x509 -req \
                -in $CsrFile \
                -CAkey "$CaKeyFile" \
                -CA "$CaPemFile" \
                -CAserial "$CaSrlFile" \
                -extfile "$x503ExtFile" \
                -extensions v3_req \
                -out "$CrtFile" \
                -days 1825 \
                -sha256 \
                -passin "pass:$CaPemFilePw"
        rm "$x503ExtFile"
        openssl x509 -in "$CrtFile" -noout -text | egrep '(Issuer:|Subject:|DNS:)'

}

#
# Gotcha: if the alias used here is the same as the alias in the private key entry,
#         the check for the existing CRT below does not work - use another alias or reinstall
#
#function install_crt_wildfly () {
#        Alias=''
#        CrtFile=''
#        Keystore=''
#        Storepass=''
#        JavaHome=''
#        CaPemFile=''
#
#        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
#                read -r "${1#*-}" <<< "$2"
#                shift
#                shift
#        done
#        if [ -n "${1:-}" ]; then
#                echo "*F* remaining argument: $1"
#                exit 1
#        fi
#
#        if [ -z "$Alias" ] || \
#           [ -z "$CrtFile" ] || \
#           [ -z "$Keystore" ] || \
#           [ -z "$Storepass" ] || \
#           [ -z "$JavaHome" ] || \
#           [ -z "$CaPemFile" ]; then
#                echo "*F* Invalid arguments"
#                echo "*F* Alias=$Alias"
#                echo "*F* CrtFile=$CrtFile"
#                echo "*F* Keystore=$Keystore"
#                echo "*F* Storepass=$Storepass"
#                echo "*F* CaPemFile=$CaPemFile"
#                echo "*F* JavaHome=$JavaHome"
#        fi
#
#        if $JavaHome/bin/keytool -list -alias ot.testca -storepass $Storepass -noprompt -keystore $Keystore
#        then
#                echo "*D* ot.testca exists, skipping"
#        else
#                $JavaHome/bin/keytool \
#        else
#                $JavaHome/bin/keytool \
#                        -import \
#                        -trustcacerts \
#                        -alias ot.testca \
#                        -file "$CaPemFile" \
#                        -keystore "$Keystore" \
#                        -storepass "$Storepass" \
#                        -noprompt
#        fi
#
##       if $JavaHome/bin/keytool -list -alias "$Alias" -storepass "$Storepass" -noprompt -keystore "$Keystore"
##       then
##               echo "*D* $Alias exists, skipping"
##       else
##               $JavaHome/bin/keytool \
##                       -import \
##                       -alias "$Alias" \
##                       -file "$CrtFile" \
##                       -keystore "$Keystore" \
##                       -storepass "$Storepass" \
##                       -noprompt
##       fi
#
#        if $JavaHome/bin/keytool \
#                        -import \
#                        -alias "$Alias" \
#                        -file "$CrtFile" \
#                        -keystore "$Keystore" \
#                        -storepass "$Storepass" \
#                        -noprompt
#        then
#                echo "*D* Installed CRT"
#        else
#                echo "*D* CRT already installed or other error, ignoring..."
#        fi
#
#}

# create a keystore from ready made keyfile/certfile
function create_keystore_wildfly () {
        CrtFile=''
        KeyFile=''
        ExportFile=''
        Keystore=''
        Keypass=''
        Alias=''

        while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
                   read -r "${1#*-}" <<< "$2"
                        shift
                   shift
        done
        if [ -n "${1:-}" ]; then
                        echo "*F* remaining argument: $1"
                        exit 1
    fi

        if [ -z "$CrtFile" ] || \
           [ -z "$KeyFile" ] || \
           [ -z "$ExportFile" ] || \
           [ -z "$Keystore" ] || \
           [ -z "$Keypass" ] || \
           [ -z "$Alias" ]; then
        echo "*F* Invalid argument"
                        echo "*F* Alias=$Alias"
                        echo "*F* Keystore=$Keystore"
                        echo "*F* CrtFile=$CrtFile"
                        echo "*F* Keypass=$Keypass"
                        echo "*F* KeyFile=$KeyFile"
        fi

        #Build pkcs12 archive for import into keystore
        openssl pkcs12 \
                -export \
                -password "pass:$Keypass" \
                -in $CrtFile \
                -inkey $KeyFile \
                -name $Alias \
                -out $ExportFile

        #Import into keystore
        keytool -importkeystore \
                -srcstoretype PKCS12 \
                -srckeystore $ExportFile \
                -srcstorepass $Keypass \
                -srcalias $Alias \
                -destkeystore $Keystore \
                -deststorepass $Keypass \
                -destalias $Alias
}

# create a keystore / keypari from scratch
function create_new_keystore_wildfly() {

	KeyStore=''
	KeyPass=''
	Storepass=''
	Dname=''
	Alias=''

	while [ -n "${1:-}" ] && [ -n "${2:-}" ]; do
        	read -r "${1#*-}" <<< "$2"
			shift
		shift
        done
        if [ -n "${1:-}" ]; then
                        echo "*F* remaining argument: $1"
                        exit 1
	fi

        if [ -z "$keyStore" ] || \
           [ -z "$StorePass" ] || \
           [ -z "$Dname" ] || \
           [ -z "$Alias" ] || \
           [ -z "$KeyPass" ]; then
        echo "*F* Invalid argument"
                        echo "*F* KeyStore=$KeyStore"
                        echo "*F* KeyPass=$KeyPass"
                        echo "*F* Dname=$Dname"
                        echo "*F* StorePass=$StorePass"
                        echo "*F* Alias=$Alias"
        fi

       if [ -f "$Keystore" ]; then
               echo "*D* $Keystore exists, ignore keypair creation"
       else
               $JavaHome/bin/keytool -genkeypair \
                       -alias "$Alias" \
                       -keyalg RSA \
                       -keystore "$KeyStore" \
                       -keypass "$KeyPass" \
                       -storepass "$StorePass" \
                       -noprompt  \
                       -keysize 2048 \
                       -dname "$Dname"
       fi

}

