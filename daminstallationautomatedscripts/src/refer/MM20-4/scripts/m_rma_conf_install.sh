#
# Install Rich Media Analysis
#


function rma_install () {

        localconf=$(mktemp -t tmp.installer.rmaconf.XXXXXXX.txt)

        while [ -n "${1:-}" -a -n "${2:-}" ]; do
                read "${1#*-}" <<< "$2"
                shift
                shift
        done
        if [ -n "${1:-}" ]; then
                echo "*F* remaining argument: $1"
                exit 1
        fi

	if [ -d "$TmpMount/OTMM" ]; then
                echo "$TmpMount exists, ignoring mounting of iso..."
        else
                mkdir -p $TmpMount
                mount -o loop $Archive $TmpMount
        fi

        cp -f $ConfigTemplate $localconf

        chmod 777 $localconf
        perl -pi -e 's/@([^@]*)@/defined $ENV{$1} ? $ENV{$1} : $&/eg' $localconf
        >&2 diff -Nau $ConfigTemplate $localconf || /bin/true

        _d="$TmpMount/RMA/Disk1/InstData/VM"
        if [ "$StopAtInstaller" = "yes" ]; then
                echo "*D* stopped at RMA installer"
                echo "*D* set export LAX_DEBUG=1 for more debugging"
                echo "*D* set export JAVA_HOME=$JavaHome"
                echo "*D* set export TEAMS_HOME=$TeamsHome"
                echo "*D* run $_d/install.bin -l /tmp/richmedianalysis-install.log -i silent -f $localconf"
                echo "*D* post clearup with chown -R $User:$Group $InstallDir"
                exit 1
        else
                #as root!
                export JAVA_HOME=$JavaHome
                export LAX_DEBUG="1"
                echo "################################################################################"
                if $_d/install.bin -l /tmp/richmedianalysis-install.log -i silent -f $localconf; then
                        echo "*D* Installer succeeded"
                else
			echo "*F* Installer failed"
		fi
	fi

	echo "*D* install Erlang"
        if bash -c "yum list installed erlang"; then
                echo "*D* Erlang already installed.."
        else
                bash -c "BASH_XTRACEFD= yum install -q -y $InstallDir/install/erlang/erlang*"
        fi

	echo "*D* install Socat"
        if bash -c "yum list installed socat"; then
                echo "*D* Socat already installed.."
        else
                bash -c "BASH_XTRACEFD= yum install -q -y socat"
        fi

	echo "*D* install Rabbitmq"
        if bash -c "yum list installed rabbitmq-server"; then
                echo "*D* rabbitmq-server already installed.."
        else
                bash -c "BASH_XTRACEFD= yum install -q -y $InstallDir/install/rabbitmq/rabbitmq-server*"
		systemctl start rabbitmq-server
		$InstallDir/bin/stopRichMediaAnalysis.sh
        fi


}
