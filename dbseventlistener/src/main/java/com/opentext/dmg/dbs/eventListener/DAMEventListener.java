package com.opentext.dmg.dbs.eventListener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.event.Event;
import com.artesia.event.EventListener;
import com.artesia.event.services.EventServices;
import com.artesia.security.SecuritySession;
import com.artesia.security.session.services.LocalAuthenticationServices;
import com.opentext.dmg.dbs.eventListener.handlers.JobEventHandler;
import com.opentext.dmg.dbs.eventListener.handlers.TaskEventHandler;
import com.opentext.dmg.dbs.eventListener.utils.EventListenerConstants;

public class DAMEventListener implements EventListener, ServletContextListener {

	private static final Log LOGGER = LogFactory.getLog(DAMEventListener.class);
	
	public void onEvent(Event event) {
		
		LOGGER.info("Got Event :" + event.getEventId());
		
		if (event.getEventId().toString().equals(EventListenerConstants.ACTIVITY_STARTED) 
		|| event.getEventId().toString().equals(EventListenerConstants.ACTIVITY_COMPLETED)) {
				JobEventHandler handler = new JobEventHandler();
				handler.handleEvent(event);
		}else if (event.getEventId().toString().equals(EventListenerConstants.TASK_CREATED)
				|| event.getEventId().toString().equals(EventListenerConstants.TASK_COMPLETED)) {
				TaskEventHandler handler = new TaskEventHandler();
				handler.handleEvent(event);
		}
	}

	public void contextInitialized(ServletContextEvent intEvent) {
		SecuritySession createLocalSession = null;
		try {
			createLocalSession = LocalAuthenticationServices.getInstance().createLocalSession(EventListenerConstants.ADMIN_USER);
			EventServices.getInstance().removeEventListener(EventListenerConstants.CLIENT_ID, createLocalSession);
			EventServices.getInstance().addEventListener(EventListenerConstants.CLIENT_ID, this, createLocalSession);
		} catch (BaseTeamsException e) {
			LOGGER.error("An Error occured whilst registering event listener", e);
		}
	}

	public void contextDestroyed(ServletContextEvent destroyEvent) {
	
	}
}
