package com.opentext.dmg.dbs.eventListener.dto;

import java.io.Serializable;
import java.util.Map;

public class AbstractCampaignData implements Serializable{
	
	private static final long serialVersionUID = -3922905490199663925L;

	private Map<String, Object> data;

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public void attachCampaignData(Map<String, Object> data) {
		this.data = data;;
	}
}
