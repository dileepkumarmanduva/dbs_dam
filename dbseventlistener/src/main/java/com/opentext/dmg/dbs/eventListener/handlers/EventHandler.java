package com.opentext.dmg.dbs.eventListener.handlers;

import com.artesia.event.Event;

public interface EventHandler {

	void handleEvent(Event event);
}
