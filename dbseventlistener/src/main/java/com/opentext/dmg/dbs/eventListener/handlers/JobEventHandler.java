package com.opentext.dmg.dbs.eventListener.handlers;

import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.event.Event;
import com.artesia.security.SecuritySession;
import com.artesia.security.session.services.LocalAuthenticationServices;
import com.opentext.dmg.dbs.eventListener.dto.AbstractCampaignData;
import com.opentext.dmg.dbs.eventListener.utils.DBUtils;
import com.opentext.dmg.dbs.eventListener.utils.EventListenerConstants;
import com.opentext.dmg.dbs.eventListener.utils.EventListenerUtils;
import com.opentext.job.Job;
import com.opentext.job.JobCriteria;
import com.opentext.job.services.JobServices;

public class JobEventHandler implements EventHandler{

	private static final Log LOGGER = LogFactory.getLog(JobEventHandler.class);
	
	public void handleEvent(Event event) {
		int logResult = 0;
		try {
			
			SecuritySession securitySession = LocalAuthenticationServices.getInstance().createLocalSession(EventListenerConstants.ADMIN_USER);
			
			JobCriteria aCriteria = new JobCriteria();
			aCriteria.setLoadAssetDetails(false);
			aCriteria.setLoadJobContext(true);
			aCriteria.setLoadJobDetails(true);
			aCriteria.setLoadTaskDetails(true);
			
			LOGGER.info("Event ObjectID is : " + event.getObjectId());
			Job aJob = JobServices.getInstance().retrieveJob(Long.parseLong(event.getObjectId()), aCriteria, securitySession);
			
			if (EventListenerUtils.isActivityEligible(aJob.getJobType())) {
				LOGGER.info("Activity is eligible for logging.");
				Map<String, Object> data = aJob.getJobContext().getData();
				
				AbstractCampaignData campaignData = EventListenerUtils.createCampaignData(aJob.getJobType(), data);
				logResult = DBUtils.logCampaignDataToDB(campaignData, aJob, event);
				if (logResult <=0) {
					LOGGER.error ("Unable to persist the audit event for the campaign.");
				}
			}else {
				LOGGER.info("Activity is not eligible for logging.");
			}
			
		} catch (BaseTeamsException e) {
			LOGGER.error("An Error occured while handling the job event", e);
		} catch (SQLException e) {
			LOGGER.error("An Error occured while handling the job event", e);
		}
		
	}
}
