package com.opentext.dmg.dbs.eventListener.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.event.Event;
import com.opentext.dmg.dbs.eventListener.dto.AbstractCampaignData;
import com.opentext.job.Job;

import ttsg_teams.admin.db.DBMgr;
import ttsg_teams.common.common.TeamsException;

public class DBUtils {
	private static final Log LOGGER = LogFactory.getLog(DBUtils.class);

	public static Connection getConnection(){
		LOGGER.info("getConnection >> start");
		DBMgr dbMgr = null;
		Connection connection = null;
		try {
			dbMgr = new DBMgr();
			connection = dbMgr.createConnection();
		} catch (TeamsException e) {
			LOGGER.error("FAILED TO GET DATABASE CONNECTION:" + e.getMessage());
			return connection;
		}
		LOGGER.info("getConnection >> end");
		return connection;
	}

	public static int executeQuery (PreparedStatement preparedStatement) throws  SQLException{
		LOGGER.info("executeQuery >> start");
		int executeUpdate = preparedStatement.executeUpdate();
		LOGGER.info("executeQuery >> end executeUpdate Result :" + executeUpdate);
		return executeUpdate;
	}

	public static int logCampaignDataToDB(AbstractCampaignData campaignData, Job aJob, Event event) throws SQLException {
		LOGGER.info("logCampaignDataToDB >> start");
		
		Long jobId = aJob.getJobId();
		String jobType = aJob.getJobType();
		String jobName = aJob.getJobName();
		String initiatorId = aJob.getInitiatorId().getId();
		String jobData = EventListenerUtils.convertMapToJson(campaignData.getData());
		// TABLE id(pk) , Job ID, job Type, job name, initiator Id, and data (stored as JSON).
		String query = "INSERT INTO CAMPAIGN_AUDIT_TRAIL(ID,JOB_ID,JOB_TYPE,JOB_NAME,INITIATOR_ID,JOB_DATA) VALUES (?,?,?,?,?,?)";
		Connection connection = getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		int index = 1;
		String id = String.valueOf(UUID.randomUUID().toString().hashCode());
		preparedStatement.setString(index++, id); 
		preparedStatement.setString(index++, String.valueOf(jobId));
		preparedStatement.setString(index++, jobType);
		preparedStatement.setString(index++, jobName);
		preparedStatement.setString(index++, initiatorId);
		preparedStatement.setString(index++, jobData);

		int executeResult= executeQuery(preparedStatement);

		LOGGER.info("logCampaignDataToDB >> end: result: " + executeResult);
		return executeResult;
	}
	
	public static String getTaskDetails(long taskId) {
		String SELECT_TASKNAME_JOB_ID = "SELECT JOB_ID FROM OTMM_TASKS WHERE TASK_ID=?";
		try (Connection connection = getConnection(); PreparedStatement pStmt = connection.prepareStatement(SELECT_TASKNAME_JOB_ID);) {
			pStmt.setLong(1, taskId);
			ResultSet resultSet = null;
			try {
				resultSet = pStmt.executeQuery();
				if (resultSet.next()) {
					String taskNameJobId = resultSet.getString(1);
					return taskNameJobId;
				}
			} catch (SQLException e) {
				LOGGER.error("Exception in getTaskNameAndJobId() ", e);
			} finally {
				if (resultSet != null) {
					resultSet.close();
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception while getting noOfOpenCreativeReviewJobsCount", e);
		}

		return null;
	}
}
