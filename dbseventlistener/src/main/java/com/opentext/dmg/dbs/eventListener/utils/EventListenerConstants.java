package com.opentext.dmg.dbs.eventListener.utils;

public class EventListenerConstants {

	public static final String ADMIN_USER = "tsuper";
	
	public static final String CLIENT_ID = "DBS";
	
	public static final String ACTIVITY_STARTED = "6007";
	
	public static final String ACTIVITY_COMPLETED = "6009";
	
	public static final String TASK_CREATED = "8010";
	
	public static final String TASK_COMPLETED = "8012";
	
	public static final String INTERNAL_BRIEF_WF = "InternalAgencyBrief";
	
	public static final String EXTERNAL_BRIEF_WF = "ExternalAgencyBrief";
	
	public static final String ADHOC_BRIEF_WF = "AdhocCampaign";
	
	public static final String JOB_NAMES_FOR_AUDIT [] = {INTERNAL_BRIEF_WF, EXTERNAL_BRIEF_WF, ADHOC_BRIEF_WF};
	
}
