package com.opentext.dmg.dbs.eventListener.utils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.artesia.event.Event;
import com.opentext.dmg.dbs.eventListener.dto.AbstractCampaignData;
import com.opentext.dmg.dbs.eventListener.dto.AdhocCampaignData;
import com.opentext.dmg.dbs.eventListener.dto.ExternalCampaignData;
import com.opentext.dmg.dbs.eventListener.dto.InternalCampaignData;
import com.opentext.job.context.ListContextWrapper;
import com.opentext.job.task.Task;
import com.opentext.job.task.services.TaskServices;

public class EventListenerUtils {

	private static final Log LOGGER = LogFactory.getLog(EventListenerUtils.class);
	
	public static boolean isActivityEligible(String jobType) {
	 
		  List<String> jobNamesList = Arrays.asList(EventListenerConstants.JOB_NAMES_FOR_AUDIT);
		  LOGGER.info("Recieved JobType for Audit is : " + jobType);
		  if (jobNamesList.contains(jobType)) 
		  { 
			  return true; 
		  }
		  else 
		  { 
			  return false; 
		  }
	}

	public static boolean isTaskEligible(Event event) {
		try {
			Task task = TaskServices.getInstance().retrieveTaskById(Long.parseLong(event.getObjectId()), false, 0, null);
			String jobType = task.getJobType();
			LOGGER.info("Task is for jobtype:" + jobType);
			if (isActivityEligible(jobType)) {
				return true;
			}
		}  catch (Exception e) {
			LOGGER.error("An error occured while handling the task event.", e);
		}
		return false;
	}

	public static AbstractCampaignData createCampaignData(String jobType, Map<String, Object> data) {
		
		if (jobType.equals(EventListenerConstants.INTERNAL_BRIEF_WF)) {
			InternalCampaignData internalCampaignData = new InternalCampaignData();
			internalCampaignData.attachCampaignData(data);
			return internalCampaignData;
		}else if (jobType.equals(EventListenerConstants.EXTERNAL_BRIEF_WF)) {
			ExternalCampaignData externalCampaignData = new ExternalCampaignData();
			externalCampaignData.attachCampaignData(data);
			return externalCampaignData;
		}else if (jobType.equals(EventListenerConstants.ADHOC_BRIEF_WF)) {
			AdhocCampaignData adhocCampaignData = new AdhocCampaignData();
			adhocCampaignData.attachCampaignData(data);
			return adhocCampaignData;
		}
		return null;
	}

	/*
	 * private Object getDataFromContext(Object keyOrValue, JobContext jobContext) {
	 * Object value = keyOrValue; if (keyOrValue != null && keyOrValue instanceof
	 * String) { String keyOrValueString = keyOrValue.toString(); if
	 * (keyOrValueString.startsWith("{") && keyOrValueString.endsWith("}")) { String
	 * key = keyOrValueString.substring(1, keyOrValueString.length() - 1); value =
	 * jobContext.getData().get(key); } }
	 * 
	 * return value; }
	 */
	public static String convertMapToJson(Map<String, Object> data) {
		
		JSONObject returnValue = new JSONObject();
		
		Set<String> keySet = data.keySet();
		for (String aKey : keySet) {
			if (aKey.endsWith("LabelND") || aKey.equals("statusDetails")) {
				continue;
			}
			Object object = data.get(aKey);
			if (object instanceof String) {
				returnValue.put(aKey, object);				
			}else if (object instanceof ListContextWrapper<?>) {
				ListContextWrapper values = (ListContextWrapper)object;
				JSONArray array = new JSONArray();
				if (values != null) {
					List<String> listData = values.getListData();
					if (listData != null && !listData.isEmpty()) {
						Iterator listDataIterator = listData.iterator();
						while (listDataIterator.hasNext()) {
							Object next = listDataIterator.next();
							array.add(next.toString());
						}
					}
				}
				returnValue.put(aKey, array);
			}
			
		}
		LOGGER.info("Workflow state data is : " + returnValue.toJSONString());
		return returnValue.toJSONString();
	}
}
