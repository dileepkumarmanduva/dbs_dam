package com.opentext.dbs.transformer.html;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.artesia.asset.Asset;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.common.lang.ProcessResult;
import com.artesia.common.lang.ProcessUtils;
import com.artesia.common.prefs.PrefData;
import com.artesia.common.prefs.PrefDataId;
import com.artesia.content.ContentInfo;
import com.artesia.content.RenditionContentInfo;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.security.SecuritySession;
import com.artesia.server.common.plugin.ServerPluginResources;
import com.artesia.server.transformer.ImportTransformationResult;
import com.artesia.server.transformer.ImportTransformer;
import com.artesia.server.transformer.TransformerAssetWrapper;
import com.artesia.system.services.SystemServices;
import com.artesia.transformer.TransformerArgument;
import com.artesia.transformer.TransformerInfo;

public class HTMLPreviewTransformer implements ImportTransformer{
	
	private Logger LOGGER = LoggerFactory.getLogger(HTMLPreviewTransformer.class);
	
	public void initialize(TransformerInfo arg0, Map<String, String> arg1) {
		
	}

	public ImportTransformationResult transformAssetForImport(TransformerAssetWrapper assetWrapper, File workingDir,
			List<TransformerArgument> transformArguments, ServerPluginResources serverPluginResources) throws BaseTeamsException {
		ImportTransformationResult returnValue = new ImportTransformationResult(false);
		Asset asset = assetWrapper.getAsset();
		String destinationExtension = ".JPG";
		
		LOGGER.info("Starting Import tranformation of HTML File for rendition generation.");
		
		String subDirName = "ImageTransform-" + asset.getAssetId().asString() + '-' + System.currentTimeMillis();
		File subDir = new File(workingDir,subDirName);
		boolean mkdir = subDir.mkdir();
		if (!mkdir) {
			returnValue.setContinueTransformation(true);
			returnValue.setTransformationSuccessful(true);
			return returnValue;
		}
		String sourceFileAbsolutePath = assetWrapper.getMasterFile().getAbsolutePath();
		File sourceFile = assetWrapper.getMasterFile();
		
		String destName = FilenameUtils.getBaseName(sourceFile.getName());
		
		destName = destName + "-P" + destinationExtension;
		
		File destinationFile = new File(subDir , destName);
		 
		String command[] = new String[] { "wkhtmltoimage", sourceFileAbsolutePath ,destinationFile.getAbsolutePath()};
		
		try {
			ProcessResult execCommandResult = ProcessUtils.execCommand(command, subDir, 300*1000,300);
			System.out.println(execCommandResult.getExitValue());
			
			File outputFile = null;
			File[] subFiles = subDir.listFiles();
			if (subFiles != null) {
				int i;
				for (i = 0; i < subFiles.length; ++i) {
					if (subFiles[i].getName().startsWith(destName)) {
						outputFile = subFiles[i];
						break;
					}
				}
				ContentInfo previewContentInfo = new ContentInfo();
				previewContentInfo.setFile(outputFile);
				previewContentInfo.setMimeType("image/jpeg");
				previewContentInfo.setName(outputFile.getName());
				previewContentInfo.getContentData().setTempFile(true);
				PrefData prefData = new PrefData();
				
				String contentManagerId = getSystemSetting("IMPORT", "SERVER", "RENDITION_CONTENT_MANAGER_ID", null);
				if (contentManagerId == null || contentManagerId.length() == 0) {
					LOGGER.error("No Content Manager ID configurred for RENDITION_CONTENT_MANAGER_ID");
					LOGGER.warn("Rendition generation is failed for the HTML asset.");
					returnValue.setContinueTransformation(true);
					returnValue.setTransformationSuccessful(true);
					return returnValue;
				}
				previewContentInfo.setContentManagerId(new TeamsIdentifier(contentManagerId)); //TODO modify it later. 
				previewContentInfo.setContentKind("PREVIEW");
				
				if (asset.getRenditionContent() == null) {
					RenditionContentInfo renditionContentInfo = new RenditionContentInfo();
					renditionContentInfo.setPreviewContent(previewContentInfo);
					renditionContentInfo.setThumbnailContent(previewContentInfo);
					asset.setRenditionContent(renditionContentInfo);
				}
				
			}
		} catch (IOException e) {
			LOGGER.error("An error occured while trying to generate the preview for the HTML Asset.", e);
		}
		
		returnValue.setContinueTransformation(true);
		returnValue.setTransformationSuccessful(true);
		LOGGER.info("Successfuly generated HTML preview and thumbnail for HTML Asset.");
		return returnValue;
	}
	
	private String getSystemSetting(String componentName, String keyName, String settingName, SecuritySession securitySession)
	{
		PrefDataId dataId = new PrefDataId(componentName, keyName, settingName);
		PrefData retrievedData = null;
		try 
		{
			retrievedData = SystemServices.getInstance().retrieveSystemSettingsByPrefDataId(dataId, securitySession);
		} 
		catch (BaseTeamsException e) 
		{
			LOGGER.error("");
			
		}
		return retrievedData == null ? "" : retrievedData.getValue();
	}
}
