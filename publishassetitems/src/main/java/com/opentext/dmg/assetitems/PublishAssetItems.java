package com.opentext.dmg.assetitems;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.security.SecuritySession;
import com.opentext.activity.ActionProperty;
import com.opentext.activity.PropertyDataType;
import com.opentext.activity.PropertyType;
import com.opentext.dmg.assetitems.util.UploadAssetsUtil;
import com.opentext.dmg.otmm.util.StepUtils;
import com.opentext.job.JobContext;
import com.opentext.server.job.step.BaseStep;
import com.opentext.server.job.step.JobData;
import com.opentext.server.job.step.StepStatus;

public class PublishAssetItems extends BaseStep {

	private static final Log log = LogFactory.getLog(PublishAssetItems.class);

	@Override
	public StepStatus executeStep(JobData jobData, JobContext jobContext, SecuritySession session)
			throws BaseTeamsException {

		if (log.isDebugEnabled()) {
			Map<String, Object> dataMap = jobContext.getData();
			log.debug(">>>PublishAssetItems Begin. DataMap::" + dataMap);
			log.debug("beanProperties::" + getBeanProperties());
			log.debug("Job Parameters:: " + jobData.getJobParameters());
		}

		String assetIdStr = getBeanProperties().get("assetIds");
		log.debug("assetIdsStr" + ":" + assetIdStr);
		var assetIdsObject = getDataFromContext(assetIdStr, jobContext);
		List<AssetIdentifier> assetIdList = StepUtils.getAssetIds(assetIdsObject);
		return publish(assetIdList,session);
	}

	private StepStatus publish(List<AssetIdentifier> assetIdList ,SecuritySession session) throws BaseTeamsException {
		boolean isSuccess= UploadAssetsUtil.uploadAssetItems(assetIdList,session);
		if( isSuccess ) {
			return StepStatus.COMPLETED;
		}
		else {
			return StepStatus.COMPLETED_WITH_ERRORS;	
		}
	}

	@Override
	protected void initializeProperties(Set<ActionProperty> actionProperties) {
		var assetIdsProperty = new ActionProperty("assetIds", "The ids of the assets to be published",
				"Assets", true, PropertyDataType.STRING, "{assetIds}", PropertyType.INPUT);
		actionProperties.add(assetIdsProperty);

	}

}
