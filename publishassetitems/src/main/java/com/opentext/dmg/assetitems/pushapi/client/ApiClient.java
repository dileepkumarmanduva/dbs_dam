package com.opentext.dmg.assetitems.pushapi.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHeaders;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opentext.dmg.assetitems.pushapi.model.ErrorResponse;

public class ApiClient {
	private static final Log log = LogFactory.getLog(ApiClient.class);

	private ApiClient() {
		log.debug("ApiClient initialized..");
	}

	public static boolean executePost(final String serviceEndPoint, final String username, final String password,
			final String requestJsonString) {
		var isSuccess = false;
		var httpClientBuilder = HttpClients.custom();
		try(CloseableHttpClient httpClient = httpClientBuilder.build()){
			var uri = buildURI(serviceEndPoint);
			var httpPost = new HttpPost(uri);
			httpPost.setHeader(HttpHeaders.AUTHORIZATION, getAuthHeader(username, password));
			httpPost.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
			var requestEntity = new StringEntity(requestJsonString, ContentType.APPLICATION_JSON);
			httpPost.setEntity(requestEntity);
			isSuccess = invokeApi(httpClient, httpPost); 
		} catch (IOException e) {
			log.error("Error ",e);
		}
		return isSuccess;
	}


	private static boolean invokeApi(CloseableHttpClient httpClient, HttpPost httpPost) {
		var isSuccess = false;
		try(CloseableHttpResponse httpResponse = httpClient.execute(httpPost)) {
			isSuccess = handleResponse(httpResponse);
		} catch (ClientProtocolException e) {
			log.error("Error in the HTTP protocol ",e);
		} catch (IOException e) {
			log.error("Error ",e);
		}
		return isSuccess;
	}

	private static boolean handleResponse(CloseableHttpResponse httpResponse){
		var isSuccess = false;
		if(httpResponse==null) {
			return isSuccess;
		}
		var statusCode = httpResponse.getStatusLine().getStatusCode();
		log.info("DCP API http staus code: "+ statusCode);
		if(statusCode == 200) {
			isSuccess = true;
			log.info("DCP API request is sucessful");
			return isSuccess;
		}else {
			isSuccess = false;
			log.error("DCP API request is unsucessful");
			handleError(httpResponse, statusCode);
		}
		return isSuccess;
	}

	private static void handleError(CloseableHttpResponse httpResponse, int statusCode) {
		if(statusCode == 400) {
			var objectMapper = new ObjectMapper();
			try {
				var errorResponse = objectMapper.readValue(httpResponse.getEntity().getContent(), ErrorResponse.class);
				log.error("DCP API request is failed: " + errorResponse.toString());
				Notify.sendRequestFailedErrorNotification(errorResponse);	
			} catch (JsonParseException | JsonMappingException e) {
				log.error("Error in Json Parsing ", e);
			}catch (UnsupportedOperationException | IOException e) {
				log.error("Error : ", e);
			} 
		}else {	
			try {
				var responseString = EntityUtils.toString(httpResponse.getEntity());
				log.error("DCP API request is failed: " +responseString);
				Notify.sendCommonErrorNotification(responseString);
			} catch (ParseException | IOException e) {
				log.error("Error", e);
			}

		}
	}


	private static String getAuthHeader(final String username, final String password) {
		String auth = username + ":" + password;
		var encodedAuth = Base64.getEncoder().encodeToString(auth.getBytes(StandardCharsets.UTF_8));
		return "Basic " + encodedAuth;
	}


	private static URI buildURI(final String serviceEndPoint) {
		URI uri = null;
		try {
			var uriBuilder = new URIBuilder(serviceEndPoint);
			uri = uriBuilder.build();
			log.info("URI: " + uri);
		} catch (URISyntaxException e) {
			log.error("Erro while constructing URI " + e);
		}

		return uri;
	}

}