package com.opentext.dmg.assetitems.pushapi.client;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.opentext.dmg.assetitems.pushapi.model.ErrorResponse;
import com.opentext.dmg.email.util.EmailBody;
import com.opentext.dmg.email.util.EmailUtil;

public class Notify {

	private static final Log log = LogFactory.getLog(Notify.class);

	protected static void sendRequestFailedErrorNotification(ErrorResponse errorResponse) {
		var emailBody = new EmailBody();
		String emailMessage = emailBody.getEmailBody(errorResponse);
		sendNotification(emailMessage);
	}

	protected static void sendCommonErrorNotification(String responseString) {
		var emailBody = new EmailBody();
		String emailMessage = emailBody.getEmailBody(responseString);
		sendNotification(emailMessage);
	}

	private static void sendNotification(String htmlText) {
		var emailUtil = new EmailUtil();
		if (StringUtils.isNotEmpty(htmlText)) {
			emailUtil.sendEmail(htmlText);
		} else {
			log.error("sendRequestFailedErrorNotification >> HTML file/text not found");
		}
	}

	private Notify() {
		log.info("Notification inititaliazed..");
	}
}
