package com.opentext.dmg.assetitems.pushapi.config;

import com.artesia.common.exception.BaseTeamsException;
import com.opentext.dmg.otmm.util.AdminUtils;

public class ApiConfig implements ApiSettings {

	private String apiBaseUrl;
	private String userName;
	private String password;
	private String uploadAssetsApi;
	private String deleteAssetsApi;
	private String amdBaseUrl;

	@Override
	public String getApiBaseUrl() {
		return apiBaseUrl;
	}

	private void setApiBaseUrl(String apiBaseUrl) {
		this.apiBaseUrl = apiBaseUrl;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	private void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String getPassword() {
		return password;
	}

	private void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getUploadAssetsApi() {
		return uploadAssetsApi;
	}

	private void setUploadAssetsApi(String uploadAssetsApi) {
		this.uploadAssetsApi = getApiBaseUrl() + uploadAssetsApi;
	}

	@Override
	public String getDeleteAssetsApi() {
		return deleteAssetsApi;
	}

	private void setDeleteAssetsApi(String deleteAssetsApi) {
		this.deleteAssetsApi = getApiBaseUrl() + deleteAssetsApi;
	}	

	@Override
	public String getAmdBaseUrl() {
		return amdBaseUrl;
	}

	private void setAmdBaseUrl(String amdBaseUrl) {
		this.amdBaseUrl = amdBaseUrl;
	}

	public ApiConfig() throws BaseTeamsException  {
		setApiBaseUrl(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP, CONFIG, DCP_BASE_URL));
		setUserName(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP, CONFIG, DCP_USERNAME));
		setPassword(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP, CONFIG, DCP_PASSWORD));
		setUploadAssetsApi(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP, CONFIG, UPLOAD_ASSETS_PATH));
		setDeleteAssetsApi(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP, CONFIG, DELETE_ASSETS_PATH));
		setAmdBaseUrl(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP, CONFIG, AMD_BASE_URL));
		
	}


}
