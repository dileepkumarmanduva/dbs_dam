package com.opentext.dmg.assetitems.pushapi.config;

public interface ApiSettings {

	String getApiBaseUrl();

	String getUserName();

	String getPassword();

	String getUploadAssetsApi();

	String getDeleteAssetsApi();

	String getAmdBaseUrl();
	

	
	public static String DCP_BASE_URL = "DCP_BASE_URL";
	public static String DCP_USERNAME = "DCP_USERNAME";
	public static String DCP_PASSWORD = "DCP_PASSWORD";
	public static String DELETE_ASSETS_PATH = "DELETE_ASSETS_PATH";
	public static String UPLOAD_ASSETS_PATH = "UPLOAD_ASSETS_PATH";
	public static final String AMD_BASE_URL = "AMD_BASE_URL";
	
	//component
	public static final String PUBLISH_TO_DCP = "PUBLISH_TO_DCP";
	public static final String CONFIG = "CONFIG";
	
}