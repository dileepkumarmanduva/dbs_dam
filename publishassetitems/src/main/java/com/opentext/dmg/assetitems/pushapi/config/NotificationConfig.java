package com.opentext.dmg.assetitems.pushapi.config;

import com.artesia.common.exception.BaseTeamsException;
import com.opentext.dmg.otmm.util.AdminUtils;

public class NotificationConfig implements NotificationSettings {

	@Override
	public String getSender() {
		return sender;
	}

	@Override
	public String getRecipients() {
		return recipients;
	}

	@Override
	public String getCcRecepients() {
		return ccRecepients;
	}

	@Override
	public String getSubject() {
		return subject;
	}

	@Override
	public String getEmailTemplateFilePath() {
		return emailTemplateFilePath;
	}

	private void setSender(String sender) {
		this.sender = sender;
	}

	private void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	private void setCcRecepients(String ccRecepients) {
		this.ccRecepients = ccRecepients;
	}

	private void setSubject(String subject) {
		this.subject = subject;
	}

	private void setEmailTemplateFilePath(String emailTemplateFilePath) {
		this.emailTemplateFilePath = emailTemplateFilePath;
	}

	public NotificationConfig() throws BaseTeamsException {
		setSender(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP,CONFIG,SENDER_EMAIL_ID));
		setRecipients(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP,CONFIG,CC_RECIPIENT_EMAIL_ID));
		setCcRecepients(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP,CONFIG,CC_RECIPIENT_EMAIL_ID));
		setSubject(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP,CONFIG,EMAIL_SUBJECT));
		setEmailTemplateFilePath(AdminUtils.getOTMMSettingValue(PUBLISH_TO_DCP,CONFIG,EMAIL_TEMPLATE_FILE_PATH));
	}

	private String sender;
	private String recipients;
	private String ccRecepients;
	private String subject;
	private String emailTemplateFilePath;

}
