package com.opentext.dmg.assetitems.pushapi.config;

public interface NotificationSettings {

	String getSender();

	String getRecipients();

	String getCcRecepients();

	String getSubject();

	String getEmailTemplateFilePath();

	public static String SENDER_EMAIL_ID = "SENDER_EMAIL_ID";
	public static String RECIPIENT_EMAIL_ID = "RECIPIENT_EMAIL_ID";
	public static String CC_RECIPIENT_EMAIL_ID = "CC_RECIPIENT_EMAIL_ID";
	public static String EMAIL_SUBJECT = "EMAIL_SUBJECT";
	public static String EMAIL_TEMPLATE_FILE_PATH = "EMAIL_TEMPLATE_FILE_PATH";
	
	//component
	public static final String PUBLISH_TO_DCP = "PUBLISH_TO_DCP";
	public static final String CONFIG = "CONFIG";

}