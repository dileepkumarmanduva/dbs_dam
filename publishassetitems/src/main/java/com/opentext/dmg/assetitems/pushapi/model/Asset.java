
package com.opentext.dmg.assetitems.pushapi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.opentext.dmg.assetitems.util.DcpPayloadConstants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	DcpPayloadConstants.ASSET_ID
})
public class Asset {

@JsonProperty(DcpPayloadConstants.ASSET_ID)
private String assetID;

@JsonProperty(DcpPayloadConstants.ASSET_ID)
public String getAssetID() {
return assetID;
}

@JsonProperty(DcpPayloadConstants.ASSET_ID)
public void setAssetID(String assetID) {
this.assetID = assetID;
}

@Override
public String toString() {
StringBuilder sb = new StringBuilder();
sb.append(Asset.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
sb.append("assetID");
sb.append('=');
sb.append(((this.assetID == null)?"<null>":this.assetID));
sb.append(',');
if (sb.charAt((sb.length()- 1)) == ',') {
sb.setCharAt((sb.length()- 1), ']');
} else {
sb.append(']');
}
return sb.toString();
}

}