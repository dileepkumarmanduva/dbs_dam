
package com.opentext.dmg.assetitems.pushapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.opentext.dmg.assetitems.util.DcpPayloadConstants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	DcpPayloadConstants.ASSET_CONTENT,
	DcpPayloadConstants.ASSET_ID,
	DcpPayloadConstants.ASSET_TYPE,
	DcpPayloadConstants.EXPIRY_DATE,
	DcpPayloadConstants.ASSET_URL,
	DcpPayloadConstants.OFFER_CODE,
	DcpPayloadConstants.ON_CLICK_URL,
	DcpPayloadConstants.CATEGORIZATION
})

public class AssetItem {

	@JsonProperty(DcpPayloadConstants.ASSET_CONTENT)
	private String assetContent;
	@JsonProperty(DcpPayloadConstants.ASSET_ID)
	private String assetID;
	@JsonProperty(DcpPayloadConstants.ASSET_TYPE)
	private Integer assetType;
	@JsonProperty(DcpPayloadConstants.EXPIRY_DATE)
	private String expiryDate;
	@JsonProperty(DcpPayloadConstants.ASSET_URL)
	private String assetURL;
	@JsonProperty(DcpPayloadConstants.OFFER_CODE)
	private String offerCode;
	@JsonProperty(DcpPayloadConstants.ON_CLICK_URL)
	private String onClickURL;
	@JsonProperty(DcpPayloadConstants.CATEGORIZATION)
	private List<Categorization> categorization = null;

	@JsonProperty(DcpPayloadConstants.ASSET_CONTENT)
	public String getAssetContent() {
		return assetContent;
	}

	@JsonProperty(DcpPayloadConstants.ASSET_CONTENT)
	public void setAssetContent(String assetContent) {
		this.assetContent = assetContent;
	}

	@JsonProperty(DcpPayloadConstants.ASSET_ID)
	public String getAssetID() {
		return assetID;
	}

	@JsonProperty(DcpPayloadConstants.ASSET_ID)
	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}

	@JsonProperty(DcpPayloadConstants.ASSET_TYPE)
	public Integer getAssetType() {
		return assetType;
	}

	@JsonProperty(DcpPayloadConstants.ASSET_TYPE)
	public void setAssetType(Integer assetType) {
		this.assetType = assetType;
	}

	@JsonProperty(DcpPayloadConstants.EXPIRY_DATE)
	public String getExpiryDate() {
		return expiryDate;
	}

	@JsonProperty(DcpPayloadConstants.EXPIRY_DATE)
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@JsonProperty(DcpPayloadConstants.ASSET_URL)
	public String getAssetURL() {
		return assetURL;
	}

	@JsonProperty(DcpPayloadConstants.ASSET_URL)
	public void setAssetURL(String assetURL) {
		this.assetURL = assetURL;
	}

	@JsonProperty(DcpPayloadConstants.OFFER_CODE)
	public String getOfferCode() {
		return offerCode;
	}

	@JsonProperty(DcpPayloadConstants.OFFER_CODE)
	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}

	@JsonProperty(DcpPayloadConstants.ON_CLICK_URL)
	public String getOnClickURL() {
		return onClickURL;
	}

	@JsonProperty(DcpPayloadConstants.ON_CLICK_URL)
	public void setOnClickURL(String onClickURL) {
		this.onClickURL = onClickURL;
	}

	@JsonProperty(DcpPayloadConstants.CATEGORIZATION)
	public List<Categorization> getCategorization() {
		return categorization;
	}

	@JsonProperty(DcpPayloadConstants.CATEGORIZATION)
	public void setCategorization(List<Categorization> categorization) {
		this.categorization = categorization;
	}

}
