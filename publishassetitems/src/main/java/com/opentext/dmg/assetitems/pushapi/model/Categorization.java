
package com.opentext.dmg.assetitems.pushapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.opentext.dmg.assetitems.util.DcpPayloadConstants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    DcpPayloadConstants.CATEGORIZATION_NAME,
    DcpPayloadConstants.CATEGORIZATION_VALUES
})

public class Categorization {

    @JsonProperty(DcpPayloadConstants.CATEGORIZATION_NAME)
    private String categorizationName;
    @JsonProperty(DcpPayloadConstants.CATEGORIZATION_VALUES)
    private List<String> categorizationValues = null;

    @JsonProperty( DcpPayloadConstants.CATEGORIZATION_NAME)
    public String getCategorizationName() {
        return categorizationName;
    }

    @JsonProperty( DcpPayloadConstants.CATEGORIZATION_NAME)
    public void setCategorizationName(String categorizationName) {
        this.categorizationName = categorizationName;
    }

    @JsonProperty(DcpPayloadConstants.CATEGORIZATION_VALUES)
    public List<String> getCategorizationValues() {
        return categorizationValues;
    }

    @JsonProperty(DcpPayloadConstants.CATEGORIZATION_VALUES)
    public void setCategorizationValues(List<String> categorizationValues) {
        this.categorizationValues = categorizationValues;
    }

}
