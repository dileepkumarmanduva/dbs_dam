package com.opentext.dmg.assetitems.pushapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.opentext.dmg.assetitems.util.DcpPayloadConstants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
DcpPayloadConstants.ASSET_LIST
})
public class DeleteAssets {

@JsonProperty(DcpPayloadConstants.ASSET_LIST)
private List<Asset> assetList = null;

@JsonProperty(DcpPayloadConstants.ASSET_LIST)
public List<Asset> getAssetList() {
return assetList;
}

@JsonProperty(DcpPayloadConstants.ASSET_LIST)
public void setAssetList(List<Asset> assetList) {
this.assetList = assetList;
}

@Override
public String toString() {
StringBuilder sb = new StringBuilder();
sb.append(DeleteAssets.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
sb.append("assetList");
sb.append('=');
sb.append(((this.assetList == null)?"<null>":this.assetList));
sb.append(',');
if (sb.charAt((sb.length()- 1)) == ',') {
sb.setCharAt((sb.length()- 1), ']');
} else {
sb.append(']');
}
return sb.toString();
}

}