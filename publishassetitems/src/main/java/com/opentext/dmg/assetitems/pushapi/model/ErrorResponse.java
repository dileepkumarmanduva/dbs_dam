
package com.opentext.dmg.assetitems.pushapi.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.opentext.dmg.assetitems.util.DcpPayloadConstants;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse {

	@JsonProperty(DcpPayloadConstants.JOB_ID)
	private String jobID = null;

	@JsonProperty(DcpPayloadConstants.FAILED_ASSETS)
	private List<ErrorResponseFailedAsset> failedAssets = null;

	public ErrorResponse jobID(String jobID) {
		this.jobID = jobID;
		return this;
	}

	public String getJobID() {
		return jobID;
	}

	public void setJobID(String jobID) {
		this.jobID = jobID;
	}

	public ErrorResponse failedAssets(List<ErrorResponseFailedAsset> failedAssets) {
		this.failedAssets = failedAssets;
		return this;
	}

	public ErrorResponse addFailedAssetsItem(ErrorResponseFailedAsset failedAssetsItem) {
		if (this.failedAssets == null) {
			this.failedAssets = new ArrayList<>();
		}
		this.failedAssets.add(failedAssetsItem);
		return this;
	}

	public List<ErrorResponseFailedAsset> getFailedAssets() {
		return failedAssets;
	}

	public void setFailedAssets(List<ErrorResponseFailedAsset> failedAssets) {
		this.failedAssets = failedAssets;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ErrorResponse errorResponse = (ErrorResponse) o;
		return Objects.equals(this.jobID, errorResponse.jobID)
				&& Objects.equals(this.failedAssets, errorResponse.failedAssets);
	}

	@Override
	public int hashCode() {
		return Objects.hash(jobID, failedAssets);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ErrorResponse {\n");

		sb.append("    jobID: ").append(toIndentedString(jobID)).append("\n");
		sb.append("    failedAssets: ").append(toIndentedString(failedAssets)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
