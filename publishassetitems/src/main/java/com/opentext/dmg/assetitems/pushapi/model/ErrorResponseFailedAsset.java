
package com.opentext.dmg.assetitems.pushapi.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opentext.dmg.assetitems.util.DcpPayloadConstants;

public class ErrorResponseFailedAsset {


	@JsonProperty(DcpPayloadConstants.FAILED_ASSET_ID)
	private String assetID = null;

	@JsonProperty(DcpPayloadConstants.ERROR_CODE)
	private Integer errorCode = null;

	@JsonProperty(DcpPayloadConstants.ERROR_MESSAGE)
	private String errorMessage = null;

	public ErrorResponseFailedAsset assetID(String assetID) {
		this.assetID = assetID;
		return this;
	}

	/**
	 * Unique assetID
	 * @return assetID
	 **/
	public String getAssetID() {
		return assetID;
	}

	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}

	public ErrorResponseFailedAsset errorCode(Integer errorCode) {
		this.errorCode = errorCode;
		return this;
	}

	/**
	 * Error code
	 * @return errorCode
	 **/
	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public ErrorResponseFailedAsset errorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		return this;
	}

	/**
	 * Error message
	 * @return errorMessage
	 **/
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		var errorResponseFailedAsset = (ErrorResponseFailedAsset) o;
		return Objects.equals(this.assetID, errorResponseFailedAsset.assetID) &&
				Objects.equals(this.errorCode, errorResponseFailedAsset.errorCode) &&
				Objects.equals(this.errorMessage, errorResponseFailedAsset.errorMessage);
	}

	@Override
	public int hashCode() {
		return Objects.hash(assetID, errorCode, errorMessage);
	}


	@Override
	public String toString() {
		var sb = new StringBuilder();
		sb.append("class ErrorResponseFailedAsset {\n");
		sb.append("    assetID: ").append(toIndentedString(assetID)).append("\n");
		sb.append("    errorCode: ").append(toIndentedString(errorCode)).append("\n");
		sb.append("    errorMessage: ").append(toIndentedString(errorMessage)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
