
package com.opentext.dmg.assetitems.pushapi.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.opentext.dmg.assetitems.util.DcpPayloadConstants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
  DcpPayloadConstants.ASSET_LIST
})

public class UploadAssets {

    @JsonProperty(DcpPayloadConstants.ASSET_LIST)
    private List<AssetItem> assetList = null;

    @JsonProperty(DcpPayloadConstants.ASSET_LIST)
    public List<AssetItem> getAssetList() {
        return assetList;
    }

    @JsonProperty(DcpPayloadConstants.ASSET_LIST)
    public void setAssetList(List<AssetItem> assetList) {
        this.assetList = assetList;
    }

}
