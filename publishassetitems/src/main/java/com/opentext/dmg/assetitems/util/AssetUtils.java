package com.opentext.dmg.assetitems.util;

import java.io.InputStream;
import java.util.List;

import com.artesia.asset.Asset;
import com.artesia.asset.AssetIdentifier;
import com.artesia.asset.content.services.AssetContentLoadRequest;
import com.artesia.asset.content.services.AssetContentServices;
import com.artesia.asset.services.AssetDataLoadRequest;
import com.artesia.asset.services.AssetServices;
import com.artesia.asset.services.RetrieveAssetsCriteria;
import com.artesia.common.MetadataFieldConstants;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.content.ContentData.ContentDataSource;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataTableField;
import com.artesia.security.SecuritySession;

public class AssetUtils {
	
	public static InputStream getAssetContentInputStream(AssetIdentifier assetID, SecuritySession session)
			throws BaseTeamsException {
		var contentLoadRequest = new AssetContentLoadRequest();
		contentLoadRequest.setLoadMasterContent(true);
		contentLoadRequest.setRequestedFormat(ContentDataSource.INPUTSTREAM);
		var assetContentInfo = AssetContentServices.getInstance().retrieveAssetContent(assetID,
				contentLoadRequest, null, session);
		var masterContentInfo = assetContentInfo.getMasterContent();
		var contentData = masterContentInfo.getContentData();
		return contentData.getInputStream();
	}

	public static Asset[]  retrieveAssets(List<AssetIdentifier> assetIdList, SecuritySession session) throws BaseTeamsException {
		AssetIdentifier[] assetIds = assetIdList.toArray(new AssetIdentifier[0]);

		// configure which metadata fields we want: Asset Name and
		// Content Type
		TeamsIdentifier[] fieldIds = new TeamsIdentifier[]
				{ MetadataFieldConstants.METADATA_FIELD_ID__ASSET_NAME,
				MetadataFieldConstants.METADATA_FIELD_ID__CONTENT_TYPE,
				MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE,
				MetadataFieldConstants.METADATA_FIELD_ID_TAG,
				CustomMetadataFieldConstants.CAMPAIGN_PRODUCT_SUBPRODUCT,
				CustomMetadataFieldConstants.CAMPAIGN_TYPE,
				CustomMetadataFieldConstants.OFFER_CODE,
				CustomMetadataFieldConstants.FORMAT_COMPONENT,
				CustomMetadataFieldConstants.DEMOGRAPHICS,
				CustomMetadataFieldConstants.PURPOSE,
				CustomMetadataFieldConstants.TOPIC,
				CustomMetadataFieldConstants.STYLE,
				CustomMetadataFieldConstants.CONTENTS,
				CustomMetadataFieldConstants.LOCATION
				};

		// retrieve Asset objects for all hits, requesting specified fields
		// be populated
		RetrieveAssetsCriteria criteria = new RetrieveAssetsCriteria(assetIds);

		AssetDataLoadRequest dataLoadRequest = new AssetDataLoadRequest();
		dataLoadRequest.setLoadAssetContentInfo(true);
		dataLoadRequest.setMetadataFieldsToRetrieve(fieldIds);
		dataLoadRequest.setLoadMetadata(true);
				
		return AssetServices.getInstance().retrieveAssets(criteria, dataLoadRequest, session);
	}

	public static String getTabularDataAsCsvString(MetadataTableField field) {
		if (field == null)
			return "";
		int rowCount = field.getRowCount();
		StringBuilder data = new StringBuilder();
		String rowValue = "";
		for (int loop = 0; loop < rowCount; loop++) {
			rowValue = field.getValueAt(loop).getStringValue();
			if (rowValue != null && !rowValue.equals("")) {
				data.append(rowValue).append(",");
			}
		}
		String returnString = data.toString();
		if (!returnString.equals("") && returnString.endsWith(",")) {
			returnString = returnString.substring(0, returnString.length() - 1);
		}
		return returnString;
	}
	
	private AssetUtils() {
		
	}
}
