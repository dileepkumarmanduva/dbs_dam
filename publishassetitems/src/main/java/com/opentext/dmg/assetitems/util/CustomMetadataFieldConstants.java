package com.opentext.dmg.assetitems.util;

import com.artesia.entity.TeamsIdentifier;

public class CustomMetadataFieldConstants {
	
	//Campaign Info
	public static final TeamsIdentifier OFFER_CODE = new TeamsIdentifier("DBS.FIELD.CAMPAIGN OFFER CODE");
	public static final TeamsIdentifier CAMPAIGN_PRODUCT_SUBPRODUCT = new TeamsIdentifier("DBS.FIELD.CAMPAIGN PROD SUBPROD");
	public static final TeamsIdentifier CAMPAIGN_TYPE = new TeamsIdentifier("DBS.FIELD.CAMPAIGN TYPE");
	
	//Channel Info
	public static final TeamsIdentifier FORMAT_COMPONENT = new TeamsIdentifier("DBS.FIELD.COMMUNICATION COMPONENT");
	
	//Content Info
	public static final TeamsIdentifier DEMOGRAPHICS = new TeamsIdentifier("DBS.FIELD.DEMOGRAPHICS");
	public static final TeamsIdentifier PURPOSE = new TeamsIdentifier("DBS.FIELD.CONTENT PURPOSE");
	public static final TeamsIdentifier TOPIC = new TeamsIdentifier("DBS.FIELD.CONTENT TOPIC");
	public static final TeamsIdentifier STYLE = new TeamsIdentifier("DBS.FIELD.IMAGE STYLE");
	public static final TeamsIdentifier CONTENTS = new TeamsIdentifier("DBS.FIELD.IMAGE CONTENT");
	public static final TeamsIdentifier LOCATION = new TeamsIdentifier("DBS.FIELD.IMAGE LOCATION");

	 
	 
	 private CustomMetadataFieldConstants() {
		 
	 }
}
