package com.opentext.dmg.assetitems.util;

public class DcpPayloadConstants {

	public static final String ASSET_LIST = "AssetList";
	
	//Asset Item Fields
	
	public static final String ASSET_CONTENT = "AssetContent";
	public static final String ASSET_ID = "AssetID";
	public static final String ASSET_TYPE = "AssetType";
	
	public static final String EXPIRY_DATE = "ExpiryDate";
	public static final String OFFER_CODE = "OfferCode";

	public static final String ASSET_URL = "AssetURL";
	public static final String ON_CLICK_URL = "OnClickURL";
	
	public static final String CATEGORIZATION = "Categorization";
	public static final String CATEGORIZATION_NAME ="CategorizationName";
	public static final String CATEGORIZATION_VALUES = "CategorizationValues";
	
	// Campaign Info
	public static final String CAMPAIGN_PRODUCT = "CAMPAIGN-PRODUCT_CAMPAIGN-PRODUCT";
	public static final String SUBPRODUCT = "SUBPRODUCT_SUBPRODUCT";
	public static final String CAMPAIGN_TYPE = "CAMPAIGN-TYPE_CAMPAIGN-TYPE";

	// Channel Info
	public static final String FORMAT = "Format_Format";
	public static final String COMPONENTS = "Components_Components";

	// Content Info
	public static final String DEMOGRAPHICS = "Demographics_Demographics";
	public static final String PURPOSE = "Purpose_Purpose";
	public static final String TOPIC = "Topic_Topic";
	public static final String STYLE = "Style_Style";
	public static final String CONTENTS = "Contents_Contents";
	public static final String LOCATION = "Location_Location";	
	//ErrorResponse
	public static final String ERROR_CODE = "errorCode";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String JOB_ID = "jobID";
	public static final String FAILED_ASSETS = "failedAssets";
	public static final String FAILED_ASSET_ID = "assetID";

	private DcpPayloadConstants() {

	}
}
