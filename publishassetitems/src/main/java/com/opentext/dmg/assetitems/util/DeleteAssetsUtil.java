package com.opentext.dmg.assetitems.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.security.SecuritySession;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opentext.dmg.assetitems.pushapi.client.ApiClient;
import com.opentext.dmg.assetitems.pushapi.config.ApiConfig;
import com.opentext.dmg.assetitems.pushapi.config.ApiSettings;
import com.opentext.dmg.assetitems.pushapi.model.Asset;
import com.opentext.dmg.assetitems.pushapi.model.DeleteAssets;

public class DeleteAssetsUtil {
	private static final Log log = LogFactory.getLog(DeleteAssetsUtil.class);

	public static boolean deleteAssetItems(List<AssetIdentifier> assetIds, SecuritySession session) throws BaseTeamsException {
		ApiSettings apiConfig = new ApiConfig();
		String serviceEndPoint = apiConfig.getDeleteAssetsApi();
		String userName = apiConfig.getUserName();
		String password = apiConfig.getPassword();
		String requestJson = getDeleteAssetsJsonToPublish(assetIds, session);
		log.debug("executing deleteAssetItems >> DCP Payload - Delete Assets Request JSON:\n"+ requestJson );
		return ApiClient.executePost(serviceEndPoint, userName, password, requestJson);
	}

	private static String getDeleteAssetsJsonToPublish(List<AssetIdentifier> assetIds, SecuritySession session)
			throws BaseTeamsException {

		String jsonStr = "{}";
		var deleteAssets = new DeleteAssets();
		List<Asset> assetList = buildAssetList(assetIds, session);
		deleteAssets.setAssetList(assetList);
		try {	
			// Creating Object of ObjectMapper define in Jakson Api
			var objectMapper = new ObjectMapper();
			jsonStr = objectMapper.writeValueAsString(deleteAssets);
		} catch (IOException e) {
			log.error("Error in building Json payload", e);
		}

		return jsonStr;
	}

	private static List<Asset> buildAssetList(List<AssetIdentifier> assetIds, SecuritySession session) throws BaseTeamsException  {
		log.debug("start of getAssetList");
		List<Asset> assetList = new ArrayList<>();
		com.artesia.asset.Asset[] assets = AssetUtils.retrieveAssets(assetIds, session);
		for (com.artesia.asset.Asset asset : assets) {
			// DCP Requested Asset Name(as this will be unique Asset ID to DCP based on naming conventions as agreed by parties) 
			String assetID = 	asset.getName();
			log.debug("asset" + assetID);
			Asset dcpAsset = new Asset();	
			dcpAsset.setAssetID(assetID);
			assetList.add(dcpAsset);
		}
		log.info("end of getAssetList");
		return assetList;
	}

	private DeleteAssetsUtil() {

	}
}
