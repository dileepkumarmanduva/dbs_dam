package com.opentext.dmg.assetitems.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.Asset;
import com.artesia.asset.AssetIdentifier;
import com.artesia.common.ContentTypeConstants;
import com.artesia.common.MetadataFieldConstants;
import com.artesia.common.exception.BaseTeamsException;
import com.artesia.entity.TeamsIdentifier;
import com.artesia.metadata.MetadataCollection;
import com.artesia.security.SecuritySession;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opentext.dmg.assetitems.pushapi.client.ApiClient;
import com.opentext.dmg.assetitems.pushapi.config.ApiConfig;
import com.opentext.dmg.assetitems.pushapi.config.ApiSettings;
import com.opentext.dmg.assetitems.pushapi.model.AssetItem;
import com.opentext.dmg.assetitems.pushapi.model.Categorization;
import com.opentext.dmg.assetitems.pushapi.model.UploadAssets;

public class UploadAssetsUtil {

	private static final Log log = LogFactory.getLog(UploadAssetsUtil.class);

	public static boolean uploadAssetItems(List<AssetIdentifier> assetIdList, SecuritySession session)
			throws BaseTeamsException {
		ApiSettings apiConfig = new ApiConfig();
		String serviceEndPoint = apiConfig.getUploadAssetsApi();
		String userName = apiConfig.getUserName();
		String password = apiConfig.getPassword();
		String requestJsonString = UploadAssetsUtil.getUploadAssetsJsonToPublish(assetIdList, session);
		log.debug("executing uploadAssetItems >> DCP Payload - Upload Assets Request JSON:\n" + requestJsonString);
		return ApiClient.executePost(serviceEndPoint, userName, password, requestJsonString);
	}

	private static String getUploadAssetsJsonToPublish(List<AssetIdentifier> assetIds, SecuritySession session)
			throws BaseTeamsException {

		var jsonStr = "{}";
		var uploadAssets = new UploadAssets();
		List<AssetItem> assetItems = buildAssetItems(assetIds, session);
		uploadAssets.setAssetList(assetItems);
		// Creating Object of ObjectMapper define in Jakson Api
		var obj = new ObjectMapper();
		try {
			// get Oraganisation object as a json string
			jsonStr = obj.writeValueAsString(uploadAssets);
		} catch (IOException e) {
			log.error("Error in building Json payload", e);
		}

		return jsonStr;
	}

	private static List<AssetItem> buildAssetItems(List<AssetIdentifier> assetIds, SecuritySession session)
			throws BaseTeamsException {
		log.debug("start of buildAssetItems");
		List<AssetItem> assetItems = new ArrayList<>();
		Asset[] assets = AssetUtils.retrieveAssets(assetIds, session);
		for (Asset asset : assets) {
			log.info("asset" + asset.getAssetId().getId());
			AssetItem assetItem = new AssetItem();
			// DCP Requested Asset Name(as this will be unique Asset ID to DCP based on
			// naming conventions as agreed by parties)
			assetItem.setAssetID(asset.getName());
			assetItem.setAssetContent(null); // update based on need
			log.debug("in buildAssetItems() : ContentType" + asset.getContentType());
			assetItem.setAssetType(0);

			MetadataCollection assetMetadata = asset.getMetadata();

			if (assetMetadata == null) {
				log.error("assetMetadata is null" + assetMetadata);
				return assetItems;
			}
			// extract the values of the fields we requested
			var assetNameValue = assetMetadata.getValueForField(MetadataFieldConstants.METADATA_FIELD_ID__ASSET_NAME).getStringValue();
			var expirationDateValue = assetMetadata.getValueForField(MetadataFieldConstants.METADATA_FIELD_ID__EXPIRATION_DATE).getStringValue();
			assetItem.setExpiryDate(expirationDateValue);
			final String amdrenditionurl = new ApiConfig().getAmdBaseUrl() + "rendition?id="
					+ asset.getAssetId().getId();
			assetItem.setAssetURL(amdrenditionurl);

			if (asset.getContentType().equals(ContentTypeConstants.HTML_CONTENT_TYPE)) {
				assetItem.setAssetType(1);
				log.debug("   Asset name is " + assetNameValue);
				var inputStream = AssetUtils.getAssetContentInputStream(asset.getAssetId(), session);
				if (inputStream != null) {
					final var htmlDataString = convertInputStreamtoString(inputStream);
					// convert to Base64EncodedString
					var base64EncodedString = getBase64EncodedString(htmlDataString);
					assetItem.setAssetContent(base64EncodedString);

				} else {
					log.error(
							asset.getAssetId().getId() + " has no content input stream -> inputStream: " + inputStream);
				}

			}

			var offerCodeValue = assetMetadata.getValueForField(CustomMetadataFieldConstants.OFFER_CODE).getStringValue();
			assetItem.setOfferCode(offerCodeValue);

			// logic of categorization values
			List<Categorization> categorizations = new ArrayList<>();
			buildCategorizations(assetMetadata, categorizations);
			assetItem.setCategorization(categorizations);
			assetItems.add(assetItem);

		}
		log.info("end of buildAssetItems");
		return assetItems;
	}

	private static void buildCategorizations(MetadataCollection assetMetadata, List<Categorization> categorizations) {

		var campaignProdAndSubProdValue = assetMetadata.getValueForField(CustomMetadataFieldConstants.CAMPAIGN_PRODUCT_SUBPRODUCT).getStringValue();
		if (campaignProdAndSubProdValue != null) {
			String[] parts = campaignProdAndSubProdValue.split("\\^", 2);
			if (parts.length == 2) {
				var campaignProduct = buildCategorization(DcpPayloadConstants.CAMPAIGN_PRODUCT, parts[0].toLowerCase());
				categorizations.add(campaignProduct);
				var campaignSubProduct = buildCategorization(DcpPayloadConstants.SUBPRODUCT, parts[1].toLowerCase());
				categorizations.add(campaignSubProduct);
			}
		}

		addCategorization(assetMetadata, categorizations, DcpPayloadConstants.CAMPAIGN_TYPE, CustomMetadataFieldConstants.CAMPAIGN_TYPE);

		var formatAndComponentsValue = assetMetadata.getValueForField(CustomMetadataFieldConstants.FORMAT_COMPONENT).getStringValue();
		if (formatAndComponentsValue != null) {
			String[] parts = formatAndComponentsValue.split("\\^", 2);
			if (parts.length == 2) {
				var format = buildCategorization(DcpPayloadConstants.FORMAT, parts[0].toLowerCase());
				categorizations.add(format);
				var components = buildCategorization(DcpPayloadConstants.COMPONENTS, parts[1].toLowerCase());
				categorizations.add(components);
			}
		}
		
		addCategorization(assetMetadata, categorizations, DcpPayloadConstants.DEMOGRAPHICS, CustomMetadataFieldConstants.DEMOGRAPHICS);
		addCategorization(assetMetadata, categorizations, DcpPayloadConstants.PURPOSE, CustomMetadataFieldConstants.PURPOSE);
		addCategorization(assetMetadata, categorizations, DcpPayloadConstants.TOPIC, CustomMetadataFieldConstants.TOPIC);
		addCategorization(assetMetadata, categorizations, DcpPayloadConstants.STYLE, CustomMetadataFieldConstants.STYLE);
		addCategorization(assetMetadata, categorizations, DcpPayloadConstants.CONTENTS, CustomMetadataFieldConstants.CONTENTS);
		addCategorization(assetMetadata, categorizations, DcpPayloadConstants.LOCATION, CustomMetadataFieldConstants.LOCATION);

	}

	private static void addCategorization(MetadataCollection assetMetadata, List<Categorization> categorizations,
			String categorizationName, TeamsIdentifier fieldId) {
		var categorizationValue = assetMetadata.getValueForField(fieldId).getStringValue();
		var categorization = buildCategorization(categorizationName,
				categorizationValue != null ? categorizationValue.toLowerCase() : "");
		categorizations.add(categorization);
	}

	private static Categorization buildCategorization(String categorizationName, String categorizationsValue) {
		var categorization = new Categorization();
		categorization.setCategorizationName(categorizationName);
		List<String> categorizationValues = new ArrayList<>();
		categorizationValues.add(categorizationsValue);
		categorization.setCategorizationValues(categorizationValues);
		return categorization;
	}

	private static String getBase64EncodedString(final String htmlDataString) {
		final byte[] contentBytes = htmlDataString.getBytes(StandardCharsets.UTF_8);
		return Base64.getEncoder().encodeToString(contentBytes);
	}

	private static String convertInputStreamtoString(InputStream inputStream) {
		var textBuilder = new StringBuilder();
		try (Reader reader = new BufferedReader(
				new InputStreamReader(inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
			int c = 0;
			while ((c = reader.read()) != -1) {
				textBuilder.append((char) c);
			}
		} catch (IOException e) {
			log.error("error converting inputstream ", e);
		}
		final var htmlDataString = textBuilder.toString();
		log.debug("inputStream to String: " + htmlDataString);
		return htmlDataString;
	}

	private UploadAssetsUtil() {
	}
}
