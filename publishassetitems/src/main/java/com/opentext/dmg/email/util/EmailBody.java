package com.opentext.dmg.email.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.common.exception.BaseTeamsException;
import com.opentext.dmg.assetitems.pushapi.config.NotificationConfig;
import com.opentext.dmg.assetitems.pushapi.config.NotificationSettings;
import com.opentext.dmg.assetitems.pushapi.model.ErrorResponse;
import com.opentext.dmg.assetitems.pushapi.model.ErrorResponseFailedAsset;

public class EmailBody {
	private static final Log LOGGER = LogFactory.getLog(EmailUtil.class);

	public String getEmailBody(ErrorResponse errorResponse) {
		var htmlText = "";
		var jobId = errorResponse.getJobID();
		List<ErrorResponseFailedAsset> failedAssets = errorResponse.getFailedAssets();
		try {
			htmlText = getBodyTemplate();
			htmlText = htmlText.replace(JOB_ID_TOKEN, jobId == null ? "" : jobId);
			if (failedAssets != null && !failedAssets.isEmpty()) {
				var htmlData = new StringBuilder();
				htmlData = getTableData(htmlData, failedAssets);
				htmlText = htmlText.replace(FAILED_ASSETS_TOKEN, htmlData.toString());
				htmlText = htmlText.replace(SHOW_ERROR_MSG_TOKEN, DISPLAY_NONE_MSO_HIDE_ALL);
			} else {
				htmlText = htmlText.replace(FAILED_ASSETS_TOKEN, "").replace(SHOW_FAILED_ASSETS_TOKEN,
						DISPLAY_NONE_MSO_HIDE_ALL);
			}
			LOGGER.debug("Email Body: " + htmlText);
		} catch (Exception e) {
			LOGGER.error("There is an error preparing email data ", e);
		}
		return htmlText;
	}

	public String getEmailBody(String responseString) {
		var htmlText = "";
		try {
			htmlText = getBodyTemplate();
			if (responseString != null) {
				htmlText = htmlText.replace(ERROR_MSG_TOKEN, responseString);
				htmlText = htmlText.replace(JOB_ID_TOKEN, "");
				htmlText = htmlText.replace(FAILED_ASSETS_TOKEN, "");
				htmlText = htmlText.replace(SHOW_FAILED_ASSETS_TOKEN, DISPLAY_NONE_MSO_HIDE_ALL);
				return htmlText;
			}
			LOGGER.debug("Email Body: " + htmlText);
		} catch (Exception e) {
			LOGGER.error("There is an error preparing email data ", e);
		}

		return htmlText;
	}

	private String getBodyTemplate() throws BaseTeamsException {
		var htmlText = "";
		NotificationSettings notifcationConfig = new NotificationConfig();
		String emailTemplatePath = notifcationConfig.getEmailTemplateFilePath();
		LOGGER.debug("emailTemplatePath>>" + emailTemplatePath);

		if (emailTemplatePath != null) {
			htmlText = readHtmlText(emailTemplatePath);
		}
		return htmlText;
	}

	private String readHtmlText(String filePath) {
		StringBuilder contentBuilder = new StringBuilder();

		try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (IOException e) {
			LOGGER.error("Didn't find the HTML file :" + e);
		}

		return contentBuilder.toString();
	}

	private StringBuilder getTableData(StringBuilder htmlData, List<ErrorResponseFailedAsset> failedAssets) {

		for (ErrorResponseFailedAsset failedAsset : failedAssets) {

			htmlData.append(TR_START_TAG_ALIGN_CENTER).append(TD_START_TAG + failedAsset.getAssetID() + TD_END_TAG)
					.append(TD_START_TAG + failedAsset.getErrorCode() + TD_END_TAG)
					.append(TD_START_TAG + failedAsset.getErrorMessage() + TD_END_TAG).append(TR_END_TAG);

		}
		return htmlData;

	}

	private static final String TR_START_TAG_ALIGN_CENTER = "<TR  ALIGN='center'>";
	private static final String TR_END_TAG = "</TR>";
	private static final String TD_START_TAG = "<TD>";
	private static final String TD_END_TAG = "</TD>";
	private static final String ERROR_MSG_TOKEN = "%ERROR_MSG%";
	private static final String JOB_ID_TOKEN = "%JOB_ID%";
	private static final String SHOW_ERROR_MSG_TOKEN = "%SHOW_ERROR_MSG%";
	private static final String SHOW_FAILED_ASSETS_TOKEN = "%SHOW_FAILED_ASSETS%";
	private static final String FAILED_ASSETS_TOKEN = "%FAILED_ASSETS%";

	private static final String DISPLAY_NONE_MSO_HIDE_ALL = "display:none;mso-hide:all";
}