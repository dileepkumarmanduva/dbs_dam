package com.opentext.dmg.email.util;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class EmailSender {

	private static final Log LOGGER = LogFactory.getLog(EmailSender.class);
	private Session session;
	private boolean isInitialized = false;

	public void initializeSession( String hostname, String port) {
		var sysProps = System.getProperties();
		sysProps.setProperty( "mail.smtp.host", hostname );
		sysProps.setProperty( "mail.smtp.port", port );
		session = Session.getDefaultInstance( sysProps );
		isInitialized = true;
	}

	
	public void sendEMail(HtmlEmailData emailData) throws EmailSenderException, MessagingException, IOException {
		if (!isInitialized) {
			throw new EmailSenderException("EmailSender not (yet) initialized");
		}
		try {
			var message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailData.getSender()));
			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(emailData.getRecipient()));
			if (emailData.containsCC()) {
				message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(emailData.getCCRecipient()));
			}
			if (emailData.containsBCC()) {
				message.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(emailData.getBCCRecipient()));
			}
			LOGGER.trace("subject: " + emailData.getSubject());
			message.setSubject(emailData.getSubject(), "ISO-8859-1");
			// set the email body
			var multiPart = new MimeMultipart();
			List<String> attachFiles = emailData.getAttachFiles();
			addAttachments(emailData, multiPart, attachFiles);
			// HTML text
			var htmlPart = new MimeBodyPart();
			htmlPart.setContent(emailData.getHtmlText(), "text/html; charset=utf-8");
			// bring it all together
			multiPart.addBodyPart(htmlPart);
			message.setContent(multiPart);
			Transport.send(message);
		} catch (MessagingException e) {
			LOGGER.error("failed to send email to " + emailData.getRecipient() +" : "+e);
			throw e;
		}
	}


	private void addAttachments(HtmlEmailData emailData, MimeMultipart multiPart, List<String> attachFiles)
			throws MessagingException, IOException {
		if(attachFiles!=null && !attachFiles.isEmpty()) {
			for(String fileName : attachFiles) {
				addAttachment(multiPart, fileName);
			}
		} else if(StringUtils.isNotEmpty(emailData.getAttachFile())) {
			MimeBodyPart attachment;
			attachment = new MimeBodyPart();
			try {
				attachment.attachFile(emailData.getAttachFile());
			} catch (IOException e) {
				LOGGER.error("failed to add attachment to email " + emailData.getAttachFile()+" : "+e);
				throw e;
			}
			multiPart.addBodyPart(attachment);
		}
	}
	
	private static void addAttachment(Multipart multipart, String filename) throws MessagingException
	{
	    DataSource source = new FileDataSource(filename);
	    BodyPart messageBodyPart = new MimeBodyPart();        
	    messageBodyPart.setDataHandler(new DataHandler(source));
	    messageBodyPart.setFileName(new File(filename).getName());
	    multipart.addBodyPart(messageBodyPart);
	}

}
