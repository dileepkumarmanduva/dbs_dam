package com.opentext.dmg.email.util;

public class EmailSenderException extends Exception {

	private static final long serialVersionUID = 1L;

	public EmailSenderException() {
		super();
	}

	public EmailSenderException(String message, Throwable cause) {
		super(message, cause);
		}

	public EmailSenderException(String message) {
		super(message);
	}

	public EmailSenderException(Throwable cause) {
		super(cause);
	}

}
