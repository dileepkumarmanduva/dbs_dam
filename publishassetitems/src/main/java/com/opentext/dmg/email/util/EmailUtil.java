package com.opentext.dmg.email.util;

import java.io.IOException;

import javax.mail.MessagingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.common.exception.BaseTeamsException;
import com.opentext.dmg.assetitems.pushapi.config.NotificationConfig;
import com.opentext.dmg.assetitems.pushapi.config.NotificationSettings;
import com.opentext.dmg.otmm.util.AdminUtils;

public class EmailUtil {

	private static final Log LOGGER = LogFactory.getLog(EmailUtil.class);

	public void sendEmail(String htmlText) {
		try {
			NotificationSettings notificationSettings = new NotificationConfig();
			var emailData = new HtmlEmailData();
			emailData.setHtmlText(htmlText);
			emailData.setSubject(notificationSettings.getSubject());
			emailData.setSender(notificationSettings.getSender());
			emailData.setRecipient(notificationSettings.getRecipients());
			emailData.setCCRecipient(notificationSettings.getCcRecepients());
			var emailSender = new EmailSender();
			emailSender.initializeSession(getEmailServer(), getEmailServerPort());
			emailSender.sendEMail(emailData);
			LOGGER.info("Mail sent sucessfully to " + emailData.getRecipient() + "!");

		} catch (BaseTeamsException | IOException | EmailSenderException | MessagingException e) {
			LOGGER.error("Error occured while sending the email" , e);
		}

	}

	private String getEmailServer() throws BaseTeamsException {
		return AdminUtils.getOTMMSettingValue("COMMON", "SERVER", "EMAIL_SERVER");
	}

	private String getEmailServerPort() throws BaseTeamsException {
		return AdminUtils.getOTMMSettingValue("COMMON", "SERVER", "EMAIL_SERVER_PORT");
	}


}
