package com.opentext.dmg.email.util;

import java.util.List;

public class HtmlEmailData {

	private String sender;
	private String recipient;
	private String ccRecipient;
	private String bccRecipient;
	private String subject;
	private String htmlText;
	private String attachFile;
	private List<String> attachFiles;

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setHtmlText(String htmlText) {
		this.htmlText = htmlText;
	}

	public String getSender() {
		return sender;
	}

	public String getRecipient() {
		return recipient;
	}

	public String getSubject() {
		return subject;
	}

	public String getHtmlText() {
		return htmlText;
	}

	public void setCCRecipient(String ccRecipient) {
		this.ccRecipient = ccRecipient;
	}

	public String getCCRecipient() {
		return ccRecipient;
	}

	public void setBCCRecipient(String bccRecipient) {
		this.bccRecipient = bccRecipient;
	}

	public String getBCCRecipient() {
		return bccRecipient;
	}

	public boolean containsCC() {
		boolean response = false;
		if (ccRecipient != null && !ccRecipient.equals("")) {
			response = true;
		}
		return response;
	}

	public boolean containsBCC() {
		boolean response = false;
		if (bccRecipient != null && !bccRecipient.equals("")) {
			response = true;
		}
		return response;
	}

	public String getAttachFile() {
		return attachFile;
	}

	public void setAttachFile(String attachFile) {
		this.attachFile = attachFile;
	}

	public List<String> getAttachFiles() {
		return attachFiles;
	}

	public void setAttachFiles(List<String> attachFiles) {
		this.attachFiles = attachFiles;
	}

	@Override
	public String toString() {
		return "HtmlEmailData [sender=" + sender + ", recipient=" + recipient + ", ccRecipient=" + ccRecipient
				+ ", bccRecipient=" + bccRecipient + ", subject=" + subject + ", htmlText=" + htmlText + ", attachFile="
				+ attachFile + ", attachFiles=" + attachFiles + "]";
	}
	
	

}
