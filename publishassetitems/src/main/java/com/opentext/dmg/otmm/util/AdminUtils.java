package com.opentext.dmg.otmm.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.common.exception.BaseTeamsException;
import com.artesia.common.prefs.PrefData;
import com.artesia.common.prefs.PrefDataId;
import com.artesia.system.services.SystemServices;

public class AdminUtils {
	private static Log log = LogFactory.getLog(AdminUtils.class);
	
	public static String getOTMMSettingValue(String component, String key,String name) throws BaseTeamsException {
		log.debug(">>> getOTMMSettingValue >>> ");

		PrefDataId dataId = new PrefDataId(component, key,name);
		PrefData retrievedData = SystemServices.getInstance().retrieveSystemSettingsByPrefDataId(dataId, null);

		log.debug("<<< getOTMMSettingValue <<< ");

		return retrievedData.getValue();

	}
	
	private AdminUtils() {
		
	}
}
