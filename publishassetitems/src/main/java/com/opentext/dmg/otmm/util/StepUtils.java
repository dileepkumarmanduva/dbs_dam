package com.opentext.dmg.otmm.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.artesia.asset.AssetIdentifier;
import com.artesia.entity.TeamsIdentifier;
import com.opentext.job.context.ListContextWrapper;

public class StepUtils {

	private static final Log log = LogFactory.getLog(StepUtils.class);
	
	public static List<AssetIdentifier> getAssetIds(Object assetIdsObject) {
		log.trace(">>getAssetIds()");
		List<AssetIdentifier> assetIds = new ArrayList<>();
		if (assetIdsObject == null) {
			return assetIds;
		}else if (assetIdsObject instanceof ListContextWrapper) {

				ListContextWrapper<Object> assetIdListContextWrapper = (ListContextWrapper<Object>) assetIdsObject;
				List<Object> assetIdObjects = assetIdListContextWrapper.getListData();
					for (Object assetIdObject : assetIdObjects) {
						if (assetIdObject instanceof TeamsIdentifier) {

							assetIds.add(new AssetIdentifier(assetIdObject.toString()));

							continue;
						}
						assetIds.add(new AssetIdentifier(assetIdObject.toString()));
					}

				
			} else if (assetIdsObject instanceof TeamsIdentifier || assetIdsObject instanceof AssetIdentifier) {

				assetIds.add(new AssetIdentifier(assetIdsObject.toString()));
			} else {

				String[] assetIdsArray = assetIdsObject.toString().split(",");
				for (String assetId : assetIdsArray) {
					assetIds.add(new AssetIdentifier(assetId));
				}
			}
		
		log.trace("<<getAssetIds()");
		return assetIds;
	}
	
	private StepUtils() {
		
	}
}
