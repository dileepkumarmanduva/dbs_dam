package com.opentext.mediaanalysis.aws.utils;

public class AWSRekognitionModerationLabelConstants {

	public static final String MODERATION_TOP_LEVEL_LABELS []  = {"Explicity Nudity", "Suggestive", "Voilence", 
			"Visually Disturbing", "Rude Gestures", "Drugs", "Tobacco", "Alcohol", "Gambling", "Hate Symbols"};
}
