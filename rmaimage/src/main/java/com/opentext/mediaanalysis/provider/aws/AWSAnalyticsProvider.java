package com.opentext.mediaanalysis.provider.aws;

import com.opentext.mediaanalysis.provider.AnalyticsProvider;


public class AWSAnalyticsProvider implements AnalyticsProvider{

	private static final String type = "AWS";
	
	public String getProviderType() {
		return type;
	}
} 