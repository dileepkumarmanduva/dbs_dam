/**
 * 
 */
package com.opentext.mediaanalysis.provider.aws;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opentext.mediaanalysis.common.AnalyticsRequest;
import com.opentext.mediaanalysis.model.rma.Provider;
import com.opentext.mediaanalysis.model.rma.Status;
import com.opentext.mediaanalysis.provider.AnalyticsProviderCodes;
import com.opentext.mediaanalysis.provider.AnalyticsProviderException;
import com.opentext.mediaanalysis.provider.AnalyticsProviderRawData;
import com.opentext.mediaanalysis.provider.AnalyticsResults;
import com.opentext.mediaanalysis.provider.ImageAnalyticsProvider;
import com.opentext.mediaanalysis.provider.image.models.AWSImageSummary;

import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.http.SdkHttpClient;
import software.amazon.awssdk.http.apache.ApacheHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.rekognition.RekognitionClient;
import software.amazon.awssdk.services.rekognition.model.Attribute;
import software.amazon.awssdk.services.rekognition.model.Celebrity;
import software.amazon.awssdk.services.rekognition.model.DetectFacesRequest;
import software.amazon.awssdk.services.rekognition.model.DetectFacesResponse;
import software.amazon.awssdk.services.rekognition.model.DetectLabelsRequest;
import software.amazon.awssdk.services.rekognition.model.DetectLabelsResponse;
import software.amazon.awssdk.services.rekognition.model.DetectModerationLabelsRequest;
import software.amazon.awssdk.services.rekognition.model.DetectModerationLabelsResponse;
import software.amazon.awssdk.services.rekognition.model.DetectTextRequest;
import software.amazon.awssdk.services.rekognition.model.DetectTextResponse;
import software.amazon.awssdk.services.rekognition.model.GetCelebrityInfoRequest;
import software.amazon.awssdk.services.rekognition.model.GetCelebrityInfoResponse;
import software.amazon.awssdk.services.rekognition.model.Image;
import software.amazon.awssdk.services.rekognition.model.RecognizeCelebritiesRequest;
import software.amazon.awssdk.services.rekognition.model.RecognizeCelebritiesResponse;
import software.amazon.awssdk.services.rekognition.model.RekognitionResponse;

/**
 * @author dmanduva@opentext.com
 *
 */

public class AWSImageAnalyticsProvider extends AWSAnalyticsProvider implements ImageAnalyticsProvider {

	private static final Logger logger = LoggerFactory.getLogger(AWSAnalyticsProvider.class);

	public static String providerName = "OpenText AWS Image Analytics Provider";

	private AWSRekognitionTransformer rekognitionTransformer;

	public AWSImageAnalyticsProvider(AWSRekognitionTransformer rekognitionTransformer) {
		this.rekognitionTransformer = rekognitionTransformer;
	}

	public AnalyticsResults processImage(AnalyticsRequest request) throws AnalyticsProviderException {
		logger.info("Processing image using AWS Rekognition");
		AnalyticsResults results = new AnalyticsResults();
		AnalyticsProviderRawData objRawData = new AnalyticsProviderRawData();
		Status status = new Status();
		Provider provider = new Provider();
		provider.setName(providerName);
		results.setProvider(provider);
		results.setRawData(objRawData);
		
		AWSImageSummary imageSummary = new AWSImageSummary();
		byte[] imageData = request.getData();
		
		this.analyzeImageData(imageData, request.getAnalyticsParameters().getLanguageLocale(), objRawData, imageSummary);
		
		AnalyticsProviderCodes analyticsProviderCodes = new AnalyticsProviderCodes("ProviderSuccess");
			
		status.setCode(AnalyticsProviderCodes.PROVIDER_SUCCESS);
		status.setMessage(analyticsProviderCodes.getMessage());
		
		results.setStatus(status);
		results.setSummary(imageSummary);
		results.setRawData(objRawData);
		results.setStatus(status);

		logger.info("Image Summary is : " + imageSummary.toString());
		logger.info("processImage>>");
		return results;
	}
	
	public static void main(String args[]) {
		/*
		 * 
		 * try { SdkHttpClient httpClient = ApacheHttpClient.builder().build();
		 * 
		 * RekognitionClient rekognitionClient = RekognitionClient.builder()
		 * .credentialsProvider(ProfileCredentialsProvider.create("A4L-MASTER"))
		 * .region(Region.US_EAST_1).httpClient(httpClient).build();
		 * 
		 * 
		 * SdkBytes sdkBytes = SdkBytes.fromInputStream(new
		 * FileInputStream("C:\\Users\\dmanduva\\Desktop\\AWSRekTest\\Master.jpg"));
		 * 
		 * Image sourceImage = Image.builder() .bytes(sdkBytes) .build();
		 * 
		 * List<RekognitionResponse> rekognitionReponses = new ArrayList<>();
		 * 
		 * DetectFacesRequest facesRequest =
		 * DetectFacesRequest.builder().attributes(Attribute.ALL).image(sourceImage).
		 * build(); DetectLabelsRequest labelsRequest =
		 * DetectLabelsRequest.builder().image(sourceImage).build(); DetectTextRequest
		 * textRequest = DetectTextRequest.builder().image(sourceImage).build();
		 * RecognizeCelebritiesRequest celebrityRequest =
		 * RecognizeCelebritiesRequest.builder().image(sourceImage).build();
		 * 
		 * DetectFacesResponse detectFacesResponse =
		 * rekognitionClient.detectFaces(facesRequest);
		 * 
		 * if (detectFacesResponse.sdkHttpResponse().isSuccessful()) {
		 * rekognitionReponses.add(detectFacesResponse); } DetectLabelsResponse
		 * detectLabelsResponse = rekognitionClient.detectLabels(labelsRequest); if
		 * (detectLabelsResponse.sdkHttpResponse().isSuccessful()) {
		 * rekognitionReponses.add(detectLabelsResponse); } DetectTextResponse
		 * detectTextResponse = rekognitionClient.detectText(textRequest); if
		 * (detectTextResponse.sdkHttpResponse().isSuccessful()) {
		 * rekognitionReponses.add(detectTextResponse); } RecognizeCelebritiesResponse
		 * recognizeCelebritiesResponse =
		 * rekognitionClient.recognizeCelebrities(celebrityRequest); if
		 * (recognizeCelebritiesResponse.sdkHttpResponse().isSuccessful()) {
		 * rekognitionReponses.add(recognizeCelebritiesResponse); }
		 * 
		 * List<Celebrity> celebrityFaces = null;
		 * 
		 * if (recognizeCelebritiesResponse != null &&
		 * recognizeCelebritiesResponse.hasCelebrityFaces()) { celebrityFaces =
		 * recognizeCelebritiesResponse.celebrityFaces();
		 * 
		 * for (Celebrity aCeleb : celebrityFaces) { String id = aCeleb.id();
		 * GetCelebrityInfoRequest aCelebInfoRequest =
		 * GetCelebrityInfoRequest.builder().id(id).build(); GetCelebrityInfoResponse
		 * celebrityInfoResponse =
		 * rekognitionClient.getCelebrityInfo(aCelebInfoRequest);
		 * rekognitionReponses.add(celebrityInfoResponse); } }
		 * System.out.println(rekognitionReponses.size());
		 * System.out.println("HaseLabels: " + detectLabelsResponse.hasLabels());
		 * System.out.println("Labels:" +detectLabelsResponse.labels().size());
		 * System.out.println("HasFaces: " + detectFacesResponse.hasFaceDetails());
		 * System.out.println("Faces:" + detectFacesResponse.faceDetails().size());
		 * System.out.println("Celebrities:" +
		 * recognizeCelebritiesResponse.hasCelebrityFaces());
		 * System.out.println("Celebrities:" +
		 * recognizeCelebritiesResponse.celebrityFaces().size());
		 * 
		 * ImageSummary summary = new ImageSummary(); AnalyticsProviderRawData
		 * objectRawData = new AnalyticsProviderRawData(); if
		 * (!rekognitionReponses.isEmpty()) {
		 * 
		 * new AWSRekognitionTransformer().buildAnalysisSummary(summary, objectRawData,
		 * rekognitionReponses, celebrityFaces, sdkBytes.asByteArray()); }
		 * System.out.println("Image Summary: " + summary); }catch (Exception e) {
		 * e.printStackTrace(); }
		 * 
		 */}
	
	protected void analyzeImageData(byte [] imageData, Locale locale, AnalyticsProviderRawData objectRawData, AWSImageSummary summary)  {
		logger.info("AnalysImageData using AWS.");
		try {
			SdkHttpClient httpClient = ApacheHttpClient.builder().build();
			
			RekognitionClient rekognitionClient = RekognitionClient.builder()
					.credentialsProvider(DefaultCredentialsProvider.create())
					.region(Region.US_EAST_1).httpClient(httpClient).build();
			
			SdkBytes sdkBytes = SdkBytes.fromByteArray(imageData);
			
			Image sourceImage = Image.builder()
	                .bytes(sdkBytes)
	                .build();
			List<RekognitionResponse> rekognitionReponses = new ArrayList<>();
			
			DetectFacesRequest facesRequest = DetectFacesRequest.builder().attributes(Attribute.ALL).image(sourceImage).build();
			DetectLabelsRequest labelsRequest = DetectLabelsRequest.builder().image(sourceImage).build();
			DetectTextRequest textRequest = DetectTextRequest.builder().image(sourceImage).build();
			DetectModerationLabelsRequest detectModerationLabelsRequest = DetectModerationLabelsRequest.builder().image(sourceImage).build();
			RecognizeCelebritiesRequest celebrityRequest = RecognizeCelebritiesRequest.builder().image(sourceImage).build();
			
			DetectFacesResponse detectFacesResponse = rekognitionClient.detectFaces(facesRequest);
			if (detectFacesResponse.sdkHttpResponse().isSuccessful() && detectFacesResponse.hasFaceDetails()) {
				logger.info("DetectFacesResponse.hasFaceDetails:" + detectFacesResponse.hasFaceDetails());
				rekognitionReponses.add(detectFacesResponse);
			}
			
			DetectLabelsResponse detectLabelsResponse = rekognitionClient.detectLabels(labelsRequest);
			if (detectLabelsResponse.sdkHttpResponse().isSuccessful() && detectLabelsResponse.hasLabels()) {
				rekognitionReponses.add(detectLabelsResponse);
			}
			
			DetectTextResponse detectTextResponse = rekognitionClient.detectText(textRequest);
			if (detectTextResponse.sdkHttpResponse().isSuccessful() && detectTextResponse.hasTextDetections()) {
				rekognitionReponses.add(detectTextResponse);			
			}
			
			DetectModerationLabelsResponse detectModerationResponse = rekognitionClient.detectModerationLabels(detectModerationLabelsRequest);
			if (detectModerationResponse.sdkHttpResponse().isSuccessful() && detectModerationResponse.hasModerationLabels()) {
				rekognitionReponses.add(detectModerationResponse);
			}
			
			RecognizeCelebritiesResponse recognizeCelebritiesResponse = rekognitionClient.recognizeCelebrities(celebrityRequest);
			if (recognizeCelebritiesResponse.sdkHttpResponse().isSuccessful()) {
				rekognitionReponses.add(recognizeCelebritiesResponse);	
			}
			
			List<Celebrity> celebrityFaces = null;

			if (recognizeCelebritiesResponse != null && recognizeCelebritiesResponse.hasCelebrityFaces()) {
				logger.info("Found Celebrity Faces, processing celeb faces");
				celebrityFaces = recognizeCelebritiesResponse.celebrityFaces();
				
				for (Celebrity aCeleb : celebrityFaces) {
					String id = aCeleb.id();
			        GetCelebrityInfoRequest aCelebInfoRequest = GetCelebrityInfoRequest.builder().id(id).build();
			        GetCelebrityInfoResponse celebrityInfoResponse = rekognitionClient.getCelebrityInfo(aCelebInfoRequest);
			        rekognitionReponses.add(celebrityInfoResponse);
				}
			}
			
			if (!rekognitionReponses.isEmpty()) {
				this.rekognitionTransformer.buildAnalysisSummary(summary, objectRawData, rekognitionReponses, celebrityFaces, imageData);
			}
		}catch (Exception e) {
			logger.error("AN EXCEPTION OCCURED WHILE PROCESSING THE IMAGE: ", e);
		}
		logger.info("AnalysImageData using AWS >> END.");
	}
}
