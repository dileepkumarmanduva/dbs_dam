package com.opentext.mediaanalysis.provider.aws;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.opentext.mediaanalysis.model.rma.image.Age;
import com.opentext.mediaanalysis.model.rma.image.Category;
import com.opentext.mediaanalysis.model.rma.image.Face;
import com.opentext.mediaanalysis.model.rma.image.FaceDetection;
import com.opentext.mediaanalysis.model.rma.image.FaceRectangle;
import com.opentext.mediaanalysis.model.rma.image.Ocr;
import com.opentext.mediaanalysis.model.rma.image.Person;
import com.opentext.mediaanalysis.model.rma.image.Tag;
import com.opentext.mediaanalysis.provider.AnalyticsApiResponse;
import com.opentext.mediaanalysis.provider.AnalyticsProviderRawData;
import com.opentext.mediaanalysis.provider.image.models.AWSImageSummary;

import software.amazon.awssdk.services.rekognition.model.AgeRange;
import software.amazon.awssdk.services.rekognition.model.BoundingBox;
import software.amazon.awssdk.services.rekognition.model.Celebrity;
import software.amazon.awssdk.services.rekognition.model.DetectFacesResponse;
import software.amazon.awssdk.services.rekognition.model.DetectLabelsResponse;
import software.amazon.awssdk.services.rekognition.model.DetectModerationLabelsResponse;
import software.amazon.awssdk.services.rekognition.model.DetectTextResponse;
import software.amazon.awssdk.services.rekognition.model.FaceDetail;
import software.amazon.awssdk.services.rekognition.model.Gender;
import software.amazon.awssdk.services.rekognition.model.Label;
import software.amazon.awssdk.services.rekognition.model.ModerationLabel;
import software.amazon.awssdk.services.rekognition.model.Parent;
import software.amazon.awssdk.services.rekognition.model.RekognitionResponse;
import software.amazon.awssdk.services.rekognition.model.TextDetection;

@Component
public class AWSRekognitionTransformer {

	private static final Logger logger = LoggerFactory.getLogger(AWSRekognitionTransformer.class);

	public void buildAnalysisSummary(AWSImageSummary result, AnalyticsProviderRawData objRawData,
			List<RekognitionResponse> rekognitionResponses, List<Celebrity> celebrityFaces, byte[] imageData) {
		logger.info("Rekognition Responses size: " + rekognitionResponses.size());
		Map<FaceRectangle, Person> celebrityFacesMap = null;
		Map<String, Integer> tagToConfidence = new HashMap<>();
		if (celebrityFaces != null && !celebrityFaces.isEmpty()) {
			celebrityFacesMap = this.transformCelebrityFaces(celebrityFaces,getImageDimension(imageData), tagToConfidence);
		}
		
		if (!rekognitionResponses.isEmpty()) {
			for (RekognitionResponse aRekognitionResponse : rekognitionResponses) {
				if (aRekognitionResponse instanceof DetectFacesResponse) {
					logger.info("DetectFacesResponse size: " + ((DetectFacesResponse)aRekognitionResponse).faceDetails().size());
					FaceDetection faceDetection = this.transformFaces((DetectFacesResponse)aRekognitionResponse, getImageDimension(imageData), celebrityFacesMap);
					result.setFaceDetection(faceDetection);
				}else if (aRekognitionResponse instanceof DetectLabelsResponse) {
					result.setCategories(this.transformLabels((DetectLabelsResponse)aRekognitionResponse,tagToConfidence));
					logger.info("DetectCategoriesResponse size: " + rekognitionResponses.size());
				}else if (aRekognitionResponse instanceof DetectTextResponse) {
					DetectTextResponse temp = (DetectTextResponse) aRekognitionResponse;
					if (temp.textDetections() != null && !temp.textDetections().isEmpty()) {
						result.setOcr(this.transformText((DetectTextResponse)aRekognitionResponse));
						logger.info("DetectTextResponse size: " + rekognitionResponses.size());
					}
				}else if (aRekognitionResponse instanceof DetectModerationLabelsResponse) {
					result.setModerationLabels(this.transformModerationLabels((DetectModerationLabelsResponse)aRekognitionResponse));
					logger.info("DetectModerationLabels size: " + rekognitionResponses.size());
				}
				objRawData.getApiResponses().add(new AnalyticsApiResponse((String) null, (String) null, 200, aRekognitionResponse.toString().toLowerCase()));
				logger.info("BuildAnalysisSummary End");
			}
		}
		
		if (!tagToConfidence.isEmpty()) {
			result.setTags(this.transformTags(tagToConfidence));
		}
	}

	private List<Tag> transformTags(Map<String, Integer> tagToConfidence) {
		List<Tag> returnValue = new ArrayList<>();
		Set<Entry<String, Integer>> entrySet = tagToConfidence.entrySet();
		Iterator<Entry<String, Integer>> entrySetIterator = entrySet.iterator();
		
		while (entrySetIterator.hasNext()) {
			Entry<String, Integer> anEntry = entrySetIterator.next();
			Tag aTag = new Tag();
			aTag.setName(anEntry.getKey().toLowerCase());
			aTag.setConfidence(anEntry.getValue());
			returnValue.add(aTag);
		}
		
		return returnValue;
	}

	private List<com.opentext.mediaanalysis.provider.image.models.ModerationLabel> transformModerationLabels(DetectModerationLabelsResponse aRekognitionResponse) {
		List<ModerationLabel> moderationLabels = aRekognitionResponse.moderationLabels();
		List<com.opentext.mediaanalysis.provider.image.models.ModerationLabel> result = new ArrayList<>();
		Iterator<ModerationLabel> moderationLabelsIterator = moderationLabels.iterator();
		
		while (moderationLabelsIterator.hasNext()) {
			ModerationLabel nextModerationLabel = moderationLabelsIterator.next();
			com.opentext.mediaanalysis.provider.image.models.ModerationLabel rmaModerationLabel = new com.opentext.mediaanalysis.provider.image.models.ModerationLabel();
			rmaModerationLabel.setType(nextModerationLabel.name().toLowerCase());
			rmaModerationLabel.setConfidence(nextModerationLabel.confidence().intValue());
			result.add(rmaModerationLabel);
		}
		
		return result;
	}

	private Map<FaceRectangle, Person> transformCelebrityFaces(List<Celebrity> celebrityFaces, Dimension dimension, Map<String, Integer> tagToConfidence) {
		logger.info("TransformCelebrityFaces Start");
		Map<FaceRectangle, Person> resultMap = new HashMap<>();
		Iterator<Celebrity> celebrityFacesIterator = celebrityFaces.iterator();

		while (celebrityFacesIterator.hasNext()) {
			Celebrity aCelebrity = celebrityFacesIterator.next();
			Person aPerson = new Person();
			aPerson.setName(aCelebrity.name().toLowerCase());
			aPerson.setConfidence(aCelebrity.matchConfidence().intValue());
			FaceRectangle aCelebRectangle = this.transformFdBoundingPolyToFaceRectangle(aCelebrity.face().boundingBox(), dimension);
			aCelebrity.face().boundingBox();
			resultMap.put(aCelebRectangle, aPerson);
			if (tagToConfidence.get(aCelebrity.name()) == null) {
				tagToConfidence.put(aCelebrity.name(), aCelebrity.matchConfidence().intValue());
			}
		}
		logger.info("TransformCelebrityFaces End");
		return resultMap;
	}

	private List<Category> transformLabels(DetectLabelsResponse detectLabelsResponse, Map<String, Integer> tagToConfidence) {
		logger.info("TransformLabels Start");
//		Map<String, Integer> tagToConfidence = new HashMap<>();
		List<Category> result = null;
		if (detectLabelsResponse.hasLabels()) {
			List<Label> labelsList = detectLabelsResponse.labels();
			result = new ArrayList<>();
			Iterator<Label> labelsIterator = labelsList.iterator();
			
			while (labelsIterator.hasNext()) {
				Label nextLabel = labelsIterator.next();
				Category aCategory = new Category();
				aCategory.setName(nextLabel.name());
				Float confidence = nextLabel.confidence();
				aCategory.setConfidence(confidence.intValue());
				result.add(aCategory);
				if (nextLabel.hasParents()) {
					List<Parent> parents = nextLabel.parents();
					Iterator<Parent> parentsIterator = parents.iterator();
					while (parentsIterator.hasNext()) {
						Parent aParent = parentsIterator.next();
						if (tagToConfidence.get(aParent.name()) == null) {
							tagToConfidence.put(aParent.name(), confidence.intValue());
						}
					}
				}
			}
		}
		logger.info("TransformLabels End");
		return result;
	}

	private FaceDetection transformFaces(DetectFacesResponse detectFacesResponse, Dimension dimension, Map<FaceRectangle, Person> celebrityPersonMap) {
		logger.info("TransformFaces Start");
		FaceDetection faceDetection = new FaceDetection ();
		if (detectFacesResponse.hasFaceDetails()) {
			List<FaceDetail> faceDetails = detectFacesResponse.faceDetails();
			faceDetection.setCount(faceDetails.size());
			Iterator<FaceDetail> faceDetailsIterator = faceDetails.iterator();
			while (faceDetailsIterator.hasNext()) {
				FaceDetail nextFaceDetail = faceDetailsIterator.next();
				logger.info("Processing a Face >> Start");
				Face aFace = new Face();
				
				AgeRange ageRange = nextFaceDetail.ageRange();
				Integer low = ageRange.low();
				Integer high = ageRange.high();
				Age age = new Age();
				age.setValue((low+high)/2);
				aFace.setAge(age);
				
				
				aFace.setPerson(this.getPersonByRectangle(celebrityPersonMap, nextFaceDetail, dimension));
				Gender gender = nextFaceDetail.gender();
				
				com.opentext.mediaanalysis.model.rma.image.Gender faceGender = new com.opentext.mediaanalysis.model.rma.image.Gender();
				faceGender.setValue(gender.valueAsString().toLowerCase());
				faceGender.setConfidence(100);
				
				aFace.setGender(faceGender);
				BoundingBox boundingBox = nextFaceDetail.boundingBox();
				aFace.setFaceRectangle(this.transformFdBoundingPolyToFaceRectangle(boundingBox, dimension));
				
				if (faceDetection.getFaces() == null) {
					List<Face> faceList = new ArrayList<>();
					faceList.add(aFace);
					faceDetection.setFaces(faceList);
					logger.info("Added a face >> Start");
				}else {
					faceDetection.getFaces().add(aFace);
				}
				
				logger.info("Processing a Face >> End");
			}
		}else {
			logger.info("No Faces found.");
			faceDetection.setCount(0);
			faceDetection.setFaces(new ArrayList<>());
		}
		logger.info("TransformFaces End");
		return faceDetection;
	}
	
	private Person getPersonByRectangle(Map<FaceRectangle, Person> personMap, FaceDetail providerFace, Dimension dimension) {
		logger.info("Get Person By Rectangle Start");
		if (personMap != null) {
			Iterator<Entry<FaceRectangle, Person>> mapIterator = personMap.entrySet().iterator();

			FaceRectangle providerRectangle;
			FaceRectangle personRectangle;
			do {
				if (!mapIterator.hasNext()) {
					return null;
				}
				Entry<FaceRectangle, Person> personEntry = (Entry) mapIterator.next();
				providerRectangle = new FaceRectangle();
				providerRectangle.setLeft((double) providerFace.boundingBox().left());
				providerRectangle.setTop((double) providerFace.boundingBox().top());
				providerRectangle.setWidth((double) providerFace.boundingBox().width());
				providerRectangle.setHeight((double) providerFace.boundingBox().height());
				personRectangle = new FaceRectangle();
				personRectangle.setLeft(((FaceRectangle) personEntry.getKey()).getLeft());
				personRectangle.setTop(((FaceRectangle) personEntry.getKey()).getTop());
				personRectangle.setWidth(((FaceRectangle) personEntry.getKey()).getWidth());
				personRectangle.setHeight(((FaceRectangle) personEntry.getKey()).getHeight());
			} while (!this.matchRectangles(providerRectangle, personRectangle));
			logger.info("Get Person By Rectangle End");
			return (Person) personMap.get(personRectangle);
		}
		return null;		
	}

	private boolean matchRectangles(FaceRectangle providerRect, FaceRectangle rmaRect) {
		logger.info("Match Rectangles Start");
		Point faceRectangleCenterPoint = this.getRectangleCenterPoint(providerRect.getLeft().intValue(), providerRect.getTop().intValue(),
				providerRect.getWidth().intValue(), providerRect.getHeight().intValue());
		Point personRectangleCenterPoint = this.getRectangleCenterPoint(rmaRect.getLeft().intValue(),
				rmaRect.getTop().intValue(), rmaRect.getWidth().intValue(), rmaRect.getHeight().intValue());
		logger.info("Matching center faceRectangleCenterPoint: " + faceRectangleCenterPoint
				+ "  and personRectangleCenterPoint: " + personRectangleCenterPoint);
		double distanceBetweenPoints = Math.sqrt((double) ((faceRectangleCenterPoint.x - personRectangleCenterPoint.x)
				* (faceRectangleCenterPoint.x - personRectangleCenterPoint.x)
				+ (faceRectangleCenterPoint.y - personRectangleCenterPoint.y)
						* (faceRectangleCenterPoint.y - personRectangleCenterPoint.y)));
		logger.info("Match Rectangles End");
		return distanceBetweenPoints <= 3.0D;
	}
	
	private Point getRectangleCenterPoint(int left, int top, int w, int h) {
		return new Point(left + w / 2, top + h / 2);
	}
	
	private FaceRectangle transformFdBoundingPolyToFaceRectangle(BoundingBox boundingBox, Dimension dimension) {
		
		FaceRectangle aFaceRectangle = new FaceRectangle();
		aFaceRectangle.setLeft((double) boundingBox.left());
		aFaceRectangle.setTop((double) boundingBox.top());
		aFaceRectangle.setHeight((double) boundingBox.height());
		aFaceRectangle.setWidth((double) boundingBox.width());
		return aFaceRectangle;
	}

	private Dimension getImageDimension(byte[] imageData) {
		Dimension dimension = null;

		try {
			try {
				BufferedImage bImage2 = ImageIO.read(new ByteArrayInputStream(imageData));
				int width = bImage2.getWidth();
				int height = bImage2.getHeight();
				dimension = new Dimension(width, height);
			} catch (IOException var9) {
				logger.error("Exception to get image dimensions", var9);
			}
			return dimension;
		} finally {
			;
		}
	}

	private Ocr transformText(DetectTextResponse aRekognitionResponse) {
		Ocr ocr = null;
		StringBuffer ocrText = new StringBuffer();
		if (aRekognitionResponse.hasTextDetections()) {
			List<TextDetection> textDetections = aRekognitionResponse.textDetections();
			
			Iterator<TextDetection> textDetectionIterator = textDetections.iterator();
		
			while(textDetectionIterator.hasNext()) {
				TextDetection nextText = textDetectionIterator.next();
				ocrText.append(nextText.detectedText());
			}
			if (ocrText.length() > 0) {
				ocr = new Ocr();
				ocr.setText(ocrText.toString().toLowerCase());
			}
		}
		
		return ocr;
	}
}
