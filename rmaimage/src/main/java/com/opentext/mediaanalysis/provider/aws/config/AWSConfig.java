package com.opentext.mediaanalysis.provider.aws.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AWSConfig {

	private static final Logger logger = LoggerFactory.getLogger(AWSConfig.class);

	@Value("${mediaanalysis.aws.retry-count:5}")
	private int nRetryCount = 5;
	
	private final String name = "AWS Cloud Service";

	public String getName() {
		return name;
	}
	
	public int getRetryCount() {
		return this.nRetryCount;
	}
}
