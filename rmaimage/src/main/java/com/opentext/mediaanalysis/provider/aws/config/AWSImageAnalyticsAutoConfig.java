package com.opentext.mediaanalysis.provider.aws.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.opentext.mediaanalysis.config.MediaAnalysisConfig;
import com.opentext.mediaanalysis.provider.ImageAnalyticsProvider;
import com.opentext.mediaanalysis.provider.aws.AWSImageAnalyticsProvider;
import com.opentext.mediaanalysis.provider.aws.AWSRekognitionTransformer;

@Configuration
public class AWSImageAnalyticsAutoConfig {

	@Bean(name = "AWSMediaAnalysisConfig")
	public MediaAnalysisConfig getMediaAnalysisConfig() {
		return new MediaAnalysisConfig();
	}

	@Bean(name = "AWSRekongtionTransformer")
	@Primary
	public AWSRekognitionTransformer getAWSProviderModelTransformer() {
		return new AWSRekognitionTransformer();
	}

	@Bean (name = "AWSRekognitionConfig")
	@Primary
	public AWSRekognitionConfig getAWSRekognitionConfig() {
		return new AWSRekognitionConfig(getMediaAnalysisConfig());
	}

	@Bean (name = "AWSImageAnalyticsProvider")
	public ImageAnalyticsProvider getAWSImageAnalyticsProvider(AWSRekognitionTransformer transformer) {
		return (ImageAnalyticsProvider) new AWSImageAnalyticsProvider(transformer);
	}
}
