package com.opentext.mediaanalysis.provider.aws.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opentext.mediaanalysis.config.MediaAnalysisConfig;

public class AWSRekognitionConfig extends AWSConfig {

	private static final Logger logger = LoggerFactory.getLogger(AWSRekognitionConfig.class);

	private static final String name = "AWS Rekognition Service";

	public AWSRekognitionConfig(MediaAnalysisConfig mediaAnalysisConfig) {
		logger.info("ImageProvider: " + mediaAnalysisConfig.getImageProvider());
	}

	public String getName() {
		return name;
	}
}
