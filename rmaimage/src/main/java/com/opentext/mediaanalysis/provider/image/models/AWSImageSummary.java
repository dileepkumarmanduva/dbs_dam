package com.opentext.mediaanalysis.provider.image.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.opentext.mediaanalysis.model.rma.image.ImageSummary;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"area_of_interest", "captions", "categories", "color_summary", "face_detection", "image_type",
		"moderation_labels", "safe_content", "tags", "ocr", "objects", "brands"})
public class AWSImageSummary extends ImageSummary{

	private static final long serialVersionUID = -6100829684577910724L;
	@JsonProperty("moderation_labels")
	private List<ModerationLabel> moderationLabels;

	@JsonProperty("moderation_labels")
	public List<ModerationLabel> getModerationLabels() {
		return moderationLabels;
	}

	@JsonProperty("moderation_labels")
	public void setModerationLabels(List<ModerationLabel> moderationLabels) {
		this.moderationLabels = moderationLabels;
	}
	
}
