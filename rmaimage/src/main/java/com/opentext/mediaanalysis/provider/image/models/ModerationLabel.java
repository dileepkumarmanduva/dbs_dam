package com.opentext.mediaanalysis.provider.image.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"name", "confidence"})
public class ModerationLabel implements Serializable{
	
	private static final long serialVersionUID = 7785847727227687122L;
	@JsonProperty("type")
	@JsonPropertyDescription("Identifies the moderation label.")
	private String type;
	@JsonProperty("confidence")
	@JsonPropertyDescription("Identifies the confidence of the identified moderation label.")
	private Integer confidence;
	
	@JsonProperty("type")
	public String getType() {
		return type;
	}
	
	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}
	
	@JsonProperty("confidence")
	public Integer getConfidence() {
		return confidence;
	}
	
	@JsonProperty("confidence")
	public void setConfidence(Integer confidence) {
		this.confidence = confidence;
	}
	
}
