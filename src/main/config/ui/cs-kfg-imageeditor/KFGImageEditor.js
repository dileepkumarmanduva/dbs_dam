(function(exports)
{



	var COORDINATE_DELIMITER = "@";
	var DIALOG_NAME = "kfgimagecroppingdialog";
	var SUBMISSION_MSG_CLOSE_EL = ".ot-submitted-notification-identifer-class .ot-notification-closer.ot-notification-item";
//	var IMAGE_CROP_DATA = "KFG.FIELD.IMAGE_CROP_DATA";
//	var IMAGE_CROP_NAME = "KFG.FIELD.IMAGE_CROP_NAME";
//	var IMAGE_CROP_FIELD_GRP = "KFG.CATEGORY.IMAGE CROP DATA";
	var IMAGE_CROP_DATA = "ARTESIA.FIELD.INTELLIGENT CROP.RECOMMENDED DATA";
	var IMAGE_CROP_NAME = "ARTESIA.FIELD.INTELLIGENT CROP.PROFILE NAME";
	var IMAGE_CROP_FIELD_GRP = "ARTESIA.CATEGORY.INTELLIGENTCROP DATA";
	
	var IMAGE_CROP_FIELD_TYPE = "com.artesia.metadata.MetadataTableField";
	var MESSAGE = Object.freeze({
		"SUCCESS" : {
			'message' : otui.tr("The new crop area was saved successfully."),
			'stayOpen': false,
			'status' : "ok"
		},
		"ERROR" : {
			'message' : otui.tr("An error occurred while saving the new crop area."),
			'stayOpen': true,
			'status' : "error"
		},
		"CROP_GENERATED" : {
			'message' : otui.tr("The crop area was generated for the asset."),
			'stayOpen': false,
			'status' : "ok"
		},
		"CROP_GENERATION_ERROR" : {
			'message' : otui.tr("An error occurred while the crop area was being generated."),
			'stayOpen': true,
			'status' : "error"
		},
		"CROP_SUBMISSION" : {
			'message' : otui.tr("The asset has been submitted for image cropping. Please check the status in Jobs."),
			'stayOpen': true,
			'classIdentifier' : 'ot-submitted-notification-identifer-class',
			'status' : "ok"
		},
		"CROP_SUBMIT_FAIL" : {
			'message' : otui.tr("An error occurred while the asset was submitted for intelligent cropping."),
			'stayOpen': true,
			'status' : "error"
		},
		"COPY_URI_CLIPBOARD_MSG" : {
			'message' : otui.tr("The URL was copied to the Clipboard."),
			'stayOpen': false,
			'status' : "ok"
		},
		"DOWNLOAD_ERROR" : {
			'message' : otui.tr("An error occurred while saving the crop area."),
			'stayOpen': true,
			'status' : "error"
		},
		"TXTFLD_ERROR" : {
			'message' : otui.tr("Please provide correct data in the text field."),
			'stayOpen': true,
			'status' : "error"
		}
	});
	// This method is specific to this view
	 var __blockContent = function __blockContent()
	 {
		 var contentArea = this.contentArea();
		 if (contentArea)
			{
				var dialogBody = contentArea.find(".kfg-image-cropping-container");
				var block = otui.Templates.get(otui.Templates.LOADER, $);
				block.addClass("lightloader nomessage");
				block.addClass("ot-contentblock").appendTo(dialogBody);
			}
	 };

	 addQueryParamToURL = function (url, param) {
		var returnUrl = url;
		if (url.includes("?")) {
			returnUrl += "&" + param;
		} else {
			returnUrl += "?" + param;
		}
		return returnUrl;
	}
	 var __unblockContent = function __unblockContent()
	 {
		var contentArea = this.contentArea();
		if (contentArea)
		{
			var dialogBody = contentArea.find(".kfg-image-cropping-container");
			dialogBody.children(".ot-contentblock").remove();
		}
	 };

	 // This method will set the state of undo and redo buttons based on operations
	var __setStateUndoRedo = function __setStateUndoRedo()
	{
		var contentArea = this.contentArea(),
			undoButton = contentArea.find(".kfg-image-cropping-undo-button"),
			redoButton = contentArea.find(".kfg-image-cropping-redo-button"),
			opsArr = this.properties.operationList,
			opsPos = this.properties.opPos;

		// Initial state of the array when the image is loaded with crop data for first time.
		if( opsArr.length === 1 )
		{
			undoButton.addClass("kfg-image-cropping-undo-disabled");
			redoButton.addClass("kfg-image-cropping-redo-disabled");
			return;
		}
		
		if( opsArr.length > 1 && opsPos > 0 && opsPos <= opsArr.length - 1 )
		{
			undoButton.removeClass("kfg-image-cropping-undo-disabled");			
		}
		else
		{
			if(opsArr.length === 1 || (opsPos === 0))
			{
				undoButton.addClass("kfg-image-cropping-undo-disabled");
			}
		}

		if( opsPos >= 0 && opsPos < opsArr.length - 1 )
		{
			redoButton.removeClass("kfg-image-cropping-redo-disabled");
		}
		else
		{
			if(opsArr.length === 1 || (opsArr.length - 1 === opsPos))
			{
				redoButton.addClass("kfg-image-cropping-redo-disabled");
			}
		}
	};
	var __removeRedoStates = function __removeRedoStates()
	{
		if(this.properties.opPos < this.properties.operationList.length - 1)
			{
				/**
				 * The idea is to prevent redo operation, if we do multiple undo
				 * and then perform a user operation.
				 * 
				 *  In case we do multiple undo operations and then perform a user operation.
				 *  We will add the user operation at the end of the last performed undo op and 
				 *  cleanup other undo operations so that user cannot do any redo.
				 */
				var listSize = this.properties.operationList.length;
				var opPosition = this.properties.opPos;
				this.properties.operationList.splice(opPosition + 1, listSize - opPosition);
			}	
	}
	
	exports.KFGImageCroppingView = otui.define("KFGImageCroppingView", ['withContent'],
	/** @lends KFGImageCroppingView
	 *  @this KFGImageCroppingView
	 */
	function()
	{
		var __isDirty = function __isDirty(newCropZone)
		{
			var currentCropZone = this.properties.cropPayload;
			var isDirty = true;
			
			if( !newCropZone )
			{
				return false;
			}
			if(currentCropZone.width === newCropZone.width &&
			currentCropZone.height === newCropZone.height &&
			currentCropZone.top === newCropZone.top &&
			currentCropZone.left === newCropZone.left)
			{
				isDirty = false;
			}

			return isDirty;
		};
		var __getNewCropZone = function __getNewCropZone(event)
		{
			var newCropZone = this.getCropCoords();
			var view = otui.Views.containing(this);

			if( !__isDirty.call(view, newCropZone))
				return;

            __removeRedoStates.call(view);
			view.properties.operationList.push(newCropZone);
			view.properties.opPos++;
			var contentArea = view.contentArea();
			// Update the sliders and text field.
			var widthSlider = contentArea.find(".kfg-image-cropping-crop-slider-width ot-slider");
			var heightSlider = contentArea.find(".kfg-image-cropping-crop-slider-height ot-slider");
			var widthSliderTxt = contentArea.find(".kfg-image-cropping-crop-slider-width .ot-slider-value-width");
			var heightSliderTxt = contentArea.find(".kfg-image-cropping-crop-slider-height .ot-slider-value-height");
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			var imgContainerWidth = imgContainer.width();
			var imgContainerHeight = imgContainer.height();
			var renditionProperties = (view.properties && view.properties.renditionProperties) || {};
			var assetOriginalWidth = (renditionProperties && renditionProperties.width) || imgContainerWidth;
			var assetOriginalHeight = (renditionProperties && renditionProperties.height) || imgContainerHeight;
			widthSliderTxt.val(Math.round((newCropZone.width/imgContainerWidth) * assetOriginalWidth));
			heightSliderTxt.val(Math.round((newCropZone.height/imgContainerHeight) * assetOriginalHeight));
			var widthFactor = ( newCropZone.width / imgContainerWidth ) * 100;
			var heightFactor = ( newCropZone.height / imgContainerHeight ) * 100;
			widthSlider.attr("ot-value", Math.round(widthFactor));
			heightSlider.attr("ot-value", Math.round(heightFactor));
			__setStateUndoRedo.call(view);
		};

		var __updateWidthSliderTxt = function __updateWidthSliderTxt(sliderPayload, event)
		{
			
			var contentArea = this.contentArea();
			var editableRenditionEl = contentArea.find("ot-editable-rendition")[0];
			var currentCoords = editableRenditionEl.getCropCoords();
			editableRenditionEl.cancelCrop();
			var widthTextBox = contentArea.find(".kfg-image-cropping-crop-slider-width .ot-slider-value");
			var percentWidth = parseInt(event.detail.value);
			var absoluteWidth = (percentWidth / 100) * sliderPayload.imgWidth;
			absoluteWidth = Math.round(absoluteWidth);
			widthTextBox.val(absoluteWidth);
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			var imgContainerWidth = Math.floor(imgContainer.width());
			var renditionProperties = (this.properties && this.properties.renditionProperties) || {};
			var assetOriginalWidth = (renditionProperties && renditionProperties.width) || imgContainerWidth;
			var delta = absoluteWidth - currentCoords.width;
			var absDelta = Math.abs(delta);
			var left = currentCoords.left, width = currentCoords.width;
			var hasReachedWidthEnd = ((left + width) >= sliderPayload.imgWidth);
			if(delta > 0)
			{
				left = currentCoords.left - (( hasReachedWidthEnd ) ? Math.round(absDelta) : Math.round(absDelta / 2));
				width = currentCoords.width + (( hasReachedWidthEnd ) ? 0 : Math.round(absDelta));
			}
			else if(delta < 0)
			{
				left = currentCoords.left + Math.round(absDelta / 2);
				width = currentCoords.width - Math.round(absDelta);
			}
			var newCropZone = {
				"top" : currentCoords.top,
				"left" : (left > 0) ? left : 0,
				"width" : (width > 0)? (width * (imgContainerWidth / assetOriginalWidth)) : 0,
				"height" : currentCoords.height
			};
			editableRenditionEl.crop(newCropZone);
			if( event.type === "thumb-move-end" )
			{
				__removeRedoStates.call(this);
				this.properties.operationList.push(newCropZone);
				this.properties.opPos++;
				__setStateUndoRedo.call(this);
			}
		};

		var __updateHeightSliderTxt = function __updateHeightSliderTxt(sliderPayload, event)
		{
			var contentArea = this.contentArea();
			var editableRenditionEl = contentArea.find("ot-editable-rendition")[0];
			var currentCoords = editableRenditionEl.getCropCoords();
			editableRenditionEl.cancelCrop();
			var heightTextBox = contentArea.find(".kfg-image-cropping-crop-slider-height .ot-slider-value");
			var percentHeight = parseInt(event.detail.value);
			var absoluteHeight = (percentHeight / 100) * sliderPayload.imgHeight;
			heightTextBox.val(Math.round(absoluteHeight));
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			var imgContainerHeight = Math.floor(imgContainer.height());
			var renditionProperties = (this.properties && this.properties.renditionProperties) || {};
			var assetOriginalHeight = (renditionProperties && renditionProperties.height) || imgContainerHeight;
			var delta = absoluteHeight - currentCoords.height;
			var absDelta = Math.abs(delta);
			var top = currentCoords.top, height = currentCoords.height;
			var hasReachedHeightEnd = ((top + height) >= sliderPayload.imgHeight);			
			if(delta > 0)
			{
				top = currentCoords.top - (( hasReachedHeightEnd ) ? Math.round(absDelta) : Math.round(absDelta / 2));
				height = currentCoords.height + (( hasReachedHeightEnd ) ? 0 : Math.round(absDelta));
			}
			else if(delta < 0)
			{
				top = currentCoords.top + Math.round(absDelta / 2);
				height = currentCoords.height - Math.round(absDelta);
			}
			var newCropZone = {
				"top" : (top > 0) ? top : 0,
				"left" : currentCoords.left,
				"width" : currentCoords.width,
				"height" : (height > 0) ? (height * (imgContainerHeight / assetOriginalHeight )) : 0
			};
			editableRenditionEl.crop(newCropZone);
			if( event.type === "thumb-move-end" )
			{
				__removeRedoStates.call(this);
				this.properties.operationList.push(newCropZone);
				this.properties.opPos++;
				__setStateUndoRedo.call(this);
			}
		};

		var _attachCropZoneMoveListener = function _attachCropZoneMoveListener()
		{
			var canvasContainer = this.querySelector(".canvas-container");
			canvasContainer.addEventListener("mouseup", __getNewCropZone.bind(this));
		};

		var _getCropData = function _getCropData(assetResponse)
		{
			var cropData = [];
			var coordinatesArray = [];
			var namesArray =[];
			var fieldId = [];
			var metadataList = assetResponse.metadata.metadata_element_list || [];
			var imageCropFieldGroup = metadataList.find(function(metadata) {  return metadata.id === IMAGE_CROP_FIELD_GRP });
			var imageCropTabularFieldList = (imageCropFieldGroup) ? imageCropFieldGroup.metadata_element_list[0].metadata_element_list
																	 : metadataList;
			var cropCoordinates = null;
			var view = this;
			imageCropTabularFieldList.forEach( function(tabularField) {
				
				if(tabularField.id === IMAGE_CROP_DATA)
				{
					cropCoordinates = tabularField.value && tabularField.value.value && tabularField.value.value.value || cropCoordinates;
					coordinatesArray = tabularField.values;
				}else if (tabularField.id == IMAGE_CROP_NAME){
					namesArray = tabularField.values;
				}
			});
			console.log("coordinatesArray: " + coordinatesArray);
			console.log("namesArray: " + namesArray);
			var cropSelectionElement = $('.kfg-image-cropping-name select')[0];
			if (cropSelectionElement.options.length == 0){
				_addCropOptions(cropSelectionElement, namesArray);
			}
			var selectedValue = cropSelectionElement.selectedOptions[0].value;
			cropCoordinates = _findCropDataForSelection(coordinatesArray, namesArray, selectedValue);
			return cropCoordinates == null ? "1@1@0@0" : cropCoordinates.value.value;
		};
		
		var _addCropOptions = function _addCropOptions(cropSelectionElement, namesArray){
			
			cropSelectionElement.addEventListener("change", function(){
				var imageEditorContainerE1 = $('.kfg-image-cropping-container')[0];
				xtag.fireEvent(imageEditorContainerE1, "imageCroppingInitialized");
			});
			namesArray.forEach(function(item,index){
				var anOption = new Option(item.value.value,item.value.value);
				cropSelectionElement.add(anOption);
			});
		};
		
		var _findCropDataForSelection = function _findCropDataForSelection(coordinatesArray, namesArray, selectedCropValue){
			var coordinatesIndex = undefined;
			namesArray.forEach(function(item, index){
				if (item.value.value == selectedCropValue){
					coordinatesIndex = index;
				}
			});
			var coordinates = coordinatesArray[coordinatesIndex];
			return coordinates;
		};
		// Fetches cropcoordinates if available or returns null
		var _getCropData1 = function _getCropData(assetResponse)
		{
			
			var metadataList = assetResponse.metadata.metadata_element_list || [];
			var imageCropFieldGroup = metadataList.find(function(metadata) {  return metadata.id === IMAGE_CROP_FIELD_GRP });
			var imageCropTabularFieldList = (imageCropFieldGroup) ? imageCropFieldGroup.metadata_element_list[0].metadata_element_list
																	 : metadataList;
			var cropCoordinates = null;
			var view = this;
			imageCropTabularFieldList.forEach( function(tabularField) {
				if(tabularField.id === IMAGE_CROP_DATA)
				{
					cropCoordinates = tabularField.value && tabularField.value.value && tabularField.value.value.value || cropCoordinates;
				}
				
			});
			return cropCoordinates == null ? "1@1@0@0" : cropCoordinates;
		};

		var _setInitialSlider = function _setInitialSlider(sliderPayload)
		{
			var contentArea = this.contentArea();
			var widthSliderHolder = contentArea.find(".kfg-image-cropping-crop-slider-width");
			var heightSliderHolder = contentArea.find(".kfg-image-cropping-crop-slider-height");
			var widthSlider = widthSliderHolder.find("ot-slider");
			var heightSlider = heightSliderHolder.find("ot-slider");
			var widthTextBox = widthSliderHolder.find(".ot-slider-value");
			var heightTextBox = heightSliderHolder.find(".ot-slider-value");
			var heightFactor = ( sliderPayload.cropHeight / sliderPayload.imgHeight ) * 100;
			var widthFactor = ( sliderPayload.cropWidth / sliderPayload.imgWidth ) * 100;
			widthTextBox.val(sliderPayload.cropWidth);
			heightTextBox.val(sliderPayload.cropHeight);
			widthSlider.attr("ot-value", Math.round(widthFactor));
			heightSlider.attr("ot-value", Math.round(heightFactor));
			widthSlider[0].addEventListener("thumb-moved", __updateWidthSliderTxt.bind(this, sliderPayload));
			heightSlider[0].addEventListener("thumb-moved", __updateHeightSliderTxt.bind(this, sliderPayload));
			widthSlider[0].addEventListener("thumb-move-end", __updateWidthSliderTxt.bind(this, sliderPayload));
			heightSlider[0].addEventListener("thumb-move-end", __updateHeightSliderTxt.bind(this, sliderPayload));
		};

		var _positionImage = function(viewImgContainer, imgContainer)
		{
			var width = imgContainer.width();
			var height = imgContainer.height();
			var viewImgWidth = viewImgContainer[0].getBoundingClientRect().width;
			var viewImgHeight = viewImgContainer[0].getBoundingClientRect().height;
			height = Math.round(height);
			width = Math.round(width);
			viewImgHeight = Math.round(viewImgHeight);
			viewImgWidth = Math.round(viewImgWidth);
			var hDelta = viewImgHeight - height;
			var wDelta = viewImgWidth - width;
			if( hDelta > 2 )
				viewImgContainer.css("padding-top", Math.round(hDelta / 2));
			else
				viewImgContainer.css("padding-top", 0);
			
			if( wDelta > 2 )
				viewImgContainer.css("padding-left", Math.round(wDelta / 2));
			else
				viewImgContainer.css("padding-left", 0);
			
		};

		var _showExistingCropArea = function _showExistingCropArea(cropData)
		{
			var contentArea = this.contentArea();
			var fullViewContent = this.internalProperty("physicalContent");
			var editableRenditionEl = contentArea.find("ot-editable-rendition")[0];
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			var saveButton = fullViewContent.find(".kfg-image-cropping-save")[0];
			var cropCoordinates = (cropData) ? cropData : "0@0@1@1";
			var width = imgContainer.width();
			var height = imgContainer.height();
			var coords = cropCoordinates.split(COORDINATE_DELIMITER);
			var renditionProperties = (this.properties && this.properties.renditionProperties) || {};
			var assetOriginalWidth = (renditionProperties && renditionProperties.width) || width;
			var assetOriginalHeight = (renditionProperties && renditionProperties.height) || height;

			var cropPayload = {
				"top" : Math.round(parseFloat(coords[0]) * height),
				"left" : Math.round(parseFloat(coords[1]) * width),
				"width" : Math.round(parseFloat(coords[2]) * width),
				"height" : Math.round(parseFloat(coords[3]) * height),
			};
			var sliderPayload = {
				"cropWidth" : Math.round(parseFloat(coords[2]) * assetOriginalWidth),
				"cropHeight" : Math.round(parseFloat(coords[3]) * assetOriginalHeight),
				"imgWidth" : Math.round(assetOriginalWidth),
				"imgHeight" : Math.round(assetOriginalHeight)
			};
			var currentCrop = cropPayload || this.properties.currentCropZone;
			this.properties.cropPayload = currentCrop;
			if (this.properties.currentCropZone){
				editableRenditionEl.cancelCrop();
			}
			editableRenditionEl.crop(currentCrop);
			if( !this.properties.currentCropZone )
			{
				_setInitialSlider.call(this, sliderPayload);
				
			}
			this.properties.opPos = 0;
			// If currentCropZone is present, we will show not add it to operation list for undo redo
			// we will delete the entry post display.
//			if( !this.properties.currentCropZone )
//			{
//				this.properties.operationList.push(currentCrop);
//				this.properties.opPos = 0;
//			}
//			else
//			{
//				delete this.properties.currentCropZone;
//			}
			this.properties.currentCropZone = currentCrop;
			__setStateUndoRedo.call(this);
			_attachCropZoneMoveListener.call(editableRenditionEl);
			saveButton.removeAttribute("disabled");
			__unblockContent.call(this);
		};

		var _fetchCropData = function _fetchCropData()
		{
			
			/*var view = this;
			var assetId = view.properties.assetID;
			
			otui.services.asset.crop(assetId, function(response, status, success) {
				var submissionMsgEl = document.querySelector(SUBMISSION_MSG_CLOSE_EL);
				view.properties.submissionMsgEl = submissionMsgEl;
				if(! success )
				{
					otui.NotificationManager.closeNotification(submissionMsgEl);
					otui.NotificationManager.showNotification(MESSAGE.CROP_SUBMIT_FAIL);
					return;
				}

				if( status === 202 )
				{
					otui.NotificationManager.showNotification(MESSAGE.CROP_SUBMISSION);
					var contentArea = view.contentArea();
					otui.DialogUtils.cancelDialog(contentArea[0]);
				}
			});
			*/
		};

		var _showCropArea = function _showCropArea(assetResponse)
		{
			if (!(assetResponse instanceof Event)){
				this.properties.assetResponse = assetResponse;
			}
			
			if (assetResponse instanceof Event){
				assetResponse = this.properties.assetResponse;
			}
			 
			if(! this.properties.assetResponse.metadata )
				return;
			
			var cropData = _getCropData.call(this, assetResponse);
			
			// If Crop Data is not available, request for crop data and wait for it.
			if(cropData !=null)
			{
				 this.properties.hasCropData = true;
				_showExistingCropArea.call(this, cropData);
			}else {
				otui.NotificationManager.showNotification(MESSAGE.CROP_GENERATION_ERROR);
			}
		};

		var _generateCrop = function _generateCrop(props)
		{
			console.log("GenerateCrop...");
			var currentDialogElm = otui.DialogUtils.getDialog(this);
			var currentView = otui.Views.containing(currentDialogElm);
			var assetId = props.assetID;
			var contentArea = currentView.contentArea();
			var viewImgContainer = contentArea.find(".kfg-image-cropping-img-container");
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			_positionImage(viewImgContainer, imgContainer);
			__blockContent.call(currentView);
			if (currentView.properties.assetResponse == undefined){
				otui.services.asset.read({'assetID' : assetId, 'unwrap' : true}, _showCropArea.bind(currentView));
			}else {
				_showCropArea.call(currentView, currentView.properties.assetResponse);
			}
			
		};

		var _addEditableRenditionEl = function _addEditableRenditionEl(viewProperties)
		{
//			var content = this.getTemplate("content", undefined, $) || this[0].getTemplate("content", undefined, $);

			var renditionProps = viewProperties.renditionProperties;
			var renditionURL = renditionProps.previewURL || renditionProps.thumbnailURL;
			var dateParm = "r=" + Date.now();
			var renditionImgURL = addQueryParamToURL(renditionURL, dateParm);
			var renditionContainer = this[0].querySelector(".kfg-image-cropping-img-container");
			var renditionContainerWd = renditionContainer.getBoundingClientRect().width,
				renditionContainerHt = renditionContainer.getBoundingClientRect().height;
			
			if (! this[0].querySelector('ot-editable-rendition')){
				var editableRendition = document.createElement("ot-editable-rendition");
				editableRendition.src = renditionImgURL;
				editableRendition.mimeType = renditionProps.mimeType;
				editableRendition.dimensions = {
					"width" : renditionContainerWd,
					"height" : renditionContainerHt
				};
				editableRendition.setAttribute("ot-disable-context-menu", true);
				editableRendition.addEventListener("rendition-loaded", _generateCrop.bind(editableRendition, viewProperties));
				renditionContainer.appendChild(editableRendition);
			}else {
				var anElement = this[0].querySelector('ot-editable-rendition');
				_generateCrop.call(anElement, viewProperties);
			}
			
			var selectElement = $('.kfg-image-cropping-name select')[0];
			var currentDialogElm = otui.DialogUtils.getDialog(this);
			var currentView = otui.Views.containing(currentDialogElm);
//			selectElement.addEventListener("change", _showCropArea.bind(currentView));
//			selectElement.addEventListener("change", _generateCrop.bind(editableRendition, viewProperties));
			
			/*selectElement.addEventListener("change", function(){
				var imageEditorContainerE1 = $('.kfg-image-cropping-container')[0];
				xtag.fireEvent(imageEditorContainerE1, "imageCroppingInitialized");
			});*/
		};

		this.properties =
			{
			'name' : undefined,
			'title' : otui.tr("Image Cropping"),
			'type' : 'KFGImageCropping'
			};

		/**
		 * This function loads the Image Cropping view.
		 * 
		 * @internal
		 * @param {KFGImageCroppingView} view - the {@link KFGImageCroppingView} object to load.
		 * @param {Views~ContentPlacer} callback - a callback to execute after completion which takes the content for this view and inserts into the page.
		 */
		this._initContent = function _initContent(self, callback)
		{
			var content = self.getTemplate("content", undefined, $);
			var imageEditorContainerE1 = content.find('.kfg-image-cropping-container');
			if(imageEditorContainerE1 && imageEditorContainerE1.length > 0)
			{
				imageEditorContainerE1[0].addEventListener("imageCroppingInitialized", function() {
					_addEditableRenditionEl.call(content, properties);	
				});
			}
			var properties = self.properties || {};
			properties.operationList = [];
			callback(content);
		};

		this.bind("resize", function(){
			var contentArea = this.contentArea();
			var viewImgContainer = contentArea.find(".kfg-image-cropping-img-container");
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			_positionImage(viewImgContainer, imgContainer);
		});
		var constructor = function(){};
		return constructor;
	});

	exports.KFGImageCroppingView.show = function show(options, data)
	{
		otui.dialog(DIALOG_NAME, options);
	};

	var _updateCallback = function _updateCallback(response, status, success)
	{
		this.unblockContent();
		if( success )
		{
			otui.Logger.info("Metadata", "Updated image cropping data for asset : " + this.properties.assetID);
			var viewAttachedToCB = this.contentArea()[0];
			var dialogElement = otui.DialogUtils.getDialog(viewAttachedToCB);
			otui.DialogUtils.cancelDialog(dialogElement, true);
			otui.NotificationManager.showNotification(MESSAGE.SUCCESS);
		}
		else
		{
			otui.Logger.error("Metadata", "Update failed for image cropping for asset : "  + this.properties.assetID);
			otui.NotificationManager.showNotification(MESSAGE.ERROR);
		}
	};

	exports.KFGImageCroppingView.save = function save(event)
	{
		//console.log("save here..");
		var view = otui.Views.containing(event.target);
		var contentArea = view.contentArea();
		var otEditableRenditionEl = contentArea.find("ot-editable-rendition")[0];
		var activityNameInput = content.find(".ot-share-activity-name");
		var newCoords = otEditableRenditionEl.getCropCoords();
		var canvasContainer = otEditableRenditionEl.querySelector(".canvas-container");
		var originalWidth = canvasContainer.getBoundingClientRect().width;
		var originalHeight = canvasContainer.getBoundingClientRect().height;
		var xProportionalFactor = (newCoords) ? (newCoords.left / originalWidth) : 0,
		    yProportionalFactor = (newCoords) ? (newCoords.top / originalHeight) : 0,
		    wProportionalFactor = (newCoords) ? (newCoords.width / originalWidth) : 1,
		    hProportionalFactor = (newCoords) ? (newCoords.height / originalHeight) : 1;
		var stringCoords = yProportionalFactor +COORDINATE_DELIMITER+ xProportionalFactor +COORDINATE_DELIMITER+ wProportionalFactor +COORDINATE_DELIMITER+ hProportionalFactor;
		var recommendedCoords = (view.properties.recommendedCrop) ? view.properties.recommendedCrop : stringCoords;
		var assetId = view.properties.assetID;
	//	console.log("newCoords: "+newCoords);
		//console.log("stringCoords: "+stringCoords);
		
		var newCropZonePayload = {"edited_asset":{"data":{"metadata":[]}}};
		newCropZonePayload.edited_asset.data.metadata.push({"id": IMAGE_CROP_NAME,"type" : IMAGE_CROP_FIELD_TYPE, "values": {"value": {"type": "string", "value": 'Name todo'}}});
		newCropZonePayload.edited_asset.data.metadata.push({"id": IMAGE_CROP_DATA,"type" : IMAGE_CROP_FIELD_TYPE, "values": {"value": {"type": "string", "value": stringCoords}}});
		otui.services.asset.lock({'assetID' : assetId}, function(response, status, success) {
			if(success)
				otui.services.asset.update({'assetID' : assetId, 'modifiedData' : newCropZonePayload}, _updateCallback.bind(view));
			else
			{
				otui.NotificationManager.showNotification(MESSAGE.ERROR);
				view.unblockContent();
			}
		});
		view.blockContent();
		
	};

	exports.KFGImageCroppingView.undo = function undo(event)
	{
		var view = otui.Views.containing(event.target);
		var contentArea = view.contentArea();
		var otEditableRenditionEl = contentArea.find("ot-editable-rendition")[0];
		var opList = view.properties.operationList;
		var opListSize = opList.length;
		if( opListSize > 1 && view.properties.opPos > 0  && view.properties.opPos < opListSize)
		{
			var position = --view.properties.opPos;
			var crop = opList[position];
			otEditableRenditionEl.cancelCrop();
			otEditableRenditionEl.crop(crop);
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			var imgContainerWidth = imgContainer.width();
			var imgContainerHeight = imgContainer.height();
			var renditionProperties = (view.properties && view.properties.renditionProperties) || {};
			var assetOriginalWidth = (renditionProperties && renditionProperties.width) || imgContainerWidth;
			var assetOriginalHeight = (renditionProperties && renditionProperties.height) || imgContainerHeight;

			// Update width
			var widthSliderHolder = contentArea.find(".kfg-image-cropping-crop-slider-width");
			var widthTextBox = widthSliderHolder.find(".ot-slider-value");
			var widthSlider = widthSliderHolder.find("ot-slider");
			var widthFactor = ( crop.width / imgContainerWidth ) * 100;
			widthSlider.attr("ot-value", Math.round(widthFactor));
			widthTextBox.val(Math.round((crop.width/imgContainerWidth) * assetOriginalWidth));
			// Update height
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			var heightSliderHolder = contentArea.find(".kfg-image-cropping-crop-slider-height");
			var heightTextBox = heightSliderHolder.find(".ot-slider-value");
			var heightSlider = heightSliderHolder.find("ot-slider");
			var heightFactor = ( crop.height / imgContainerHeight ) * 100;
			heightSlider.attr("ot-value", Math.round(heightFactor));
			heightTextBox.val(Math.round((crop.height/imgContainerHeight) * assetOriginalHeight));
			__setStateUndoRedo.call(view);
		}
	};

	exports.KFGImageCroppingView.redo = function redo(event)
	{
		var view = otui.Views.containing(event.target);
		var contentArea = view.contentArea();
		var otEditableRenditionEl = contentArea.find("ot-editable-rendition")[0];
		var opList = view.properties.operationList;
		var opListSize = opList.length;
		if( opListSize > 1 && view.properties.opPos >= 0  && view.properties.opPos < opListSize - 1 )
		{
			var position = ++view.properties.opPos;
			var crop = opList[position];
			otEditableRenditionEl.cancelCrop();
			otEditableRenditionEl.crop(crop);

			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			var imgContainerWidth = imgContainer.width();
			var imgContainerHeight = imgContainer.height();
			var renditionProperties = (view.properties && view.properties.renditionProperties) || {};
			var assetOriginalWidth = (renditionProperties && renditionProperties.width) || imgContainerWidth;
			var assetOriginalHeight = (renditionProperties && renditionProperties.height) || imgContainerHeight;

			// Update width
			var widthSliderHolder = contentArea.find(".kfg-image-cropping-crop-slider-width");
			var widthTextBox = widthSliderHolder.find(".ot-slider-value");
			var widthSlider = widthSliderHolder.find("ot-slider");
			var widthFactor = ( crop.width / imgContainerWidth ) * 100;
			widthSlider.attr("ot-value", Math.round(widthFactor));
			widthTextBox.val(Math.round((crop.width/imgContainerWidth) * assetOriginalWidth));
			// Update height
			var heightSliderHolder = contentArea.find(".kfg-image-cropping-crop-slider-height");
			var heightTextBox = heightSliderHolder.find(".ot-slider-value");
			var heightSlider = heightSliderHolder.find("ot-slider");
			var heightFactor = ( crop.height / imgContainerHeight ) * 100;
			heightSlider.attr("ot-value", Math.round(heightFactor));
			heightTextBox.val(Math.round((crop.height/imgContainerHeight) * assetOriginalHeight));
			__setStateUndoRedo.call(view);
		}
	};


	var _downloadBlob = function _downloadBlob(otEditableRenditionEl, response)
	{
		// TODO : Download issue due to double canvas container
		var blobObject = response.blob;
		var assetId = this.properties.assetID;
		var fileName = this.properties.renditionName;
		var renditionProps = this.properties.renditionProperties;
		var renditionURL = renditionProps.previewURL || renditionProps.thumbnailURL;
		if( !fileName )
			fileName = otui.OTMMHelpers.getRandomNumber();
		fileName = fileName.replace(/(.*)\.+?(.*)$/, "$1");
		fileName = fileName + "." + response.format;
		try
		{
			var isDownloaded = otui.OTMMHelpers.saveBlob(blobObject, fileName);
		}
		catch(err)
		{
			otui.NotificationManager.showNotification(MESSAGE.DOWNLOAD_FAILED);
		}
		otEditableRenditionEl.clear();
		otEditableRenditionEl.src = renditionURL + "?r="+ Date.now();
		otEditableRenditionEl.setAttribute("ot-disable-context-menu", true);
		this.unblockContent();
	};

	exports.KFGImageCroppingView.download = function download(event)
	{
		var view = otui.Views.containing(event.target);
		var contentArea = view.contentArea();
		var otEditableRenditionEl = contentArea.find("ot-editable-rendition")[0];
		var currentCropZone = otEditableRenditionEl.getCropCoords();
		view.properties.currentCropZone = currentCropZone;
		view.blockContent();
		otEditableRenditionEl.apply();
		setTimeout(function() {
			var blob = otEditableRenditionEl.getBlob();
			blob.then(_downloadBlob.bind(view, otEditableRenditionEl))
				.catch(function(reason) {
					otui.NotificationManager.showNotification(MESSAGE.DOWNLOAD_FAILED);
					view.unblockContent();
			});
		}, 100);
	};

	exports.KFGImageCroppingView.updateWidth = function updateWidth(event)
	{
		if(event.keyCode !== 13)
			return;
		
		MESSAGE.TXTFLD_ERROR.message = otui.tr("Please provide correct data in the width text field.");
		event.preventDefault();
		var data = parseInt(event.target.value);
		if(isNaN(data))
		{
			otui.NotificationManager.showNotification(MESSAGE.TXTFLD_ERROR);
			return;
		}

		var view = otui.Views.containing(event.target);
		var contentArea = view.contentArea();
		var editableRenditionEl = contentArea.find("ot-editable-rendition")[0];
		var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
		var maxWidth = imgContainer.width();
		var renditionProperties = (view.properties && view.properties.renditionProperties) || {};
		var assetOriginalWidth = (renditionProperties && renditionProperties.width) || maxWidth;
		if(data < 0 || data > assetOriginalWidth)
		{
			otui.NotificationManager.showNotification(MESSAGE.TXTFLD_ERROR);
			return;
		}

		var currentCoords = editableRenditionEl.getCropCoords();
		// Now move the slider
		var widthSliderHolder = contentArea.find(".kfg-image-cropping-crop-slider-width");
		var widthSlider = widthSliderHolder.find("ot-slider");
		var widthRatio = ( data / assetOriginalWidth );
		var widthFactor = widthRatio * 100;
		widthSlider.attr("ot-value", Math.round(widthFactor));
		// Move the crop area
		editableRenditionEl.cancelCrop();
		var newCropZone = {
				"top" : currentCoords.top,
				"left" : currentCoords.left,
				"width" : Math.round(maxWidth * widthRatio),
				"height" : currentCoords.height
		};
		editableRenditionEl.crop(newCropZone);
		__removeRedoStates.call(view);
		view.properties.operationList.push(newCropZone);
		view.properties.opPos++;
		__setStateUndoRedo.call(view);
	};

	/*exports.KFGImageCroppingView.cropSelectionChanged = function cropSelectionChanged(event){
		console.log("Selection Changed Event Fired...");
		console.log(event);
		var selectElement = event.target;
		//TODO need to add a condition for empty selection.
		var selectedCropOption = selectElement.selectedOptions[0].value;
		var currentView = otui.Views.containing(event.currentTarget);

		var imageEditorContainerE1 = currentView.contentArea()[0].querySelector('.kfg-image-cropping-container');
		if(imageEditorContainerE1)
		{
			xtag.fireEvent(imageEditorContainerE1, "imageCroppingInitialized");
		}
	};*/
	
	exports.KFGImageCroppingView.updateHeight = function updateHeight(event)
	{
		if(event.keyCode !== 13)
			return;
		
		MESSAGE.TXTFLD_ERROR.message = otui.tr("Please provide correct data in the height text field.");
		event.preventDefault();
		var data = parseInt(event.target.value);
		if(isNaN(data))
		{
			otui.NotificationManager.showNotification(MESSAGE.TXTFLD_ERROR);
			return;
		}

		var view = otui.Views.containing(event.target);
		var contentArea = view.contentArea();
		var editableRenditionEl = contentArea.find("ot-editable-rendition")[0];
		var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
		var maxHeight = imgContainer.height();
		var renditionProperties = (view.properties && view.properties.renditionProperties) || {};
		var assetOriginalHeight = (renditionProperties && renditionProperties.height) || maxHeight;
		if(data < 0 || data > assetOriginalHeight)
		{
			otui.NotificationManager.showNotification(MESSAGE.TXTFLD_ERROR);
			return;
		}

		var currentCoords = editableRenditionEl.getCropCoords();
		// Now move the slider
		var heightSliderHolder = contentArea.find(".kfg-image-cropping-crop-slider-height");
		var heightSlider = heightSliderHolder.find("ot-slider");
		var heightRatio = ( data / assetOriginalHeight );
		var heightFactor = heightRatio * 100;
		heightSlider.attr("ot-value", Math.round(heightFactor));
		
		// Move the crop area
		editableRenditionEl.cancelCrop();
		var newCropZone = {
				"top" : currentCoords.top,
				"left" : currentCoords.left,
				"width" : currentCoords.width,
				"height" : Math.round(maxHeight * heightRatio)
		};
		editableRenditionEl.crop(newCropZone);
		__removeRedoStates.call(view);
		view.properties.operationList.push(newCropZone);
		view.properties.opPos++;
		__setStateUndoRedo.call(view);
	};

})(window);