(function(exports) {




	/**
	 * This function decides whether to enable "Show Clipping Path" action menu or not.
	 */
	 function enableIfImage(event, resource, point) {
		if(resource && otui.UserFETManager.isTokenAvailable('KFG_IMAGE_CROPPING') && resource.content_type == 'BITMAP') {
			return true;
		}

		return true;
	}


    function imageCropping(event, resource, props)
    {
		event.stopPropagation();
		var currentView = AssetActions.getCurrentView(event);
		var dialogOptions = { "viewProperties" : { 
								"KFGImageCroppingView" : {
									"assetID" : resource.asset_id,
									"originalAssetID" : resource.original_asset_id,
									"deliveryURL" : resource.delivery_service_url,
									"renditionName" : (((resource || {}).rendition_content || {}).preview_content || {}).name,
									"hasCropData" : false,
									"assetResponse" : undefined,
									"renditionProperties" : {
										"thumbnailURL" : (((resource || {}).rendition_content || {}).preview_content || {}).url,
										"previewURL" : (((resource || {}).rendition_content || {}).preview_content || {}).url,
										"mimeType" : resource.mime_type,
										"height": (((resource || {}).rendition_content || {}).preview_content || {}).height,
										"width": (((resource || {}).rendition_content || {}).preview_content || {}).width
									}
								}
							},
							"confirmClose" : true,
							"initialiseCallback" : function (dialog) {
								return function (dialog)
									{
										var view = otui.Views.containing(dialog);
										var assetId = view.properties.assetID;
										var lockHandler = function(response, status, success) {
											var imageEditorContainerE1 = view.contentArea()[0].querySelector('.kfg-image-cropping-container');
											if(imageEditorContainerE1)
											{
												xtag.fireEvent(imageEditorContainerE1, "imageCroppingInitialized");
											}
										};
										otui.services.asset.lock({'assetID' : assetId}, lockHandler);
									};
								},
							"closeCallback" : function (dialog) {
								var view = otui.Views.containing(dialog);
								if(view.properties.operationList)
									view.properties.operationList.length = 0;
								var assetId = view.properties.assetID;
								var contentArea = view.contentArea();
								var cropCanvas = contentArea.find(".canvas-container");
								cropCanvas.off("mouseup");
								if(view.properties.submissionMsgEl)
								{
									otui.NotificationManager.closeNotification(view.properties.submissionMsgEl);
								}
								otui.services.asset.unlock({'assetID' : assetId});
							}
		};
		KFGImageCroppingView.show(dialogOptions);
		
	};

	function registerClippingPathAction() {

        otui.InspectorAssetActions.register({'name' : 'imagecropping', 'text' : otui.tr('Image Cropping'), 'img' : './style/img/add24.svg', 'select' : imageCropping, 'setup' : enableIfImage}, 14, 4);
        otui.GalleryAssetActions.register({'name' : 'imagecropping', 'text' : otui.tr('Image Cropping'), 'img' : './style/img/add24.svg', 'select' : imageCropping, 'setup' : enableIfImage}, 14, 4);
        
	};
	
	
	otui.ready(registerClippingPathAction, true);
})(window);