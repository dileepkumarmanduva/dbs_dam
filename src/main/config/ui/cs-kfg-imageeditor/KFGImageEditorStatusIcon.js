otui.ready(function () {

    var specificAccessor = otui.MetadataModelManager.accessors.specific;
    var fullAccessor = otui.MetadataModelManager.accessors.full;
    
    var imageCroppingStatusIconInDetailView = {
    
        'name'  : 'KFG.ICON.IMAGE_CROPPING', // Unique name for the status icon
        'img'   : './style/img/editimage/crop24_mo.svg',
        'text'  : otui.tr("Cropped image available"),
        'setup' : function (event, resource, point) {
            var enable = false;
            if (resource) {
                
                    var cropDataField = fullAccessor.metadata.value(resource, "KFG.TABLE.IMAGE_CROPPED_DATA");
                    if (cropDataField != undefined && cropDataField.value != undefined && cropDataField.value[0] != undefined && cropDataField.value[0].length > 0) {
                        Array.prototype.forEach.call(cropDataField.value[0], function (field) {
                            if (field.id == "KFG.FIELD.IMAGE_CROP_DATA" && field.value != undefined && field.value.split("@").length == 4) {
                                enable = true;
                            }
                        });
                    }
            }
            return enable;
        }
    };
    
    otui.InspectorStatusIcons.register(imageCroppingStatusIconInDetailView);
    
    
    
    
    var imageCroppingStatusIconInGalleryView = {
    
        'name' : 'KFG.ICON.IMAGE_CROPPING', // Unique name for the status icon
        'text'  : otui.tr("Cropped image available"),
        'img'  : './style/img/editimage/crop24_mo.svg',
        'setup' : function (event, resource, point) {
    
            if (resource) {
    
                  
                    var cropDataField = specificAccessor.metadata.value(resource, "KFG.FIELD.IMAGE_CROP_DATA");
                    if(cropDataField !=undefined && cropDataField.value != undefined && cropDataField.value.split("@").length == 4){
                        return true;
                    }
    
            }
    
            return false;
        }
    };
    
    
    otui.GalleryStatusIcons.register(imageCroppingStatusIconInGalleryView);
    
    
    }, true);