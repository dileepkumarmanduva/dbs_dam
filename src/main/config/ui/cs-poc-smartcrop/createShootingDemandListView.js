(function(exports) {
	
	exports.ShootingDemandList = otui.define("ShootingDemandList", function() {
		
		this.properties = {
				'name' : 'ShootingDemandList', 		
				'title': undefined,
				'message' : otui.tr("Shooting Demand List"),
				'id': 'shotting_demand_list_view',
				'resizable' : false,
				'confirmClose' : false, 
				'minWidth': 820
		} ;

		this._initContent = function initShootingDemandList( self, placeContent) {
			
			// remove resizeable button
//			self.internalProperties.physicalContent[0].lastElementChild.remove();
			var createView = this.getTemplate("shootingdemandlist");
			
			placeContent(createView);
			
			
			
			/*otui.get(serviceUrl, undefined, otui.contentTypes.json, function(response){
				console.log("Got the user...");
				var emailAddress = response.user_resource.user["email_address"];
				
				if (!emailAddress){
					otui.alert("Could not retrieve email address for current user, cannot launch shooting demain list.");
					return;
				}
				
			}.bind(this));*/
		}
		/*this.copyToClipboards = function (event){
			console.log("CopyinView Called");
		}*/
		
	}); // end of view

	exports.ShootingDemandList.copyToClipboard = function(event,profile){
		console.log("CopyinView Called");
		var view = otui.Views.containing(event.target);
		
		var assetId = view.properties.assetId;
		
		var linkToCopy = "http://localhost:48080/adaptivemedia/rendition?id="+assetId+"&prid="+profile;
		var smartCrop = document.getElementById(profile).checked;
		if (smartCrop){
			linkToCopy = linkToCopy + "&smartcrop=Y";
		}
//		var linkToCopy = "http://localhost:48080/adaptivemedia/rendition?id="+assetId+"&prid="+profile; 
		var shortLinkInput = $('<textarea style="position: absolute; left: -9999px;">' + linkToCopy + '</textarea>').appendTo( "body" );
		if(otui.Browser.isSafari && otui.Browser.isiOS)
		{
			shortLinkInput[0].readOnly = true;
			var range = document.createRange();
			range.selectNodeContents(shortLinkInput[0]);
			var selection = window.getSelection();
			selection.removeAllRanges();
			selection.addRange(range);
			shortLinkInput[0].setSelectionRange(0, 99999);
		}
		else
		{
			shortLinkInput[0].select();
		}
		
		document.execCommand("copy");
		shortLinkInput.remove();
		otui.NotificationManager.showNotification(
		{
			'message' : otui.tr('A link has been copied to your clipboard.'),
			'stayOpen': false,
			'status' : 'ok'
		});
	

	}
	
	window.ShootingDemandList.openRendition = function (event, profile){
		event.stopPropagation();
		var view = otui.Views.containing(event.target);
		var assetId = view.properties.assetId;
		
		var linkToCopy = "http://localhost:48080/adaptivemedia/rendition?id="+assetId+"&prid="+profile; 
		var smartCrop = document.getElementById(profile).checked;
		if (smartCrop){
			linkToCopy = linkToCopy + "&smartcrop=Y";
		}
		window.open(linkToCopy);
	}
	exports.ShootingDemandList.selectAll = function (target){}
	
	exports.ShootingDemandList.deSelectAll = function (target){}

	
	exports.ShootingDemandList.submitRequest = function (target){
		
		otui.DialogUtils.cancelDialog(event.target);
	}
	

	otui.ready(function() {
		
		 otui.augment("ShootingDemandList", function() {
		        this.bind("setup-finished", function(view) {
		            console.log("Setup Finished...");
		        });

		    });

		function launchShootingDemandList(event, resource, point) {
			var view = (point.store && point.store.view) || otui.Views.containing(point);
			var dlg = ShootingDemandList.asDialog({ "title" : "Delivery Channel Assets", "assetId" : resource.asset_id});
			var dlg_header=dlg;		
		};

		var setupShootingDemandList = function(event,resource,point) {
			return true; // Can be used in future to check for FETs and roles to enable this function.
		} 

		var shootingDemandList = {
				'name' : 'create_shooting_demand_list', // Unique name for the menu entry
				'text' : otui.tr("Channel Assets"), // Display name for the menu entry, translated using otui.tr
//				'img' : 'cs_create_activity_project/activityImg.png',	// Path to the icon to show for this menu entry
				'img' : './style/img/asset_action/edit16.svg',
				'select' : launchShootingDemandList, // Function to execute when the menu entry is selected. We pass an "event promise" here to promise to handle the "select" event.
				'setup' : function(){
					return true;
				}	// Function to execute when the entry is to be displayed. We pass an "event promise" here to promise to handle the "setup" event.
		};

		otui.GalleryAssetActions.register(shootingDemandList, 1);

	});

})(window);