(function(exports)
{
	
	var SMART_COMPOSE_FIELD_GRP = "ARTESIA.CATEGORY.INTELLIGENTCROP DATA";
	var SMART_COMPOSE_RECOMMENDED_FIELD = "ARTESIA.FIELD.INTELLIGENT CROP.RECOMMENDED DATA";
	var SMART_COMPOSE_SELECTED_FIELD = "ARTESIA.FIELD.INTELLIGENT CROP.SELECTED CROP";
	var SMART_COMPOSE_COLLECTION_NAME_FIELD = "ARTESIA.FIELD.INTELLIGENT CROP.COLLECTION NAME";
	var SMART_COMPOSE_PROFILE_NAME_FIELD = "ARTESIA.FIELD.INTELLIGENT CROP.PROFILE NAME";
	var COORDINATE_DELIMITER = "@";
	var SMART_COMPOSE_FIELD_TYPE = "com.artesia.metadata.MetadataTableField";
	
	exports.CustomSmartComposeView = otui.define("CustomSmartComposeView", ['withContent'], function(){
		
		this.properties =
		{
		'name' : undefined,
		'title' : otui.tr("Custom Smart compose"),
		'type' : 'customsmartcompose'
		};
		
		this._initContent = function _initContent(self, callback)
		{
//			var parentView= this.parent;
//			parentView._initContent.call(this, self, callback);
			
			var content = self.getTemplate("content", undefined, $);
			var smartComposeContainerEl = content.find('.ot-smart-compose-container');
			if(smartComposeContainerEl && smartComposeContainerEl.length > 0)
			{
				smartComposeContainerEl[0].addEventListener("smartComposeInitialized", function() {
					_addEditableRenditionEl1.call(content, properties);	
				});
			}
			var properties = self.properties || {};
			properties.operationList = [];
			callback(content);
		};
		
		var _addEditableRenditionEl1 = function _addEditableRenditionEl1(viewProperties)
		{
			var renditionProps = viewProperties.renditionProperties;
			var renditionURL = renditionProps.previewURL || renditionProps.thumbnailURL;
			var dateParm = "r=" + Date.now();
			var renditionImgURL = otui.OTMMHelpers.addQueryParamToURL(renditionURL, dateParm);
			var renditionContainer = this[0].querySelector(".ot-smart-compose-img-container");
			var renditionContainerWd = renditionContainer.getBoundingClientRect().width,
				renditionContainerHt = renditionContainer.getBoundingClientRect().height;
			var editableRendition = document.createElement("ot-editable-rendition");
			editableRendition.src = renditionImgURL;
			editableRendition.mimeType = renditionProps.mimeType;
			editableRendition.dimensions = {
				"width" : renditionContainerWd,
				"height" : renditionContainerHt
			};
			editableRendition.setAttribute("ot-disable-context-menu", true);
			editableRendition.addEventListener("rendition-loaded", _generateCrop.bind(editableRendition, viewProperties));
			renditionContainer.appendChild(editableRendition);
			xtag.fireEvent(editableRendition, "rendition-loaded");
		};
		
		var _positionImage = function(viewImgContainer, imgContainer)
		{
			var width = imgContainer.width();
			var height = imgContainer.height();
			var viewImgWidth = viewImgContainer[0].getBoundingClientRect().width;
			var viewImgHeight = viewImgContainer[0].getBoundingClientRect().height;
			height = Math.round(height);
			width = Math.round(width);
			viewImgHeight = Math.round(viewImgHeight);
			viewImgWidth = Math.round(viewImgWidth);
			var hDelta = viewImgHeight - height;
			var wDelta = viewImgWidth - width;
			if( hDelta > 2 )
				viewImgContainer.css("padding-top", Math.round(hDelta / 2));
			else
				viewImgContainer.css("padding-top", 0);
			
			if( wDelta > 2 )
				viewImgContainer.css("padding-left", Math.round(wDelta / 2));
			else
				viewImgContainer.css("padding-left", 0);
			
		};
		
		var __blockContent = function __blockContent()
		 {
			 var contentArea = this.contentArea();
			 if (contentArea)
				{
					var dialogBody = contentArea.find(".ot-smart-compose-container");
					var block = otui.Templates.get(otui.Templates.LOADER, $);
					block.addClass("lightloader nomessage");
					block.addClass("ot-contentblock").appendTo(dialogBody);
				}
		 };
		var _generateCrop = function _generateCrop(props)
		{
			var currentDialogElm = otui.DialogUtils.getDialog(this);
			var currentView = otui.Views.containing(currentDialogElm);
			var assetId = props.assetID;
			var contentArea = currentView.contentArea();
			var viewImgContainer = contentArea.find(".ot-smart-compose-img-container");
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			_positionImage(viewImgContainer, imgContainer);
			__blockContent.call(currentView);
			otui.services.asset.read({'assetID' : assetId, 'unwrap' : true}, _showCropArea.bind(currentView));
		};
		
		var _showCropArea = function _showCropArea(assetResponse)
		{
			otui.alert("Show Crop Area Called...");
			if(! assetResponse.metadata )
				return;
			
			var cropData = _getCropData.call(this, assetResponse);
			
			// If Crop Data is not available, request for crop data and wait for it.
			if( !cropData )
				_fetchCropData.call(this);
			else
			{
				 this.properties.hasCropData = true;
				_showExistingCropArea.call(this, cropData);
			}
		};
		
		var _getCropData = function _getCropData(assetResponse)
		{
			var metadataList = assetResponse.metadata.metadata_element_list || [];
			var smartComposeFieldGroup = metadataList.find(function(metadata) {  return metadata.id === SMART_COMPOSE_FIELD_GRP });
			var smartCropTabularFieldList = (smartComposeFieldGroup) ? smartComposeFieldGroup.metadata_element_list[0].metadata_element_list
																	 : metadataList;
			var cropCoordinates = null;
			var view = this;
			smartCropTabularFieldList.forEach( function(tabularField) {
				if(tabularField.id === SMART_COMPOSE_SELECTED_FIELD)
				{
					cropCoordinates = tabularField.value && tabularField.value.value && tabularField.value.value.value || cropCoordinates;
				}
				else if(tabularField.id === SMART_COMPOSE_RECOMMENDED_FIELD && !cropCoordinates)
				{
					cropCoordinates = tabularField.value && tabularField.value.value && tabularField.value.value.value || null;
					view.properties.recommendedCrop = cropCoordinates;
				}
			});
			return cropCoordinates;
		};
		
		var _showExistingCropArea = function _showExistingCropArea(cropData)
		{
			var contentArea = this.contentArea();
			var fullViewContent = this.internalProperty("physicalContent");
			var editableRenditionEl = contentArea.find("ot-editable-rendition")[0];
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			var saveButton = fullViewContent.find(".ot-smart-compose-save")[0];
			var cropCoordinates = (cropData) ? cropData : "0@0@1@1";
			var width = imgContainer.width();
			var height = imgContainer.height();
			var coords = cropCoordinates.split(COORDINATE_DELIMITER);
			var renditionProperties = (this.properties && this.properties.renditionProperties) || {};
			var assetOriginalWidth = (renditionProperties && renditionProperties.width) || width;
			var assetOriginalHeight = (renditionProperties && renditionProperties.height) || height;

			var cropPayload = {
				"top" : Math.round(parseFloat(coords[0]) * height),
				"left" : Math.round(parseFloat(coords[1]) * width),
				"width" : Math.round(parseFloat(coords[2]) * width),
				"height" : Math.round(parseFloat(coords[3]) * height),
			};
			var sliderPayload = {
				"cropWidth" : Math.round(parseFloat(coords[2]) * assetOriginalWidth),
				"cropHeight" : Math.round(parseFloat(coords[3]) * assetOriginalHeight),
				"imgWidth" : Math.round(assetOriginalWidth),
				"imgHeight" : Math.round(assetOriginalHeight)
			};
			var currentCrop = this.properties.currentCropZone || cropPayload;
			this.properties.cropPayload = currentCrop;
			editableRenditionEl.crop(currentCrop);
			if( !this.properties.currentCropZone )
			{
				_setInitialSlider.call(this, sliderPayload);
			}
			
			// If currentCropZone is present, we will show not add it to operation list for undo redo
			// we will delete the entry post display.
			if( !this.properties.currentCropZone )
			{
				this.properties.operationList.push(currentCrop);
				this.properties.opPos = 0;
			}
			else
			{
				delete this.properties.currentCropZone;
			}
			__setStateUndoRedo.call(this);
			_attachCropZoneMoveListener.call(editableRenditionEl);
			saveButton.removeAttribute("disabled");
			__unblockContent.call(this);
		};
		
		var _fetchCropData = function _fetchCropData()
		{
			var view = this;
			var assetId = view.properties.assetID;
			
			otui.services.asset.crop(assetId, function(response, status, success) {
				var submissionMsgEl = document.querySelector(SUBMISSION_MSG_CLOSE_EL);
				view.properties.submissionMsgEl = submissionMsgEl;
				if(! success )
				{
					otui.NotificationManager.closeNotification(submissionMsgEl);
					otui.NotificationManager.showNotification(MESSAGE.CROP_SUBMIT_FAIL);
					return;
				}

				if( status === 202 )
				{
					otui.NotificationManager.showNotification(MESSAGE.CROP_SUBMISSION);
					var contentArea = view.contentArea();
					otui.DialogUtils.cancelDialog(contentArea[0]);
				}
			});
		};

	});
	
	otui.ready(function() {
		
		function launchShootingDemandList(event, resource, point) {
//			var view = (point.store && point.store.view) || otui.Views.containing(point);
//			var dlg = CustomSmartComposeView.asDialog({ "title" : "Delivery Channel Assets", "assetId" : resource.asset_id});
//			var dlg_header=dlg;		
			event.stopPropagation();
			var currentView = AssetActions.getCurrentView(event);
			var dialogOptions = { "viewProperties" : { 
									"CustomSmartComposeView" : {
										"assetID" : resource.asset_id,
										"originalAssetID" : resource.original_asset_id,
										"deliveryURL" : resource.delivery_service_url,
										"renditionName" : (((resource || {}).rendition_content || {}).preview_content || {}).name,
										"hasCropData" : false,
										"renditionProperties" : {
											"thumbnailURL" : (((resource || {}).rendition_content || {}).preview_content || {}).url,
											"previewURL" : (((resource || {}).rendition_content || {}).preview_content || {}).url,
											"mimeType" : resource.mime_type,
											"height": (((resource || {}).rendition_content || {}).preview_content || {}).height,
											"width": (((resource || {}).rendition_content || {}).preview_content || {}).width
										}
									}
								},
								"confirmClose" : false,
								"initialiseCallback" : function (dialog) {
									return function (dialog)
										{
											var view = otui.Views.containing(dialog);
											var assetId = view.properties.assetID;
											var lockHandler = function(response, status, success) {
												var smartComposeContainerEl = view.contentArea()[0].querySelector('.ot-smart-compose-container');
												if(smartComposeContainerEl)
												{
													xtag.fireEvent(smartComposeContainerEl, "smartComposeInitialized");
												}
											};
											otui.services.asset.lock({'assetID' : assetId}, lockHandler);
										};
									},
								"closeCallback" : function (dialog) {
									var view = otui.Views.containing(dialog);
									if(view.properties.operationList)
										view.properties.operationList.length = 0;
									var assetId = view.properties.assetID;
									var contentArea = view.contentArea();
									var cropCanvas = contentArea.find(".canvas-container");
									cropCanvas.off("mouseup");
									if(view.properties.submissionMsgEl)
									{
										otui.NotificationManager.closeNotification(view.properties.submissionMsgEl);
									}
									otui.services.asset.unlock({'assetID' : assetId});
								}
			};
			
			CustomSmartComposeView.show(dialogOptions);
			
			/*var DIALOG_NAME = "customsmartcomposedialog";
			otui.dialog(DIALOG_NAME, dialogOptions);*/
		};

		var setupShootingDemandList = function(event,resource,point) {
			return true; // Can be used in future to check for FETs and roles to enable this function.
		} 

		var shootingDemandList = {
				'name' : 'create_shooting_demand_list', // Unique name for the menu entry
				'text' : otui.tr("Channel Assets"), // Display name for the menu entry, translated using otui.tr
//				'img' : 'cs_create_activity_project/activityImg.png',	// Path to the icon to show for this menu entry
				'img' : './style/img/asset_action/edit16.svg',
				'select' : launchShootingDemandList, // Function to execute when the menu entry is selected. We pass an "event promise" here to promise to handle the "select" event.
				'setup' : function(){
					return true;
				}	// Function to execute when the entry is to be displayed. We pass an "event promise" here to promise to handle the "setup" event.
		};

		otui.GalleryAssetActions.register(shootingDemandList, 1);

	});
	
	exports.CustomSmartComposeView.show = function show(options, data)
	{
//		var DIALOG_NAME = "CustomSmartComposeDialog";
		var DIALOG_NAME = "customsmartcomposedialog";
		otui.dialog(DIALOG_NAME, options);
	};
})(window);