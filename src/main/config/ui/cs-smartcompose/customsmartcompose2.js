(function(exports)
{
	exports.CustomSmartComposeView = otui.extend("SmartComposeView", "CustomSmartComposeView", function(){
		
		this.properties =
		{
		'name' : undefined,
		'title' : otui.tr("Custom Smart compose"),
		'type' : 'customsmartcompose'
		};
		
		this._initContenta = function _initContent(self, callback)
		{
			var parentView= this.parent;
			parentView._initContent.call(this, self, callback);
			
			/*var content = self.getTemplate("content", undefined, $);
			var smartComposeContainerEl = content.find('.ot-smart-compose-container');
			if(smartComposeContainerEl && smartComposeContainerEl.length > 0)
			{
				smartComposeContainerEl[0].addEventListener("smartComposeInitialized", function() {
					_addEditableRenditionEl.call(content, properties);	
				});
			}
			var properties = self.properties || {};
			properties.operationList = [];*/
//			callback(content);
		};
		
		var _generateCrop = function _generateCrop(props)
		{
			var parent = this.parent;
			parent._generateCrop.bind(this, props);
			/*var currentDialogElm = otui.DialogUtils.getDialog(this);
			var currentView = otui.Views.containing(currentDialogElm);
			var assetId = props.assetID;
			var contentArea = currentView.contentArea();
			var viewImgContainer = contentArea.find(".ot-smart-compose-img-container");
			var imgContainer = contentArea.find("ot-editable-rendition .canvas-container");
			_positionImage(viewImgContainer, imgContainer);
			__blockContent.call(currentView);
			otui.services.asset.read({'assetID' : assetId, 'unwrap' : true}, _showCropArea.bind(currentView));*/
		};
	}.bind(this.parent));
	
	otui.ready(function() {
		
		function launchShootingDemandList(event, resource, point) {
//			var view = (point.store && point.store.view) || otui.Views.containing(point);
//			var dlg = CustomSmartComposeView.asDialog({ "title" : "Delivery Channel Assets", "assetId" : resource.asset_id});
//			var dlg_header=dlg;		
			event.stopPropagation();
			var currentView = AssetActions.getCurrentView(event);
			var dialogOptions = { "viewProperties" : { 
									"CustomSmartComposeView" : {
										"assetID" : resource.asset_id,
										"originalAssetID" : resource.original_asset_id,
										"deliveryURL" : resource.delivery_service_url,
										"renditionName" : (((resource || {}).rendition_content || {}).preview_content || {}).name,
										"hasCropData" : false,
										"renditionProperties" : {
											"thumbnailURL" : (((resource || {}).rendition_content || {}).preview_content || {}).url,
											"previewURL" : (((resource || {}).rendition_content || {}).preview_content || {}).url,
											"mimeType" : resource.mime_type,
											"height": (((resource || {}).rendition_content || {}).preview_content || {}).height,
											"width": (((resource || {}).rendition_content || {}).preview_content || {}).width
										}
									}
								},
								"confirmClose" : false,
								"initialiseCallback" : function (dialog) {
									return function (dialog)
										{
											var view = otui.Views.containing(dialog);
											var assetId = view.properties.assetID;
											var lockHandler = function(response, status, success) {
												var smartComposeContainerEl = view.contentArea()[0].querySelector('.ot-smart-compose-container');
												if(smartComposeContainerEl)
												{
													xtag.fireEvent(smartComposeContainerEl, "smartComposeInitialized");
												}
											};
											otui.services.asset.lock({'assetID' : assetId}, lockHandler);
										};
									},
								"closeCallback" : function (dialog) {
									var view = otui.Views.containing(dialog);
									if(view.properties.operationList)
										view.properties.operationList.length = 0;
									var assetId = view.properties.assetID;
									var contentArea = view.contentArea();
									var cropCanvas = contentArea.find(".canvas-container");
									cropCanvas.off("mouseup");
									if(view.properties.submissionMsgEl)
									{
										otui.NotificationManager.closeNotification(view.properties.submissionMsgEl);
									}
									otui.services.asset.unlock({'assetID' : assetId});
								}
			};
//			CustomSmartComposeView.show(dialogOptions);
			
			var DIALOG_NAME = "customsmartcomposedialog";
			otui.dialog(DIALOG_NAME, dialogOptions);
		};

		var setupShootingDemandList = function(event,resource,point) {
			return true; // Can be used in future to check for FETs and roles to enable this function.
		} 

		var shootingDemandList = {
				'name' : 'create_shooting_demand_list', // Unique name for the menu entry
				'text' : otui.tr("Channel Assets"), // Display name for the menu entry, translated using otui.tr
//				'img' : 'cs_create_activity_project/activityImg.png',	// Path to the icon to show for this menu entry
				'img' : './style/img/asset_action/edit16.svg',
				'select' : launchShootingDemandList, // Function to execute when the menu entry is selected. We pass an "event promise" here to promise to handle the "select" event.
				'setup' : function(){
					return true;
				}	// Function to execute when the entry is to be displayed. We pass an "event promise" here to promise to handle the "setup" event.
		};

		otui.GalleryAssetActions.register(shootingDemandList, 1);

	});
	
})(window);